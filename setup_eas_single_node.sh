#####################################################
# EAS 1.13 Installation script for CentOS 5.5
# created by Jeffrey Smith for Farallon Geographics
#####################################################

clear

####################################
# Set Configuration Variables
POSTGRES_OS_PASSWORD=postgis
POSTGRES_DB_PASSWORD=postgis
JAVA_JDK_RPM_LOCATION='/home/eas/jdk-7u9-linux-x64.rpm'

####################################
# Check if current users is in sudoers file
if [ "$(whoami)" != "root" ]; then
	echo "Please run this script as root or using sudo"
	read -n1 -r -p "Press any key to continue..."
	exit
fi


####################################
# Print Configuration Variables
echo ''
echo 'Make sure you have set all the configuration variables!'
echo '	postgres OS user password =' $POSTGRES_OS_PASSWORD
echo '	postgres DB user password =' $POSTGRES_DB_PASSWORD
echo '	Java JDK rpm location = ' $JAVA_JDK_RPM_LOCATION
echo ''

read -n1 -r -p "Press y to continue if the parameters are correct" key
if [ "$key" != "y" ]; then
	echo ''
	exit
fi
echo ''

####################################
# Download and Install Postgres
cd ~
mkdir PostgreSQL
cd PostgreSQL
wget http://yum.postgresql.org/9.1/redhat/rhel-5-x86_64/pgdg-centos91-9.1-4.noarch.rpm
rpm -ivh pgdg-centos91-9.1-4.noarch.rpm
yum -y install postgresql91.x86_64 postgresql91-contrib.x86_64 postgresql91-devel.x86_64 postgresql91-libs.x86_64 postgresql91-server.x86_64 postgresql91-jdbc.x86_64 
/sbin/service postgresql-9.1 initdb
/sbin/service postgresql-9.1 start
/sbin/chkconfig postgresql-9.1 on
SQL_SET_POSTGRES_PASS='ALTER USER postgres WITH PASSWORD '$POSTGRES_DB_PASSWORD
su postgres -c psql -c '$SQL_SET_POSTGRES_PASS' template1
echo $POSTGRES_OS_PASSWORD | passwd postgres --stdin


####################################
# Make postgres listen to connections outside of this machine (Optional)
/etc/init.d/postgresql-9.1 stop
PG_CONFIG_FILE_LOCATION=$(eval find / -name postgresql.conf)
PG_HBA_FILE_LOCATION=$(eval find / -name pg_hba.conf)
sed -i "s,#listen_addresses = 'localhost',listen_addresses = '*',g" $PG_CONFIG_FILE_LOCATION
sed -i "s,# IPv6 local connections:,host	all	all	0.0.0.0/0	trust\n# IPv6 local connections:,g" $PG_HBA_FILE_LOCATION
sleep 10
/etc/init.d/postgresql-9.1 start
/etc/init.d/iptables save
/etc/init.d/iptables stop
/sbin/chkconfig iptables off


####################################
# Setup Environment for Building libraries from source
yum -y install gcc.x86_64 gcc-c++.x86_64
yum -y install python-devel swig
yum -y install libxml2.x86_64  libxml2-devel.x86_64
chmod +x $JAVA_JDK_RPM_LOCATION
rpm -i $JAVA_JDK_RPM_LOCATION
/usr/sbin/alternatives --install /usr/bin/java java /usr/java/jdk1.7.0_09/bin/java 2 
/usr/sbin/alternatives --install /usr/bin/javac javac /usr/java/jdk1.7.0_09/bin/javac 2 
echo 3 | /usr/sbin/alternatives --config java


####################################
# Download and Install Python2.7
cd ~
mkdir Python27
cd Python27
wget http://www.python.org/ftp/python/2.7.3/Python-2.7.3.tgz
tar xzf Python-2.7.3.tgz
cd Python-2.7.3
./configure --enable-unicode=ucs4 --with-zlib --enable-shared
make
make altinstall



####################################
# Download and Install Proj4
cd ~
mkdir Proj4
cd Proj4
wget http://download.osgeo.org/proj/proj-4.8.0.tar.gz
wget http://download.osgeo.org/proj/proj-datumgrid-1.5.tar.gz
tar xzf proj-4.8.0.tar.gz
cd proj-4.8.0/nad
tar xzf ../../proj-datumgrid-1.5.tar.gz
cd ..
./configure
make
make install

####################################
# Download and Install Geos
cd ~
mkdir Geos
cd Geos
wget http://download.osgeo.org/geos/geos-3.3.5.tar.bz2
tar xjvf geos-3.3.5.tar.bz2
cd geos-3.3.5
./configure
make
make install
echo '/usr/local/lib/' >> /etc/ld.so.conf.d/local-lib.conf
/sbin/ldconfig


####################################
# Download and Install GDAL
cd ~
mkdir GDAL
cd GDAL
wget http://download.osgeo.org/gdal/gdal-1.9.2.tar.gz
tar xzf gdal-1.9.2.tar.gz
cd gdal-1.9.2
./configure --with-python
make
make install


####################################
# Download and Install GeoJson
cd ~
mkdir GeoJson
cd GeoJson
wget http://pypi.python.org/packages/source/g/geojson/geojson-1.0.1.tar.gz
tar xzf geojson-1.0.1.tar.gz
cd geojson-1.0.1
/usr/local/bin/python2.7 setup.py install


####################################
# Download and Install PostGIS
cd ~
mkdir PostGIS
cd PostGIS
wget http://postgis.refractions.net/download/postgis-1.5.5.tar.gz
tar xzf postgis-1.5.5.tar.gz
cd postgis-1.5.5
./configure --with-pgconfig=$PG_CONFIG_LOCATION --with-geosconfig=/usr/local/bin/geos-config
make
make install
su -c 'createdb template_postgis' - postgres
su -c 'createlang plpgsql template_postgis' - postgres
cd postgis
cp postgis.sql /tmp/postgis.sql
cd ..
cp spatial_ref_sys.sql /tmp/spatial_ref_sys.sql
su -c 'psql -d template_postgis -f /tmp/postgis.sql' - postgres
su -c 'psql -d template_postgis -f /tmp/spatial_ref_sys.sql' - postgres
echo "UPDATE pg_database SET datistemplate=true WHERE datname='template_postgis';" > /tmp/create_template.sql
su -c 'psql -d template_postgis -f /tmp/create_template.sql' - postgres


###################################
# Download and Install PsycoPG2
cd ~
mkdir psycopg2
cd psycopg2
wget http://initd.org/psycopg/tarballs/PSYCOPG-2-4/psycopg2-2.4.5.tar.gz
tar xzf psycopg2-2.4.5.tar.gz
cd psycopg2-2.4.5
PG_CONFIG_LOCATION=$(eval find / -name pg_config)
sed -i "s,#pg_config=,pg_config=$PG_CONFIG_LOCATION,g" setup.cfg
/usr/local/bin/python2.7 setup.py install


####################################
# Download and Install Django
cd ~
mkdir Django
cd Django
wget https://www.djangoproject.com/download/1.3.1/tarball/
tar xzf Django-1.3.1.tar.gz
cd Django-1.3.1
/usr/local/bin/python2.7 setup.py install


###################################
# Download was very slow so I skipped this for now
####################################
# Download and Install JAI
#cd ~
#mkdir JAI
#cd JAI
#wget http://download.java.net/media/jai/builds/release/1_1_3/jai-1_1_3-lib-linux-amd64-jdk.bin
#cd /usr/java/jdk1.7.0_09
#sh jai-1_1_3-lib-linux-amd64-jdk.bin
#############################################################



####################################
# Download and Install Apache and Mode_WSGI
yum -y install httpd httpd-devel.x86_64 
/sbin/chkconfig httpd on
cd ~
mkdir mod_wsgi
cd mod_wsgi
wget http://modwsgi.googlecode.com/files/mod_wsgi-3.3.tar.gz
tar xzf mod_wsgi-3.3.tar.gz
cd mod_wsgi-3.3
./configure --with-apxs=/usr/sbin/apxs --with-python=/usr/local/bin/python2.7
make
make install
echo '/usr/lib64/httpd/modules/' >> /etc/ld.so.conf.d/local-lib.conf
/sbin/ldconfig



####################################
# Download and Restore Database Backups
cd ~
mkdir SF_Database_Backups
cd SF_Database_Backups
wget http://eas.googlecode.com/files/easproddb.sfgov.org-eas_prod-20121024.dmp
cp easproddb.sfgov.org-eas_prod-20121024.dmp /tmp/mad.dmp
su -c 'createdb -e -O postgres -E UTF8 -e mad' - postgres
su -c 'pg_restore -d mad -v /tmp/mad.dmp' - postgres
wget http://eas.googlecode.com/files/easproddb.sfgov.org-sfmaps_prod-20121024.dmp
cp easproddb.sfgov.org-sfmaps_prod-20121024.dmp /tmp/sfmaps.dmp
su -c 'createdb -e -O postgres -E UTF8 -e sfmaps' - postgres
su -c 'pg_restore -d sfmaps -v /tmp/sfmaps.dmp' - postgres



####################################
# Download and Install Apache Tomcat
yum -y install tomcat5.x86_64 
/sbin/chkconfig tomcat5 on


####################################
# Download, Deploy, and Configure GeoServer
cd ~
mkdir GeoServer
cd GeoServer
wget http://downloads.sourceforge.net/project/geoserver/GeoServer/1.7.4/geoserver-1.7.4-war.zip?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fgeoserver%2Ffiles%2FGeoServer%2F1.7.4%2F&ts=1351276476&use_mirror=iweb
unzip geoserver-1.7.4-war.zip
cp geoserver.war /usr/share/tomcat5/webapps/
/etc/init.d/tomcat5 start
sleep 60
/etc/init.d/tomcat5 stop
rm -R /usr/share/tomcat5/webapps/geoserver/data
mkdir /usr/share/tomcat5/webapps/geoserver/data
cp -R /var/www/html/eas/geoserver/configuration/examples/* /usr/share/tomcat5/webapps/geoserver
chmod -R 777 /usr/share/tomcat5/webapps/geoserver/data
/etc/init.d/tomcat5 start

####################################
# Download and Install SVN
yum -y install subversion.x86_64  


####################################
# Download EAS Codebase and Configure
cd ~
mkdir eas
cd eas
svn co http://eas.googlecode.com/svn/branches/1.1.3
cp -R ~/eas/1.1.3/ /var/www/html/eas/ 
cd /var/www/html/eas/web/
mkdir logs
cd logs
touch eas.log
touch eas_xmit.log
touch eas_xmit_running
touch eas_xmit_stop_request
chmod -R 777 /var/www/html/eas/web/logs

cd ~
mkdir OpenLayers
cd OpenLayers
wget http://openlayers.org/download/OpenLayers-2.12.tar.gz
tar xzf OpenLayers-2.12.tar.gz
cp -R OpenLayers-2.12 /var/www/html/eas/Media/ui_frameworks/

cd ~
mkdir ExtJS
cd ExtJS
wget http://dev.fargeo.com/ext-4.1.1a-gpl.zip
unzip ext-4.1.1a-gpl.zip
cp -R ext-4.1.1a /var/www/html/eas/Media/ui_frameworks/extjs-4.1.0

mv /var/www/html/eas/Media/css/MAD.css /var/www/html/eas/Media/css/MAD_1.1.3.css

chmod 777 /var/www/html/eas/Media/

cd /tmp/
wget http://dev.fargeo.com/mad.txt
cp mad.txt /var/www/html/eas/web/mad.wsgi

wget http://dev.fargeo.com/settings.py
cp settings.py /var/www/html/eas/web/settings.py

wget http://dev.fargeo.com/jsbuild.py
cp jsbuild.py /var/www/html/eas/web/MAD/utils/jsbuild.py

wget http://dev.fargeo.com/httpd.txt
cp httpd.txt /etc/httpd/conf/httpd.conf

sleep 10
/etc/init.d/httpd start



################################
# Manual Steps (do this on the server)
# 1. GeoServer downloaded but for some reason didn't unzip and copy to the tomcat webapps folder.
#    * Run these commands to finish deploying geoserver
#     sudo /etc/init.d/tomcat5 stop
#     cd ~/GeoServer
#     sudo unzip geoserver-1.7.4-war.zip
#     sudo cp geoserver.war /usr/share/tomcat5/webapps/
#     sudo /etc/init.d/tomcat5 start
#     <wait about 1 minute for geoserver to deploy (you can make sure it is deployed by going to http://localhost:8080/geoserver) >
#     sudo /etc/init.d/tomcat5 stop
#     sudo rm -R /usr/share/tomcat5/webapps/geoserver/data
#     sudo mkdir /usr/share/tomcat5/webapps/geoserver/data
#     sudo cp -R /var/www/html/eas/geoserver/configuration/examples/* /usr/share/tomcat5/webapps/geoserver
#     sudo chmod -R 777 /usr/share/tomcat5/webapps/geoserver/data
#     sudo /etc/init.d/tomcat5 start
#
# 2. Open web browser and go to http://localhost:8080/geoserver
# 3. Log into admin section and edit the mad and sfmaps datastore values
#    Geoserver credentials are username = 'admin', password = 'geoserver'
#	* Check 'Enabled' checkbox	
#	* port		5432
#	* username	postgres
#	* password	postgis (or whatever you used when running the script)
#	* host		use IP of machine (use '/sbin/ifconfig' command to find ip)
#	* database	'mad' for mad datastore and 'sfmaps' for sfmaps datastore
# 4. Click Apply, then Save, and then Load to save changes
#    * There still seems to be an issue with geoserver loading the data
# 5. Go to URL 'http://localhost/build' in web browser


