#!/bin/bash

# only root
if [ "$(id -u)" != "0" ]; then
  echo "This script must be run as root"
  exit 1
fi

. /var/www/html/set_eas_env.sh

PYTHON_SCRIPT=$EAS_HOME_LIVE/manage.py

# Run under the apache because of logging output permissions.
su apache -s /bin/bash -c "$PYTHON_EXE -u $PYTHON_SCRIPT test"
