
EAS_HOME_LIVE=/var/www/html/eas_live
EAS_HOME_MAINT=/var/www/html/eas_maint
EAS_HOME_SYM_LINK=/var/www/html/eas
PYTHON_EXE=/usr/local/python27/bin/python2.7

export EAS_HOME_MAINT
export EAS_HOME_SYM_LINK
export EAS_HOME_LIVE
export PYTHON_EXE
