
import sys, os
from django.core.management import setup_environ

madHomeDir = None
if os.name == 'posix':
    madHomeDir = r'/var/www/html/eas'
elif os.name == 'nt':
    madHomeDir = r'C:\svn\eas-1.1\web'
sys.path.append(madHomeDir)

import settings
setup_environ(settings)
import utils
from django.contrib.auth.models import User

userTuples = (

    # DT
    #('jjohnson', 'jjohnson', 'Jeffery', 'Johnson', 'Jeffrey.Johnson@sfgov.org', [1,2],),
    #('pmccullough', 'pmccullough', 'Paul', 'McCullough', 'Paul.McCullough@sfgov.org', [1,2],),

    # DEM
    #('jchen', 'jchen', 'Jun', 'Chen', 'Jun.Chen@sfgov.org', [2],),
    ('jchen', 'jchen', 'Jun', 'Chen', 'junchensf@gmail.com', [2],),

    # PARK AND REC
    ('sstasio', 'sstasio', 'Sean', 'Stasio', 'Sean.Stasio@sfgov.org', [2],),

    # DBI
    #('vday', 'vday', 'Vivian', 'Day', 'Vivian.Day@sfgov.org', [1,2],),
    #('vbacharach', 'vbacharach', 'Val', 'Bacharach', 'Val.Bacharach@sfgov.org', [1,2],),
    #('hnekkanti', 'hnekkanti', 'Hemalatha', 'Nekkanti', 'Hemalatha.Nekkanti@sfgov.org', [1,2],),
    #('byan', 'byan', 'Brenda', 'Yan', 'Brenda.Yan@sfgov.org', [1,2],),
    #('wfcheung', 'wfcheung', 'Wai-Fong', 'Cheung', 'Wai-Fong.Cheung@sfgov.org', [1,2],),
    #('achan', 'achan', 'Amaris', 'Chan', 'Amaris.Chan@sfgov.org', [1,2],),
    #('kshek', 'kshek', 'Kathy', 'Shek', 'Kathy.Shek@sfgov.org', [1,2],),
    #('gsecondez', 'gsecondez', 'Grace', 'Secondez', 'Grace.Secondez@sfgov.org', [1,2],),

    # TEST
    #('requestor1', 'requestor1', 'Requestor', 'One', 'noreply@sfgov.org', [2],),
    #('reviewer1', 'reviewer1', 'Reviewer', 'One', 'noreply@sfgov.org', [1],),

    # PARTNERS
    #('kfogel', 'kfogel', 'Karl', 'Fogel', 'kfogel@oreilly.com', [1,2],),
    #('rgaston', 'rgaston', 'Rob', 'Gaston', 'rgaston@fargeo.com', [1,2],),
    #('alodge', 'alodge', 'Adam', 'Lodge', 'alodge@fargeo.com', [1,2],),
    #('dwuthrich', 'dwuthrich', 'Dennis', 'Wuthrich', 'dwuthrich@fargeo.com', [1,2],),

    # DPW
    #('eching', 'eching', 'Eddy', 'Ching', 'Eddy.Ching@sfdpw.org', [1,2],),
    #('gschneider', 'gschneider', 'Geoffrey', 'Schneider', 'Geoffrey.Schneider@sfdpw.org', [1,2],),
    #('mhuff', 'mhuff', 'Marivic', 'Huff', 'Marivic.Huff@sfdpw.org', [1,2],),
    #('enaizghi', 'enaizghi', 'Ephrem', 'Naizghi', 'Ephrem.Naizghi@sfdpw.org', [1,2],),
    #('snorthrup', 'snorthrup', 'Scott', 'Northrup', 'Scott.Northrup@sfdpw.org', [1,2],),

)

for userTuple in userTuples:

    try:
          username = userTuple[0]
          print 'processing %s' % username
          users = User.objects.filter(username = username)
          if users.count() > 1:
              raise Exception('skipping - the user count was %s' % users.count())

          user = None
          password = utils.generatePassword('alphanum', 8)
          email = userTuple[4]
          if users.count() == 1:
              user = users[0]
              user.set_password(password)
          else:
              username = userTuple[0]
              user = User.objects.create_user(
                      username = username,
                      password = password,
                      email = email
              )

          user.is_active = True
          user.first_name = userTuple[2]
          user.last_name = userTuple[3]
          user.groups = userTuple[5]
          user.email = email

          user.save()

          utils.sendEmail('EAS - account setup', 'Your new production EAS username is %s.\nYour password will be sent in a separate email.\nPlease change your password at your earliest convenience.\nTo do this, sign in as usual. Then, use the sign in button again but choose "Change Password".\n' % username, recipientList=[email])
          utils.sendEmail('EAS', '%s' % password, recipientList=[email])

    except Exception, e:
        print e
