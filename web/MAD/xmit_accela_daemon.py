# A full on unix Daemon (pep 3143) is probably a bit more than we need.
# Also, we would like to run on windows or linux.
# Although this is an anemic implementation - but it is adequate at the moment.
#
# To use...
#
# on the command line:
#     python xmit_accela_daemon.py start|stop|status|restart
#
# as a linux "daemon":
#    ./web/bin/xmit_accela.bsh start|stop|status|restart
# "tail" the log file to see the stdout and stderr.
#

import sys, os
import inspect
import time
import datetime
import httplib, urllib

from django.core.management import setup_environ

# provide access to the django settings
KEY = 'MAD_HOME'
try:
    madHomeDir = os.environ[KEY]
except Exception, e:
    sys.stdout.write("%s\n" % e.message)
    sys.stdout.write('we might be missing a required system variable: %s\n' % KEY)
    sys.exit(-1)
sys.path.append(madHomeDir)
import settings
setup_environ(settings)
import logging
logger = logging.getLogger('XMIT_ACCELA')

from django.utils import simplejson as json
from MAD.utils.HttpUtils import HttpUtils
from MAD.utils.geojson_encode import *

# todo - better encapsulation
class Semaphore:

    @staticmethod
    def check(file):
        return os.path.exists(file)

    @staticmethod
    def create(file):
        try:
            outputPath = os.path.dirname(file)
            if not os.path.exists(outputPath):
                os.makedirs(outputPath)
            f = open(file, 'w')
            f.close()
        except Exception, e:
            if os.path.exists(file):
                os.remove(file)
            raise

    @staticmethod
    def clear(file):
        try:
            if os.path.exists(file):
                os.remove(file)
        except Exception, e:
            raise


class Notifier:

    def __init__(self, notificationInterval=10):
        self.__notificationInterval__ = notificationInterval
        self.__allIsWell__ = True
        self.__DEFAULT_NOTIFICATION_INTERVAL__ = notificationInterval
        self.__last_notification__ = None

    def allIsWell(self):
        if not self.__allIsWell__:
            self.notify(message='xmit accela process has recovered')
        self.__allIsWell__ = True
        self.__notificationInterval__ = self.__DEFAULT_NOTIFICATION_INTERVAL__

    def __notify__(self, message=None):
        # TODO - send email instead
        logger.info('***** NOTIFICATION *****\n%s' % message)
        self.__last_notification__ = datetime.datetime.now()
        self.__notificationInterval__ = self.__notificationInterval__ * 2 if self.__notificationInterval__ < 3600 else 3600

    def notify(self, message=None):
            self.__allIsWell__ = False
            now = datetime.datetime.now()
            if self.__last_notification__ is None:
                self.__last_notification__ = now
            timeDelta = now - self.__last_notification__
            if timeDelta.seconds == 0 or timeDelta.seconds >= self.__notificationInterval__:
                self.__notify__(message)
            else:
                secondsUntilNextNotification = self.__notificationInterval__ - int(timeDelta.seconds)
                logger.info('next notification in %s seconds' % secondsUntilNextNotification)


class Runner:

    def __init__(self):
        self.layerId = None

    def sleep(self, seconds):
        for i in range(1, seconds+1):
            time.sleep(1)
            if Semaphore.check(settings.XMIT_ACCELA_STOP_REQUEST_SEMAPHORE):
                raise StopIteration()

    def unix_time(self, dt):
        epoch = datetime.datetime.utcfromtimestamp(0)
        delta = dt - epoch
        return delta.total_seconds()

    def unix_time_millis(self, dt):
        if dt is None:
            return None
        else:
            return self.unix_time(dt) * 1000.0

    def postJson(self, queuedItem):
        logger.info('posting JSON')
        import base64
        import string
        connection = None
        try:
            path = None
            if queuedItem.address_x_parcel_history.history_action == "insert":
                logger.info('insert')
                path = "%s/%s/addFeatures" % (settings.XMIT_ACCELA_PATH, self.layerId)
            elif queuedItem.address_x_parcel_history.history_action == "retire":
                logger.info('update')
                path = "%s/%s/updateFeatures" % (settings.XMIT_ACCELA_PATH, self.layerId)
            logger.info('path:%s' % path)
            logger.info('user:%s' % settings.XMIT_ACCELA_USER)
            logger.info('password:%s' % settings.XMIT_ACCELA_PASSWORD)
            # "Basic" authentication encodes userid:password in base64. Note that base64.encodestring adds some extra
            # newlines/carriage-returns to the end of the result. string.strip is a simple way to remove these
            # characters.
            auth = base64.encodestring("%s:%s" % (settings.XMIT_ACCELA_USER, settings.XMIT_ACCELA_PASSWORD))
            auth = 'Basic ' + string.strip(auth)
            connectionString = '%s:%s' % (settings.XMIT_ACCELA_HOST, settings.XMIT_ACCELA_PORT)
            logger.info('connectionString :%s' % connectionString)
            connection = httplib.HTTPConnection(connectionString)
            jsonFeatures = geojson_encode([queuedItem.jsonDict])
            logger.info("json features: %s" % jsonFeatures)
            encodedParams = urllib.urlencode({
                "f": "pjson",
                "token": self.token['token'],
                "features": jsonFeatures
            })
            headers = {
                "Host": settings.XMIT_ACCELA_HOST,
                "User-Agent": "Python post",
                "Content-type": "application/x-www-form-urlencoded",
                "Accept": "text/plain",
                "Content-length": "%s" % len(encodedParams),
                'Authorization': auth
            }
            connection.request("POST", path, encodedParams, headers)
            # If the server cannot send the response within 5 seconds, a socket.error will be raised (python 2.5).
            connection.sock.settimeout(5.0)
            response = connection.getresponse()
            if response.status != 200:
                message = 'arcgis sever request failure - response status was ' + str(response.status) + ' - message was ' + str(response.msg)
                raise IOError(message)
            textResponse = HttpUtils.metaDataAsText(response)
            logger.info("response meta data: %s" % textResponse)
            content = response.read()
            logger.info("response content: %s" % content)
            contentDict = json.loads(content)
            try:
                x = contentDict['error']
                raise Exception('unanticipated error')
            except KeyError:
                pass
        except Exception, e:
            raise
        finally:
            if connection:
                connection.close()


    def getObjectId(self, queuedItem):

        if queuedItem.address_x_parcel_history.history_action == "insert":
            return None
        elif queuedItem.address_x_parcel_history.history_action == "retire":
            pass
        else:
            raise Exception("invalid action: %s; expected insert or retire" % queuedItem.address_x_parcel_history.history_action)
        import base64
        auth = base64.encodestring("%s:%s" % (settings.XMIT_ACCELA_USER, settings.XMIT_ACCELA_PASSWORD))
        headers = {"Authorization" : "Basic %s" % auth}
        connectionString = '%s:%s' % (settings.XMIT_ACCELA_HOST, settings.XMIT_ACCELA_PORT)
        connection = httplib.HTTPConnection(connectionString)
        urlString = "%s/%s/query?where=eas_address_x_parcel_id=%d&returnIdsOnly=true&f=pjson&token=%s" % (settings.XMIT_ACCELA_PATH, self.layerId, queuedItem.address_x_parcel_history.address_x_parcel.id, self.token['token'])
        logger.info("connectionString: %s" % connectionString)
        logger.info("urlString: %s" % urlString)
        connection.request("GET", urlString, headers=headers)
        response = connection.getresponse()
        if response.status != 200:
            message = 'arcgis request failure - response status was ' + str(response.status) + ' - message was ' + str(response.msg)
            print message
            raise IOError(message)
        stringResponse = response.read()
        sys.stdout.write(stringResponse + '\n')
        responseJson = json.loads(stringResponse)
        objectIds = responseJson['objectIds']
        if len(objectIds) != 1:
            message = "expected single object id but got %s\nresponse: %s" % (len(objectIds), stringResponse)
            raise Exception(message)
        return objectIds[0]


    def getToken(self):
        connectionString = '%s:%s' % (settings.XMIT_ACCELA_HOST, settings.XMIT_ACCELA_PORT)
        connection = httplib.HTTPConnection(connectionString)
        url = "/arcgis/tokens/generateToken?f=pjson&username=%s&password=%s" % (settings.XMIT_ACCELA_USER, settings.XMIT_ACCELA_PASSWORD)
        connection.request("GET", url)
        response = connection.getresponse()
        if response.status != 200:
            message = 'arcgis request failure - response status was ' + str(response.status) + ' - message was ' + str(response.msg)
            print message
            raise IOError(message)
        stringResponse = response.read()
        sys.stdout.write(stringResponse + '\n')
        tokenJson = json.loads(stringResponse)
        return tokenJson


    def getLayerId(self):
        import base64
        auth = base64.encodestring("%s:%s" % (settings.XMIT_ACCELA_USER, settings.XMIT_ACCELA_PASSWORD))
        headers = {"Authorization" : "Basic %s" % auth}
        connectionString = '%s:%s' % (settings.XMIT_ACCELA_HOST, settings.XMIT_ACCELA_PORT)
        connection = httplib.HTTPConnection(connectionString)
        url = "%s?f=pjson&token=%s" % (settings.XMIT_ACCELA_PATH, self.token['token'])
        connection.request("GET", url, headers=headers)
        response = connection.getresponse()
        if response.status != 200:
            message = 'arcgis request failure - response status was ' + str(response.status) + ' - message was ' + str(response.msg)
            print message
            raise IOError(message)
        stringResponse = response.read()
        sys.stdout.write(stringResponse + '\n')
        responseJson = json.loads(stringResponse)
        layers = responseJson['layers']
        for layer in layers:
            if layer['name'] == settings.XMIT_ACCELA_LAYER_NAME:
                return layer['id']
        raise Exception('unable to find layer: %s' % settings.XMIT_ACCELA_LAYER_NAME)


    def getDictForJson(self, objectId, queuedItem):

        # For details see
        # https://sfgovdt.jira.com/wiki/display/MAD/Address+Change+Publisher+-+Accela
        baseAddressHistoric = queuedItem.address_base_history
        unitAddressHistoric = queuedItem.address_history
        addressXParcelHistoric = queuedItem.address_x_parcel_history
        lastChangeTms = queuedItem.last_change_tms
        lonLat = baseAddressHistoric.geometry.transform(4326, True)
        status = 'A'
        if baseAddressHistoric.retire_tms is not None or unitAddressHistoric.retire_tms is not None or addressXParcelHistoric.retire_tms is not None:
            status = 'I'

        # The arcgis specific fields (at least geometry and attributes) are case sensitive.
        # If you make a mistake in this area, a common result is that record will be inserted but values will not be set.
        return {
            'geometry': {
                'x' : baseAddressHistoric.geometry.x,
                'y': baseAddressHistoric.geometry.y
            },
            'attributes': {
                'OBJECTID':                     objectId,
                'EAS_ADDRESS_BASE_ID':          baseAddressHistoric.address_base_id,
                'BASE_ADDRESS_NUM':             baseAddressHistoric.base_address_num,
                'BASE_ADDRESS_SUFFIX':          baseAddressHistoric.base_address_suffix,
                'BASE_ADDRESS_CREATE_TMS':      self.unix_time_millis(baseAddressHistoric.create_tms),
                'BASE_ADDRESS_RETIRE_TMS':      self.unix_time_millis(baseAddressHistoric.retire_tms),
                'STREET_NAME':                  baseAddressHistoric.street_segment.getStreetName().base_street_name.strip(),
                'STREET_TYPE':                  baseAddressHistoric.street_segment.getStreetName().street_type.strip() if baseAddressHistoric.street_segment.getStreetName().street_type else None,
                'STREET_POST_DIRECTION':        baseAddressHistoric.street_segment.getStreetName().post_direction if baseAddressHistoric.street_segment.getStreetName().post_direction else None,
                'STREET_FULL_STREET_NAME':      baseAddressHistoric.street_segment.getStreetName().full_street_name,
                'STREET_CNN':                   baseAddressHistoric.street_segment.seg_cnn,
                'LONGITUDE':                    lonLat.x,
                'LATITUDE':                     lonLat.y,
                'EAS_UNIT_ADDRESS_ID':          unitAddressHistoric.address_id,
                'UNIT_ADDRESS_BASE_FLG':        unitAddressHistoric.address_base_flg,
                'UNIT_ADDRESS':                 unitAddressHistoric.unit_num,
                'UNIT_ADDRESS_CREATE_TMS':      self.unix_time_millis(unitAddressHistoric.create_tms),
                'UNIT_ADDRESS_RETIRE_TMS':      self.unix_time_millis(unitAddressHistoric.retire_tms),
                'BLOCK_LOT':                    addressXParcelHistoric.parcel.blk_lot,
                'EAS_ADDRESS_X_PARCEL_ID':      addressXParcelHistoric.address_x_parcel.id,
                'ADDRESS_X_PARCEL_CREATE_TMS':  self.unix_time_millis(addressXParcelHistoric.create_tms),
                'ADDRESS_X_PARCEL_RETIRE_TMS':  self.unix_time_millis(addressXParcelHistoric.retire_tms),
                'ZIPCODE':                      baseAddressHistoric.zone.zipcode,
                'STATUS':                       status
            }
        }


    # I was unable to get @transaction.commit_manually to work here.
    def run(self):
        sys.stdout.write('polling is running\n')
        from MAD.models.models import XmitAccelaQueue
        from django.db import connection, transaction
        import time
        notifier = Notifier(notificationInterval=settings.XMIT_ACCELA_POLLING_INTERVAL)
        self.token = self.getToken()
        self.layerId = self.getLayerId()

        while True:
            try:
                if int(round(time.time() * 1000)) > self.token['expires']:
                    self.token = self.getToken()

                cursor = connection.cursor()
                XmitAccelaQueue.objects.enqueue(cursor)
                transaction.commit_unless_managed()
                cursor.close()

                queuedItems = XmitAccelaQueue.objects.getQueued()
                if len(queuedItems) == 0:
                    # We want to use the default polling interval here in case polling_interval is set to a high value.
                    logger.info('next poll in %s seconds' % settings.XMIT_ACCELA_POLLING_INTERVAL)
                    connection.close()
                    self.sleep(settings.XMIT_ACCELA_POLLING_INTERVAL)
                    continue

                # Commit here so we do not get an idle transaction if we have problems with the web server connection below.
                transaction.commit_unless_managed()

                for queuedItem in queuedItems:
                    objectId = self.getObjectId(queuedItem)
                    queuedItem.jsonDict = self.getDictForJson(objectId, queuedItem)
                    self.postJson(queuedItem)
                    queuedItem.xmit_tms = datetime.datetime.now()
                    queuedItem.save()
                    transaction.commit_unless_managed()

                notifier.allIsWell()

            except StopIteration:
                connection.close()
                sys.stdout.write('stopping polling...\n')
                break
            except Exception, e:
                connection.close()
                logger.critical(e)
                notifier.notify(message=e.message)
                self.sleep(settings.XMIT_ACCELA_POLLING_INTERVAL)

        sys.stdout.write('stopped polling\n')


class Daemon:

    # Implements a daemon interface but is not really a daemon in that it does no forking and remains connected to parent shell.

    def status(self):
        if Semaphore.check(settings.XMIT_ACCELA_RUNNING_SEMAPHORE):
            status = True
            message = 'daemon process is running'
        else:
            status = False
            message = 'daemon process is not running'
        sys.stdout.write(message+'\n')
        return status


    def start(self):
        if Semaphore.check(settings.XMIT_ACCELA_RUNNING_SEMAPHORE):
            sys.stdout.write('daemon process is already running\n')
            return False
        else:
            sys.stdout.write('starting daemon process...\n')
            try:
                Semaphore.create(settings.XMIT_ACCELA_RUNNING_SEMAPHORE)
                runner = Runner()
                runner.run()
                # I told you it's not a real daemon.
            except Exception, e:
                sys.stdout.write(e.message)
            finally:
                Semaphore.clear(settings.XMIT_ACCELA_RUNNING_SEMAPHORE)


    def stop(self):
        if not Semaphore.check(settings.XMIT_ACCELA_RUNNING_SEMAPHORE):
            sys.stdout.write('daemon process is not running\n')
            return True
        else:
            try:
                # process is running
                sys.stdout.write('stopping daemon process...\n')
                Semaphore.create(settings.XMIT_ACCELA_STOP_REQUEST_SEMAPHORE)
                seconds = settings.XMIT_ACCELA_POLLING_INTERVAL
                for i in range(1, seconds):
                    time.sleep(1)
                    if not Semaphore.check(settings.XMIT_ACCELA_RUNNING_SEMAPHORE):
                        sys.stdout.write('daemon process stopped\n')
                        return True
                sys.stdout.write('stop daemon process request failed\n')
                return False
            except Exception, e:
                sys.stdout.write(e.message)
            finally:
                Semaphore.clear(settings.XMIT_ACCELA_STOP_REQUEST_SEMAPHORE)


    def restart(self):
        sys.stdout.write('restarting daemon process...\n')
        if not self.stop():
            return False
        else:
            self.start()




def main():

    daemon = Daemon()

    methods = [key for key, value in inspect.getmembers(daemon) if inspect.ismethod(value)]
    usage = 'usage: %s %s\n' % (sys.argv[0], '|'.join(methods))

    if len(sys.argv) != 2:
        sys.stdout.write(usage)
        sys.exit(0)

    if sys.argv[1] not in methods:
        sys.stdout.write(usage)
        sys.exit(0)

    success = eval('daemon.' + sys.argv[1] + '()')


if __name__ == "__main__":
    main()
