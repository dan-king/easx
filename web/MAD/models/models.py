# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
import datetime
import re
import math
from django.conf import settings as settings
from django.contrib.auth.models import User
from django.contrib.gis.db import models
from django.contrib.gis.geos.geometry import GEOSGeometry
from django.utils import simplejson as json
from MAD.exceptions import ConcurrentUpdateWarning
from MAD.utils.DbUtils import DbUtils
from MAD.models.ModelUtils import ModelUtils
from MAD.exceptions import ConcurrentUpdateWarning, ValidationWarning, AddressIsRetiredWarning
from MAD.utils.GenericResponseObj import GenericResponseObj
from MAD.utils.cache_manager import *
from django.core.exceptions import ValidationError
from operator import attrgetter
from django.db.models import Q
from django.template.defaultfilters import date as templateDate
import logging

logger = logging.getLogger('EAS')

class Validation:
    def __init__(self, valid = True, message = '', object=None, changeRequest=None):
        self.valid = valid
        self.message = message
        self.object = object
        self.changeRequest = changeRequest

class ParcelsAbstract(models.Model):
    parcel_id = models.AutoField(primary_key=True)
    map_blk_lot = models.CharField(max_length=10)
    blk_lot = models.CharField(unique=True, max_length=9)
    block_num = models.CharField(max_length=5)
    lot_num = models.CharField(max_length=5)
    date_rec_add = models.DateField()
    date_rec_drop = models.DateField()
    date_map_add = models.DateField()
    date_map_drop = models.DateField()
    date_map_alt = models.DateField()
    project_id_add = models.CharField(max_length=50)
    project_id_drop = models.CharField(max_length=50)
    project_id_alt = models.CharField(max_length=50)
    exception_notes = models.CharField(max_length=50)
    create_tms = models.DateTimeField()
    update_tms = models.DateTimeField()
    geometry = models.MultiPolygonField(srid=2227) # This field type is a guess.
    distance = 0

    objects = models.GeoManager() # required for spatial filtering

    class Meta:
        abstract = True

    # instance, non-model
    blockNumPart = None
    blockAlphaPart = None
    lotNumPart = None
    lotAlphaPart = None

    # class variables
    regExpForInt = re.compile('\d+')

    def __init__(self, *args, **kwargs):

        # you must provide init variables
        super(ParcelsAbstract, self).__init__(*args, **kwargs)

        # todo - when there is no parcel this code blows up because self.block_num and self.lot_num are NoneType

        # split each block and lot into 2 parts: leading number and trailing alpha

        # block
        self.blockNumPart = []
        self.blockAlphaPart = []
        for i, char in enumerate(self.block_num):
            if char.isdigit():
                self.blockNumPart.append(char)
            else:
                self.blockAlphaPart = self.block_num[i:]
                break
        self.blockNumPart = ''.join(self.blockNumPart)
        self.blockAlphaPart = ''.join(self.blockAlphaPart)
        if not self.blockNumPart:
            self.blockNumPart = '0'
        if not self.blockAlphaPart:
            self.blockAlphaPart = ''

        # lot
        self.lotNumPart = []
        self.lotAlphaPart = []
        for i, char in enumerate(self.lot_num):
            if char.isdigit():
                self.lotNumPart.append(char)
            else:
                self.lotAlphaPart = self.lot_num[i:]
                break
        self.lotNumPart = ''.join(self.lotNumPart)
        self.lotAlphaPart = ''.join(self.lotAlphaPart)
        if not self.lotNumPart:
            self.lotNumPart = '0'
        if not self.lotAlphaPart:
            self.lotAlphaPart = ''

    def __unicode__(self):
        return (
            '\nParcel\n\tblock_num: %s\n\tblockNumPart: %s\n\tblockAlphaPart: %s\n\tlot_num: %s\n\tlotNumPart: %s\n\tlotAlphaPart: %s\n\tdate_map_drop: %s' %
            (self.block_num, self.blockNumPart, self.blockAlphaPart, self.lot_num, self.lotNumPart, self.lotAlphaPart, self.date_map_drop)
        )

    def _compareAlphaParts(self, selfPart, otherPart):
        if selfPart == '' and otherPart != '':
            return -1
        if selfPart != '' and otherPart == '':
            return -1
        if selfPart != '' and otherPart != '':
            if selfPart < otherPart:
                return -1
            if selfPart > otherPart:
                return 1
        return 0

    def _compareIntegerParts(self, selfPart, otherPart):
        if int(selfPart) < int(otherPart):
            return -1
        if int(selfPart) > int(otherPart):
            return 1
        return 0

    def __cmp__(self, otherBlockLot):

        # block
        compareValue = self._compareIntegerParts(self.blockNumPart, otherBlockLot.blockNumPart)
        if compareValue != 0:
            return compareValue

        compareValue = self._compareAlphaParts(self.blockAlphaPart, otherBlockLot.blockAlphaPart)
        if compareValue != 0:
            return compareValue

        # lot
        compareValue = self._compareIntegerParts(self.lotNumPart, otherBlockLot.lotNumPart)
        if compareValue != 0:
            return compareValue

        compareValue = self._compareAlphaParts(self.lotAlphaPart, otherBlockLot.lotAlphaPart)
        if compareValue != 0:
            return compareValue

        return 0

    # todo - change upon django upgrade
    def validate_block_num(self):
        if not self.block_num:
            raise ValidationError('block number must be specified')
        if len(self.block_num) not in (4,5):
            raise ValidationError('block number must be 4 or 5 characters in length')
        if not self.block_num[0:4].isdigit():
            raise ValidationError('first 4 characters of block number must be numbers')
        if len(self.block_num) == 5:
            if not self.block_num[4:].isalpha():
                raise ValidationError('the optional 5th character of block number must be a letter')
            if self.block_num[4:].islower():
                raise ValidationError('the optional 5th character of block number must be an uppercase letter')


    # todo - change upon django upgrade
    def validate_lot_num(self):
        if not self.lot_num:
            raise ValidationError('lot number must be specified')
        if len(self.lot_num) not in (3,4):
            raise ValidationError('lot number must be 3 or 4 characters in length')
        if not self.lot_num[0:3].isdigit():
            raise ValidationError('first 3 characters of a lot number must all be numbers')
        if len(self.lot_num) == 4:
            if not self.lot_num[3:].isalpha():
                raise ValidationError('the optional 4th character of lot number must be a letter')
            if self.lot_num[3:].islower():
                raise ValidationError('the optional 4th character of lot number must be an uppercase letter')

    # todo - change upon django upgrade
    def validate(self):
        parcels = Parcels.objects.filter(block_num=self.block_num).filter(lot_num=self.lot_num)
        if parcels.count() > 0:
            raise ValidationError('a parcel with that block and lot already exists')
        self.validate_block_num()
        self.validate_lot_num()

    @staticmethod
    def mergeWithParcelsProvisioning(parcels):
        modelUtils = ModelUtils()
        parcels_provisioning = ParcelsProvisioning.objects.filter(parcel__in=parcels).order_by('parcel__parcel_id')
        parcels = sorted(parcels.values(), key=lambda k: k['parcel_id'])

        class OneToOneMergeImpl:
            def parentGetKey(self, parent):
                return parent['parcel_id']
            def parentSetValue(self, parent, parcelProvisioning):
                if parcelProvisioning:
                    parent['provisioned_tms'] = parcelProvisioning.provisioned_tms
                    parent['suspended_tms'] = parcelProvisioning.suspended_tms
                    parent['suspended_comment'] = parcelProvisioning.suspended_comment
                    parent['provisioning_create_tms'] = parcelProvisioning.create_tms
                else:
                    parent['provisioned_tms'] = None
                    parent['suspended_tms'] = None
                    parent['suspended_comment'] = None
                    parent['provisioning_create_tms'] = None
            def childGetKey(self, child):
                return child.parcel_id
            def childGetValue(self, child):
                return child

        return modelUtils.mergeOneToOne(
            parents=parcels,
            children =parcels_provisioning,
            mergeImpl=OneToOneMergeImpl()
        )


class Parcels(ParcelsAbstract):
    # The standard model for parcels.
    class Meta:
        db_table = u'parcels'

class ParcelsByBaseAddress(ParcelsAbstract):

    # A very slightly modified model for parcels.
    # Read only.
    # The data comes from a different source.
    # A convenient and fast way to get the parcels for an address.
    # Used for search

    address_base_id = models.IntegerField()

    class Meta:
        db_table = u'vw_base_address_x_parcels'

class ParcelBlocks(models.Model):
    parcel_block_id = models.AutoField(primary_key=True)
    block_num = models.CharField(max_length=5)
    geometry = models.PolygonField(srid=2227)
    pt_geometry = models.PointField(srid=2227)
    class Meta:
        db_table = u'parcel_blocks'


class BuildingFootprints(models.Model):
    bf_id = models.AutoField(primary_key=True)
    geometry = models.MultiPolygonField(srid=2227) # This field type is a guess.

    objects = models.GeoManager() # required for spatial filtering
    class Meta:
        db_table = u'building_footprints'

class DAddressDisposition(models.Model):
    disposition_code = models.IntegerField(primary_key=True)
    disposition_description = models.CharField(max_length=20)

    class Meta:
        db_table = u'd_address_disposition'

    def __unicode__(self):
        return ('disposition_code: %s \n disposition_description: %s \n' % (self.disposition_code, self.disposition_description))


class DUnitType(models.Model):
    unit_type_id = models.AutoField(primary_key=True)
    unit_type_description = models.CharField(max_length=50)
    class Meta:
        db_table = u'd_unit_type'
    def __unicode__(self):
        return ('unit_type_id: %s, unit_type_description: %s' % (self.unit_type_id, self.unit_type_description))

class DFloors(models.Model):
    floor_id = models.AutoField(primary_key=True)
    floor_description = models.CharField(max_length=20)
    class Meta:
        db_table = u'd_floors'
    def __unicode__(self):
        return ('\nDFloors\n\tfloor_id: %s\n\tfloor_description: %s\n' % (self.floor_id, self.floor_description))

class DDataSources(models.Model):
    data_source_id = models.AutoField(primary_key=True)
    data_source_code = models.CharField(max_length=20)
    data_source_name = models.CharField(max_length=20)
    class Meta:
        db_table = u'd_data_sources'

class DChangeRequestStatus(models.Model):
    status_id = models.IntegerField(primary_key=True)
    status_description = models.CharField(max_length=50)
    class Meta:
        db_table = u'd_change_request_status'


class DAddressBaseNumberSuffix(models.Model):
    id = models.IntegerField(primary_key=True)
    suffix_value = models.CharField(max_length=1)
    suffix_display = models.CharField(max_length=4)
    class Meta:
        db_table = u'd_address_base_number_suffix'


class Zones(models.Model):
    zone_id = models.IntegerField(primary_key=True)
    zipcode = models.CharField(max_length=50)
    jurisdiction = models.CharField(max_length=50)
    active_date = models.DateTimeField()
    retire_date = models.DateTimeField()
    geometry = models.MultiPolygonField(srid=2227) # This field type is a guess.
    objects = models.GeoManager() # required for spatial filtering
    class Meta:
        db_table = u'zones'
    def __unicode__(self):
        return (' Zones - jurisdiction: %s zone_id: %s zipcode: %s' % (self.jurisdiction, self.zone_id, self.zipcode))

class StreetSegments(models.Model):
    street_segment_id = models.AutoField(primary_key=True)
    seg_cnn = models.IntegerField()
    str_seg_cnn = models.TextField() # This field type is a guess.
    l_f_add = models.IntegerField()
    l_t_add = models.IntegerField()
    r_f_add = models.IntegerField()
    r_t_add = models.IntegerField()
    f_st = models.TextField() # This field type is a guess.
    t_st = models.TextField() # This field type is a guess.
    f_node_cnn = models.IntegerField()
    t_node_cnn = models.IntegerField()
    date_added = models.DateField()
    gds_chg_id_add = models.TextField() # This field type is a guess.
    date_dropped = models.DateField()
    gds_chg_id_dropped = models.TextField() # This field type is a guess.
    date_altered = models.DateField()
    gds_chg_id_altered = models.TextField() # This field type is a guess.
    zip_code = models.TextField() # This field type is a guess.
    district = models.TextField() # This field type is a guess.
    accepted = models.TextField() # This field type is a guess.
    jurisdiction = models.TextField() # This field type is a guess.
    n_hood = models.TextField() # This field type is a guess.
    layer = models.TextField() # This field type is a guess.
    active = models.SmallIntegerField()
    future = models.SmallIntegerField()
    toggle_date = models.DateField()
    create_tms = models.DateTimeField()
    update_tms = models.DateTimeField()
    geometry = models.MultiLineStringField(srid=2227)

    objects = models.GeoManager() # required for spatial filtering
    class Meta:
        db_table = u'street_segments'

    def __unicode__(self):
        return (
            '\nStreetSegments\n\tstreet_segment_id: %s\n\tseg_cnn: %s\n\tstr_seg_cnn: %s\n l_f_add: %s\n l_t_add: %s\n r_f_add: %s\n r_t_add: %s\n date_dropped: %s\n' %
            (self.street_segment_id, self.seg_cnn, self.str_seg_cnn, self.l_f_add, self.l_t_add, self.r_f_add, self.r_t_add, self.date_dropped)
        )

    def getAddressRangeMin(self):
        # dual carriageway
        if self.l_f_add == 0 and self.l_t_add == 0:
            return min((
                int(self.r_f_add),
                int(self.r_t_add)
            ))

        # dual carriageway
        if self.r_f_add == 0 and self.r_t_add == 0:
            return min((
                int(self.l_f_add),
                int(self.l_t_add)
            ))

        return min((
            int(self.l_f_add),
            int(self.l_t_add),
            int(self.r_f_add),
            int(self.r_t_add)
        ))

    def getAddressRangeMax(self):
        # dual carriageway
        if self.l_f_add == 0 and self.l_t_add == 0:
            return max((
                int(self.r_f_add),
                int(self.r_t_add)
            ))

        # dual carriageway
        if self.r_f_add == 0 and self.r_t_add == 0:
            return max((
                int(self.l_f_add),
                int(self.l_t_add)
            ))

        return max((
            int(self.l_f_add),
            int(self.l_t_add),
            int(self.r_f_add),
            int(self.r_t_add)
        ))

    def getSideForAddress(self, addressNumber):
        side = None
        if int(addressNumber) % 2 == 0:
            # addressNumber is even
            return self.getAddressRangeDict()['EVEN']
        else:
            # addressNumber is odd
            return self.getAddressRangeDict()['ODD']

    def getAddressRangeMinimumDifference(self, addressNumber):
        # given inputs of 3 and [1, 4, 10, 11] this should return 1
        # we expect very small arrays
        # used for street validation
        minDiff = None
        for addressRangeValue in [self.l_f_add, self.l_t_add, self.r_f_add, self.r_t_add]:
            absDiff = abs(addressNumber - addressRangeValue)
            if minDiff is None:
                minDiff = absDiff
            if minDiff > absDiff:
                minDiff = absDiff
        return minDiff

    def getAddressRangeDict(self):
        # returns a combination of the following
        # ({'EVEN':'R', 'ODD': 'L'}) typical street
        # ({'EVEN': None, 'ODD': 'R'}) divided street
        evenSide = None
        oddSide = None

        if self.l_f_add is None or self.l_t_add is None:
            pass
        if self.l_f_add == 0 and self.l_t_add == 0:
            evenSide = None
        elif self.l_f_add % 2 == 0 and self.l_t_add % 2 == 0:
            evenSide = 'L'
        elif self.l_f_add % 2 != 0 and self.l_t_add % 2 != 0:
            oddSide = 'L'
        else:
            pass

        if self.r_f_add is None or self.r_t_add is None:
            pass
        if self.r_f_add == 0 and self.r_t_add == 0:
            evenSide = None
        elif self.r_f_add % 2 == 0 and self.r_t_add % 2 == 0:
            evenSide = 'R'
        elif self.r_f_add % 2 != 0 and self.r_t_add % 2 != 0:
            oddSide = 'R'
        else:
            pass

        return {'EVEN': evenSide, 'ODD': oddSide}


    def contains(self, addressNumber):
        if addressNumber % 2 == 0:
            if self.getAddressRangeDict()['EVEN'] is None:
                return False
            elif self.getAddressRangeDict()['EVEN'] == 'L':
                # even on left
                return self.l_f_add <= addressNumber <= self.l_t_add
            elif self.getAddressRangeDict()['EVEN'] == 'R':
                # even on right
                return self.r_f_add <= addressNumber <= self.r_t_add
        else:
            if self.getAddressRangeDict()['ODD'] is None:
                return False
            elif self.getAddressRangeDict()['ODD'] == 'L':
                # odd on left
                return self.l_f_add <= addressNumber <= self.l_t_add
            elif self.getAddressRangeDict()['ODD'] == 'R':
                # odd on right
                return self.r_f_add <= addressNumber <= self.r_t_add
        return False


    def repr(self):
        streetName = self.getStreetName()
        return '%s%s%s%s%s' % (
                streetName.base_street_name,
                " " if streetName.street_type else "",
                streetName.street_type or "",
                " " if streetName.post_direction else "",
                streetName.post_direction or "",
            )


    def getStreetName(self):
        return Streetnames.objects.filter(street_segment = self).filter(category = 'MAP')[0]

    @staticmethod
    def getUnknownSegmentId():
        # returns Id of street_segment record designed to support legacy 'situs to be assigned' addresses AKA 'UNKNOWN'
        # this assumes that there is a single 'UKNOWN' street segment
        # here is the SQL to get this id from the db should this fall out of date somehow:
        # SELECT street_segment_id FROM vw_addresses_flat_native WHERE base_street_name = 'UNKNOWN' limit 1;
        return 18874

    def getDescription(self):
        return "%s (%s-%s)" % (self.repr(), self.getAddressRangeMin(), self.getAddressRangeMax())

    @staticmethod
    def getNearestTo(geometry=geometry, distanceWithin=1000, includeSegment=None, limit=10):
        queryset = StreetSegments.objects.filter(geometry__dwithin=(geometry, distanceWithin)).filter(date_dropped = None)
        queryset = queryset.extra(select={"distance" : "st_distance(street_segments.geometry, GeomFromText('%s', %s))" % (geometry.wkt, geometry.srs.srid)}, order_by=["distance"])
        nearbyStreetsList = list(queryset[0:limit])

        if (includeSegment):
            # Ensure that the includeSegment is included. This allows us to include a segment that has been previously selected but is now retired.
            segmentPresent = False
            for street in nearbyStreetsList:
                if street.street_segment_id == includeSegment.street_segment_id:
                    segmentPresent = True
            if not segmentPresent:
                nearbyStreetsList.insert(0, StreetSegments.objects.get(pk = includeSegment.street_segment_id))

        availableStreets = []
        for street in nearbyStreetsList:
            availableStreets.append({
                'street_segment_id': street.street_segment_id,
                'description': street.getDescription(),
                'geometry': street.geometry,
                'date_dropped': street.date_dropped,
                'l_f_add': street.l_f_add,
                'l_t_add': street.l_t_add,
                'r_f_add': street.r_f_add,
                'r_t_add': street.r_t_add
            })

        return availableStreets


class AddressBase(models.Model):
    address_base_id = models.AutoField(primary_key=True)
    base_address_prefix = models.CharField(max_length=10)
    base_address_num = models.IntegerField()
    base_address_suffix = models.CharField(max_length=10)
    create_tms = models.DateTimeField(null=True, auto_now_add=True)
    retire_tms = models.DateTimeField(blank = True, null = True)
    zone = models.ForeignKey(Zones)
    street_segment = models.ForeignKey(StreetSegments)
    distance_to_segment = models.FloatField()
    geometry = models.PointField(srid=2227)
    last_change_tms = models.DateTimeField(null = False)

    objects = models.GeoManager() # required for spatial filtering

    class Meta:
        db_table = u'address_base'

    def repr(self):
        return (
            'address_base_id: %s\n base_address_prefix: %s\n base_address_num: %s\n base_address_suffix: %s\n street_segment: %s\n zone.jurisdiction: %s' % (
                self.address_base_id,
                (self.base_address_prefix or '').strip(),
                self.base_address_num,
                (self.base_address_suffix or '').strip(),
                self.street_segment.repr(),
                self.zone.jurisdiction
            )
        )

    def reprForUser(self):
        return (
            '%s %s %s %s %s' % (
                (self.base_address_prefix or '').strip(),
                self.base_address_num,
                (self.base_address_suffix or '').strip(),
                self.street_segment.repr(),
                self.zone.jurisdiction
            )
        )

    def __unicode__(self):
        return self.repr()


    def getMostRecentChangeRequest(self):
        # What we want is who last changed anything about this whole address and when did they change it.
        # todo - this seems terribly inconvenient - maybe we should enrich the data model a bit.
        # At the very least we need foreign keys in django model.
        # Leaving that off now because we are out of time.

        addresses = Addresses.objects.filter(address_base = self)
        changeRequestIds = []
        for address in addresses:
            changeRequestIds.extend([address.activate_change_request_id, address.update_change_request_id, address.retire_change_request_id])

        addressXParcels = AddressXParcels.objects.filter(address__in = addresses)
        for axp in addressXParcels:
            changeRequestIds.extend([address.activate_change_request_id, address.retire_change_request_id])

        changeRequestIds = [changeRequestId for changeRequestId in changeRequestIds if changeRequestId]

        changeRequests = ChangeRequests.objects.filter(pk__in = changeRequestIds)

        mostRecentChangeRequest = changeRequests[0]
        for changeRequest in changeRequests:
            if mostRecentChangeRequest.create_tms < changeRequest.create_tms:
                mostRecentChangeRequest = changeRequest

        return mostRecentChangeRequest


    @staticmethod
    def prepareFromCrAddressBase(crBaseAddress, timeStamp, parcelDict):
        logger.info('prepareFromCrAddressBase')
        if crBaseAddress.address_base_id is not None:
            # existing
            addressBase = AddressBase.objects.get(pk = crBaseAddress.address_base_id)
            if crBaseAddress.retire_flg == True:
            # retire
                addressBase.retire_tms = timeStamp
            else:
                # update
                addressBase.geometry = crBaseAddress.geometry_proposed
        else:
            # new
            addressBase = AddressBase()
            addressBase.base_address_num = crBaseAddress.base_address_num
            addressBase.base_address_prefix = crBaseAddress.base_address_prefix
            addressBase.base_address_suffix = crBaseAddress.base_address_suffix
            geom = crBaseAddress.geometry_proposed
            geom.transform(2227)
            addressBase.geometry = geom
            addressBase.street_segment = StreetSegments.objects.get(street_segment_id = crBaseAddress.street_segment)
            if crBaseAddress.zone_id == '' or crBaseAddress.zone_id == None:
                addressBase.zone = Zones.objects.get(geometry__intersects=addressBase.geometry)
            else:
                addressBase.zone = Zones.objects.get(pk=crBaseAddress.zone_id)

        addressBase.last_change_tms = timeStamp

        return addressBase



    @staticmethod
    def filterForPrefix(queryset, prefix):
        if prefix:
            return queryset.filter(base_address_prefix = prefix)
        else:
            return queryset.exclude(base_address_prefix__isnull = False)


    @staticmethod
    def filterForSuffix(queryset, suffix):
        if suffix:
            return queryset.filter(base_address_suffix = suffix)
        else:
            return queryset.exclude(base_address_suffix__isnull = False)


# todo - investigate use of model inheritance
class AddressBaseHistory(models.Model):
    id = models.IntegerField(primary_key=True)
    address_base_id = models.IntegerField()
    base_address_prefix = models.CharField(max_length=10)
    base_address_num = models.IntegerField()
    base_address_suffix = models.CharField(max_length=10)
    create_tms = models.DateTimeField()
    retire_tms = models.DateTimeField()
    zone = models.ForeignKey(Zones)
    street_segment = models.ForeignKey(StreetSegments)
    distance_to_segment = models.FloatField()
    geometry = models.PointField(srid=2227)
    last_change_tms = models.DateTimeField()
    history_action = models.CharField(max_length=10)
    objects = models.GeoManager()
    class Meta:
        db_table = u'address_base_history'

    def __unicode__(self):
        return (
            'address_base_id: %s, base_address_num: %s, last_change_tms: %s' % (self.address_base_id, self.base_address_num, self.last_change_tms)
        )


class CrChangeRequestsManager(models.Manager):

    def getSqlStatementsToDeleteContents(self, changeRequestId):
        #
        # Here again the ORM is not up to the task of a bulk delete - or is it our lack of ORM expertise?
        # There is some question about how the transaction is handled so here we just emit SQL and let the client handle it.
        #
        sqlStatements = []

        sqlStatements.append("""
            delete
            from cr_address_x_parcels
            where cr_address_id in (
                select cr_address_id
                from cr_addresses
                where change_request_id = %s
            );
        """ % changeRequestId)

        sqlStatements.append("""
            delete
            from cr_addresses
            where change_request_id = %s;
        """ % changeRequestId)

        sqlStatements.append("""
            delete
            from cr_address_base
            where cr_address_base_id in (
                select cr_address_base_id
                from cr_addresses
                where change_request_id = %s
            );
        """ % changeRequestId)

        return sqlStatements


    def getSqlStatementsForDelete(self, changeRequestId):

        sqlStatements = self.getSqlStatementsToDeleteContents(changeRequestId)

        sqlStatements.append("""
            delete
            from cr_change_requests
            where change_request_id = %s;
        """ % changeRequestId)

        return sqlStatements


class CrChangeRequests(models.Model):

    # http://www.b-list.org/weblog/2007/nov/02/handle-choices-right-way/
    STATUS_UNDER_REVIEW = 1
    STATUS_APPROVED = 2
    STATUS_REJECTED = 3
    STATUS_EDITING = 4
    STATUS_SUBMITTED = 5
    STATUS_CHOICES = (
        (STATUS_UNDER_REVIEW, 'under review'),
        (STATUS_APPROVED, 'approved'),
        (STATUS_REJECTED, 'rejected'),
        (STATUS_EDITING, 'editing'),
        (STATUS_SUBMITTED, 'submitted'),
    )

    change_request_id = models.AutoField(primary_key=True)
    concurrency_id = models.IntegerField(blank=False, null=False, default=0)
    name = models.CharField(max_length=100)
    requestor_user = models.ForeignKey(User, related_name='requestor')
    requestor_comment = models.CharField(max_length=500, blank = True, null = True)
    reviewer_user = models.ForeignKey(User, blank = True, null = True, related_name='reviewer')
    reviewer_comment = models.CharField(max_length=500, blank = True, null = True)
    review_status = models.ForeignKey(DChangeRequestStatus, db_column='review_status')
    resolve_tms = models.DateTimeField(blank = True, null = True)
    requestor_last_update = models.DateTimeField(blank = True, null = True)
    reviewer_last_update = models.DateTimeField(blank = True, null = True)

    objects = CrChangeRequestsManager()

    class Meta:
        db_table = u'cr_change_requests'

    def __unicode__(self):
        return (
            '\nCrChangeRequest\n\tchange_request_id: %s\n\tconcurrency_id: %s\n\trequestor_comment: %s\n\treviewer_comment: %s\n' %
            (
                self.change_request_id,
                self.concurrency_id,
                self.requestor_comment,
                self.reviewer_comment
            )
        )

    def validateGeometricRegistration(self, unretiredCrAddresses = None):
        # Since the parcel geometry may change because of the ETL, we must make sure that the selected parcels are on the same block as the base address point geometry.
        #
        # This has become more complicated than the original implementation because of a performance issue.
        #   http://sfgovdt.jira.com/browse/MAD-126
        # Let the database to the work. Asking django to do this is folly.

        from MAD.models.ModelUtils import ModelUtils
        from sets import Set

        logger.info('validateGeometricRegistration() start')
        modelUtils = ModelUtils()

        # Group the unretiredCrAddresses by cr base address.
        # e.g. {'crBaseAddressPk': {'crBaseAddress': crBaseAddress, 'crAddresses': []}, ...}
        logger.info('grouping unretiredCrAddresses...')
        crBaseAddresses = {}
        for unretiredCrAddress in unretiredCrAddresses:
            crBaseAddress = unretiredCrAddress.cr_address_base
            crAddressBaseId = crBaseAddress.cr_address_base_id
            if crAddressBaseId not in crBaseAddresses:
                crBaseAddresses[crAddressBaseId] = {'crBaseAddress':crBaseAddress, 'crAddresses': []}
            crBaseAddresses[crAddressBaseId]['crAddresses'].append(unretiredCrAddress)

        # for each cr_base_address, we query for all CrAddressXParcel in the cr_base_address addresses and that are not at the cr base address geom.
        logger.info('starting geometric validations...')
        validations = []
        try:
            for crBaseAddressDict in crBaseAddresses.values():
                crBaseAddress = crBaseAddressDict['crBaseAddress']
                crUnitAddresses = crBaseAddressDict['crAddresses']
                # Check for "parcel link changes" where we are creating new links from addresses to retired parcels.
                # We do not want to do this.
                # How can this happen?
                # Day one, you go in and create a change request, and link to an unretired parcel.
                # You save to change request but it is not approved.
                # Overnight, the parcel is retired.
                # In this case, we do not want to allow the link.
                logger.info('looking for parcel link changes that are are no longer legal')
                allowedBlockLotSet = Set([parcel['apn'] for parcel in modelUtils.buildParcelPickList(crBaseAddress.geometry, includeRetired=False)])
                # optimized for performance (defer geometry)
                crAddressXParcels = CrAddressXParcels.objects.filter(cr_address__in = crUnitAddresses).exclude(unlink = True)
                parcels = Parcels.objects.filter(parcel_id__in = [axp.parcel_id for axp in crAddressXParcels]).defer('geometry')
                selectedBlockLotSet = Set([parcel.blk_lot for parcel in parcels])
                illegalBlockLots = selectedBlockLotSet.difference(allowedBlockLotSet)
                illegalBlockLots = list(illegalBlockLots)
                if len(illegalBlockLots) > 0:
                    craxp = crAddressXParcels.filter(parcel__blk_lot=illegalBlockLots[0])[0]
                    message = 'This change request contains an address "%s" that you are trying to link to the parcel "%s" but the link is no longer allowed. This is probably due to a recent change in the parcel data.  To proceed please ensure that the address point is within the right parcel and that the linked parcels are all within the same block.' % (craxp.cr_address.repr(), craxp.parcel.blk_lot)
                    raise ValidationWarning(message)
        except Exception, e:
            logger.info(e.message)
            validations.append(Validation(valid=False, message=e.message))

        logger.info('validateGeometricRegistration() end')
        return validations


    def getUnretiredCrAddresses(self):
        # This is a little bit subtle.
        # Recall that the Base Address has its own unit address (address_base_flg = True).
        # If this unit address is marked retired, we imply that all unit addresses under the base address will be retired.
        crAddressesForBase = CrAddresses.objects.filter(change_request = self.change_request_id).filter(retire_flg = False).filter(address_base_flg = True)
        crAddressBases = [crAddressForBase.cr_address_base for crAddressForBase in crAddressesForBase]
        unretiredCrAddresses = CrAddresses.objects.select_related('cr_address_base').filter(cr_address_base__in = crAddressBases).filter(retire_flg = False)
        return unretiredCrAddresses


    def validateForSave(self):
        logger.info('running validateForSave')
        validations = []

        # todo - throw exceptions to get out quickly
        # todo - do cheap or validations first OR do the ones that fail most often first.
        # todo - refactor
        # note - the duplicate checks will not physically prevent duplicates unless we resort to locking the table  

        # Everything must be in the DB for the validations to work properly.

        unretiredCrAddresses = self.getUnretiredCrAddresses()

        crAddresses = CrAddresses.objects.filter(change_request = self.change_request_id).filter(address_base_flg = True)
        crAddressBaseIds = []
        for crAddress in crAddresses:
            crAddressBaseIds.append(crAddress.cr_address_base.cr_address_base_id)
        crAddressBases = CrAddressBase.objects.filter(cr_address_base_id__in=crAddressBaseIds)
        for crAddressBase in crAddressBases:
            crAddress = CrAddresses.objects.filter(cr_address_base = crAddressBase).get(address_base_flg = True)
            # Duplicate addresses in queue not allowed
            validations.append(crAddressBase.validateDuplicatesInQueue())
            # Duplicate addresses not allowed.
            validations.append(crAddressBase.validateDuplicateInActive(crAddressBases, crAddresses))
            # Invalid unit numbers not allowed.
            validations.append(crAddressBase.validateUnitNumbers())
            # Duplicate units not allowed.
            validations.append(crAddressBase.validateDuplicateUnits())

        failedValidations = self.validateGeometricRegistration(unretiredCrAddresses = unretiredCrAddresses)
        validations.extend(failedValidations)

        # We originally passed back all failed validations - but this is too much for the UI (is it?).
        # For now we strip that down to a single message.
        # todo - We need to come up with a good structured way to handle this across all validation activities.
        validationSuccess = True
        validationMessage = ''
        for validation in validations:
            if not validation.valid:
                validationSuccess = False
                validationMessage = validation.message
                break

        if validationSuccess:
            pass
        else:
            raise ValidationWarning(validationMessage)

        logger.info('completed running validateForSave')
        return None

    @staticmethod
    def prepareFromCrJsonChangeRequest(crJsonChangeRequest, user, currentTimestamp):
        logger.info('prepareFromCrJsonChangeRequest - begin')

        try:
            crChangeRequest = CrChangeRequests.objects.get(pk = crJsonChangeRequest.change_request_id)
        except:
            crChangeRequest = CrChangeRequests()
            if crJsonChangeRequest.review_status_id is None:
                crJsonChangeRequest.review_status_id = CrChangeRequests.STATUS_EDITING

        crChangeRequest.name = crJsonChangeRequest.name
        crChangeRequest.concurrency_id = crJsonChangeRequest.concurrency_id + 1
        status = DChangeRequestStatus.objects.get(status_id=crJsonChangeRequest.review_status_id)
        crChangeRequest.review_status = status

        if status.status_description == 'editing' or status.status_description == 'submitted':
            # requestor is saving
            crChangeRequest.requestor_user = user
            if crJsonChangeRequest.requestor_comment:
                crChangeRequest.requestor_comment = crJsonChangeRequest.requestor_comment
            crChangeRequest.requestor_last_update = currentTimestamp
        else:
            # reviewer is saving
            crChangeRequest.reviewer_user = user
            if crJsonChangeRequest.reviewer_comment:
                crChangeRequest.reviewer_comment = crJsonChangeRequest.reviewer_comment
            crChangeRequest.reviewer_last_update = currentTimestamp



        logger.info('prepareFromCrJsonChangeRequest - end')
        return crChangeRequest


class ChangeRequests(models.Model):
    change_request_id = models.AutoField(primary_key=True)
    cr_change_request = models.ForeignKey(CrChangeRequests)
    requestor_user = models.ForeignKey(User, related_name='requestor')
    requestor_comment = models.CharField(max_length=500)
    reviewer_user = models.ForeignKey(User, blank = True, null = True, related_name='reviewer')
    reviewer_comment = models.CharField(max_length=500)
    create_tms = models.DateTimeField()

    class Meta:
        db_table = u'change_requests'

    def __unicode__(self):
        return (
            'requestor_user_id: %s, requestor_comment: %s, reviewer_user_id: %s, reviewer_comment: %s, create_tms: %s ' %
            (   self.requestor_user.id,
                self.requestor_comment,
                self.reviewer_user.id,
                self.reviewer_comment,
                self.create_tms
            )
        )

    # todo - model needs work (http://code.google.com/p/eas/issues/detail?id=422)
    # is this really a view?
    def getSummary(self):
        summary = {}
        summary['change_request_id'] = self.change_request_id
        summary['change_tms'] = templateDate(self.create_tms, settings.DATETIME_FORMAT)

        requestor = User.objects.get(pk = self.requestor_user.id)
        summary['requestor_name'] = requestor.get_full_name() or requestor.username
        summary['requestor_email'] = requestor.email
        summary['requestor_comment'] = self.requestor_comment or 'no comment'

        reviewer = User.objects.get(pk = self.reviewer_user_id)
        summary['reviewer_name'] = reviewer.get_full_name() or reviewer.username
        summary['reviewer_email'] = reviewer.email
        summary['reviewer_comment'] = self.reviewer_comment or 'no comment'

        return summary

    @staticmethod
    def prepareFromCrChangeRequest(crChangeRequest=None, timeStamp=None):
        logger.info('prepareFromCrChangeRequest')
        changeRequest = ChangeRequests()
        changeRequest.cr_change_request = crChangeRequest
        changeRequest.requestor_comment = crChangeRequest.requestor_comment
        changeRequest.requestor_user_id = crChangeRequest.requestor_user.pk
        changeRequest.reviewer_comment = crChangeRequest.reviewer_comment
        changeRequest.reviewer_user_id = crChangeRequest.reviewer_user.pk
        changeRequest.create_tms = timeStamp
        return changeRequest


class AddressesManager(models.Manager):

    def getByBaseAdddressId(self, addressBaseId=None, includeBaseAddresses=False):
        # todo - migrate to ModelUtils.merge***()
        modelUtils = ModelUtils()

        # Note: order_by is critical for merge below.
        unitAddresses = self.select_related(depth=1).filter(address_base = addressBaseId).filter(retire_tms = None).filter(address_base_flg = includeBaseAddresses).order_by('address_id')
        unitAddressesXParcels = AddressXParcels.unretiredObjects.filter(address__in = unitAddresses).order_by('address')

        # I was unable to get select_related() to work for all the fields I wanted.
        # This was in some cases resulting in hundreds of extra db queries - for things like floor and disposition.
        # So here I switch to dictionary to improve performance for the subsequent accesses by clients.
        unitAddressesDict = unitAddresses.values()

        returnUnitAddresses = []

        if unitAddresses.count() > 0:
            if unitAddressesXParcels.count() == 0:
                for unitAddress in unitAddressesDict:
                    unitAddress['linked_parcels'] = []
                    returnUnitAddresses.append(unitAddress)

            else:
                addressXParcelIterator = unitAddressesXParcels.iterator()
                addressXParcel = addressXParcelIterator.next()
                # We may have hundreds of unit addresses and more than a thousand Parcels, so we want this next block to be fairly efficient.
                for unitAddress in unitAddressesDict:
                    linked_parcels = []
                    while 1==1:
                        if unitAddress['address_id'] == addressXParcel.address.address_id:
                           linked_parcels.append(addressXParcel.getShortDict())
                           try:
                                addressXParcel = addressXParcelIterator.next()
                           except StopIteration:
                                break
                        elif unitAddress['address_id'] < addressXParcel.address.address_id:
                           break
                        elif unitAddress['address_id'] > addressXParcel.address.address_id:
                           try:
                                addressXParcel = addressXParcelIterator.next()
                           except StopIteration:
                                break
                    linked_parcels = sorted(linked_parcels, key=lambda x: x['apn'])
                    unitAddress['linked_parcels'] = linked_parcels
                    returnUnitAddresses.append(unitAddress)

        # http://sfgovdt.jira.com/browse/MAD-125
        returnUnitAddresses = sorted(returnUnitAddresses, key=lambda x: x['unit_num'])

        return returnUnitAddresses


class Addresses(models.Model):

    address_id = models.AutoField(primary_key=True)
    address_base = models.ForeignKey(AddressBase)
    unit_type = models.ForeignKey(DUnitType)
    floor = models.ForeignKey(DFloors)
    unit_num = models.CharField(max_length=10)
    create_tms = models.DateTimeField(null=False, auto_now_add=True)
    retire_tms = models.DateTimeField(null = True)
    retire_change_request = models.ForeignKey(ChangeRequests, related_name='retired_address')
    activate_change_request = models.ForeignKey(ChangeRequests, related_name='activated_address')
    update_change_request = models.ForeignKey(ChangeRequests, related_name='updated_address')
    mailable_flg = models.BooleanField()
    disposition_code = models.ForeignKey(DAddressDisposition, db_column='disposition_code', to_field='disposition_code')
    address_base_flg = models.BooleanField()
    last_change_tms = models.DateTimeField(null = False)

    objects = AddressesManager()

    class Meta:
        db_table = u'addresses'

    def repr(self):
        return  'address_base_id: %s\n address_id: %s\n unit_num: %s\n create_tms: %s\n retire_tms: %s' % (
                    self.address_base.address_base_id or ''
                    ,(self.address_id or '')
                    ,(self.unit_num or '')
                    ,(self.create_tms or '')
                    ,(self.retire_tms or '')
                )

    def __unicode__(self):
        return self.repr()

    def reprForRetired(self):
        assert self.retire_tms, 'expected self to be retired but it is not'
        reviewer = User.objects.get(pk = self.retire_change_request.reviewer_user_id)
        return self.address_base.reprForUser() + ' was retired by %s (%s) on %s.' % (reviewer.get_full_name(), reviewer.email, templateDate(self.retire_tms, settings.DATETIME_FORMAT))


    @staticmethod
    def prepareFromCr(changeRequest=None, addressBase=None, crUnit=None, cacheManager=None, timeStamp=None, primaryKey=None):
        # fieldsToUpdate - supports SQL batches
        address = Addresses()
        address.fieldsToUpdate = ['address_id', 'last_change_tms']
        if crUnit.address_id is not None:
            address.pk = crUnit.address_id
            if crUnit.retire_flg == True or addressBase.retire_tms is not None:
                action = 'retire'
                address.retire_tms = timeStamp
                address.retire_change_request = changeRequest
                address.fieldsToUpdate.extend(['retire_tms', 'retire_change_request'])
            else:
                action = 'update'
                address.mailable_flg = crUnit.mailable_flg_proposed
                address.disposition_code = cacheManager.get('DAddressDisposition', crUnit.disposition_proposed)
                address.update_change_request = changeRequest
                address.fieldsToUpdate.extend(['mailable_flg', 'disposition_code', 'update_change_request'])
        else:
            action = 'insert'
            address.pk = primaryKey
            address.activate_change_request = changeRequest
            address.address_base = addressBase
            address.address_base_flg = crUnit.address_base_flg
            address.disposition_code = cacheManager.get('DAddressDisposition', crUnit.disposition_proposed)
            if crUnit.floor != None and crUnit.floor != '':
                address.floor = cacheManager.get('DFloors', crUnit.floor)
            address.mailable_flg = crUnit.mailable_flg_proposed
            address.unit_num = crUnit.unit_num
            if crUnit.unit_type != None and crUnit.unit_type != '':
                address.unit_type = cacheManager.get('DUnitType', crUnit.unit_type)

        return (action, address)


    @staticmethod
    def prepareAddressForBaseAddressFromCr(changeRequest=None, addressBase=None, crBaseAddress=None, cacheManager=None, timeStamp=None, parcelDict=None, connection=None):
        logger.info('prepareAddressForBaseAddressFromCr')

        addresses = Addresses.objects.filter(address_base = addressBase)
        address = addresses.filter(address_base_flg = True)

        # determine which: insert, update, retire
        if address.count() == 1:
            address = address[0]
            if addressBase.retire_tms is not None:
                action = 'retire'
            else:
                action = 'update'
        elif address.count() == 0:
            action = 'insert'
            address = Addresses()
        else:
            assert False, 'found address count of %s where we expected 0 or 1' % address.count()

        def setFieldsForUpdateInsert(crBaseAddress, cacheManager, address):
            address.mailable_flg = crBaseAddress.unit_mailable_flg_proposed
            address.disposition_code = cacheManager.get('DAddressDisposition', crBaseAddress.unit_disposition_proposed)

        if action == 'update':
            setFieldsForUpdateInsert(crBaseAddress, cacheManager, address)
            address.update_change_request = changeRequest
        elif action == 'insert':
            # also see section below common to update and insert
            address = Addresses()
            addressMinPk, addressMaxPk = DbUtils.getNextSequenceRange(connection, 'Addresses', 1)
            addressPk = addressMinPk
            address.pk = addressPk
            address.activate_change_request = changeRequest
            address.address_base = addressBase
            address.address_base_flg = crBaseAddress.address_base_flg
            if crBaseAddress.floor != None and crBaseAddress.floor != '':
                address.floor = cacheManager.get('DFloors', crBaseAddress.floor)
            address.unit_num = crBaseAddress.unit_num
            address.create_tms = timeStamp
            if crBaseAddress.unit_type != None and crBaseAddress.unit_type != '':
                address.unit_type = cacheManager.get('DUnitType', crBaseAddress.unit_type)
            setFieldsForUpdateInsert(crBaseAddress, cacheManager, address)
        elif action == 'retire':
            address.retire_tms = timeStamp
            address.retire_change_request = changeRequest
        else:
            assert False, 'found action %s where we expected insert, update, retire' % action

        address.last_change_tms = timeStamp

        addressXParcelInsertList, addressXParcelUpdateList = AddressXParcels.prepareParcelLinkChanges(parcelLinkChanges=crBaseAddress.parcel_link_changes, unitAddress=address, changeRequest=changeRequest, timeStamp=timeStamp, parcelDict=parcelDict)

        return (address, addressXParcelInsertList, addressXParcelUpdateList)


    @staticmethod
    def prepareListsFromCrBaseAddress(crBaseAddress=None, changeRequest=None, connection=None, cacheManager=None, addressBase=None, parcelDict=None, timeStamp=None):
        logger.info('prepareListsFromCrBaseAddress')

        insertList = []
        addressXParcelInsertList = []
        addressXParcelUpdateList = []
        updateList = []
        retireList = []

        # inserts - special treatment to handle the primary keys
        crUnitsForInserts = []
        for crUnit in crBaseAddress.unit_addresses:
            if crUnit.address_id is None:
                crUnitsForInserts.append(crUnit)
        addressMinPk, addressMaxPk = DbUtils.getNextSequenceRange(connection, 'Addresses', len(crUnitsForInserts))
        addressPk = addressMinPk - 1
        for crUnit in crUnitsForInserts:
            addressPk+=1
            (action, unit) = Addresses.prepareFromCr(changeRequest=changeRequest, addressBase=addressBase, crUnit=crUnit, cacheManager=cacheManager, primaryKey=addressPk, timeStamp=timeStamp)
            assert action=='insert', 'expected action value to be insert but it was %s ' % action
            insertList.append(unit)
            addressXParcelInserts, addressXParcelUpdates = AddressXParcels.prepareParcelLinkChanges(parcelLinkChanges=crUnit.parcel_link_changes, unitAddress=unit, parcelDict=parcelDict, changeRequest=changeRequest, timeStamp=timeStamp)
            addressXParcelInsertList.extend(addressXParcelInserts)
            addressXParcelUpdateList.extend(addressXParcelUpdates)
        assert addressPk==addressMaxPk, 'primary key misalignment detected for Addresses'

        # updates, retires
        crUnitsForUpdatesAndRetires = []
        for crUnit in crBaseAddress.unit_addresses:
            if crUnit.address_id is not None:
                crUnitsForUpdatesAndRetires.append(crUnit)
        for crUnit in crUnitsForUpdatesAndRetires:
            (action, unit) = Addresses.prepareFromCr(changeRequest=changeRequest, addressBase=addressBase, crUnit=crUnit, cacheManager=cacheManager, primaryKey=None, timeStamp=timeStamp)
            assert action in ('update','retire'), 'expected action value to be "update" or "retire" but it was %s ' % action
            if (action=='update'):
                updateList.append(unit)
            elif (action=='retire'):
                retireList.append(unit)
            addressXParcelInserts, addressXParcelUpdates = AddressXParcels.prepareParcelLinkChanges(parcelLinkChanges=crUnit.parcel_link_changes, unitAddress=unit, parcelDict=parcelDict, changeRequest=changeRequest, timeStamp=timeStamp)
            addressXParcelInsertList.extend(addressXParcelInserts)
            addressXParcelUpdateList.extend(addressXParcelUpdates)

        for list in (insertList, updateList, retireList):
            for unitAddress in list:
                unitAddress.last_change_tms = timeStamp

        return (insertList, updateList, retireList, addressXParcelInsertList, addressXParcelUpdateList)

# todo - investigate use of model inheritance
class AddressesHistory(models.Model):
    id = models.IntegerField(primary_key=True)
    address_id = models.IntegerField()
    address_base_id = models.IntegerField()
    unit_type_id = models.IntegerField()
    floor_id = models.IntegerField()
    unit_num = models.CharField(max_length=20)
    create_tms = models.DateTimeField()
    retire_tms = models.DateTimeField()
    retire_change_request_id = models.IntegerField()
    activate_change_request_id = models.IntegerField()
    concurrency_id = models.IntegerField()
    mailable_flg = models.BooleanField()
    disposition_code = models.ForeignKey(DAddressDisposition, db_column='disposition_code')
    address_base_flg = models.BooleanField()
    update_change_request_id = models.IntegerField()
    last_change_tms = models.DateTimeField()
    history_action = models.CharField(max_length=10)
    class Meta:
        db_table = u'addresses_history'

    def  __unicode__(self):
        return 'unit_num: %s' % self.unit_num



class AddressSourceData(models.Model):
    address_src_id = models.AutoField(primary_key=True)
    address = models.ForeignKey(Addresses)
    data_source_code = models.ForeignKey(DDataSources, db_column='data_source_code')
    class Meta:
        db_table = u'address_source_data'


class AddressXParcelsCustomManager(models.Manager):
    def get_query_set(self):
        return super(AddressXParcelsCustomManager, self).get_query_set().select_related(depth=1).filter(retire_tms__isnull = True)

class AddressXParcels(models.Model):
    id = models.AutoField(primary_key=True)
    parcel = models.ForeignKey(Parcels, null=False)
    address = models.ForeignKey(Addresses, null=False)
    create_tms = models.DateTimeField(null=False)
    last_change_tms = models.DateTimeField(null=False)
    retire_tms = models.DateTimeField(null=True)
    activate_change_request = models.ForeignKey(ChangeRequests, related_name='activated_address_x_parcel', null = False)
    retire_change_request = models.ForeignKey(ChangeRequests, related_name='retired_address_x_parcel', null = True)
    class Meta:
        db_table = u'address_x_parcels'
    def  __unicode__(self):
        return 'parcel_id: %s, address_id: %s, create_tms: %s, retire_tms: %s' % (self.parcel.pk, self.address.pk, self.create_tms, self.retire_tms)

    unretiredObjects = AddressXParcelsCustomManager()
    objects = models.Manager()

    def getShortDict(self):
        return {'apn': self.parcel.blk_lot, 'address_x_parcel_id': self.id}

    def prepareForRetire(self, timeStamp, changeRequest):
        self.retire_tms = timeStamp
        self.last_change_tms = timeStamp
        self.retire_change_request=changeRequest
        self.fieldsToUpdate = ['retire_change_request', 'retire_tms', 'last_change_tms']

    @staticmethod
    def buildLinkedParcelList(addressId=None):
        # performance counts - avoid parcel geometry
        logger.info('buildLinkedParcelList begin')
        orderedAxps = AddressXParcels.objects.filter(address__address_id=addressId).order_by('id').filter(retire_tms__isnull = True)
        orderedBlockLots = orderedAxps.values_list('parcel__blk_lot', flat=True)
        linkedParcelList = []
        for tuple in zip(orderedAxps, orderedBlockLots):
            linkedParcelList.append({'apn': tuple[1], 'address_x_parcel_id': tuple[0].id})

        # order by block lot
        linkedParcelList = sorted(linkedParcelList, key=lambda x: x['apn'])

        #logger.info(linkedParcelList)
        logger.info('buildLinkedParcelList end')
        return linkedParcelList


    @staticmethod
    def prepareParcelLinkChanges(parcelLinkChanges=None, unitAddress=None, changeRequest=None, timeStamp=None, parcelDict=None):
        #logger.info('prepareParcelLinkChanges')
        addressXParcelInsertList = []
        addressXParcelUpdateList = []

        if unitAddress.retire_tms:
            # retire all associated address_x_parcels
            assert unitAddress.address_id is not None, 'It is not possible to retire a unit address that has no primary key.'
            addressXParcels = AddressXParcels.unretiredObjects.filter(address=unitAddress)
            for addressXParcel in addressXParcels:
                addressXParcel.prepareForRetire(timeStamp, changeRequest)
                addressXParcelUpdateList.append(addressXParcel)
        else:
            for parcelLinkChange in parcelLinkChanges:
                if parcelLinkChange['address_x_parcel_id']:
                    # update is the same as retire
                    axp = AddressXParcels(id=parcelLinkChange['address_x_parcel_id'])
                    axp.prepareForRetire(timeStamp, changeRequest)
                    addressXParcelUpdateList.append(axp)
                else:
                    # insert
                    axp = AddressXParcels(address=unitAddress, parcel=parcelDict[parcelLinkChange['apn']], activate_change_request=changeRequest, create_tms=timeStamp, last_change_tms=timeStamp)
                    addressXParcelInsertList.append(axp)

        return (addressXParcelInsertList, addressXParcelUpdateList)


class AddressXParcelsHistory(models.Model):
    id = models.IntegerField(primary_key=True)
    address_x_parcel = models.ForeignKey(AddressXParcels)
    parcel = models.ForeignKey(Parcels)
    address_id = models.IntegerField()
    activate_change_request_id = models.IntegerField()
    retire_change_request_id = models.IntegerField()
    create_tms = models.DateTimeField()
    last_change_tms = models.DateTimeField()
    retire_tms = models.DateTimeField()
    history_action = models.CharField(max_length=10)
    class Meta:
        db_table = u'address_x_parcels_history'


class XmitQueueManager(models.Manager):
    def enqueue(self, cursor):


        # base address changes that have no unit address changes and have no APX changes
        sql = """
            insert into xmit_queue (address_base_history_id, address_history_id, address_x_parcel_history_id, last_change_tms, sort_order)
            select  abh.id,
                    null,
                    null,
                    abh.last_change_tms,
                    (case
                        when abh.history_action = 'update' then 1
                        when abh.history_action = 'retire' then 2
                        when abh.history_action = 'insert' then 3
                        else 4
                    end)
            from address_base_history abh
            where abh.history_action in ('insert', 'update', 'retire')
            and not exists(
                select 1
                from addresses_history ah
                where abh.address_base_id = ah.address_base_id
                and abh.last_change_tms = ah.last_change_tms
                and ah.history_action in ('insert', 'update', 'retire')
            )
            and not exists (
                select 1
                from addresses_history ah,
                address_x_parcels_history axph
                where abh.address_base_id = ah.address_base_id
                and abh.last_change_tms = ah.last_change_tms
                and ah.address_id = axph.address_id
                and axph.last_change_tms = ah.last_change_tms
            )
            and not exists (
                select  1
                from    xmit_queue xq
                where 	xq.address_base_history_id = abh.id
                and     xq.last_change_tms = abh.last_change_tms
                and 	xq.address_history_id is null
                and 	xq.address_x_parcel_history_id is null
            );
        """
        cursor.execute(sql)

        # unit address changes that have no apx changes
        sql = """
            insert into xmit_queue (address_base_history_id, address_history_id, address_x_parcel_history_id, last_change_tms, sort_order)
            select  abh.id,
                    ah.id,
                    null,
                    abh.last_change_tms,
                    (case
                        when ah.history_action = 'update' then 1
                        when ah.history_action = 'retire' then 2
                        when ah.history_action = 'insert' then 3
                        else 4
                    end)
            from address_base_history abh
            inner join addresses_history ah on (abh.address_base_id = ah.address_base_id and abh.last_change_tms = ah.last_change_tms)
            where ah.history_action in ('insert', 'update', 'retire')
            and not exists (
                select 1
                from address_x_parcels_history axph
                where ah.address_id = axph.address_id
                and axph.last_change_tms = ah.last_change_tms
            )
            and not exists (
                select  1
                from    xmit_queue xq
                where 	xq.address_base_history_id = abh.id
                and     xq.last_change_tms = abh.last_change_tms
                and 	xq.address_history_id = ah.id
                and 	xq.address_x_parcel_history_id is null
            );
        """
        cursor.execute(sql)

        # apx changes
        sql = """
            insert into xmit_queue (address_base_history_id, address_history_id, address_x_parcel_history_id, last_change_tms, sort_order)
            select  abh.id,
                    ah.id,
                    axph.id,
                    abh.last_change_tms,
                    (case
                        when axph.history_action = 'update' then 1
                        when axph.history_action = 'retire' then 2
                        when axph.history_action = 'insert' then 3
                        else 4
                    end)
            from address_base_history abh
            inner join addresses_history ah on (abh.address_base_id = ah.address_base_id and abh.last_change_tms = ah.last_change_tms)
            inner join address_x_parcels_history axph on (ah.address_id = axph.address_id and ah.last_change_tms = axph.last_change_tms)
            and not exists (
                select  1
                from    xmit_queue xq
                where 	xq.address_base_history_id = abh.id
                and     xq.last_change_tms = abh.last_change_tms
                and 	xq.address_history_id = ah.id
                and 	xq.address_x_parcel_history_id = axph.id
            );
        """
        cursor.execute(sql)

    def getQueued(self):
        # NOTE: order by must be (last_change_tms, sort_order)
        # Why the fuss?
        # First we want the inserts to precede upadates and retires.
        # This is accomplished with the following
        #  - the order by last_change_tms
        #  - the same base address is not allowed into the change request queue more than once
        # An additional refinement is needed because we do not necessarily know what sort of constraints might be on the client system.
        # Take the following case - which is odd but nothing stops a user from doing this.
        # Within the same change request...
        # - create 1 South Van Ness
        # - retire 1 South Van Ness
        # Here we have 2 essentially identical addresses; the retire must be processed first.
        # Why?
        # A reasonable constraint on the client system is to allow only one active address at a time.
        # And of course if we violate a constraint like this here, the insert will fail.  
        return XmitQueue.objects.select_related(depth=2).filter(xmit_tms__isnull = True).order_by('last_change_tms', 'sort_order')


class XmitQueue(models.Model):
    id = models.IntegerField(primary_key=True)
    address_base_history = models.ForeignKey(AddressBaseHistory, null=False)
    address_history = models.ForeignKey(AddressesHistory, null=True)
    address_x_parcel_history = models.ForeignKey(AddressXParcelsHistory, null=True)
    last_change_tms = models.DateTimeField(null=False)
    xmit_tms = models.DateTimeField(null=True)
    sort_order = models.IntegerField()
    xml = None # non-persistent

    class Meta:
        db_table = u'xmit_queue'

    objects = XmitQueueManager()


class XmitAccelaQueueManager(models.Manager):

    def enqueue(self, cursor):
        sql = """
            insert into xmit_accela_queue (address_base_history_id, address_history_id, address_x_parcel_history_id, last_change_tms, sort_order)
            select  abh.id,
                    ah.id,
                    axph.id,
                    axph.last_change_tms,
                    (case
                        when axph.history_action = 'update' then 1
                        when axph.history_action = 'retire' then 2
                        when axph.history_action = 'insert' then 3
                        else 4
                    end)
            from address_base_history abh
            inner join addresses_history ah on (abh.address_base_id = ah.address_base_id and abh.last_change_tms = ah.last_change_tms)
            inner join address_x_parcels_history axph on (ah.address_id = axph.address_id and ah.last_change_tms = axph.last_change_tms)
            where axph.last_change_tms > timestamp '%s'
            and not exists (
                select  1
                from    xmit_accela_queue xaq
                where 	xaq.address_base_history_id = abh.id
                and     xaq.last_change_tms = abh.last_change_tms
                and 	xaq.address_history_id = ah.id
                and 	xaq.address_x_parcel_history_id = axph.id
            );
        """ % settings.XMIT_ACCELA_GO_LIVE_TMS
        cursor.execute(sql)

    def getQueued(self):
        # NOTE: order by must be (last_change_tms, sort_order)
        # Why the fuss?
        # First we want the inserts to precede upadates and retires.
        # This is accomplished with the following
        #  - the order by last_change_tms
        #  - the same base address is not allowed into the change request queue more than once
        # An additional refinement is needed because we do not necessarily know what sort of constraints might be on the client system.
        # Take the following case - which is odd but nothing stops a user from doing this.
        # Within the same change request...
        # - create 1 South Van Ness
        # - retire 1 South Van Ness
        # Here we have 2 essentially identical addresses; the retire must be processed first.
        # Why?
        # A reasonable constraint on the client system is to allow only one active address at a time.
        # And of course if we violate a constraint like this here, the insert will fail.
        return XmitAccelaQueue.objects.select_related(depth=2).filter(xmit_tms__isnull = True).order_by('last_change_tms', 'sort_order')


class XmitAccelaQueue(models.Model):
    id = models.IntegerField(primary_key=True)
    address_base_history = models.ForeignKey(AddressBaseHistory, null=False)
    address_history = models.ForeignKey(AddressesHistory, null=True)
    address_x_parcel_history = models.ForeignKey(AddressXParcelsHistory, null=True)
    last_change_tms = models.DateTimeField(null=False)
    xmit_tms = models.DateTimeField(null=True)
    sort_order = models.IntegerField()
    json = None # non-persistent

    class Meta:
        db_table = u'xmit_accela_queue'

    objects = XmitAccelaQueueManager()


class Landmarks(models.Model):
    land_landmark_id = models.IntegerField()
    land_landmark_desc = models.CharField(max_length=50)
    class Meta:
        db_table = u'landmarks'

class PortsFacilities(models.Model):
    ports_id = models.AutoField(primary_key=True)
    class Meta:
        db_table = u'ports_facilities'

class StreetAddressRanges(models.Model):
    range_id = models.AutoField(primary_key=True)
    left_from_address = models.IntegerField()
    left_to_address = models.IntegerField()
    right_from_address = models.IntegerField()
    right_to_address = models.IntegerField()
    update_tms = models.DateTimeField()
    create_tms = models.DateTimeField()
    street_segment = models.ForeignKey(StreetSegments)
    class Meta:
        db_table = u'street_address_ranges'

class Streetnames(models.Model):
    streetname_id = models.AutoField(primary_key=True)
    seg_cnn = models.IntegerField()
    bsm_streetnameid = models.IntegerField()
    pre_direction = models.CharField(max_length=2)
    base_street_name = models.CharField(max_length=60)
    street_type = models.CharField(max_length=6)
    post_direction = models.CharField(max_length=2)
    full_street_name = models.CharField(max_length=255)
    category = models.CharField(max_length=10)
    street_segment = models.ForeignKey(StreetSegments)
    create_tms = models.DateTimeField()
    class Meta:
        db_table = u'streetnames'

    def getStreetTypeUnabbreviated(self):
        if self.street_type:
            streetType = DStreetType.objects.get(abbreviated = self.street_type.strip())
            return streetType.unabbreviated
        else:
            return None

    def __unicode__(self):
        return (
            'seg_cnn: %s base_street_name: %s street_type: %s category: %s post_direction: %s' % (
                self.seg_cnn,
                (self.base_street_name or '').strip(),
                (self.street_type or '').strip(),
                (self.category or '').strip(),
                (self.post_direction or '').strip(),
            )
        )


class AddressKeys(models.Model):
    address_key = models.AutoField(primary_key=True)
    address = models.ForeignKey(Addresses)
    tms = models.DateTimeField()
    class Meta:
        db_table = u'address_keys'


################################################
#
#                     VIEWS
#
################################################

# Base address data source class to support the data_source value of a base address.
class BaseAddressDataSource(models.GeoManager):
    def filter(self, address_base_id = None):
        data_sources = []
        if address_base_id != None:
            from django.db import connection
            cursor = connection.cursor()
            query = """SELECT
                            address_sources.source_system
                        FROM
                            address_sources
                        WHERE
                            address_sources.eas_table LIKE 'address_base'
                        AND
                            address_sources.eas_id = %s"""
            cursor.execute(query, [address_base_id])
            records = cursor.fetchall()
            for data_source in records:
                d = str(data_source[0])
                data_sources.append(d)
            return data_sources

# Address data source class to support the unit data_source value(s) of a base address.
class UnitAddressDataSource(models.GeoManager):
    def filter(self, address_base_id = None):
        data_sources = []
        if address_base_id != None:
            from django.db import connection
            cursor = connection.cursor()

            # Perform LEFT JOIN to get records with no entry in address_sources.
            # Inlcude only records with a unit value. 
            # Exclude historical records from the query.
            query = """SELECT
                            addresses.unit_num,
                            address_sources.source_system
                        FROM
                            addresses LEFT OUTER JOIN address_sources
                        ON
                            addresses.address_id = address_sources.eas_id
                        WHERE
                        (
                            address_sources.eas_table IS NULL
                            OR
                            address_sources.eas_table LIKE 'addresses'
                        )
                        AND
                            NOT (addresses.unit_num IS NULL)
                        AND
                            addresses.retire_tms IS NULL
                        AND
                            addresses.address_base_id = %s
                        ORDER BY
                            addresses.unit_num"""

            # query = """SELECT
            #                 addresses.unit_num,
            #                 address_sources.source_system
            #             FROM
            #                 addresses,
            #                 address_sources
            #             WHERE
            #                 address_sources.eas_table LIKE 'addresses'
            #             AND
            #                 address_sources.eas_id = addresses.address_id
            #             AND
            #                 addresses.address_base_id = %s
            #             ORDER BY
            #                 addresses.unit_num"""

            cursor.execute(query, [address_base_id])
            records = cursor.fetchall()
            for data_source in records:
                d = str(data_source[0]) + ',' + str(data_source[1])
                data_sources.append(DataSources(data_source_unit = str(data_source[0]), data_source_value = str(data_source[1])))
            return data_sources

# manager class to support the Aliases virtual table
class ManagerAliases(models.GeoManager):

    def filter(self, address_base_id = None):
        aliases = []

        if address_base_id != None:
            from django.db import connection
            cursor = connection.cursor()

            query = """SELECT distinct  base_address_num, full_street_name
                        FROM vw_address_search
                        WHERE address_base_id = %s AND category = 'ALIAS'"""
            cursor.execute(query, [address_base_id])

            records = cursor.fetchall()
            for alias in records:
                aliases.append(Aliases(alias = str(alias[0]) + ' ' + str(alias[1])))

        return aliases

    # for some reason this def doesn't work, though it should
    def get(self, address_base_id = None):
        aliases = self.filter(address_base_id = address_base_id)
        if len(aliases) == 1:
            return aliases[0]
        else:
            return Exception('expecting one record but got %s' % len(aliases))


# this is a virtual table
class DataSources(models.Model):
    data_source_unit = models.CharField(max_length=255)
    data_source_value = models.CharField(max_length=255)
    objects = UnitAddressDataSource()
    class Meta:
        app_label = '' # need this for the virtual table to work

# this is a virtual table
class Aliases(models.Model):
    alias = models.CharField(max_length=255)

    objects = ManagerAliases()
    class Meta:
        app_label = '' # need this for the virtual table to work



# Intended to be used as an object to search against all addresses
class VwAddressSearch(models.Model):
    address_id = models.IntegerField(primary_key=True)
    address_base_id = models.IntegerField()
    base_prefix = models.CharField(max_length=10)
    base_address_num = models.IntegerField()
    base_suffix = models.CharField(max_length=10)
    unit_num = models.CharField(max_length=20)
    full_street_name = models.CharField(max_length=255)
    category = models.CharField(max_length=10)
    zipcode = models.CharField(max_length=50)
    jurisdiction = models.CharField(max_length=50)

    geometry = models.PointField(srid=2227)
    objects = models.GeoManager() # required for spatial filtering

    class Meta:
        db_table = u'vw_address_search'
        unique_together = ("address_id", "address_base_id", "full_street_name")

    def parse(self, AddressString):
        if AddressString.isalpha():
           self.full_street_name = AddressString
        elif AddressString.isdigit():
           self.base_address_num = AddressString
        else:
           splitString = AddressString.split(' ')
           self.base_address_num = splitString[0]
           self.full_street_name = splitString[1]

        return self



EARTHRADIUS = 6378137
EARTHCIRCUM = EARTHRADIUS * 2.0 * math.pi
EARTHHALFCIRC = EARTHCIRCUM / 2
PIXELSPERTILE = 256
CLUSTERDISTANCE = 20
# Intended to provide a streamlined and meaningful rendering of address geometry
# Best to this view to render points on a map
class VwBaseAddresses(models.Model):
    address_base_id = models.IntegerField(primary_key=True)
    address = models.CharField(max_length=300)
    validation_warning_count = models.IntegerField()
    retire_tms = models.DateTimeField()
    service_address_flg = models.BooleanField()
    geometry = models.PointField(srid=900913)
    objects = models.GeoManager() # required for spatial filtering
    pixelX = -1
    pixelY = -1
    class Meta:
        db_table = u'vw_base_addresses'


    def getPixelX(self, zoom):
        if self.pixelX < 0:
            arc = EARTHCIRCUM / ((1 << zoom) * PIXELSPERTILE)
            self.pixelX = round((EARTHHALFCIRC + self.geometry.get_x()) / arc)

        return self.pixelX


    def getPixelY(self, zoom):
        if self.pixelY < 0:
            arc = EARTHCIRCUM / ((1 << zoom) * PIXELSPERTILE)
            self.pixelY = round((EARTHHALFCIRC - self.geometry.get_y()) / arc)

        return self.pixelY


    @staticmethod
    def querysetToClusterList(queryset, zoom):
        clusters = list(queryset)
        searchindex = 0
        try:
            for index in range(len(clusters)):
                finished = False
                searchindex = index + 1
                clusters[index].count = 1
                clusters[index].isClustered = False
                clusters[index].clusteredItems = []
                while not finished and searchindex < len(clusters):
                    # check to see if the clusters[index] item is within the clusterwidth 
                    # and clusterheight of the clusters[searchindex] item, if it is, 
                    # cluster those two points together
                    if math.fabs(clusters[searchindex].getPixelX(zoom) - clusters[index].getPixelX(zoom)) < CLUSTERDISTANCE: #within the same x range
                        if math.fabs(clusters[searchindex].getPixelY(zoom) - clusters[index].getPixelY(zoom)) < CLUSTERDISTANCE: #within the same y range = cluster needed
                            # cluster this clusters[index] with clusters[searchindex] and 
                            # remove the clusters[searchindex] item off the list
                            clusters[index].isClustered = True
                            clusters[index].count += 1
                            clusters[index].clusteredItems.append(clusters[searchindex])
                            clusters.pop(searchindex)

                            # need to decrement because we popped the "next" item in the sequence off the list
                            searchindex -= 1
                    else:
                        finished = True
                        index += 1

                    searchindex += 1

        except (IndexError, KeyError):
            pass
        finally:
            return clusters


# A simply way to access the address, assuming you know the blocklot value
class VwBlocklotXAddress(models.Model):
    blk_lot = models.CharField(max_length=9)
    date_map_drop = models.DateField()
    parcel_id = models.IntegerField()
    address_id = models.IntegerField()
    address_base_id = models.IntegerField()

    class Meta:
        db_table = u'vw_blocklot_x_address'
        unique_together = ("address_id", "parcel_id")


class VBaseAddressSearch(models.Model):
    address_base_id = models.IntegerField(primary_key=True)
    base_address_prefix = models.CharField(max_length=10)
    base_address_num = models.IntegerField()
    base_address_suffix = models.CharField(max_length=10)
    street_name = models.CharField(max_length=60)
    street_type = models.CharField(max_length=6)
    street_category = models.CharField(max_length=10)
    street_pre_direction = models.CharField(max_length=2)
    street_post_direction = models.CharField(max_length=2)
    street_segment_id = models.IntegerField()
    full_street_name = models.CharField(max_length=255)
    zipcode = models.CharField(max_length=50)
    jurisdiction = models.CharField(max_length=50)
    disposition = models.TextField()
    geometry = models.PointField(srid=900913)
    create_tms = models.DateTimeField()
    last_change_tms = models.DateTimeField()
    retire_tms = models.DateTimeField()

    objects = models.GeoManager()

    class Meta:
        db_table = u'v_base_address_search'

    def __unicode__(self):
        return 'address_base_id: %s, base_address_num: %s , full_street_name; %s' % (self.address_base_id, self.base_address_num , self.full_street_name)


class VwInvalidAddresses(models.Model):
    invalid_address_id = models.IntegerField(primary_key=True)
    message = models.CharField(max_length=256)
    address_base_id = models.IntegerField()
    base_address_num = models.IntegerField()
    base_address_suffix = models.CharField(max_length=10)
    geometry = models.PointField(srid=900913)
    full_street_name = models.CharField(max_length=255)
    unit_num = models.CharField(max_length=20)
    address_base_flg = models.BooleanField()
    invalid_type_desc = models.CharField(max_length=64)

    class Meta:
        db_table = u'vw_invalid_addresses'

    def __unicode__(self):
        return 'invalid_address_id: %s' % self.invalid_address_id


class DStreetType(models.Model):
    id = models.IntegerField(primary_key=True)
    abbreviated = models.CharField(unique=True, max_length=32)
    unabbreviated = models.CharField(unique=True, max_length=32)
    domain = models.CharField(max_length=4)
    class Meta:
        db_table = u'd_street_type'

    def __unicode__(self):
        return ('abbreviated: %s unabbreviated: %s domain: %s' % (self.abbreviated, self.unabbreviated, self.domain))


class DStreetTypeAliases(models.Model):
    id = models.IntegerField(primary_key=True)
    street_type = models.ForeignKey(DStreetType)
    alias = models.CharField(max_length=32)
    class Meta:
        db_table = u'd_street_type_aliases'


##### BEGIN change request models


class CrAddressBase(models.Model):
    cr_address_base_id = models.AutoField(primary_key=True)
    base_address_prefix = models.CharField(max_length=20, blank = True, null = True)
    base_address_num = models.IntegerField()
    base_address_suffix = models.CharField(max_length=20, blank = True, null = True)
    street_segment = models.ForeignKey(StreetSegments)
    zone = models.ForeignKey(Zones)
    address_base_id = models.IntegerField()
    geometry = models.PointField(srid=2227) # This field type is a guess.

    objects = models.GeoManager()

    class Meta:
        db_table = u'cr_address_base'

    def repr(self):
        return (
            '%s %s %s %s %s' % (  self.base_address_prefix if self.base_address_prefix else '',
                                    self.base_address_num,
                                    self.base_address_suffix if self.base_address_suffix else '',
                                    self.street_segment.repr(),
                                    self.zone.jurisdiction )
        )

    def getChangeRequest(self):
        return CrAddresses.objects.filter(cr_address_base = self)[0].change_request

    def __unicode__(self):
        return (
            '\nCrAddressBase\n\tcr_address_base_id: %s\n\tbase_address_num: %s\n\tzone: %s\n\tstreet_segment: %s\n\tgeometry: %s\n' %
            (   self.cr_address_base_id,
                self.base_address_num,
                self.zone,
                self.street_segment,
                self.geometry
            )
        )

    def findDuplicatesInQueue(self):
        # The "queue" means the unapproved change requests.
        # Duplicate: has the same prefix, number, suffix, street name, and jurisdiction.

        # queryset for detecting duplicates (streetnumber, streetname, jurisdiction)
        # first we get the streetname
        streetNames = Streetnames.objects.filter(street_segment = self.street_segment).filter(category = 'MAP')
        assert streetNames.count() == 1, 'findDuplicatesInQueue expected 1 streetname but got %s' % streetNames.count()
        streetName = streetNames[0]
        # next, we get all the segments with that same name
        streetSegments = Streetnames.objects.filter(full_street_name = streetName.full_street_name).values('street_segment')
        # then we get all change request addresses with the a matching address number, prefix, and suffix...
        duplicateCrBaseAddresses = CrAddressBase.objects.filter(base_address_num = self.base_address_num)
        duplicateCrBaseAddresses = CrAddressBase.filterForPrefix(duplicateCrBaseAddresses, self.base_address_prefix)
        duplicateCrBaseAddresses = CrAddressBase.filterForSuffix(duplicateCrBaseAddresses, self.base_address_suffix)
        # we now filter for just the base addresses with the street segments in our list
        duplicateCrBaseAddresses = duplicateCrBaseAddresses.filter(street_segment__in = streetSegments)

        # Exclude "UNKNOWN" addresses if they do not have the same base_address_id as self.
        # "UNKNOWN" addresses are special, deprecated addresses and allow legacy data into the system.
        if self.address_base_id is not None and self.street_segment.street_segment_id == StreetSegments.getUnknownSegmentId():
            # NOTE: The "~" means NOT.
            duplicateCrBaseAddresses = duplicateCrBaseAddresses.exclude(Q(street_segment__street_segment_id = StreetSegments.getUnknownSegmentId()) &
                                                                Q(address_base_id__isnull = False) &
                                                                ~Q(address_base_id = self.address_base_id)
            )

        # filter for the jurisdiction
        zones = Zones.objects.filter(jurisdiction = self.zone.jurisdiction)
        duplicateCrBaseAddresses = duplicateCrBaseAddresses.filter(zone__in = zones)

        if duplicateCrBaseAddresses.count() == 0:
            return duplicateCrBaseAddresses

        # exclude approved
        approvedStatus = DChangeRequestStatus.objects.get(status_id=2)
        duplicateCrAddresses = CrAddresses.objects.filter(cr_address_base__in = duplicateCrBaseAddresses)
        duplicateCrAddresses = duplicateCrAddresses.exclude(change_request__review_status = approvedStatus)

        # exclude self
        duplicateCrAddresses = duplicateCrAddresses.exclude(cr_address_base__cr_address_base_id = self.cr_address_base_id)

        # make it a set (of unique)
        duplicateCrAddresses = duplicateCrAddresses.filter(address_base_flg = True)

        duplicateBaseAddressIds = []
        for duplicateBaseAddress in duplicateCrAddresses:
            duplicateBaseAddressIds.append(duplicateBaseAddress.cr_address_base.pk)

        duplicateCrBaseAddresses = CrAddressBase.objects.filter(pk__in=duplicateBaseAddressIds)

        return duplicateCrBaseAddresses


    def findDuplicateInActive(self, crAddressBases, crAddresses):
        if self.address_base_id:
            # This means that we have an address that "already exists", meaning that it has been previously committed to the business tables.
            # Therefore, we already know that it is unique; there is no active equivalent.
            return None
        else:
            # This means we have a brand new address, never before committed to the buisness tables.
            # An active equivalent has the same (number, street name, jurisdiction) and is not retired.

            # first we get the streetname...
            streetName = Streetnames.objects.filter(street_segment = self.street_segment).get(category = 'MAP')
            # next, we get all the segments with that same name
            streetSegments = Streetnames.objects.filter(full_street_name = streetName.full_street_name).values('street_segment')
            # then we get all base addresses with a matching prefix, number, and suffix
            candidateDuplicateBaseAddresses = AddressBase.objects.filter(base_address_num = self.base_address_num)
            candidateDuplicateBaseAddresses = AddressBase.filterForPrefix(candidateDuplicateBaseAddresses, self.base_address_prefix)
            candidateDuplicateBaseAddresses = AddressBase.filterForSuffix(candidateDuplicateBaseAddresses, self.base_address_suffix)
            # closer...we now filter for just the base addresses with the street segments in our list
            candidateDuplicateBaseAddresses = candidateDuplicateBaseAddresses.filter(street_segment__in = streetSegments)

            # Exclude "UNKNOWN" addresses if they do not have the same base_address_id as self.
            # "UNKNOWN" addresses are special, deprecated addresses and allow legacy data into the system.
            if self.address_base_id is not None and self.street_segment.street_segment_id == StreetSegments.getUnknownSegmentId():
                # NOTE: The "~" means NOT.
                candidateDuplicateBaseAddresses = candidateDuplicateBaseAddresses.exclude(Q(street_segment__street_segment_id = StreetSegments.getUnknownSegmentId()) &
                                                                    Q(address_base_id__isnull = False) &
                                                                    ~Q(address_base_id = self.address_base_id)
                )

            # almost there...we filter for the jurisdiction
            zones = Zones.objects.filter(jurisdiction = self.zone.jurisdiction)
            candidateDuplicateBaseAddresses = candidateDuplicateBaseAddresses.filter(zone__in = zones)
            # and finally - exclude retired - whew!
            candidateDuplicateBaseAddresses = candidateDuplicateBaseAddresses.filter(retire_tms__isnull = True)

            assert candidateDuplicateBaseAddresses.count() in (0,1), 'findDuplicateInActive expected 0 or 1 but found %s' % candidateDuplicateBaseAddresses.count()

            # We should have 1 or none.
            if candidateDuplicateBaseAddresses.count() == 0:
                return None
            else:
                # If we find a duplicate base address we need to get the crAddress for that base address and
                # investigate the retire_flg property.  If the duplicate is a retire then we return none
                # because a user should be allowed to add and retire the same address in a single CR
                duplicateCrAddressBases = crAddressBases.filter(address_base_id = candidateDuplicateBaseAddresses[0].address_base_id)
                if duplicateCrAddressBases.count() == 1:
                    crAddress = crAddresses.get(cr_address_base = duplicateCrAddressBases[0])
                    if crAddress.retire_flg:
                        return None
                    else:
                        return candidateDuplicateBaseAddresses[0]
                else:
                    return candidateDuplicateBaseAddresses[0]

    def validateDuplicateInActive(self, crAddressBases, crAddresses):
        duplicateActiveBaseAddress = self.findDuplicateInActive(crAddressBases, crAddresses)
        if not duplicateActiveBaseAddress:
            validation = Validation(valid=True, message='')
        else:
            message = 'This change request contains a base address "%s" that duplicates an existing address.  To proceed, ensure that the base address has a unique combination of number, street, and jurisdiction.' % self.repr().strip()
            validation = Validation(valid=False, message=message, object=duplicateActiveBaseAddress)

        return validation

    def validateDuplicatesInQueue(self):
        validation = Validation(valid=True, message='', object=None)
        crBaseAddressDuplicates = self.findDuplicatesInQueue()
        crAddressSelf = CrAddresses.objects.filter(cr_address_base = self).get(address_base_flg = True)
        for crBaseAddressOther in crBaseAddressDuplicates:
            crAddressOther = CrAddresses.objects.filter(cr_address_base = crBaseAddressOther).get(address_base_flg = True)
            # test for retire - replace
            if (
                (crAddressSelf.change_request.change_request_id == crAddressOther.change_request.change_request_id)
                and
                (crAddressSelf.retire_flg != crAddressOther.retire_flg)
                and
                (not crAddressSelf.address_id or not crAddressOther.address_id)
               ):
                # this means we have a retire - replace
                pass
            else:
                message = 'This change request contains a base address "%s" that duplicates an address in another change request titled "%s" which is owned by %s (%s). To proceed, please ensure that this base address is referenced in only one change request.' % (self.repr(), crAddressOther.change_request.name, crAddressOther.change_request.requestor_user.get_full_name(), crAddressOther.change_request.requestor_user.email )
                validation = Validation(valid=False, message=message, object=crBaseAddressOther, changeRequest=crAddressOther.change_request )

        return validation

    def validateUnitNumbers(self):
        # Replicates the client-side validation to make sure all unit numbers follow the correct form.
        # Apply to new addresses only.
        validation = Validation(valid=True, message='', object=None)

        unitNumbers = [ crAddress.unit_num for crAddress in CrAddresses.objects.filter(cr_address_base = self).filter(unit_num__isnull = False).filter(address_id__isnull = True) ]

        for unitNumber in unitNumbers:
            if (len(unitNumber) > 10) or (not re.search('^[a-zA-Z0-9\-\/\s]+$', unitNumber)):
                message = 'This change request contains an invalid unit number, "%s". Unit numbers must contain only letters, digits, dashes, slashes or white space, and cannot exceed 10 characters. To proceed, correct the unit number.' % (unitNumber)
                validation = Validation(valid=False, message=message, object=self)
                break                

        return validation

    def validateDuplicateUnits(self):
        # no duplicate unit_nums allowed
        validation = Validation(valid=True, message='', object=None)
        crAddresses = CrAddresses.objects.filter(cr_address_base = self).filter(address_base_flg = False).exclude(retire_flg = True)
        unitNumHistogram = dict([ (crAddress.unit_num, 0) for crAddress in crAddresses ])
        for crAddress in crAddresses:
            unitNumHistogram[crAddress.unit_num] += 1
            if unitNumHistogram[crAddress.unit_num] > 1:
                message = 'This change request contains a base address "%s" that contains a duplicate unit address "%s".  To proceed, ensure that the base address has a unique unit addresses.' % (self.repr().strip(), crAddress.unit_num)
                validation = Validation(valid=False, message=message, object=self)
                break

        return validation

    def validateAddressNumber(self):
        # here again we see the need for a more comprehensive validation solution

        if self.base_address_num is None:
            raise ValidationWarning("address number must be specified")

        try:
            self.base_address_num = int(self.base_address_num)
        except ValueError, e:
            raise ValidationWarning("address number must be an integer")

        if self.base_address_num < 0 or self.base_address_num > 999999:
            raise ValidationWarning("address number must between 0 and 999999 (inclusive)")

    def validateStreetSegment(self):
        # This has changed as of http://code.google.com/p/eas/issues/detail?id=556
        # The UI will:
        #   not offer retired streets for selection for a new address
        #   warn a user if they are editing an existing address that is using a retired street
        # Users try to retire addresses with retired street segments,
        # but because of certain business processes we allow the editing and continued use of such addresses.
        # We should consider an checking if the street is no longer on the "10 nearest streets" list.
        # This could happen in the case where a user stays logged into the web application overnight, the ETL moves a street, and they then save the change request.
        pass

    def validate(self):
        self.validateAddressNumber()
        self.validateStreetSegment()


    @staticmethod
    def prepareFromCrJsonBaseAddress(jsonBaseAddress):
        from django.contrib.gis.measure import Distance
        logger.info('prepareFromCrJsonBaseAddress')
        crAddressBase = CrAddressBase()
        if jsonBaseAddress.base_address_prefix:
            crAddressBase.base_address_prefix = jsonBaseAddress.base_address_prefix
        crAddressBase.base_address_num = jsonBaseAddress.base_address_num
        if jsonBaseAddress.base_address_suffix:
            crAddressBase.base_address_suffix = jsonBaseAddress.base_address_suffix
        if jsonBaseAddress.street_segment is not None:
            crAddressBase.street_segment = StreetSegments.objects.get(street_segment_id = jsonBaseAddress.street_segment)
        else:
            # todo - move to explicit validate method
            raise ValidationWarning("One of your base addresses has an unspecified street. To proceed please specify the street.")
        crAddressBase.address_base_id = jsonBaseAddress.address_base_id
        crAddressBase.geometry = jsonBaseAddress.geometry_proposed
        if jsonBaseAddress.zone_id == '' or jsonBaseAddress.zone_id == None:
            # http://code.google.com/p/eas/issues/detail?id=281
            # Start with a fast query - this will work 9999/10000 times.
            queryset = Zones.objects.filter(geometry__intersects=crAddressBase.geometry)
            if queryset.count() < 1:
                # When there is no coincident Zone polygon - we grab the nearest.
                # This query is a bit slower but will still work even if the user tries a very silly location or if the zones layer needs to be updated.
                queryset = Zones.objects.all()
                queryset = queryset.extra(select={"distance" : "st_distance(zones.geometry, GeomFromText('%s', %s))" % (crAddressBase.geometry.wkt, crAddressBase.geometry.srs.srid)}, order_by=["distance"])

            zone = queryset[0]
            crAddressBase.zone = zone
            jsonBaseAddress.zone_id = crAddressBase.zone.zone_id
        else:
            crAddressBase.zone = Zones.objects.get(pk=jsonBaseAddress.zone_id)

        logger.info('prepareFromCrJsonBaseAddress complete')
        return crAddressBase


    @staticmethod
    def filterForPrefix(queryset, prefix):
        if prefix:
            return queryset.filter(base_address_prefix = prefix)
        else:
            return queryset.exclude(base_address_prefix__isnull = False)


    @staticmethod
    def filterForSuffix(queryset, suffix):
        if suffix:
            return queryset.filter(base_address_suffix = suffix)
        else:
            return queryset.exclude(base_address_suffix__isnull = False)


class CrAddresses(models.Model):
    cr_address_id = models.AutoField(primary_key=True)
    cr_address_base = models.ForeignKey(CrAddressBase)
    unit_type = models.ForeignKey(DUnitType, db_column='unit_type', default = 0)
    floor = models.ForeignKey(DFloors, blank = True, null = True, default = 105)
    unit_num = models.CharField(max_length=10, blank = True, null = True)
    change_request = models.ForeignKey(CrChangeRequests)
    address_id = models.IntegerField(blank = True, null = True)
    mailable_flg = models.BooleanField()
    disposition_code = models.ForeignKey(DAddressDisposition, db_column='disposition_code', to_field='disposition_code')
    retire_flg = models.BooleanField()
    address_base_flg = models.BooleanField()
    class Meta:
        db_table = u'cr_addresses'

    def repr(self):
        return (
            '%s unit: %s retire_flg: %s' % (  self.cr_address_base.repr(), self.unit_num if self.unit_num else '', self.retire_flg)
        )

    def __unicode__(self):
        return (
            '\nCrAddress\n\tcr_address_id: %s\n\tcr_address_base.cr_address_base_id: %s\n\tunit_type: %s\n\tfloor: %s\n\tunit_num: %s\n\tchange_request: %s\n' %
            (
                self.cr_address_id,
                self.cr_address_base.cr_address_base_id,
                self.unit_type,
                self.floor,
                self.unit_num,
                self.change_request.change_request_id
            )
        )

    def validate(self):
        # todo - a lot more structure around validation please (see native django approaches)

        if self.address_base_flg:
            return

        if self.unit_num is None:
            message = 'This change request contains a unit address "%s" that has an unspecified unit number.  To proceed please ensure that the unit number is specified.' % self.cr_address_base.repr()
            raise ValidationWarning(message)

        if len(self.unit_num) > 10:
            message = 'This change request contains a base address "%s" with a unit address "%s" with a unit number that is too long.  To proceed please ensure that the unit number 10 characters or less' % (self.cr_address_base.repr(), self.unit_num)
            raise ValidationWarning(message)


    @staticmethod
    def validateForRetiredParcelLinks(unretiredCrAddresses=None):
        logger.info('validateForRetiredParcelLinks')
        validations = []

        # Use select_related carefully so we do not join address_x_parcel because it might be null.
        # Allow user to unlink from retired parcels; do not allow user to link to retired parcels.
        crAddressXParcels = CrAddressXParcels.objects.select_related('parcel', 'cr_address').filter(cr_address__in=unretiredCrAddresses).exclude(parcel__date_map_drop__isnull = True).exclude(unlink=True)
        for crAddressXParcel in crAddressXParcels:
            message = 'This change request contains an address "%s" that is associated with a parcel "%s" that has been retired.  To proceed please ensure that the retired parcel is not being used.' % (crAddressXParcel.cr_address.cr_address_base.repr(), crAddressXParcel.parcel.blk_lot)
            validation = Validation(valid=False, message=message)
            validations.append(validation)

        # Force user to unlink from retired.  In other words, do not allow a save if address has an existing link to a retired parcel.
        addressIds = [unretiredCrAddress.address_id for unretiredCrAddress in unretiredCrAddresses if unretiredCrAddress.address_id]
        unlinkingCrAddressXParcels = CrAddressXParcels.objects.select_related('parcel', 'cr_address').filter(cr_address__in=unretiredCrAddresses).exclude(parcel__date_map_drop__isnull = True).filter(unlink=True)
        unlinkingAddressIds = [unlinkingCrAddressXParcel.cr_address.address_id for unlinkingCrAddressXParcel in unlinkingCrAddressXParcels if unlinkingCrAddressXParcel.cr_address.address_id]
        addressXParcels = AddressXParcels.unretiredObjects.filter(address__address_id__in=addressIds).exclude(parcel__date_map_drop__isnull = True).exclude(address__address_id__in=unlinkingAddressIds)
        for addressXParcel in addressXParcels:
            message = 'This change request contains an address "%s" that is associated with a parcel "%s" that has been retired.  To proceed please ensure that the retired parcel is not being used.' % (addressXParcel.address.address_base.reprForUser(), addressXParcel.parcel.blk_lot)
            validation = Validation(valid=False, message=message)
            validations.append(validation)

        return validations


class CrAddressXParcels(models.Model):
    id = models.AutoField(primary_key=True)
    cr_address = models.ForeignKey(CrAddresses, null=False)
    parcel = models.ForeignKey(Parcels, null=False)
    address_x_parcel = models.ForeignKey(AddressXParcels, null=True)
    link = models.BooleanField()
    unlink = models.BooleanField()

    class Meta:
        db_table = u'cr_address_x_parcels'

    def __unicode__(self):
        return (
            '\nCrAddressXParcels\n\tid: %s\n\tcr_address.cr_address_d: %s\n\tparcel.parcel_id: %s\n\taddress_x_parcel: %s\n\tlink: %s\n\tunlink: %s\n' %
            (self.id, self.cr_address.cr_address_id, self.parcel.parcel_id, self.address_x_parcel, self.link, self.unlink, )
        )

    def asDictForJson(self):
        d = {}
        d['apn'] = self.parcel.blk_lot
        d['retire_tms'] = self.parcel.date_map_drop
        d['address_x_parcel_id'] = self.address_x_parcel.id if self.address_x_parcel else None
        d['link'] = self.link
        d['unlink'] = self.unlink
        return d


    @staticmethod
    def getParcelLinkChanges(baseCrAddress = None):

        # Some optimization for speed (defer geometry).
        # It is sad to have to work so hard around the ORM.

        logger.info('getParcelLinkChanges begin')

        craxps = CrAddressXParcels.objects.filter(cr_address = baseCrAddress).order_by('id')
        parcelIds = craxps.values_list('parcel__parcel_id', flat=True)
        parcels = Parcels.objects.filter(parcel_id__in = parcelIds).defer('geometry')

        # we need to set the order of parcels to match the order of parcelIds
        parcelDict = {}
        for parcel in parcels:
            parcelDict[parcel.parcel_id] = parcel

        orderedParcels = []
        for parcelId in parcelIds:
            orderedParcels.append(parcelDict[parcelId])

        assert len(orderedParcels) == craxps.count(), 'parcel count does not match crAddressXParcels count'

        parcelLinkChanges = []
        for tuple in zip(craxps, orderedParcels):
            craxp = tuple[0]
            parcel = tuple[1]
            parcelLinkChanges.append({
                'apn': parcel.blk_lot,
                'retire_tms': parcel.date_map_drop,
                'address_x_parcel_id': (craxp.address_x_parcel.id if craxp.address_x_parcel else None),
                'link': craxp.link,
                'unlink': craxp.unlink
            })

        #logger.info(parcelLinkChanges)
        logger.info('getParcelLinkChanges end')
        return parcelLinkChanges


    @staticmethod
    def prepareParcelLinkChangesFromJson(crAddress, jsonParcelLinkChanges, parcelDict):
        #logger.info('prepareParcelLinkChangesFromJson')
        crAddressesXParcels = []
        for parcelLinkChange in jsonParcelLinkChanges:
            parcel = parcelDict[parcelLinkChange['apn']]
            addressXParcelId = parcelLinkChange['address_x_parcel_id']
            if addressXParcelId == 0:
                addressXParcelId = None
            addressXParcel = AddressXParcels(id = addressXParcelId)
            crAddressXParcel = CrAddressXParcels(cr_address = crAddress, parcel = parcel, address_x_parcel = addressXParcel, link=parcelLinkChange['link'], unlink=parcelLinkChange['unlink'])
            crAddressesXParcels.append(crAddressXParcel)
        return crAddressesXParcels


class CrJsonModel(object):

    def __init__(self, arg = None):

        logger.info('CrJsonModel init...')

        self.base_addresses = []
        self.change_request_id = None
        self.concurrency_id = None
        self.create_tms = None
        self.name = ''
        self.requestor_comment = ''
        self.requestor_id = None
        self.requestor_last_update = None
        self.requestor_name = ''
        self.review_status_id = None
        self.review_status_description = ''
        self.reviewer_comment = ''
        self.reviewer_id = None
        self.reviewer_last_update = None
        self.reviewer_name = ''

        if arg != None:
            if isinstance(arg, int):
                self.getFromDb(arg)
            else:# isinstance(arg, str):
                self.parseFromString(arg)

        logger.info('CrJsonModel init complete')

    class BaseAddress(object):
        def __init__(self):
            self.address_base_flg = True
            self.address_base_id = None
            self.address_id = None
            self.base_address_num = None
            self.base_address_prefix = ''
            self.base_address_suffix = ''
            self.floor = None
            self.geometry = ''
            self.geometry_proposed = ''
            self.isNew = True
            self.retire_flg = False
            self.street_segment = None
            self.unit_addresses = []
            self.unit_disposition = None
            self.unit_disposition_proposed = 1 # default to official
            self.unit_mailable_flg = None
            self.unit_mailable_flg_proposed = False
            self.unit_num = None
            self.unit_type = 0
            self.zone_id = None
            self.parcel_geometry = ''
            self.linked_parcels = []
            self.parcel_link_changes = []
            self.parcel_pick_list = []
            self.available_streets = []

        def __repr__(self):
            return (
                'address_base_id: %s, address_id: %s' % (self.address_base_id, self.address_id)
            )

        # DRY up field access with these setters.
        # todo - enforce this?

        def setParcelPickList(self, parcelPickList):
            self.parcel_pick_list = parcelPickList

        def setParcelGeometry(self, parcelGeometry):
            self.parcel_geometry = parcelGeometry

        def setLinkedParcels(self, linkedParcels):
            self.linked_parcels = linkedParcels

        def setUnitAddresses(self, unitAddresses):
            self.unit_addresses = unitAddresses

        def setAvailableStreets(self, availableStreets):
            self.available_streets = availableStreets

        def proposeNew(self):
            modelUtils = ModelUtils()
            if self.geometry != '':
                self.base_address_num = ""
                self.isNew = True

                # get the related parcels
                self.setParcelPickList(modelUtils.buildParcelPickList(self.geometry))

                # get the nearby streets
                self.setAvailableStreets(StreetSegments.getNearestTo(geometry=self.geometry))

                # empty sets for client model
                self.setLinkedParcels([])
                self.setUnitAddresses([])

            return self


    class UnitAddress(object):

        def __init__(self):
            self.address_base_flg = False
            self.address_id = None
            self.disposition = None
            self.disposition_proposed = None
            self.floor = None
            self.isNew = True
            self.mailable_flg = False
            self.mailable_flg_proposed = False
            self.retire_flg = False
            self.unit_num = None
            self.unit_type = 0
            self.linked_parcels = []
            self.parcel_link_changes = []
            self.parcel_pick_list = []

        def repr(self):
            return ('address_id: %s, unit_num: %s' % (self.address_id, self.unit_num))

        @staticmethod
        def addParcelLinks(jsonUnitAddresses):
            logger.info('addParcelLinks')

            # mandatory order by address_id
            from operator import attrgetter
            jsonUnitAddresses.sort(key=attrgetter('address_id'))
            addressIds = [jsonUnitAddress.address_id for jsonUnitAddress in jsonUnitAddresses if jsonUnitAddress.address_id]
            addressXParcels = AddressXParcels.unretiredObjects.filter(address__address_id__in = addressIds).order_by('address__address_id', 'parcel__blk_lot')
            modelUtils = ModelUtils()

            class OneToManyMergeImpl:
                def parentGetKey(self, parent):
                    return parent.address_id
                def parentInitNode(self, parent):
                    pass
                def parentAddChild(self, parent, parcelLink):
                    parent.linked_parcels.append(parcelLink)
                def childGetKey(self, child):
                    return child.address.address_id
                def childGetValue(self, child):
                    return child.getShortDict()
                def parentSummarizeGroup(self):
                    # here is where we would put something like a group count, average, etc
                    pass

            jsonUnitAddresses = modelUtils.mergeOneToMany(
                parents=jsonUnitAddresses,
                children = addressXParcels,
                mergeImpl=OneToManyMergeImpl()
            )

            return None

        @staticmethod
        def addOriginalAddressInfo(jsonUnitAddresses):
            logger.info('addOriginalAddressInfo')

            # mandatory order by address_id
            from operator import attrgetter
            jsonUnitAddresses.sort(key=attrgetter('address_id'))
            addressIds = [jsonUnitAddress.address_id for jsonUnitAddress in jsonUnitAddresses if jsonUnitAddress.address_id]
            addresses = Addresses.objects.filter(address_id__in = addressIds).order_by('address_id')
            modelUtils = ModelUtils()

            class OneToOneMergeImpl:
                def parentGetKey(self, parent):
                    return parent.address_id
                def parentSetValue(self, parent, originalAddress):
                    if originalAddress:
                        parent.disposition = originalAddress.disposition_code_id
                        parent.mailable_flg = originalAddress.mailable_flg
                def childGetKey(self, child):
                    return child.address_id
                def childGetValue(self, child):
                    return child

            crUnitAddressesDictReturn = modelUtils.mergeOneToOne(
                parents=jsonUnitAddresses,
                children = addresses,
                mergeImpl=OneToOneMergeImpl()
            )

            return None

        @staticmethod
        def addParcelLinkChanges(jsonUnitAddresses):
            logger.info('addParcelLinkChanges')

            # mandatory order by cr_address_id
            from operator import attrgetter
            jsonUnitAddresses.sort(key=attrgetter('cr_address_id'))
            crAddressIds = [jsonUnitAddress.cr_address_id for jsonUnitAddress in jsonUnitAddresses if jsonUnitAddress.cr_address_id]
            parcelLinkChanges = CrAddressXParcels.objects.select_related(depth=1).filter(cr_address__cr_address_id__in = crAddressIds).order_by('cr_address__cr_address_id', 'parcel__blk_lot')
            modelUtils = ModelUtils()

            class OneToManyMergeImpl:
                def parentGetKey(self, parent):
                    return parent.cr_address_id
                def parentInitNode(self, parent):
                    pass
                def parentAddChild(self, parent, parcelLinkChange):
                    parent.parcel_link_changes.append(parcelLinkChange)
                def childGetKey(self, child):
                    return child.cr_address.cr_address_id
                def childGetValue(self, child):
                    return child.asDictForJson()
                def parentSummarizeGroup(self):
                    # here is where we would put something like a group count, average, etc
                    pass

            crUnitAddressesDictReturn = modelUtils.mergeOneToMany(
                parents=jsonUnitAddresses,
                children = parcelLinkChanges,
                mergeImpl=OneToManyMergeImpl()
            )

            return None


    def populateCrAddressFromBaseAddress(self, crAddress, changeRequest, baseAddress, jsonBaseAddress, cacheManager):
        crAddress.cr_address_base = baseAddress
        crAddress.change_request = changeRequest
        crAddress.mailable_flg = jsonBaseAddress.unit_mailable_flg_proposed
        crAddress.disposition_code = cacheManager.get('DAddressDisposition', jsonBaseAddress.unit_disposition_proposed)
        crAddress.retire_flg = jsonBaseAddress.retire_flg
        crAddress.address_base_flg = True
        if jsonBaseAddress.unit_type: crAddress.unit_type = cacheManager.get('DUnitType', jsonBaseAddress.unit_type)
        if jsonBaseAddress.floor: crAddress.floor = cacheManager.get('DFloors', jsonBaseAddress.floor)
        if jsonBaseAddress.unit_num: crAddress.unit_num = jsonBaseAddress.unit_num
        if jsonBaseAddress.address_id: crAddress.address_id = jsonBaseAddress.address_id

    def populateCrAddressFromUnitAddress(self, crAddress, changeRequest, baseAddress, jsonUnitAddress, cacheManager):

        crAddress.cr_address_base = baseAddress
        crAddress.change_request = changeRequest

        crAddress.mailable_flg = jsonUnitAddress.mailable_flg_proposed
        crAddress.disposition_code = cacheManager.get('DAddressDisposition', jsonUnitAddress.disposition_proposed)
        crAddress.retire_flg = jsonUnitAddress.retire_flg
        crAddress.address_base_flg = False
        if jsonUnitAddress.unit_type: crAddress.unit_type = cacheManager.get('DUnitType', jsonUnitAddress.unit_type)
        if jsonUnitAddress.floor: crAddress.floor = cacheManager.get('DFloors', jsonUnitAddress.floor)
        if jsonUnitAddress.unit_num: crAddress.unit_num = jsonUnitAddress.unit_num
        if jsonUnitAddress.address_id: crAddress.address_id = jsonUnitAddress.address_id

    # Returns a parcel dictionary based on the base addresses in the instance - an optimization.
    def createParcelDictFromBaseAddressList(self):
        logger.info('caching parcel information')
        apns = []
        ###
        for baseAdd in self.base_addresses:
            for parcelLinkChange in baseAdd.parcel_link_changes:
                apns.append(parcelLinkChange['apn'])
            for unitAddress in baseAdd.unit_addresses:
                for parcelLinkChange in unitAddress.parcel_link_changes:
                    apns.append(parcelLinkChange['apn'])

        # Do not exclude retired - we validate for that independently.
        # defer geometry - http://code.google.com/p/eas/issues/detail?id=459
        parcels = Parcels.objects.filter(blk_lot__in = apns).defer('geometry')
        parcelDict = {}
        for parcel in parcels:
            parcelDict[parcel.blk_lot] = parcel

        return parcelDict


    def getFromDb(self, crId):

        # todo - rewrite this hideous monstrosity
        # We have tweaked this quite a bit for performance.  Use of the ORM naively will result in hundreds to thousands of db calls in some cases.
        # This code is somewhat subtle - take extra care in modifying.

        try:

            logger.info('CrJsonModel.getFromDb')

            modelUtils = ModelUtils()
            cacheManager = CacheManager()

            cr = CrChangeRequests.objects.select_related().get(pk = crId)
            if cr.reviewer_user:
                self.reviewer_id = cr.reviewer_user.pk
                self.reviewer_name = cr.reviewer_user.first_name + ' ' + cr.reviewer_user.last_name
            if cr.requestor_user:
                self.requestor_id = cr.requestor_user.pk
                self.requestor_name = cr.requestor_user.first_name + ' ' + cr.requestor_user.last_name
            self.change_request_id = cr.change_request_id
            self.name = cr.name
            self.requestor_comment = cr.requestor_comment
            self.requestor_last_update = cr.requestor_last_update
            self.review_status_id = cr.review_status.status_id
            self.status_description = cr.review_status.status_description
            self.reviewer_comment = cr.reviewer_comment
            self.reviewer_last_update = cr.reviewer_last_update
            self.concurrency_id = cr.concurrency_id

            baseCrAddresses = CrAddresses.objects.filter(change_request = cr).filter(address_base_flg = True)
            for baseCrAddress in baseCrAddresses:
                logger.info('prepare cr base address')

                baseAddress = baseCrAddress.cr_address_base
                jsonBaseAdd = self.BaseAddress()
                jsonBaseAdd.address_base_flg = True
                jsonBaseAdd.address_base_id = baseCrAddress.cr_address_base.address_base_id
                jsonBaseAdd.address_id = baseCrAddress.address_id

                # First we get the "parcel link changes"
                crAddresses = CrAddresses.objects.filter(cr_address_base = baseAddress)
                crAddressIds = crAddresses.values_list('cr_address_id', flat=True)
                # optimize for large geometries
                parcelIds = CrAddressXParcels.objects.filter(cr_address__cr_address_id__in=crAddressIds).values_list('parcel__parcel_id', flat=True)
                selectedParcels = list(Parcels.objects.filter(pk__in = parcelIds).defer('geometry'))
                #
                # Second we get the "parcel links"
                addressIds = [crAddress.address_id for crAddress in crAddresses if crAddress.address_id]
                # optimize for large geometries
                parcelIds = AddressXParcels.objects.filter(address__address_id__in = addressIds).filter(retire_tms__isnull = True).values_list('parcel__parcel_id', flat=True)
                selectedParcels.extend(list(Parcels.objects.filter(pk__in = parcelIds).defer('geometry')))
                #
                parcelPickList = modelUtils.buildParcelPickList(pointGeometry = baseAddress.geometry, includeParcels = selectedParcels)
                jsonBaseAdd.setParcelPickList(parcelPickList)

                availableStreets = StreetSegments.getNearestTo(geometry=baseAddress.geometry, includeSegment=baseAddress.street_segment)
                baseAddress.available_streets = availableStreets
                jsonBaseAdd.available_streets = availableStreets
                jsonBaseAdd.base_address_num = baseAddress.base_address_num
                jsonBaseAdd.base_address_prefix = baseAddress.base_address_prefix
                jsonBaseAdd.base_address_suffix = baseAddress.base_address_suffix
                jsonBaseAdd.floor = baseCrAddress.floor.floor_id
                baseAddress.geometry.transform(900913)
                jsonBaseAdd.geometry_proposed = baseAddress.geometry
                jsonBaseAdd.retire_flg = baseCrAddress.retire_flg

                # add the isNew flag: are we're editing an existing base address or not
                if jsonBaseAdd.address_id is None:
                    jsonBaseAdd.isNew = True
                    jsonBaseAdd.unit_mailable_flg = baseCrAddress.mailable_flg
                    jsonBaseAdd.geometry = baseAddress.geometry
                    jsonBaseAdd.unit_disposition = baseCrAddress.disposition_code.disposition_code
                # get the address info(only if editing an existing address) from the db and
                # then set the original values in the dictionary
                else:
                    jsonBaseAdd.isNew = False
                    original_address = Addresses.objects.get(address_id = jsonBaseAdd.address_id)
                    jsonBaseAdd.unit_mailable_flg = original_address.mailable_flg
                    geom = original_address.address_base.geometry.transform(900913, True)
                    jsonBaseAdd.geometry = geom
                    jsonBaseAdd.unit_disposition = original_address.disposition_code.disposition_code
                    jsonBaseAdd.setLinkedParcels(AddressXParcels.buildLinkedParcelList(jsonBaseAdd.address_id))

                jsonBaseAdd.parcel_link_changes = CrAddressXParcels.getParcelLinkChanges(baseCrAddress)

                jsonBaseAdd.street_segment = baseAddress.street_segment.street_segment_id
                jsonBaseAdd.unit_disposition_proposed = baseCrAddress.disposition_code.disposition_code
                jsonBaseAdd.unit_mailable_flg_proposed = baseCrAddress.mailable_flg
                jsonBaseAdd.unit_num = baseCrAddress.unit_num
                jsonBaseAdd.unit_type = baseCrAddress.unit_type.unit_type_id
                jsonBaseAdd.zone_id = baseAddress.zone.zone_id

                logger.info('prepare cr unit addresses')
                crUnitAddresses = CrAddresses.objects.filter(address_base_flg = False).filter(cr_address_base = baseCrAddress.cr_address_base.pk)
                crUnitAddresses = crUnitAddresses.values()

                logger.info('assemble cr unit addresses')
                jsonUnitAddresses = []
                for crUnitAddress in crUnitAddresses:

                    jsonUnitAdd = self.UnitAddress()
                    jsonUnitAdd.address_base_flg = False
                    jsonUnitAdd.address_id = crUnitAddress['address_id']
                    jsonUnitAdd.cr_address_base = crUnitAddress['cr_address_base_id']
                    jsonUnitAdd.cr_address_id = crUnitAddress['cr_address_id']
                    jsonUnitAdd.disposition_proposed = crUnitAddress['disposition_code_id']
                    jsonUnitAdd.mailable_flg_proposed = crUnitAddress['mailable_flg']
                    jsonUnitAdd.floor = crUnitAddress['floor_id']
                    jsonUnitAdd.retire_flg = crUnitAddress['retire_flg']

                    # add the isNew flag: are we're editing an existing unit address or not
                    if jsonUnitAdd.address_id is None:
                        jsonUnitAdd.isNew = True
                        jsonUnitAdd.disposition = crUnitAddress['disposition_code_id']
                        jsonUnitAdd.mailable_flg = crUnitAddress['mailable_flg']
                    # get the address info(only if editing an existing address) from the db and
                    # then set the original values in the dictionary
                    else:
                        jsonUnitAdd.isNew = False

                    jsonUnitAdd.unit_num = crUnitAddress['unit_num']
                    jsonUnitAdd.unit_type = crUnitAddress['unit_type_id']

                    jsonUnitAddresses.append(jsonUnitAdd)

                # Here we realize perhaps the largest performance gain by following the "relational graph" rather than the "object graph".
                self.UnitAddress.addParcelLinks(jsonUnitAddresses)
                self.UnitAddress.addOriginalAddressInfo(jsonUnitAddresses)
                self.UnitAddress.addParcelLinkChanges(jsonUnitAddresses)

                # sort on something the end user will appreciate
                from operator import attrgetter
                jsonUnitAddresses.sort(key=attrgetter('unit_num'))

                jsonBaseAdd.unit_addresses = jsonUnitAddresses

                self.base_addresses.append(jsonBaseAdd)

        except Exception, e:
            logger.critical(e.message)
            raise

        logger.info('CrJsonModel.getFromDb() end')
        return self


    def getBaseAddrFromDb(self, addrBaseId):
        logger.info('getBaseAddrFromDb begin')

        modelUtils = ModelUtils()

        ba = None
        addressesTest = Addresses.objects.filter(address_base = addrBaseId).filter(address_base_flg = True)
        assert addressesTest.count() == 1, 'expected one address but got ' + str(addressesTest.count())
        ba = addressesTest.get()
        if ba.retire_tms:
            raise AddressIsRetiredWarning(ba.reprForRetired())

        ua = ba.address_base
        baseAdd = self.BaseAddress()
        baseAdd.address_base_flg = True
        baseAdd.address_base_id = ba.address_base.address_base_id
        baseAdd.address_id = ba.address_id
        baseAdd.isNew = False
        baseAdd.retire_flg = False
        baseAdd.base_address_num = ua.base_address_num
        baseAdd.base_address_prefix = ua.base_address_prefix
        baseAdd.base_address_suffix = ua.base_address_suffix
        baseAdd.floor = ba.floor.floor_id
        baseAdd.geometry = ua.geometry.clone()
        baseAdd.geometry.transform(900913)
        baseAdd.geometry_proposed = baseAdd.geometry

        # http://sfgovdt.jira.com/browse/MAD-44
        logger.info('building parcel pick list')
        addressIds = Addresses.objects.filter(address_base = ba.address_base).values_list('address_id', flat=True)
        parcelIds = AddressXParcels.objects.filter(address__address_id__in = addressIds).filter(retire_tms__isnull = True).values_list('parcel__parcel_id', flat=True)
        linkedParcels = Parcels.objects.filter(parcel_id__in = parcelIds).defer('geometry')

        # mandatory order by APN
        baseAdd.setParcelPickList(modelUtils.buildParcelPickList(pointGeometry = ua.geometry, includeParcels = linkedParcels))

        logger.info('getting streets')
        baseAdd.setAvailableStreets(StreetSegments.getNearestTo(geometry=ua.geometry, includeSegment=ba.address_base.street_segment))

        # create the list of linked parcel objects
        # mandatory order by APN
        logger.info('getting linked parcels')
        linkedParcelList = AddressXParcels.buildLinkedParcelList(ba.address_id)
        baseAdd.setLinkedParcels(linkedParcelList)

        baseAdd.street_segment = ua.street_segment.street_segment_id
        baseAdd.unit_disposition = ba.disposition_code.disposition_code
        baseAdd.unit_disposition_proposed = ba.disposition_code.disposition_code
        baseAdd.unit_mailable_flg = ba.mailable_flg
        baseAdd.unit_mailable_flg_proposed = ba.mailable_flg
        baseAdd.unit_num = ba.unit_num
        baseAdd.unit_type = ba.unit_type.unit_type_id
        baseAdd.zone_id = ua.zone.zone_id

        logger.info('zone id: %s' % baseAdd.zone_id)
        logger.info('baseAdd setup complete')

        # In this next section we use dictionaries instead of objects.
        # Why?
        # Try as I might I was unable to get select_related() to cache everything in a single db call.
        # By switching to dictionaries, we realized a huge performance gain with little downside.
        # Should you know the ORM well, have at it.
        unitAddresses = Addresses.objects.getByBaseAdddressId(addressBaseId=addrBaseId, includeBaseAddresses=False)
        for aUnit in unitAddresses:
            unitAdd = self.UnitAddress()
            unitAdd.address_base_flg = False
            unitAdd.address_id = aUnit['address_id']
            unitAdd.disposition = aUnit['disposition_code_id']
            unitAdd.disposition_proposed = aUnit['disposition_code_id']
            unitAdd.floor = aUnit['floor_id']
            unitAdd.isNew = False
            unitAdd.mailable_flg = aUnit['mailable_flg']
            unitAdd.mailable_flg_proposed = aUnit['mailable_flg']
            unitAdd.retire_flg = False
            unitAdd.linked_parcels = aUnit['linked_parcels']
            unitAdd.unit_num = aUnit['unit_num']
            unitAdd.unit_type = aUnit['unit_type_id']

            # add the unit to base address object
            baseAdd.unit_addresses.append(unitAdd)

        self.base_addresses.append(baseAdd)

        logger.info('getBaseAddrFromDb end')

        return self.base_addresses


    def parseFromString(self, jsonString, SRS = '900913'):
        logger.info('parseFromString begin')

        userCR = json.loads(jsonString)

        crPart = userCR['changeRequest'][0]
        baseAddressPart = crPart['base_addresses']

        # reset "" to None to take advantage of django's native orm (also change_request_id is an int)
        if crPart['change_request_id'] == '':
            crPart['change_request_id'] = None
        self.change_request_id = crPart['change_request_id']
        self.name = crPart['name']
        self.requestor_comment = crPart['requestor_comment']
        self.review_status_id = crPart['review_status_id']
        self.reviewer_comment = crPart['reviewer_comment']
        self.concurrency_id = crPart['concurrency_id']

        for baseAddress in baseAddressPart:
            logger.info('processing base address')
            baseAdd = self.BaseAddress()
            baseAdd.address_base_flg = True
            baseAdd.address_base_id = baseAddress['address_base_id']
            baseAdd.address_id = baseAddress['address_id']
            baseAdd.base_address_num = baseAddress['base_address_num']
            baseAdd.base_address_prefix = baseAddress['base_address_prefix']
            baseAdd.base_address_suffix = baseAddress['base_address_suffix']
            baseAdd.floor = baseAddress['floor']
            geom = GEOSGeometry('SRID=' + SRS + ';' + baseAddress['geometry_proposed'])
            geom.transform(2227)
            baseAdd.geometry_proposed = geom

            # add the isNew flag: are we're editing an existing base address or not
            if baseAdd.address_id is None:
                baseAdd.isNew = True
            else:
                baseAdd.isNew = False
            baseAdd.retire_flg = baseAddress['retire_flg']
            baseAdd.parcel_link_changes = baseAddress['parcel_link_changes']
            baseAdd.street_segment = baseAddress['street_segment']
            baseAdd.unit_disposition_proposed = baseAddress['unit_disposition_proposed']
            baseAdd.unit_mailable_flg_proposed = baseAddress['unit_mailable_flg_proposed']
            baseAdd.unit_num = baseAddress['unit_num']
            baseAdd.unit_type = baseAddress['unit_type']
            baseAdd.zone_id = baseAddress['zone']

            logger.info('unit address count: %s ' % str(len(baseAddress['unit_addresses'])))

            for unit in baseAddress['unit_addresses']:
                unitAdd = self.UnitAddress()
                unitAdd.address_base_flg = False
                unitAdd.address_id = unit['address_id']
                unitAdd.disposition_proposed = unit['disposition_proposed']
                unitAdd.floor = unit['floor']

                # add the isNew flag: are we're editing an existing unit address or not
                if unitAdd.address_id is None:
                    unitAdd.isNew = True
                else:
                    unitAdd.isNew = False
                unitAdd.mailable_flg = unit['mailable_flg']
                unitAdd.mailable_flg_proposed = unit['mailable_flg_proposed']
                unitAdd.retire_flg = unit['retire_flg']
                unitAdd.parcel_link_changes = unit['parcel_link_changes']
                unitAdd.unit_num = unit['unit_num']
                unitAdd.unit_type = unit['unit_type']

                # add the unit to base address object
                baseAdd.unit_addresses.append(unitAdd)

            self.base_addresses.append(baseAdd)

        logger.info('parseFromString end')

        return self

    def checkForConcurrentUpdates(self):
        crId = self.change_request_id
        if crId is not None:
            cr = CrChangeRequests.objects.get(pk = self.change_request_id)
            assert self.concurrency_id is not None, 'self.concurrency_id is None which is not legal'
            assert cr.concurrency_id is not None, 'cr.concurrency_id is None which is not legal'
            if self.concurrency_id != cr.concurrency_id:
                lastUpdateUser = User.objects.get(id = cr.requestor_user.id)
                raise ConcurrentUpdateWarning('This change request has been updated by %s (%s).  To proceed please coordinate with %s.' % (lastUpdateUser.get_full_name(), lastUpdateUser.email, lastUpdateUser.get_full_name()) )

    def executeCrDeleteSql(self, sqlStatements):
        logger.info('models.CrJsonModel.executeCrDeleteSql()')
        try:
            from django.db import connection
            cursor = connection.cursor()
            for sql in sqlStatements:
                #logger.info('executing sql:%s' % sql)
                cursor.execute(sql)
        except:
            raise

    def saveToDb(self, user):

        # todo - make more readable - follow "one screen per method" rule
        # todo - clean up the way we exit; nominal case, duplicate checks, concurrent update checks, and errors all take a different and sometimes circuitous path.
        # todo - improve the cache manager which is lame.

        logger.info('saveToDb() begin')
        GR = GenericResponseObj('CrJson.saveToDb()')
        currentTimestamp = datetime.datetime.now()

        cacheManager = CacheManager()

        try:

            from django.db import connection
            modelUtils = ModelUtils()

            self.checkForConcurrentUpdates()

            changeRequestId = self.change_request_id
            if changeRequestId != '' and changeRequestId != None:
                # todo - call a DB procedure
                # RE http://code.google.com/p/eas/issues/detail?id=295
                sqlStatements = CrChangeRequests.objects.getSqlStatementsToDeleteContents(changeRequestId)
                self.executeCrDeleteSql(sqlStatements)

            logger.info('preparing cr change request from json')
            changeRequest = CrChangeRequests.prepareFromCrJsonChangeRequest(self, user, currentTimestamp)
            changeRequest.save()


#            # update self with any fields that are updated by the db after save
#            self.change_request_id = cr.pk
#            if cr.reviewer_user:
#                self.reviewer_id = cr.reviewer_user.pk
#                self.reviewer_name = cr.reviewer_user.first_name + ' ' + cr.reviewer_user.last_name
#            if cr.requestor_user:
#                self.requestor_id = cr.requestor_user.pk
#                self.requestor_name = cr.requestor_user.first_name + ' ' + cr.requestor_user.last_name


            # cache all parcel objects for the base address and its units addresses
            parcelDict = self.createParcelDictFromBaseAddressList()

            # save each base address
            crAddressesXParcels = []
            crAddresses = []
            for jsonBaseAddress in self.base_addresses:

                logger.info('base address')
                logger.info(jsonBaseAddress)
                crAddressBase = CrAddressBase.prepareFromCrJsonBaseAddress(jsonBaseAddress)
                crAddressBase.validate()
                crAddressBase.save()

                logger.info('unit address for base address')
                crAddress = CrAddresses()
                self.populateCrAddressFromBaseAddress(crAddress, changeRequest, crAddressBase, jsonBaseAddress, cacheManager)
                crAddress.validate()
                crAddress.save()

                crAddressesXParcels = CrAddressXParcels.prepareParcelLinkChangesFromJson(crAddress, jsonBaseAddress.parcel_link_changes, parcelDict)
                DbUtils.executeBulkInsert(connection, crAddressesXParcels, setPrimaryKeys=False)

                logger.info('preparing unit addresses...')
                crAddressesXParcels = []
                crAddresses = []
                crAddressMinPk, crAddressMaxPk = DbUtils.getNextSequenceRange(connection, 'CrAddresses', len(jsonBaseAddress.unit_addresses))
                crAddressPk = crAddressMinPk - 1
                for jsonUnitAddress in jsonBaseAddress.unit_addresses:
                    crAddress = CrAddresses()
                    crAddressPk+=1
                    crAddress.pk = crAddressPk
                    self.populateCrAddressFromUnitAddress(crAddress, changeRequest, crAddressBase, jsonUnitAddress, cacheManager)
                    crAddress.validate()
                    crAddresses.append(crAddress)
                    crAddressesXParcelsTemp = CrAddressXParcels.prepareParcelLinkChangesFromJson(crAddress, jsonUnitAddress.parcel_link_changes, parcelDict)
                    crAddressesXParcels.extend(crAddressesXParcelsTemp)

                assert crAddressPk==crAddressMaxPk, 'primary key misalignment detected for CrAddresses'

                logger.info('bulk insert(s) of %s addresses' % str(len(crAddresses)))
                DbUtils.executeBulkInsert(connection, crAddresses, setPrimaryKeys=True)
                logger.info('bulk insert(s) of %s AXPs' % str(len(crAddressesXParcels)))
                DbUtils.executeBulkInsert(connection, crAddressesXParcels, setPrimaryKeys=False)


            if not self.review_status_id in [CrChangeRequests.STATUS_UNDER_REVIEW, CrChangeRequests.STATUS_REJECTED]:
                # Pre commit validations
                # CR must be completely saved for validations to work properly.
                changeRequest.validateForSave()

            GR.success = True
            GR.status_code = 0
            GR.returnObj = changeRequest

        # If we hit any exceptions, we should raise an exception to force the DB transaction to rollback.
        except ConcurrentUpdateWarning, e:
            logger.info(e.message)
            raise
        except ValidationWarning, e:
            logger.info(e.message)
            raise
        except AssertionError, e:
            logger.critical(e.message)
            raise
        except Exception, e:
            logger.critical('saveToDb failed - ' + e.message)
            raise

        logger.info('saveToDb end')
        return GR

    def approve(self):

        # Note - we have had a performance problem here.
        # The biggest problem is that large object graphs cause the django ORM to makes too many trips to the DB.
        # To help alleviate this problem, we traverse parts the model relationally, and assemble the object model from the relational sets.
        # During the actual save, we use batches.
        # Alter with care.
        # todo - improve cacheManager and parcelDict

        logger.info('approve')

        from django.db import connection
        cursor = connection.cursor()


        GR = GenericResponseObj('error in approve in cr_change_requests.py')
        cacheManager = CacheManager()
        parcelDict = self.createParcelDictFromBaseAddressList()
        timeStamp = datetime.datetime.now()

        try:
            GR.message = 'error encountered during approval'
            GR.status_code = 1
            GR.success = False

            crChangeRequest = CrChangeRequests.objects.get(pk = self.change_request_id)
            changeRequest = ChangeRequests.prepareFromCrChangeRequest(crChangeRequest=crChangeRequest, timeStamp=timeStamp)
            changeRequest.save()

            # Process retires first otherwise the validation will detect duplicate active addresses.
            # http://code.google.com/p/eas/issues/detail?id=442
            self.base_addresses = sorted(self.base_addresses, key=lambda crBaseAdd: not crBaseAdd.retire_flg)

            for crBaseAdd in self.base_addresses:

                addressBase = AddressBase.prepareFromCrAddressBase(crBaseAdd, timeStamp, parcelDict)
                addressBase.save()

                (addressBaseUnit, addressXParcelInsertList, addressXParcelUpdateList) = Addresses.prepareAddressForBaseAddressFromCr(changeRequest=changeRequest, addressBase=addressBase, crBaseAddress=crBaseAdd, cacheManager=cacheManager, timeStamp=timeStamp, parcelDict=parcelDict, connection=connection)
                addressBaseUnit.save()
                DbUtils.executeBulkInsert(connection, addressXParcelInsertList, setPrimaryKeys=False)
                DbUtils.executeBulkUpdate(connection, addressXParcelUpdateList)

                (addressInsertList, addressUpdateList, addressRetireList, addressXParcelInsertList, addressXParcelUpdateList) = Addresses.prepareListsFromCrBaseAddress(crBaseAddress=crBaseAdd, changeRequest=changeRequest, timeStamp=timeStamp, connection=connection, cacheManager=cacheManager, addressBase=addressBase, parcelDict=parcelDict)
                DbUtils.executeBulkInsert(connection, addressInsertList, setPrimaryKeys=True)
                DbUtils.executeBulkUpdate(connection, addressUpdateList)
                DbUtils.executeBulkUpdate(connection, addressRetireList)
                DbUtils.executeBulkInsert(connection, addressXParcelInsertList, setPrimaryKeys=False)
                DbUtils.executeBulkUpdate(connection, addressXParcelUpdateList)

            GR.success = True
            GR.status_code = 0
            GR.message = ''

        except Exception, e:
            logger.critical(e.message)
            # need to raise an exception here anyway so that django can
            # automatically rollback any pending commits to the db
            # todo  -working on http://code.google.com/p/eas/issues/detail?id=245
            #raise ValueError, GR
            raise ValidationWarning(ModelUtils.getUserMessage(e.message))

        return GR

    #
    # call with the URL: changeRequest/delete/{crID}
    #
    def deleteChangeRequest(self, crId):
        GR = GenericResponseObj('error in deleteChangeRequest in cr_change_requests.py')
        status = False

        if crId == '' or crId == None:
            GR.message = 'no CR id supplied, so nothing to delete'
            GR.success = True
            status = True
        else:

            try:
                sqlStatements = CrChangeRequests.objects.getSqlStatementsForDelete(crId)
                self.executeCrDeleteSql(sqlStatements)

                GR.message = ''
                GR.success = True

            except Exception, e:
                logger.critical(e.message)
                raise InternalError, GR

        return GR


class ParcelsProvisioning(models.Model):
    id = models.AutoField(primary_key=True)
    parcel = models.ForeignKey(Parcels)
    create_user = models.ForeignKey(User)
    create_tms = models.DateTimeField(null=False, auto_now_add=True)
    provisioned_tms = models.DateTimeField()
    suspended_tms = models.DateTimeField()
    suspended_comment = models.CharField(max_length=500)
    class Meta:
        db_table = u'parcels_provisioning'


class VwAddressesFlatNative(models.Model):
    id = models.TextField(primary_key=True)
    address_base_id = models.IntegerField()
    base_address_num = models.IntegerField()
    base_address_suffix = models.CharField(max_length=10)
    base_street_name = models.CharField(max_length=60)
    street_type = models.CharField(max_length=6)
    post_direction = models.CharField(max_length=10)
    full_street_name = models.CharField(max_length=255)
    seg_cnn = models.IntegerField()
    street_segment_id = models.IntegerField()
    base_address_create_tms = models.DateTimeField()
    base_address_retire_tms = models.DateTimeField()
    address_id = models.IntegerField()
    address_base_flg = models.BooleanField()
    unit_num = models.CharField(max_length=10)
    unit_type_id = models.IntegerField()
    floor_id = models.IntegerField()
    disposition_code = models.IntegerField()
    disposition_description = models.CharField(max_length=20)
    unit_address_create_tms = models.DateTimeField()
    unit_address_retire_tms = models.DateTimeField()
    map_blk_lot = models.CharField(max_length=9)
    blk_lot = models.CharField(max_length=9)
    parcel_date_map_add = models.DateTimeField()
    parcel_date_map_drop = models.DateTimeField()
    address_x_parcel_id = models.IntegerField()
    address_x_parcel_create_tms = models.DateTimeField()
    address_x_parcel_retire_tms = models.DateTimeField()
    address_x_parcel_activate_change_request_id = models.IntegerField()
    address_x_parcel_retire_change_request_id = models.IntegerField()
    zipcode = models.CharField(max_length=50)
    geometry = models.PointField(srid=2227)
    objects = models.GeoManager()

    class Meta:
        db_table = u'vw_addresses_flat_native'

