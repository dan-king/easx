import unittest

from MAD.models.models import *
from MAD.models.tests.utils import ModelTestHelper
from datetime import datetime
from django.db import transaction

# TODO: a good deal of refactoring can be done here - utility methods could be broken out into
# a separate class, seems that utility methods are not used in some of the older methods resulting
# in some redundant code
from MAD.models.models import Addresses

class CrChangeRequestTestCase(unittest.TestCase):

    def setUp(self):
        return
        
    def tearDown(self):
        return

    def doExpectPass_DuplicatesInQueue_ChangeRequest(self, crChangeRequest):
        for crBaseAddress in crChangeRequest.crBaseAddresses:
            self.doExpectPass_DuplicatesInQueue(crBaseAddress)

    def doExpectFail_DuplicatesInQueue_ChangeRequest(self, crChangeRequest):
        for crBaseAddress in crChangeRequest.crBaseAddresses:
            self.doExpectFail_DuplicatesInQueue(crBaseAddress)

    def addBaseAddressNew(self, crChangeRequest=None, streetNumber=0, fullStreetName=None):

        crBaseAddress = ModelTestHelper.createInMemoryCrBaseAddress(streetNumber=streetNumber, fullStreetName=fullStreetName)
        crBaseAddress.save()
        crChangeRequest.crBaseAddresses.append(crBaseAddress)

        crAddress = ModelTestHelper.createInMemoryCrAddress(crBaseAddress=crBaseAddress, crChangeRequest=crChangeRequest, isBaseAddress=True)
        crAddress.save()
        crChangeRequest.crAddresses.append(crAddress)

        return crBaseAddress

    def addBaseAddressExisting(self, crChangeRequest=None, streetNumber=0, fullStreetName=None):

        baseAddress = ModelTestHelper.getExistingBaseAddress(fullStreetName=fullStreetName, streetNumber=streetNumber)
        crBaseAddress = ModelTestHelper.createCrBaseAddressFromBaseAddress(baseAddress)
        crBaseAddress.save()
        crChangeRequest.crBaseAddresses.append(crBaseAddress)

        addresses = Addresses.objects.filter(address_base = baseAddress)
        address = addresses.get(address_base_flg = True)
        crAddress = ModelTestHelper.createCrAddressFromAddress(address=address, crAddressBase=crBaseAddress, crChangeRequest=crChangeRequest)
        crAddress.save()
        crChangeRequest.crAddresses.append(crAddress)

        return crBaseAddress


    def doExpectFail_DuplicatesInQueue(self, crBaseAddress):
        duplicateBaseAddresses = crBaseAddress.findDuplicatesInQueue()
        self.assertTrue(duplicateBaseAddresses.count() > 0)
        validation = crBaseAddress.validateDuplicatesInQueue()
        self.assertTrue(validation.valid == False)
        duplicateBaseAddress = validation.object
        self.assertTrue(duplicateBaseAddress != None)

    def doExpectPass_DuplicatesInQueue(self, crBaseAddress):
        duplicateBaseAddresses = crBaseAddress.findDuplicatesInQueue()
        self.assertTrue(duplicateBaseAddresses.count() > 0)
        validation = crBaseAddress.validateDuplicatesInQueue()
        self.assertTrue(validation.valid == True)
        duplicateBaseAddress = validation.object
        self.assertTrue(duplicateBaseAddress == None)

    def createChangeRequestCompleteExisting(self, baseAddressPrefix=None, streetNumber=0, baseAddressSuffix=None, fullStreetName=None, jurisdiction='SF MAIN', apnForParcelLinkChange=None, link=False, unlink=False, connection=None):

        crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest('unit testing - test', 'no comment')
        crChangeRequest.save()

        baseAddress = ModelTestHelper.getExistingBaseAddress(fullStreetName=fullStreetName, streetNumber=streetNumber)
        crBaseAddress = ModelTestHelper.createCrBaseAddressFromBaseAddress(baseAddress)
        crBaseAddress.save()

        # we just create what we need for the base address because that is all we are testing
        addresses = Addresses.objects.filter(address_base = baseAddress)
        addresses = addresses.filter(address_base_flg = True)
        address = addresses[0]
        crAddress = ModelTestHelper.createCrAddressFromAddress(address=address, crAddressBase=crBaseAddress, crChangeRequest=crChangeRequest)
        crAddress.save()

        if apnForParcelLinkChange:
            crAddressXParcel = ModelTestHelper.createInMemoryCrAddressXParcel(crAddress=crAddress, apn=apnForParcelLinkChange, link=link, unlink=unlink)
            crAddressXParcel.save()
            return crChangeRequest, crBaseAddress, crAddress, crAddressXParcel

        return crChangeRequest, crBaseAddress, crAddress


    def createChangeRequestComplete(self, baseAddressPrefix=None, streetNumber=0, baseAddressSuffix=None, fullStreetName=None, jurisdiction='SF MAIN', geom=None, apnForParcelLinkChange=None, connection=None):
        crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest('unit testing', 'a comment')
        crChangeRequest.save()

        crBaseAddress = ModelTestHelper.createInMemoryCrBaseAddress(baseAddressPrefix=baseAddressPrefix, streetNumber=streetNumber, baseAddressSuffix=baseAddressSuffix, fullStreetName=fullStreetName, jurisdiction=jurisdiction, geom=geom)
        crBaseAddress.save()

        crAddress = ModelTestHelper.createInMemoryCrAddress(crBaseAddress=crBaseAddress, crChangeRequest=crChangeRequest, isBaseAddress=True)
        crAddress.save()

        if apnForParcelLinkChange:
            # In the case of a new address we can either link or not link - we cannot unlink.
            crAddressXParcel = ModelTestHelper.createInMemoryCrAddressXParcel(crAddress=crAddress, apn=apnForParcelLinkChange, link=True, unlink=False)
            crAddressXParcel.save()
            return crChangeRequest, crBaseAddress, crAddress, crAddressXParcel

        return crChangeRequest, crBaseAddress, crAddress

    def cleanup(self, address, baseAddress, changeRequest):
        address.delete()
        baseAddress.delete()
        changeRequest.delete()

    def getCrAddressesForValidation(self, crChangeRequest):
        crAddresses = CrAddresses.objects.filter(change_request = crChangeRequest.change_request_id)
        crAddresses = crAddresses.filter(address_base_flg = True)
        
        return crAddresses


    def getCrBaseAddressesForValidation(self, crAddresses):
        crAddressBaseIds = []
        for crAddress in crAddresses:
            crAddressBaseIds.append(crAddress.cr_address_base.cr_address_base_id)
        crAddressBases = CrAddressBase.objects.filter(cr_address_base_id__in=crAddressBaseIds)

        return crAddressBases


    def flagAddressForRetire(self, crAddress):
        from datetime import datetime
        crAddress.retire_tms = datetime.now
        crAddress.retire_flg = True
        crAddress.save()
        
        return crAddress


    def runDuplicateInActiveChecks(self, crChangeRequest=None, crBaseAddress=None, expectValid=False):
        try:
            crAddresses = self.getCrAddressesForValidation(crChangeRequest)
            crAddressBases = self.getCrBaseAddressesForValidation(crAddresses)

            activeBaseAddress = crBaseAddress.findDuplicateInActive(crAddressBases, crAddresses)
            if expectValid:
                self.assertTrue(activeBaseAddress is None)
            else:
                self.assertTrue(activeBaseAddress is not None)

            validation = crBaseAddress.validateDuplicateInActive(crAddressBases, crAddresses)
            if expectValid:
                self.assertTrue(validation.valid)
            else:
                self.assertFalse(validation.valid)

            activeBaseAddress = validation.object
            if expectValid:
                self.assertTrue(activeBaseAddress is None)
            else:
                self.assertTrue(activeBaseAddress is not None)

        except:
            raise

    def runDuplicateInQueueChecks(self, crChangeRequest=None, crBaseAddress=None, expectValid=False):

        try:
            crBaseAddresses = crBaseAddress.findDuplicatesInQueue()
            if expectValid:
                self.assertTrue(crBaseAddresses.count() == 0)
            else:
                self.assertTrue(crBaseAddresses.count() > 0)

            validation = crBaseAddress.validateDuplicatesInQueue()
            if expectValid:
                self.assertTrue(validation.valid)
            else:
                self.assertFalse(validation.valid)

            changeRequest = validation.object
            if expectValid:
                self.assertTrue(changeRequest is None)
            else:
                self.assertTrue(changeRequest is not None)
        except:
            raise

    ###################################################################################################################

    def testAddressNumberValidation(self):

        testDict = {
            -1: False,
            0: True,
            1: True,
            999999: True,
            "999999": True,
            "9,999": False,
            1000000: False,
            "000000": True,
            None: False,
            "": False,
            "1a": False,
            "a": False,
            "ab": False
        }
        for k, v in testDict.items():
            crBaseAddress = ModelTestHelper.createInMemoryCrBaseAddress(streetNumber=k, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
            try:
                crBaseAddress.validateAddressNumber()
                try:
                    self.assertTrue(v)
                except:
                    print "\nfailed on %s, %s " % (k, v)
                    raise
            except ValidationWarning, e:
                try:
                    self.assertFalse(v)
                except:
                    print "\nfailed on %s, %s " % (k, v)
                    raise


    @transaction.commit_manually
    def testDuplicateInActive_00(self):

        # create 33 sussex in memory;
        # 33 susex is an address that does not exist
        # there is no equivalent in the business tables
        # this should result in passed validations because we will not be inserting a duplicate

        ##### setup
        # first we create a cr change request
        crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest('unit testing - test_00', 'no comment')
        crChangeRequest.save()
        # create base address
        crBaseAddress = ModelTestHelper.createInMemoryCrBaseAddress(streetNumber=33, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        crBaseAddress.save()
        # create address
        address = ModelTestHelper.createInMemoryCrAddress(crBaseAddress = crBaseAddress, crChangeRequest=crChangeRequest, isBaseAddress=True)
        address.save()
        crAddresses = self.getCrAddressesForValidation(crChangeRequest)
        crAddressBases = self.getCrBaseAddressesForValidation(crAddresses)
        #####

        try:
            activeBaseAddress = crBaseAddress.findDuplicateInActive(crAddressBases, crAddresses)
            self.assertTrue(activeBaseAddress is None)
            validation = crBaseAddress.validateDuplicateInActive(crAddressBases, crAddresses)
            self.assertTrue(validation.valid == True)
        finally:
            transaction.rollback()



    @transaction.commit_manually
    def testDuplicateInActive_01(self):
        # create a new 20 sussex - which is a duplicate of 20 sussex that is in active
        # this should result in failed validations - we would be inserting a materially duplicate address

        ##### setup
        # first we create a cr change request
        crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest('unit testing - test_01', 'no comment')
        crChangeRequest.save()
        # create base address
        crBaseAddress = ModelTestHelper.createInMemoryCrBaseAddress(streetNumber=20, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        crBaseAddress.save()
        # create address
        address = ModelTestHelper.createInMemoryCrAddress(crBaseAddress = crBaseAddress, crChangeRequest=crChangeRequest, isBaseAddress=True)
        address.save()
        crAddresses = self.getCrAddressesForValidation(crChangeRequest)
        crAddressBases = self.getCrBaseAddressesForValidation(crAddresses)
        #####

        try:
            activeBaseAddress = crBaseAddress.findDuplicateInActive(crAddressBases, crAddresses)
            self.assertTrue(activeBaseAddress is not None)
            validation = crBaseAddress.validateDuplicateInActive(crAddressBases, crAddresses)
            self.assertTrue(validation.valid == False)
            activeBaseAddress = validation.object
            self.assertTrue(activeBaseAddress is not None)
        finally:
            transaction.rollback()

    @transaction.commit_manually
    def testDuplicateInActive_02(self):
        # create a 20 sussex from active (with identity); there is an identical in active
        # this should result in passed validations
        # This is to allow update of existing rows.

        ##### setup
        # first we create a CrBaseAddress with an existing adress inside of it
        baseAddress = ModelTestHelper.getExistingBaseAddress(fullStreetName='SUSSEX ST', streetNumber=20)
        crBaseAddress = ModelTestHelper.createCrBaseAddressFromBaseAddress(baseAddress)
        # first we create a cr change request
        crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest('unit testing - test_01', 'no comment')
        crChangeRequest.save()
        # save base address
        crBaseAddress.save()
        # create address
        address = ModelTestHelper.createInMemoryCrAddress(crBaseAddress = crBaseAddress, crChangeRequest=crChangeRequest, isBaseAddress=True)
        address.save()
        crAddresses = self.getCrAddressesForValidation(crChangeRequest)
        crAddressBases = self.getCrBaseAddressesForValidation(crAddresses)
        #####

        try:
            activeBaseAddress = crBaseAddress.findDuplicateInActive(crAddressBases, crAddresses)
            self.assertTrue(activeBaseAddress is None)
            validation = crBaseAddress.validateDuplicateInActive(crAddressBases, crAddresses)
            self.assertTrue(validation.valid == True)
            activeBaseAddress = validation.object
            self.assertTrue(activeBaseAddress is None)
        finally:
            transaction.rollback()


    @transaction.commit_manually
    def testDuplicateInActiveWithPrefixAndSuffix(self):
        # Try to create a new 20 sussex - which will be a duplicate of 20 sussex that is in active, except the new 20 sussex has a different prefix or suffix.
        # This should result in passed validations - we would be inserting a materially distinct address.
        # This supports things like "20 1/2 sussex" where the "1/2" is the suffix on the base address.

        # prefix - no prefix
        try:
            (crChangeRequest, crBaseAddress, address) = self.createChangeRequestComplete(baseAddressPrefix='X', streetNumber=20, baseAddressSuffix=None, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
            self.runDuplicateInActiveChecks(crChangeRequest=crChangeRequest, crBaseAddress=crBaseAddress, expectValid=True)
        finally:
            transaction.rollback()

        # suffix - no suffix
        try:
            (crChangeRequest, crBaseAddress, address) = self.createChangeRequestComplete(baseAddressPrefix=None, streetNumber=20, baseAddressSuffix='A', fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
            self.runDuplicateInActiveChecks(crChangeRequest=crChangeRequest, crBaseAddress=crBaseAddress, expectValid=True)
        finally:
            transaction.rollback()



    @transaction.commit_manually
    def testDuplicateInQueueWithPrefixAndSuffix(self):
        try:
            (crChangeRequest_A, crBaseAddress_A, address_A) = self.createChangeRequestComplete(baseAddressPrefix=None, streetNumber=20, baseAddressSuffix='C', fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
            (crChangeRequest_B, crBaseAddress_B, address_B) = self.createChangeRequestComplete(baseAddressPrefix=None, streetNumber=20, baseAddressSuffix='C', fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
            self.runDuplicateInQueueChecks(crChangeRequest=crChangeRequest_A, crBaseAddress=crBaseAddress_A, expectValid=False)
            self.runDuplicateInQueueChecks(crChangeRequest=crChangeRequest_B, crBaseAddress=crBaseAddress_B, expectValid=False)
        finally:
            transaction.rollback()

        try:
            (crChangeRequest_A, crBaseAddress_A, address_A) = self.createChangeRequestComplete(baseAddressPrefix=None, streetNumber=20, baseAddressSuffix='B', fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
            (crChangeRequest_B, crBaseAddress_B, address_B) = self.createChangeRequestComplete(baseAddressPrefix=None, streetNumber=20, baseAddressSuffix='C', fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
            self.runDuplicateInQueueChecks(crChangeRequest=crChangeRequest_A, crBaseAddress=crBaseAddress_A, expectValid=True)
            self.runDuplicateInQueueChecks(crChangeRequest=crChangeRequest_B, crBaseAddress=crBaseAddress_B, expectValid=True)
        finally:
            transaction.rollback()



    @transaction.commit_manually
    def testDuplicateInQueue_00(self):
        # an address that will have no Duplicate in queue

        ##### setup
        # first we create a cr change request
        crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest()
        crChangeRequest.save()
        # create base address
        crBaseAddress = ModelTestHelper.createInMemoryCrBaseAddress(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        crBaseAddress.save()
        # create address
        address = ModelTestHelper.createInMemoryCrAddress(crBaseAddress = crBaseAddress, crChangeRequest=crChangeRequest, isBaseAddress=True)
        address.save()
        #####

        try:
            crBaseAddresses = crBaseAddress.findDuplicatesInQueue()
            self.assertTrue(crBaseAddresses.count() == 0)
            validation = crBaseAddress.validateDuplicatesInQueue()
            self.assertTrue(validation.valid == True)
            changeRequest = validation.object
            self.assertTrue(changeRequest == None)
        finally:
            transaction.rollback()


    @transaction.commit_manually
    def testDuplicateInQueue_01(self):

        # a single cr base address in queue with no conflicts

        ##### setup
        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')

        # report
        #        print ''
        #        print 'change_request_id: ' + str(crChangeRequest.change_request_id)
        #        print 'cr_address_base_id: ' + str(crBaseAddress.cr_address_base_id)
        #        print 'cr_address_id: ' + str(address.cr_address_id)


        # the test
        try:
            duplicateBaseAddresses = crBaseAddress.findDuplicatesInQueue()
            self.assertTrue(duplicateBaseAddresses.count() == 0)
            validation = crBaseAddress.validateDuplicatesInQueue()
            self.assertTrue(validation.valid == True)
            duplicateBaseAddress = validation.object
            self.assertTrue(duplicateBaseAddress == None)
        finally:
            transaction.rollback()


    @transaction.commit_manually
    def testDuplicateInQueue_02(self):

        # a pair of duplicate new addresses in queue - should fail

        ##### setup
        # first we create a cr change request

        (crChangeRequest_1, crBaseAddress_1, address_1) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        (crChangeRequest_2, crBaseAddress_2, address_2) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')

        # report
        #        print ''
        #        print 'change_request_id: ' + str(crChangeRequest.change_request_id)
        #        print 'cr_address_base_id: ' + str(crBaseAddress.cr_address_base_id)
        #        print 'cr_address_id: ' + str(address.cr_address_id)

        ##### do the test
        try:
            duplicateBaseAddresses = crBaseAddress_2.findDuplicatesInQueue()
            self.assertTrue(duplicateBaseAddresses.count() > 0)
            validation = crBaseAddress_2.validateDuplicatesInQueue()
            self.assertTrue(validation.valid == False)
            duplicateBaseAddress = validation.object
            changeRequest = duplicateBaseAddress.getChangeRequest()
            self.assertTrue(changeRequest != None)
            # should be symmetrical
            duplicateBaseAddresses = crBaseAddress_1.findDuplicatesInQueue()
            self.assertTrue(duplicateBaseAddresses.count() > 0)
            validation = crBaseAddress_1.validateDuplicatesInQueue()
            self.assertTrue(validation.valid == False)
            duplicateBaseAddress = validation.object
            changeRequest = duplicateBaseAddress.getChangeRequest()
            self.assertTrue(changeRequest != None)

        finally:
            transaction.rollback()




    @transaction.commit_manually
    def testDuplicateInQueue_03(self):

        # an existing address with no duplicate in the queue - should pass

        ##### setup
        # first we create a cr change request
        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestCompleteExisting(streetNumber=20, fullStreetName='SUSSEX ST')

        try:
            duplicateBaseAddresses = crBaseAddress.findDuplicatesInQueue()
            self.assertTrue(duplicateBaseAddresses.count() == 0)
            validation = crBaseAddress.validateDuplicatesInQueue()
            self.assertTrue(validation.valid == True)
            duplicateBaseAddress = validation.object
            self.assertTrue(duplicateBaseAddress == None)
        finally:
            transaction.rollback()


    @transaction.commit_manually
    def testDuplicateInQueue_04(self):

        # two copies of the same existing addresses - should fail - xxx

        # setup
        (crChangeRequest_1, crBaseAddress_1, address_1) = self.createChangeRequestCompleteExisting(streetNumber=20, fullStreetName='SUSSEX ST')
        (crChangeRequest_2, crBaseAddress_2, address_2) = self.createChangeRequestCompleteExisting(streetNumber=20, fullStreetName='SUSSEX ST')

        # the test
        try:
            # should pass symmetrically
            self.doExpectFail_DuplicatesInQueue(crBaseAddress_1)
            self.doExpectFail_DuplicatesInQueue(crBaseAddress_2)
        finally:
            transaction.rollback()


    @transaction.commit_manually
    def testDuplicateInQueue_05(self):
        # an existing address in queue with duplicate new address
        # should fail because this is not a

        # setup
        (crChangeRequest_1, crBaseAddress_1, address_1) = self.createChangeRequestCompleteExisting(streetNumber=20, fullStreetName='SUSSEX ST')
        (crChangeRequest_2, crBaseAddress_2, address_2) = self.createChangeRequestComplete(streetNumber=20, fullStreetName='SUSSEX ST')

        # the test
        try:
            # should fail symmetrically
            self.doExpectFail_DuplicatesInQueue(crBaseAddress_1)
            self.doExpectFail_DuplicatesInQueue(crBaseAddress_2)

            # should fail symmetrically (looks like but is not a retire-replace)
            from datetime import datetime
            address_1.retire_tms = datetime.now
            address_1.save()
            self.doExpectFail_DuplicatesInQueue(crBaseAddress_1)
            self.doExpectFail_DuplicatesInQueue(crBaseAddress_2)
        finally:
            transaction.rollback()


    @transaction.commit_manually
    def testRetireReplace_00(self):
        # a retire replace:
        # retire an existing address and add a new address that matches the retired one in the same CR
        # (should pass)

        # setup
        # create the change request
        crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest()
        crChangeRequest.save()
        # add the existing base address to the change request
        existingCrBaseAddress = self.addBaseAddressExisting(crChangeRequest=crChangeRequest, streetNumber=20, fullStreetName='SUSSEX ST')

        # flag the existing address as a retire
        self.flagAddressForRetire(crChangeRequest.crAddresses[0])

        # add the new base address to replace the one being retired
        newCrBaseAddress = self.addBaseAddressNew(crChangeRequest=crChangeRequest, streetNumber=20, fullStreetName='SUSSEX ST')
        # get the address and base address lists for validation
        crAddresses = self.getCrAddressesForValidation(crChangeRequest)
        crAddressBases = self.getCrBaseAddressesForValidation(crAddresses)

        # the test
        try:
            # test validation of the existing address
            existingActiveBaseAddress = existingCrBaseAddress.findDuplicateInActive(crAddressBases, crAddresses)
            self.assertTrue(existingActiveBaseAddress == None)
            validation1 = existingCrBaseAddress.validateDuplicateInActive(crAddressBases, crAddresses)
            self.assertTrue(validation1.valid == True)
            # test validation of the new address
            newActiveBaseAddress = newCrBaseAddress.findDuplicateInActive(crAddressBases, crAddresses)
            self.assertTrue(newActiveBaseAddress == None)
            validation2 = newCrBaseAddress.validateDuplicateInActive(crAddressBases, crAddresses)
            self.assertTrue(validation2.valid == True)
        finally:
            transaction.rollback()


    @transaction.commit_manually
    def testRetireReplace_01(self):
        # a retire replace - unlikely case -
        # retire an existing address and add a new address that matches the retired one in the same CR
        # but a retire already exists for the existing address in another CR
        # (should fail)

        # setup
        # create the first change request
        # this change request will hold only a single retire
        crChangeRequestRetireOnly = ModelTestHelper.createInMemoryCrChangeRequest()
        crChangeRequestRetireOnly.save()
        # add the existing base address to the first change request
        existingCrBaseAddressForRetireOnly = self.addBaseAddressExisting(crChangeRequest=crChangeRequestRetireOnly, streetNumber=20, fullStreetName='SUSSEX ST')

        # flag the existing address as a retire
        self.flagAddressForRetire(crChangeRequestRetireOnly.crAddresses[0])

        # create the second change request
        # this change request will contain both a retire and a replace (new address matching retired one)
        crChangeRequestRetireReplace = ModelTestHelper.createInMemoryCrChangeRequest()
        crChangeRequestRetireReplace.save()
        # add the existing base address to the second change request
        existingCrBaseAddress = self.addBaseAddressExisting(crChangeRequest=crChangeRequestRetireReplace, streetNumber=20, fullStreetName='SUSSEX ST')

        # flag the existing address as a retire
        self.flagAddressForRetire(crChangeRequestRetireReplace.crAddresses[0])

        # add the new base address to replace the one being retired
        newCrBaseAddress = self.addBaseAddressNew(crChangeRequest=crChangeRequestRetireReplace, streetNumber=20, fullStreetName='SUSSEX ST')
        # get the address and base address lists for validation
        crAddresses = self.getCrAddressesForValidation(crChangeRequestRetireReplace)
        crAddressBases = self.getCrBaseAddressesForValidation(crAddresses)

        # the test
        try:
            # test for duplicates in the queue of change requests
            # should fail since the retire has been duplicated
            self.doExpectFail_DuplicatesInQueue_ChangeRequest(crChangeRequestRetireReplace)
        finally:
            transaction.rollback()

    @transaction.commit_manually
    def testStreetSegmentValidation_00(self):
        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        crBaseAddress.street_segment.date_dropped = None
        try:
            crBaseAddress.validateStreetSegment()
            self.assertTrue(True == True)
        except ValidationWarning, e:
            self.assertTrue(True == False)
        finally:
            transaction.rollback()

    @transaction.commit_manually
    def testStreetSegmentValidation_01(self):
        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        crBaseAddress.street_segment.date_dropped = datetime.now()
        try:
            crBaseAddress.validateStreetSegment()
            self.assertTrue(True == True)
        except Exception, e:
            self.assertTrue(True == False)
        finally:
            transaction.rollback()

    @transaction.commit_manually
    def testParcelValidation_00(self):
        try:
            # a change request creating a new address, then an associated parcel is retired.
            # validation should return False

            # first we create a cr change request
            (crChangeRequest, crBaseAddress, address) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')

            # add a parcel
            parcel = Parcels.objects.get( blk_lot = '0026028' )
            crAddressXParcels = CrAddressXParcels(cr_address = address, parcel = parcel)
            crAddressXParcels.save()

            # flag the parcel as retired
            parcel.date_map_drop = datetime.now()
            parcel.save()

            ##### do the test
            unretiredAddresses = crChangeRequest.getUnretiredCrAddresses()
            try:
                validations = CrAddresses.validateForRetiredParcelLinks(unretiredAddresses)
            except Exception, e:
                print e
            self.assertTrue(len(validations) > 0)
        finally:
            transaction.rollback()

    @transaction.commit_manually
    def testParcelValidation_01(self):

        # a change request creating a new address, the associated parcel is unretired.
        # validation should return True

        # first we create a cr change request
        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')

        # add a parcel
        parcel = Parcels.objects.get( blk_lot = '0026028' )
        crAddressXParcels = CrAddressXParcels(cr_address = address, parcel = parcel)
        crAddressXParcels.save()

        # flag parcel as unretired
        parcel.date_map_drop = None
        parcel.save()

        ##### do the test
        try:
            unretiredAddresses = crChangeRequest.getUnretiredCrAddresses()
            validations = CrAddresses.validateForRetiredParcelLinks(unretiredAddresses)
            self.assertTrue(len(validations) == 0)
        finally:
            transaction.rollback()


    @transaction.commit_manually
    def test_geom_reg__linking_to_moved_parcel(self):
        try:
            # create and save CR
            (crChangeRequest, crBaseAddress, address, crAddressXParcel) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN', geom='SRID=900913;POINT(-13629182.3781258 4542210.64066713)', apnForParcelLinkChange='7548017')
            # We only run the validations against unretired parcels - get those now.
            unretiredCrAddresses = crChangeRequest.getUnretiredCrAddresses()
            # run the parcel geom validations - should pass
            failedValidations = crChangeRequest.validateGeometricRegistration(unretiredCrAddresses = unretiredCrAddresses)
            self.assertTrue(len(failedValidations) == 0)
            # now move the parcel geometry
            parcelQueryset = Parcels.objects.filter(blk_lot = '7548017').translate(x=1000, y=1000)
            parcel = parcelQueryset[0]
            parcel.geometry = parcel.translate
            parcel.save()
            # run the validations again which should fail
            failedValidations = crChangeRequest.validateGeometricRegistration(unretiredCrAddresses = unretiredCrAddresses)
            self.assertTrue(len(failedValidations) > 0)
        finally:
            transaction.rollback()

    @transaction.commit_manually
    def test_geom_reg__linked_to_moved_parcel(self):
        try:
            # Existing address, already linked to a parcel; the parcel is then moved.
            # create and save CR
            (crChangeRequest, crBaseAddress, address) = self.createChangeRequestCompleteExisting(baseAddressPrefix=None, streetNumber=20, baseAddressSuffix=None, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
            # We only run the validations against unretired parcels - get those now.
            unretiredCrAddresses = crChangeRequest.getUnretiredCrAddresses()
            # run the parcel geom validations - should pass
            failedValidations = crChangeRequest.validateGeometricRegistration(unretiredCrAddresses = unretiredCrAddresses)
            self.assertTrue(len(failedValidations) == 0)
            # now move the parcel geometry
            parcelQueryset = Parcels.objects.filter(blk_lot = '7548017').translate(x=1000, y=1000)
            parcel = parcelQueryset[0]
            parcel.geometry = parcel.translate
            parcel.save()
            # Run the validations again which should not fail.
            # Even though the parcel is not spatially coincident it is not invalid
            failedValidations = crChangeRequest.validateGeometricRegistration(unretiredCrAddresses = unretiredCrAddresses)
            # warm fuzzies
            #            if failedValidations:
            #                print '\nfailed validations (%s):' % len(failedValidations)
            #                for v in failedValidations:
            #                    print v.message
            self.assertTrue(len(failedValidations) == 0)
        finally:
            transaction.rollback()
