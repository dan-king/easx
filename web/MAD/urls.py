from django.conf.urls.defaults import *
from django.conf import settings
from MAD.views.views import *

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    
    # admin functions
    #(r'^admin/(.*)', include(admin.site.urls)),
    (r'^Media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    
    # main site page
    (r'^$', Index),
    (r'^debug', Index, {'mode': 'debug'}),
    (r'^build', Index, {'mode': 'build'}),
    (r'^js/(\d+)$', Js),
    
    # reports
    (r'^AddressReport/$', AddressReport),
    (r'^Identifiers/$', Identifiers),
    (r'^UnitsForAddressReport/$', UnitsForAddressReport),
    (r'^ParcelsForAddressReport/$', ParcelsForAddressReport),
    (r'^Aliases/$', Aliases),
    (r'^History/$', History),
    (r'^BaseGeometryForParcels/$', BaseGeometryForParcels),

    # searches
    (r'^search/address$', AddressSearch),
    (r'^search/apn$', ApnSearch),
    (r'^search/addressbbox$', AddressBBoxWFS),
    (r'^search/parcelToAddresses$', parcelToAddresses),
    (r'^search/parcel/(?P<apn>\w+)/$', getParcelByApn),
    (r'^search/addressToParcels$', addressToParcels),
    (r'^search/pointToParcels$', pointToParcels),
    (r'^search/addressinfo/(\w+)$', AddressInfo),

    # change requests
    (r'^changeRequest/mine/$', getMyChangeRequests),
    (r'^changeRequest/available/$', getAvailableChangeRequests),
    (r'^changeRequest/save/$', saveCr, {'review_status_id': None}),
    (r'^changeRequest/edit/(?P<crId>\d+)/$', getCr, {'review_status_id': 4}),
    (r'^changeRequest/submitforreview/$', saveCr, {'review_status_id': 5}),
    (r'^changeRequest/approve/$', saveCr, {'review_status_id': 2}),
    (r'^changeRequest/reject/$', saveCr, {'review_status_id': 3}), 
    (r'^changeRequest/acceptforreview/$', saveCr, {'review_status_id': 1}),   
    (r'^changeRequest/(?P<crId>\d+)/$', getCr),
    (r'^changeRequest/delete/(?P<crId>\d+)/$', deleteCr),
    (r'^changeRequest/report/axp/(?P<axpId>\d+)/create/$', getChangeRequestReportForAxpCreate),
    (r'^changeRequest/report/axp/(?P<axpId>\d+)/retire/$', getChangeRequestReportForAxpRetire),

    (r'^getbaseaddr/(?P<baseAddrId>\d+)/$', getBaseAddr),

    # provision parcel
    # example values
    # block: ['1234', '1234A']
    # lot: ['001', '001T']
    # lon: -123.0001111 (we have hardcoded negative longitude)
    # lat: 42.0001111
    (r'^provision/parcel/block/(?P<block>\w+)/lot/(?P<lot>\w+)/x/(?P<x>-\d+\.\d+)/y/(?P<y>\d+\.\d+)/$', provisionParcel),

    # loosely based on OGC standard
    #     http://www.opengeospatial.org/standards/requests/89
    # e.g. usage /geocode/findAddressCandidates?f=json&Address=20 Sussex St&Zip=94131
    (r'^geocode/findAddressCandidates/$', findAddressCandidates),

    # domains
    (r'^domain/changerequest/$', getChangeRequestDomains),

    (r'^address/proposeAt/(?P<geom>.*)/$', proposeAddress),

    # authentication
    (r'^login/$', login),
    (r'^logout/$', logout),
    (r'^user/$', getUser),
    (r'^user/resetpassword/$', resetPassword),
    (r'^user/changepassword/$', changePassword),

)
