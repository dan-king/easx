
import base64
import string

class HttpUtils:

    @staticmethod
    def encodeCredentials(user, password):
        # "Basic" authentication encodes userid:password in base64. Note
        # that base64.encodestring adds some extra newlines/carriage-returns
        # to the end of the result. string.strip is a simple way to remove
        # these characters.
        auth = 'Basic ' + string.strip(base64.encodestring(user + ':' + password))
        return auth

    @staticmethod
    def responseAsText(response):
        values = [
            'status: %s' % response.status,
            'reason: %s' % response.reason,
            'version: %s' % response.version,
            'headers'
        ]
        for key, value in response.getheaders():
            values.append('\t%s: %s' % (key, value))
        content = response.read()
        values.append('content: %s' % content)
        return '\n'.join(values)

    @staticmethod
    def metaDataAsText(response):
        values = [
            'status: %s' % response.status,
            'reason: %s' % response.reason,
            'version: %s' % response.version,
            'headers'
        ]
        for key, value in response.getheaders():
            values.append('\t%s: %s' % (key, value))
        return '\n'.join(values)
