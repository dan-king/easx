import unittest
from django.db import transaction
from MAD.models.models import *
from MAD.models.tests.utils import ModelTestHelper
import time
import datetime


class DbUtilsTests(unittest.TestCase):


    def setUp(self):
        return


    def tearDown(self):
        return


    @transaction.commit_manually
    def test_bulk_insert_one(self):
        try:
            from django.db import connection
            arbitraryString = 'ABCDEFGHIJ'
            crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest(arbitraryString, arbitraryString)
            DbUtils.executeBulkInsert(connection, [crChangeRequest], setPrimaryKeys=False)
            crChangeRequests =CrChangeRequests.objects.filter(name = arbitraryString)
            self.assertTrue(crChangeRequests.count() == 1, msg="incorrect count")
        finally:
            transaction.rollback()


    @transaction.commit_manually
    def test_bulk_update_one(self):
        try:
            from django.db import connection
            # Insert an CrChangeRequest object.
            arbitraryString = 'ABCDEFGHIJ'
            crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest(arbitraryString, arbitraryString)
            crChangeRequest.save()
            # Modify our CrChangeRequest and save it using executeBulkUpdate.
            arbitraryStringReversed = arbitraryString[::-1]
            crChangeRequest.name = arbitraryStringReversed
            crChangeRequest.requestor_last_update = datetime.datetime.now()
            crChangeRequest.fieldsToUpdate = ["name", "requestor_last_update"]
            DbUtils.executeBulkUpdate(connection, [crChangeRequest])
            # Confirm that it worked.
            crChangeRequests = CrChangeRequests.objects.filter(name = arbitraryStringReversed)
            print 'expecting count of 1'
            print 'actual count is ' , crChangeRequests.count()
            self.assertTrue(crChangeRequests.count() == 1, msg="incorrect count")
        finally:
            transaction.rollback()


    @transaction.commit_manually
    def test_bulk_insert_many(self):
        try:
            from django.db import connection

            print 'preparing inserts'
            count = 1000
            manyChangeRequests = []
            arbitraryString = 'ABCDEFGHIJ'
            for i in range(1, count+1):
                crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest(arbitraryString, arbitraryString)
                manyChangeRequests.append(crChangeRequest)

            print 'preparation complete'
            print 'running executeBulkInsert'
            begin = time.time()
            DbUtils.executeBulkInsert(connection, manyChangeRequests, setPrimaryKeys=False)
            end = time.time()
            diff = end-begin
            # The bulk insert should take less than 1 second.
            print 'confirming performance'
            self.assertTrue( diff <= 1.00, msg="too slow")
            print 'confirming results'
            crChangeRequests = CrChangeRequests.objects.filter(name = arbitraryString)
            self.assertTrue(crChangeRequests.count() == count, msg = "incorrect count")

        finally:
            transaction.rollback()


    @transaction.commit_manually
    def test_bulk_update_many(self):
        try:
            from django.db import connection

            # bulk insert
            count = 1000
            insertChangeRequests = []
            arbitraryString = 'ABCDEFGHIJ'
            print 'preparing inserts'
            for i in range(1, count+1):
                crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest(arbitraryString, arbitraryString)
                insertChangeRequests.append(crChangeRequest)
            print 'preparation complete'
            print 'running executeBulkInsert'
            begin = time.time()
            DbUtils.executeBulkInsert(connection, insertChangeRequests, setPrimaryKeys=False)
            end = time.time()
            diff = end-begin
            print 'confirming performance'
            self.assertTrue( diff <= 2.00, msg="too slow")

            print 'confirming results'
            crChangeRequests = CrChangeRequests.objects.filter(name = arbitraryString)
            self.assertTrue(crChangeRequests.count() == count)

            # bulk update
            print 'preparing updates'
            arbitraryStringReversed = arbitraryString[::-1]
            updateChangeRequests = []
            for crChangeRequest in crChangeRequests:
                crChangeRequest.name = arbitraryStringReversed
                crChangeRequest.requestor_last_update = datetime.datetime.now()
                crChangeRequest.fieldsToUpdate = ["name", "requestor_last_update"]
                updateChangeRequests.append(crChangeRequest)
            print 'preparation complete'
            print 'running executeBulkUpdate'
            begin = time.time()
            DbUtils.executeBulkUpdate(connection, updateChangeRequests)
            end = time.time()
            diff = end-begin
            print 'confirming performance'
            self.assertTrue( diff <= 2.00, msg="too slow")
            updatedCrChangeRequests = CrChangeRequests.objects.filter(name = arbitraryStringReversed)
            self.assertTrue(updatedCrChangeRequests.count() == count, msg="incorrect counts")

        finally:
            transaction.rollback()
