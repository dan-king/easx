#regular expressions
import re
	
def ExtJSONResultsEncoder(data, count, request):
    data = '{count: ' + str(count) + ', results: ' + data + '}'
    #strip off beginning and trailing double quotes around the array
    data = re.sub(r'"\[', r'[',data)
    data = re.sub(r'\]"', r']',data)

    return data
