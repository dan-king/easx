from django.http import HttpResponse
from django.contrib.gis.geos.geometry import GEOSGeometry
from django.db import transaction
from django.db.models import Q
from MAD.models.models import *
from MAD.utils.geojson_encode import *
from MAD.utils.GenericResponseObj import *
from MAD.exceptions import AddressIsRetiredWarning
from MAD.models.ModelUtils import ModelUtils
from django.forms import ValidationError
from django.core.exceptions import ObjectDoesNotExist
import logging

logger = logging.getLogger('EAS')


def getBaseAddr(request, baseAddrId = None):
    GR = GenericResponseObj('error in getBaseAddr in views_change_requests.py')
    
    try:
        user = request.user          
        if not user.is_authenticated():
            GR.message = 'Please login.'
        else:
            if baseAddrId == None:
                GR.message = 'base address id was invalid'
            else:
                crObj = CrJsonModel()
                baseAddrObj = crObj.getBaseAddrFromDb(baseAddrId)
                GR.returnObj = {'base_addresses': baseAddrObj}
                GR.status_code = 0
                GR.success = True
                GR.message = ''
    except AddressIsRetiredWarning, e:
        GR.returnObj = None
        GR.status_code = 0
        GR.success = False
        GR.message = e.message
    except Exception, e:
        logger.critical(e.message)

    json = geojson_encode(GR)
    #logger.info(json)
    return HttpResponse(json)
    

@transaction.commit_manually
def getCr(request, crId = None, review_status_id = None):
    logger.info('getCr begin')
    GR = GenericResponseObj('error in getCr in views_change_requests.py')
    
    try:
        user = request.user          
        if not user.is_authenticated():
            GR.message = 'Please login.'
            transaction.rollback()
        else:
            if crId == None:
                GR.message = 'change request id was invalid'
            else:
                crObj = CrJsonModel(int(crId))
                if review_status_id != None:
                    logger.info('review status id: %s ' % review_status_id)
                    crObj.review_status_id = review_status_id
                    # todo - do not "save" during a "get" OR explain why we do this.
                    GR = crObj.saveToDb(user)
                    crObj = CrJsonModel(int(crId))

                GR.returnObj = {'changeRequest': [crObj]}
                GR.status_code = 0
                GR.success = True
                GR.message = ''
                transaction.commit()

    except Exception, e:
        transaction.rollback()
        GR.message = ModelUtils.getUserMessage(e.message)
        logger.critical(e.message)

    #logger.info(geojson_encode(GR))
    logger.info('getCr end')
    return HttpResponse(geojson_encode(GR))
    
@transaction.commit_manually
def saveCr(request, SRS = '900913', review_status_id = None):
    logger.info('views_change_requests - saveCr - BEGIN')
    GR = GenericResponseObj('error in saveCr in views_change_requests.py')
    
    try:
        user = request.user          
        if not user.is_authenticated():
            transaction.rollback()
            logger.info('rolled back transaction')
            GR.message = 'Please login.'
        else:
            GR.message = 'error parsing POSTed json CR object'
            #crObj = CrJsonModel(request.POST['json'])
            # debug only
            #logger.info('the raw json from the request')
            #logger.info(request.raw_post_data)
            crObj = CrJsonModel(request.raw_post_data)
            if review_status_id != None:
                crObj.review_status_id = review_status_id

            # debug only
            #logger.info('the populated CrJsonModel:')
            #logger.info(geojson_encode(crObj))
            

            GR.message = 'error saving POSTed json CR object to db'
            try:
                GR = crObj.saveToDb(user)
            except ValidationError, e:
                transaction.rollback()
                logger.info('rolled back transaction')
                GR.message = ', '.join(e.messages)
            else:
                if GR.success:
                    if review_status_id == 2:
                        GR = crObj.approve()
                    transaction.commit()
                else:
                    transaction.rollback()
                    logger.info('rolled back transaction')

    except Exception, e:
        transaction.rollback()
        logger.info('rolled back transaction')
        logger.warning(e.message)
        GR.success = False
        GR.message = ModelUtils.getUserMessage(e.message)

    logger.info('views_change_requests - saveCr - END')
    return HttpResponse(geojson_encode(GR))

@transaction.commit_manually
def deleteCr(request,crId):
    GR = GenericResponseObj('error in deleteCr in views_change_requests.py')
    
    try:
        user = request.user          
        if not user.is_authenticated():
            GR.message = 'Please login.'
            transaction.rollback()
        else:
            cr = CrChangeRequests.objects.get(pk = crId)
            if cr.requestor_user.pk == user.pk:
                crJsonModel = CrJsonModel()
                GR = crJsonModel.deleteChangeRequest(crId)
                GR.returnObj = { 'deleteCr': True }
            else:
                GR.message = 'user can only delete their own change requests'
                GR.status_code = 1
                        
    except Exception, e:
        logger.critical(e.message)
        GR.status_code = 0
        GR.success = False
        transaction.rollback()
    else:
        transaction.commit()

    return HttpResponse(geojson_encode(GR))

    
def proposeAddress(request, geom = '', SRS = '900913'):
    response = GenericResponseObj('error in proposeAddress in views_change_requests.py')
    
    if geom == '':
        response.status_code = -1
        response.success = False
        response.message = 'need to submit a geometry location for the proposed address'
    else:
        newBaseAddress = CrJsonModel.BaseAddress()
        point = GEOSGeometry('SRID=' + SRS + ';' + geom)
        point.transform(2227)
        newBaseAddress.geometry = point
        newBaseAddress.proposeNew()

        point.transform(900913)
        newBaseAddress.geometry = point.wkt
        newBaseAddress.geometry_proposed = point.wkt
        response.returnObj = {'base_addresses': [newBaseAddress]}

        response.status_code = 0
        response.success = True
        response.message = ''
        
    return HttpResponse(geojson_encode(response))
    
    
def getAvailableChangeRequests(request, userId = None):
    response = GenericResponseObj('error in getAvailableChangeRequests in views_change_requests.py')

    crList = CrChangeRequests.objects.filter(review_status = 5)
    user = request.user          
    if user.is_authenticated():
        crList = CrChangeRequests.objects.filter(Q(review_status = 5) | (Q(review_status = 1) & Q(reviewer_user = user)) )
    
    crJsonList = crListToJson(crList)
        
    response.returnObj = {'changeRequests': crJsonList}

    response.status_code = 0
    response.success = True
    response.message = ''
    
    return HttpResponse(geojson_encode(response))
  
    
def getMyChangeRequests(request, userId = None):
    response = GenericResponseObj('error in getMyChangeRequests in views_change_requests.py')

    #user = User.objects.get(pk=userId)
    crList = []
    user = request.user          
    if user.is_authenticated():
        crList = CrChangeRequests.objects.filter((Q(review_status = 3) | Q(review_status = 4) | Q(review_status = 5)) & Q(requestor_user = user))
        
    crJsonList = crListToJson(crList)

    response.returnObj = {'changeRequests': crJsonList}

    response.status_code = 0
    response.success = True
    response.message = ''
    
    return HttpResponse(str(response))

def crListToJson(crList):
    crJsonList = []
    for cr in crList:
        if not cr.reviewer_last_update:
            reviewerLastUpdate = ""
        else:
            reviewerLastUpdate = cr.reviewer_last_update
            
        if not cr.requestor_last_update:
            requestorLastUpdate = ""
        else:
            requestorLastUpdate = cr.requestor_last_update

        if not cr.reviewer_user:
            reviewer = ""
        else:
            reviewer = cr.reviewer_user.first_name + " " + cr.reviewer_user.last_name
        
        crJsonList.append({ 
            "change_request_id": cr.change_request_id,
            "name": cr.name,
            "requestor_name": cr.requestor_user.first_name + " " + cr.requestor_user.last_name,
            "requestor_last_update": requestorLastUpdate,
            "reviewer_name": reviewer,
            "reviewer_last_update": reviewerLastUpdate,
            "status_description": cr.review_status.status_description
        })
    return crJsonList


def getChangeReport(changeRequestId):

    addressesFlat = []
    if changeRequestId == 1:
        # If we have change request number 1, we return an empty array because
        #  - change request 1 is the beginning of EAS time (feb 15 2012)
        #  - there is no lineage before this
        pass
    else:
        addressesFlat = VwAddressesFlatNative.objects.filter(
            Q(address_x_parcel_activate_change_request_id = changeRequestId)
            |
            Q(address_x_parcel_retire_change_request_id = changeRequestId)
        )

    # Just use the CrChangeRequest - it contains the information we need.
    # We should one day remove fields from ChangeRequest or refactor that code.
    changeRequest = ChangeRequests.objects.get(pk=changeRequestId)
    changeRequest.cr_change_request.requestor_user.password = None
    changeRequest.cr_change_request.reviewer_user.password = None
    changeRequest.requestor = changeRequest.cr_change_request.requestor_user
    changeRequest.reviewer = changeRequest.cr_change_request.reviewer_user
    changeRequest.requestor_comment = changeRequest.cr_change_request.requestor_comment
    changeRequest.reviewer_comment = changeRequest.cr_change_request.reviewer_comment
    changeRequest.name = changeRequest.cr_change_request.name
    del(changeRequest._state)
    del(changeRequest._cr_change_request_cache)

    changeReport = {
        'changeRequest': changeRequest,
        'addressesFlat': addressesFlat
    }
    return changeReport


def getChangeRequestReportForAxpCreate(request, axpId):
    changeReport = {}
    try:
        axp = AddressXParcels.objects.get(id=axpId)
        changeReport = getChangeReport(axp.activate_change_request.change_request_id)
    except ObjectDoesNotExist, e:
        # The web application should never put use here, but someone hacking the URL might.
        pass
    return HttpResponse(geojson_encode(changeReport))


def getChangeRequestReportForAxpRetire(request, axpId):
    changeReport = {}
    try:
        axp = AddressXParcels.objects.get(id=axpId)
        changeReport = getChangeReport(axp.retire_change_request.change_request_id)
    except ObjectDoesNotExist, e:
        # The web application should never put use here, but someone hacking the URL might.
        pass
    return HttpResponse(geojson_encode(changeReport))
