from django.template import RequestContext, loader, Context
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, HttpResponseServerError, HttpResponseForbidden
from MAD.utils.geojson_encode import *
from MAD.utils.ExtJSONResultsEncoder import *
from django.shortcuts import render_to_response
from MAD.models.models import *
from MAD.utils.geojson_encode import *
from django.utils import simplejson as json
from MAD.models.ModelUtils import ModelUtils
from django.contrib.gis.geos import *
from django.db import connection
from django.conf import settings as settings


def AddressReport(request, mode = ''):
    addrBaseId = request.GET.get('id')
    addressBaseObj = AddressBase.objects.get(address_base_id = addrBaseId)
    addressObj = Addresses.objects.filter(address_base = addrBaseId).get(address_base_flg = True)
    if addressBaseObj.base_address_suffix == None:
        addressBaseObj.base_address_suffix = ""
    return render_to_response('addressreport.html', {'addressBase': addressBaseObj, 'addressObj': addressObj, 'streetseg': addressBaseObj.street_segment, 'zone': addressBaseObj.zone}, context_instance=RequestContext(request))

def Identifiers(request, mode = ''):
    addrBaseId = request.GET.get('id')
    addressBaseObj = AddressBase.objects.get(address_base_id = addrBaseId)
    coords = { 'easting' : round(addressBaseObj.geometry.x, 3), 'northing' : round(addressBaseObj.geometry.y, 3), 'long' : round(addressBaseObj.geometry.transform(4326, True).x, 5), 'lat' : round(addressBaseObj.geometry.transform(4326, True).y, 5) }
    # want to call Aliases.objects.filter(address_base_id = addrBaseId), but this doesn't seem to work?? - anp
    aliases = ManagerAliases().filter(address_base_id = addrBaseId)
    return render_to_response('identifiers.html', {'addressBase': addressBaseObj, 'coordinates': coords, 'streetseg': addressBaseObj.street_segment, 'aliases': aliases}, context_instance=RequestContext(request))

def UnitsForAddressReport(request):
    logger.info('begin views_report.UnitsForAddressReport')
    addressBaseId = request.GET.get('id')
    unitAddresses = Addresses.objects.filter(address_base__address_base_id = addressBaseId)\
        .filter(unit_num__isnull = False)\
        .order_by("unit_num", "address_base_flg")\
        .values("address_base_flg","unit_num","floor_id","unit_type_id","disposition_code", "retire_tms", "create_tms")
    #logger.info(unitAddresses.query)
    json = ExtJSONResultsEncoder(geojson_encode(unitAddresses), len(unitAddresses), request)
    logger.info('end views_report.UnitsForAddressReport()')
    return HttpResponse(json)

def ParcelsForAddressReport(request):
    logger.info('begin views_report.ParcelsForAddressReport')
    addressBaseId = request.GET.get('id')
    parcelAddresses = VwAddressesFlatNative.objects.filter(address_base_id = addressBaseId)\
        .filter(address_x_parcel_id__isnull = False)\
        .order_by("blk_lot", "address_base_flg", "address_x_parcel_retire_tms")
    json = ExtJSONResultsEncoder(geojson_encode(parcelAddresses), len(parcelAddresses), request)
    logger.info('end views_report.ParcelsForAddressReport')
    return HttpResponse(json)

def Aliases(request, mode = ''):
    addrBaseId = request.GET.get('id')
    aliases = VwAddressSearch.objects.filter(address_base_id = addrBaseId).filter(unit_num = "-999").filter(category__iexact='alias')
    return render_to_response('aliases.html', {'aliases': aliases}, context_instance=RequestContext(request))

def History(request, mode = ''):
    return render_to_response('history.html', {}, context_instance=RequestContext(request))

def BaseGeometryForParcels(request):
    logger.info('BaseGeometryForParcels')
    try:
        apns = json.loads(request.raw_post_data)
        #for apn in apns:
        #    logger.info(apn)
        geometry = ModelUtils.getBaseParcelGeometryWithApns(apns)
        jsonGeometry = ExtJSONResultsEncoder(geojson_encode(geometry), len(apns), request)
        #logger.info(jsonGeometry)
        return HttpResponse(jsonGeometry)

    except Exception, e:
        logger.warning(e.message)
