
from django.template import RequestContext
from django.shortcuts import render_to_response
from MAD.utils.geojson_encode import *
from MAD.utils.jsbuild import *
from MAD.utils.DbUtils import DbUtils
from MAD.views.views_search import *
from MAD.views.views_reports import *
from MAD.views.views_change_requests import *
from MAD.views.views_authentication import *
from MAD.views.views_domains import *
from MAD.utils.GenericResponseObj import *
from MAD.utils.geocoder.base import Geocoder
from MAD.exceptions import ConcurrentUpdateWarning
from MAD.models.ModelUtils import ModelUtils
from MAD.models.models import Parcels, ParcelsProvisioning, AddressBase, VwAddressesFlatNative
from django.db import transaction
from django.contrib.gis.geos.geometry import GEOSGeometry
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.conf import settings as settings
from django.utils.simplejson.encoder import JSONEncoder
from django.db.models import F
from datetime import datetime
from django.views.decorators.cache import cache_control
from collections import OrderedDict
import logging
logger = logging.getLogger('EAS')

def Js(request):
    #Response.AppendHeader("Content-Encoding", "gzip");
    response = HttpResponse(mimetype='text/javascript')
    response['Content-Encoding'] = 'gzip'
    response['Content-Disposition'] = 'attachment; filename=somefilename.pdf'

    return response


@cache_control(no_cache=True)
def Index(request, mode = ''):
    debug = False
    if mode == 'debug':
        debug = True
    if mode == 'build':
        build_scripts()

    applicationInfo = {
        'name': settings.APPLICATION_NAME,
        'nameShort': settings.APPLICATION_NAME_SHORT,
        'env': settings.APPLICATION_ENV,
        'version': settings.APPLICATION_VERSION,
        'dataCenter': settings.APPLICATION_DATACENTER,
        'buildId': settings.BUILD_ID
    }

    return render_to_response('index.html', {}, context_instance=RequestContext(request, {'debug': debug, 'applicationInfo': applicationInfo}))


def findAddressCandidates(request, addressString=None, zipCodeString=None):

    # when we test test request is None and we pass in addressString and zipCodeString
    # follow OGC geocoding standard
    # RE http://www.opengeospatial.org/standards/requests/89


    if request:
        # TODO - if its not asking for JSON tell them "format unsupported" or something
        # json only
        #format = request.GET.get('f')
        addressString = request.GET.get('Address')
        zipCodeString = request.GET.get('Zip')


    def buildAddressDict(baseAddress=None, units=None, flatAddressParcels=None):
        pointGeometry = baseAddress.geometry.transform(4326, clone = True)
        ogcAddressStrings = []
        ogcAddressStrings.append(str(baseAddress.base_address_num))
        ogcAddressStrings.append((baseAddress.base_address_suffix if baseAddress.base_address_suffix else ""))
        ogcAddressStrings.append(baseAddress.street_name)
        ogcAddressStrings.append(baseAddress.street_type if baseAddress.street_type else "")
        ogcAddressStrings.append(baseAddress.street_post_direction if baseAddress.street_post_direction else "")
        ogcAddressString = " ".join(ogcAddressStrings)
        # remove extra white spaces
        ogcAddressString = " ".join(ogcAddressString.split())
        parcelViews = []
        for flatAddressParcel in flatAddressParcels:
            od = OrderedDict([
                ('map_blk_lot', flatAddressParcel.map_blk_lot),
                ('blk_lot', flatAddressParcel.blk_lot),
                ('unit_num', flatAddressParcel.unit_num),
                ('date_map_add', flatAddressParcel.parcel_date_map_add),
                ('date_map_drop', flatAddressParcel.parcel_date_map_drop),
                ('address_base_flg', flatAddressParcel.address_base_flg)
            ])
            parcelViews.append(od)
        unitViews = []
        for unit in units:
            unitViews.append({
                'unit_num': unit.unit_num
            })
        ogcAttributes = OrderedDict([
            ('base_address_num', baseAddress.base_address_num),
            ('base_address_suffix', baseAddress.base_address_suffix),
            ('street_name', baseAddress.street_name),
            ('street_type', baseAddress.street_type),
            ('street_post_direction', baseAddress.street_post_direction),
            ('zipcode', baseAddress.zipcode),
            ('parcels', parcelViews),
            ('units', unitViews)
        ])
        return OrderedDict([
            ('address', ogcAddressString),
            ('location', { "x" : pointGeometry.x, "y" : pointGeometry.y }),
            ('score', baseAddress.score),
            ('attributes', ogcAttributes),
        ])


    def buildResponseDict(addressTuples=()):
        candidates = []
        for addressTuple in addressTuples:
            baseAddress = addressTuple[0]
            units = addressTuple[1]
            flatAddressParcels = addressTuple[2]
            addressDict = buildAddressDict(baseAddress=baseAddress, units=units, flatAddressParcels=flatAddressParcels)
            candidates.append(addressDict)
        return OrderedDict([
            ('spatialReference', {'wkid' : 4326}),
            ('candidates', candidates)
        ])


    def getAddressTuple(baseAddress):
        units = Addresses.objects.filter(address_base__address_base_id = baseAddress.address_base_id)\
                                            .filter(unit_num__isnull = False)\
                                            .filter(retire_tms__isnull = True)\
                                            .order_by("unit_num")
        flatAddressParcels = VwAddressesFlatNative.objects.filter(address_base_id = baseAddress.address_base_id)\
                                                        .filter(address_x_parcel_id__isnull = False)\
                                                        .filter(address_x_parcel_retire_tms__isnull = True)
        return baseAddress, units, flatAddressParcels


    def main(addressString, zipCodeString):

        if not addressString:
            return buildResponseDict()

        baseAddresses = Geocoder.geocode(addressString=addressString, zipCodeString=zipCodeString, useType="BATCH")

        # NOTE
        # Potential performance issue in this loop if we have lots of baseAddresses.
        # It is unlikely that any given call will result in more than a few baseAddresses.
        addressTuples = []
        for baseAddress in baseAddresses:
            addressTuple = getAddressTuple(baseAddress)
            addressTuples.append(addressTuple)

        return buildResponseDict(addressTuples=addressTuples)


    responseDict = main(addressString, zipCodeString)


    # todo - refactor - wrong smell
    # we do this odd thing to support unit testing
    if request is None:
        return responseDict
    from django.core.serializers.json import DjangoJSONEncoder
    jsonData =  json.dumps(responseDict, cls=DjangoJSONEncoder, indent=4, ensure_ascii=False)
    return HttpResponse(jsonData)




@transaction.commit_manually
def provisionParcel(request, block, lot, x, y):
    logger.info('provisionParcel(block=%s, lot=%s, lon=%s, lat=%s)' % (block, lot, x, y))
    response = GenericResponseObj('programming error')
    try:
        user = request.user
        if not user.is_authenticated():
            transaction.rollback()
            message = 'cannot provision parcel because user is not logged in'
            logger.info(message)
            response.message = message
        else:

            # build point geometry
            wktPoint = 'SRID=900913;POINT(%s %s)'% (x, y)
            geosPoint = GEOSGeometry(wktPoint)
            geosPoint.transform(2227)

            # get base parcel at this location; exclude service parcel; exclude retired
            parcels = Parcels.objects.filter(geometry__intersects=geosPoint).filter(map_blk_lot=F('blk_lot')).exclude(blk_lot = '0000000').filter(date_map_drop__isnull = True)
            # If we have more than 1 we disallow provisioning here.
            if parcels.count() > 1:
                raise ValidationError('Parcel provisiong is not allowed at this location because mulitiple base parcels exist at this location.  Please contact your system administrator if you believe you have received this message in error.')
            # If we have less than one, we are either in the water or in the service parcel.
            if parcels.count() < 1:
                raise ObjectDoesNotExist('no base parcel exists at this location')

            # we know we have a single base parcel
            baseParcel = parcels[0]

            # parcel
            blockLot = block+lot
            newParcel = Parcels(blk_lot=blockLot, block_num=block, lot_num=lot, map_blk_lot=baseParcel.blk_lot, geometry=baseParcel.geometry, create_tms=datetime.now(), update_tms=datetime.now())
            newParcel.validate()
            if baseParcel.block_num != block:
                raise ValidationError('block number must be %s' % baseParcel.block_num)
            newParcel.save()

            # parcel provisioning
            createUser = request.user
            provisionedParcel = ParcelsProvisioning(parcel=newParcel, create_user=createUser)
            provisionedParcel.save()
            transaction.commit()

        response.status_code = 0
        response.success = True

    except ObjectDoesNotExist, e:
        transaction.rollback()
        logger.warning(e.message)
        response.success = False
        response.message = e.message
    except ValidationError, e:
        message = ', '.join(e.messages)
        transaction.rollback()
        logger.warning(message)
        response.success = False
        response.message = message
    except Exception, e:
        transaction.rollback()
        logger.critical(e.message)
        response.message = 'an exception has occurred - please contact support'
        response.success = False

    return HttpResponse(str(response))
