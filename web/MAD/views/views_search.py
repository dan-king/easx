from django.http import HttpResponse
from django.contrib.gis.geos import GEOSGeometry, Polygon, WKTReader
from MAD.models.models import *
from MAD.utils.geojson_encode import *
from MAD.utils.ExtJSONResultsEncoder import *
from MAD.utils.FilterByModel import *
from MAD.utils.geocoder.base import Geocoder
from MAD.utils.geocoder.parsing import ParsingError
from MAD.utils.GenericResponseObj import *
from django.conf import settings as settings
from MAD.models.ModelUtils import ModelUtils
import logging
from django.views.decorators.cache import never_cache
from django.template.defaultfilters import date as templateDate

logger = logging.getLogger('EAS')


@never_cache
def AddressSearch(request):

    addressString = request.GET.get('query')
    includeRetired = False
    if request.GET.get('includeRetired') == "true":
        includeRetired = True

    if not addressString:
        return HttpResponse(ExtJSONResultsEncoder('', 0, request))

    addresses = Geocoder.geocode(addressString=addressString, useType="INTERACTIVE", includeRetired=includeRetired)

    addressCount = len(addresses)
    start = int(request.GET.get('start'))
    limit = int(request.GET.get('limit'))
    end = int(start) + int(limit)
    addressesSubset = addresses[start:end]
    for address in addressesSubset:
        address.validationWarnings = VwInvalidAddresses.objects.filter(address_base_id = address.address_base_id)
    #logger.info(geojson_encode(addressesSubset))
    return HttpResponse(ExtJSONResultsEncoder(geojson_encode(addressesSubset), addressCount, request))


@never_cache
def AddressBBoxWFS(request):
    logger.info('AddressBBoxWFS BEGIN')
    bbox = request.GET.get('bbox')
    includeRetired = False
    if request.GET.get('includeRetired') == 'true':
        includeRetired = True
    SRS = request.GET.get('SRS').replace('EPSG:', '')
    maxfeatures = request.GET.get('maxfeatures')
    # e.g. -13651385.720232338,4540053.731394596,-13608580.984400306,4552360.092946306
    bboxList = bbox.split(',')
    polygon = Polygon.from_bbox(bboxList)
    poly = GEOSGeometry('SRID=' + SRS + ';' + polygon.wkt)
    poly.transform(900913)

    addressQuerySet = VwBaseAddresses.objects.filter(geometry__intersects=poly).order_by('geometry')[0:maxfeatures]

    if not includeRetired:
        # exclude retired
        addressQuerySet = addressQuerySet.exclude(retire_tms__isnull = False)

    zoom = int(request.GET.get('zoomLevel'))
    zoom += 12
    clusterList = VwBaseAddresses.querysetToClusterList(addressQuerySet, zoom)
    
    logger.info('AddressBBoxWFS END')
    return HttpResponse('{results: ' + geojson_encode(clusterList) + '}')


def ApnSearch(request):
    query = request.GET.get('query')
    exact = False
    if request.GET.get('exact') == "true":
        exact = True

    includeRetired = False
    if request.GET.get('includeRetired') == "true":
        includeRetired = True

    start = int(request.GET.get('start'))
    limit = int(request.GET.get('limit'))
    # exclude service parcel
    if not query:
        return HttpResponse(ExtJSONResultsEncoder(geojson_encode([]), 0, request))

    parcels = Parcels.objects.exclude(blk_lot = '0000000').order_by('blk_lot')

    if exact:
        parcels = parcels.filter(blk_lot = query)
    else:
        parcels = parcels.filter(blk_lot__istartswith = query)

    if includeRetired:
        pass
    else:
        # exclude retired
        parcels = parcels.exclude(date_map_drop__isnull = False)
    parcelCount = parcels.count()
    end = start + limit
    parcels = parcels[start:end]
    parcelDictReturn = Parcels.mergeWithParcelsProvisioning(parcels)
    parcelDictReturn = sorted(parcelDictReturn, key=lambda k: k['blk_lot'])

    return HttpResponse(ExtJSONResultsEncoder(geojson_encode(parcelDictReturn), parcelCount, request))


def pointToParcels(request):
    pointWKT = request.GET.get('query')
    includeRetired = False
    if request.GET.get('includeRetired') == 'true':
        includeRetired = True

    GR = GenericResponseObj('error in pointToParcels')
    start = int(request.GET.get('start'))
    limit = int(request.GET.get('limit'))
    try:
        wkt = WKTReader()
        point = wkt.read(pointWKT)
        parcels = Parcels.objects.filter(geometry__intersects=point).exclude(blk_lot = '0000000').order_by('blk_lot')
        if includeRetired:
            pass
        else:
            # exclude retired
            parcels = parcels.exclude(date_map_drop__isnull = False)
        end = start + limit
        results = parcels[start:end]
        results = Parcels.mergeWithParcelsProvisioning(results)
        results = sorted(results, key=lambda k: k['blk_lot'])
    except Exception, e:
        logger.critical(e.message)
        return HttpResponse(str(GR))
    return HttpResponse(ExtJSONResultsEncoder(geojson_encode(results), parcels.count(), request))


def addressToParcels(request):
    addressBaseId = request.GET.get('query')
    includeRetired = False
    if request.GET.get('includeRetired') == 'true':
        includeRetired = True

    GR = GenericResponseObj('error in addressToParcels')
    start = int(request.GET.get('start'))
    limit = int(request.GET.get('limit'))
    try:
        addressBase = AddressBase.objects.filter(address_base_id = addressBaseId )
        addresses = Addresses.objects.filter(address_base = addressBase)
        if not includeRetired:
            addresses = addresses.filter(retire_tms__isnull = True)

        axps = AddressXParcels.objects.filter(address__in = addresses)
        if not includeRetired:
            axps = axps.filter(retire_tms__isnull = True)

        parcelIds = [axp.parcel_id for axp in axps]
        parcels = Parcels.objects.filter(parcel_id__in = parcelIds).exclude(blk_lot = '0000000').order_by('blk_lot')
        end = start + limit
        results = parcels[start:end]
        results = Parcels.mergeWithParcelsProvisioning(results)
        results = sorted(results, key=lambda k: k['blk_lot'])
    except Exception, e:
        logger.critical(e.message)
        return HttpResponse(str(GR))
    return HttpResponse(ExtJSONResultsEncoder(geojson_encode(results), parcels.count(), request))


def parcelToAddresses(request):
    parcelId = request.GET.get('query')
    includeRetired = False
    if request.GET.get('includeRetired') == 'true':
        includeRetired = True

    GR = GenericResponseObj('error in parcelToAddresses in views_search.py')
    start = int(request.GET.get('start'))
    limit = int(request.GET.get('limit'))
    try:
        parcel = Parcels.objects.get(parcel_id = parcelId)
        addressXParcels = AddressXParcels.objects.filter(parcel = parcel)
        if not includeRetired:
            addressXParcels = addressXParcels.filter(retire_tms__isnull = True)

        addressBaseIdSet = set()
        for axp in addressXParcels:
            if includeRetired:
                addressBaseIdSet.add(axp.address.address_base_id)
            else:
                if axp.address.retire_tms is None:
                    addressBaseIdSet.add(axp.address.address_base_id)

        querySet = VBaseAddressSearch.objects.filter(address_base_id__in = addressBaseIdSet).filter(street_category = 'MAP')
        end = start + limit
        results = querySet[start:end]
        for address in results:
            address.validationWarnings = VwInvalidAddresses.objects.filter(address_base_id = address.address_base_id)
            # Get the data source of each base address
            address.data_source_base = BaseAddressDataSource().filter(address_base_id = address.address_base_id)
    except Exception, e:
        logger.critical(e.message)
        return HttpResponse(str(GR))
    return HttpResponse(ExtJSONResultsEncoder(geojson_encode(results), querySet.count(), request))

def getParcelByApn(request, apn):
    # https://sfgovdt.jira.com/wiki/display/MAD/Parcel+Service
    # This is a general purpose parcel service that may be used by clients outside of the web application.
    # There is some overlap between this function and the ApnSearch function above.
    # Here we return a single record and do not support paging.
    # ApnSearch returns N records and supports paging.
    responseObject = GenericResponseObj('')
    responseObject.success = True
    responseObject.status_code = 0
    try:
        parcels = Parcels.objects.filter(blk_lot = apn).exclude(blk_lot = '0000000')
        if parcels.count() == 0:
            responseObject.returnObj = []
            return HttpResponse(geojson_encode(responseObject))
        parcelObject = parcels[0]
        centroid = None
        srid = None
        if parcelObject.geometry:
            centroid = parcelObject.geometry.point_on_surface
            srid = parcelObject.geometry.srs.srid
        # Expose only the values that most folks use.
        parcelDict = parcels.values('date_map_add', 'date_rec_drop', 'geometry', 'block_num', 'lot_num', 'blk_lot', 'map_blk_lot')[0]
        parcelDict['centroid'] = centroid
        parcelDict['srid'] = srid
    except Exception, e:
        logger.critical(e.message)
        responseObject.success = False
        responseObject.status_code = -1
        responseObject.message = str(e)
        return HttpResponse(str(responseObject))
    responseObject.returnObj = [parcelDict]
    return HttpResponse(geojson_encode(responseObject))

@never_cache
def AddressInfo(request, addressId):
    from django.core.exceptions import ObjectDoesNotExist
    logger.info('AddressInfo() begin')
    baseAddressSummary = None
    response = GenericResponseObj('')
    try:
        baseAddressSummary = VBaseAddressSearch.objects.filter(street_category = 'MAP').get(address_base_id = addressId)

        # Get a the list of addresses and address_x_parcels and prepare summary

        ## Patch - http://code.google.com/p/eas/issues/detail?id=417
        # We want last change info (user and date) across the entire address.
        # This probably needs to be reworked a bit - possibly down to the data model.
        # get the base address
        # ask the base address for the most recently changed change request
        # Get the user and the dates from the change request

        addressBase = AddressBase.objects.get(pk = baseAddressSummary.address_base_id)
        mostRecentChangeRequest = addressBase.getMostRecentChangeRequest()

        # todo - model needs work (http://code.google.com/p/eas/issues/detail?id=422)
        baseAddressSummary.last_change_date = templateDate(mostRecentChangeRequest.create_tms, settings.DATETIME_FORMAT)
        baseAddressSummary.lastChangeRequestSummary = mostRecentChangeRequest.getSummary()
        baseAddressSummary.create_date = templateDate(baseAddressSummary.create_tms, settings.DATETIME_FORMAT)
        baseAddressSummary.retire_date = templateDate(baseAddressSummary.retire_tms, settings.DATETIME_FORMAT)
        baseAddressSummary.coords = { 'easting' : round(baseAddressSummary.geometry.transform(2227, True).x, 3),
                               'northing' : round(baseAddressSummary.geometry.transform(2227, True).y, 3),
                               'long' : round(baseAddressSummary.geometry.transform(4326, True).x, 5),
                               'lat' : round(baseAddressSummary.geometry.transform(4326, True).y, 5) }
        # want to call Aliases.objects.filter(address_base_id = addrBaseId), but this doesn't seem to work?? - anp
        baseAddressSummary.aliases = ManagerAliases().filter(address_base_id = addressId)

        # Get the data source of the base address
        baseAddressSummary.data_source_base = BaseAddressDataSource().filter(address_base_id = addressId)

        # Get the data source(s) of the unit address(es)
        baseAddressSummary.data_source_units = UnitAddressDataSource().filter(address_base_id = addressId)

        # Get the "parcel footprint" for display on the map.
        axps = []
        if baseAddressSummary.retire_tms:
            # base address is retired - get the footprint as of the retire_tms
            addresses = Addresses.objects.filter(address_base = addressBase).filter(retire_tms = baseAddressSummary.retire_tms)
            axps = AddressXParcels.objects.filter(address__in = addresses).filter(retire_tms = baseAddressSummary.retire_tms)
        else:
            # base address is active
            addresses = Addresses.objects.filter(address_base = addressBase).filter(retire_tms__isnull = True)
            axps = AddressXParcels.objects.filter(address__in = addresses).filter(retire_tms__isnull = True)

        parcels = Parcels.objects.filter(parcel_id__in = [axp.parcel_id for axp in axps]).defer('geometry')
        apns = [parcel.blk_lot for parcel in parcels]
        # debug only
        #for apn in apns:
        #    logger.info(apn)
        baseAddressSummary.multiPolygon = None
        if apns:
            multiPolygon = ModelUtils.getBaseParcelGeometryWithApns(apns)
            baseAddressSummary.parcelFootprintGeometry = multiPolygon

        baseAddressSummary.streetSegmentGeometry = str(addressBase.street_segment.geometry)

        baseAddressSummary.validationWarnings = VwInvalidAddresses.objects.filter(address_base_id = addressId)

        # This payload is an embellished "VBaseAddressSearch".
        response.returnObj = baseAddressSummary
        response.status_code = 10
        response.success = True
    except ObjectDoesNotExist, e:
        response.message = 'The requested address does not exist, retired or not. address_base_id: %s' % addressId
        response.success = False
    except Exception, e:
        raise

    json = geojson_encode(response)
    #logger.info(json)
    logger.info('AddressInfo() end')
    return HttpResponse(json)

