
import os, sys

# Add PYTHON_EGG_CACHE
os.environ['PYTHON_EGG_CACHE'] = '/var/www/html/python-eggs'

# Add to PYTHONPATH whatever you need
sys.path.append('/var/www/html/eas')

# Set DJANGO_SETTINGS_MODULE
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

## Create the application for mod_wsgi
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
