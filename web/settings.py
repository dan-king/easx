
# A good source for understanding these settings is "The Definitive Guide to Django"

import os
import sys
import logging
import logging.handlers
from ctypes.util import find_library


##### application info
# This information is displayed in the UI; to see how this works, read the following:
#   releases/bin/deploy.sh
#   web/MAD/templates/index.html
#   web/views/views.py.index
#   Media/js/MAD/MAD-App.js
APPLICATION_NAME = 'Enterprise Addressing System'
APPLICATION_NAME_SHORT = 'eas'
APPLICATION_ENV = 'DESKTOP'
#####

ADMINS = (
    ('Paul McCullough - SFGOV', 'paul.mccullough@sfgov.org'),
    ('Jeff Johnson - SFGOV', 'jeffrey.johnson@sfgov.org'),
    ('Paul E. McCullough - GMAIL', 'p.e.mccullough@gmail.com'),
    ('Jeff A. Johnson - GMAIL', 'jeff.johnson.a@gmail.com'),
    ('Christophe Pettus - PGX', 'christophe.pettus@pgexperts.com'),
    ('Rob Gaston - FARGEO', 'rgaston@fargeo.com'),
)
MANAGERS = ADMINS

DEFAULT_FROM_EMAIL = 'noreply@sfgov.org'
DATABASE_ENGINE = 'postgresql_psycopg2'
TIME_ZONE = 'America/Los_Angeles'
LANGUAGE_CODE = 'en-us'
SITE_ID = 1
USE_I18N = False


if os.name == 'posix':
    try:
        KEY = 'EAS_REPOS_HOME'
        EAS_REPOS_HOME = os.environ[KEY]
        sys.path.append(os.path.join(EAS_REPOS_HOME, 'config', 'web'))
        MAD_ROOT = os.path.join(EAS_REPOS_HOME, 'eas', 'web')
        MEDIA_ROOT = os.path.join(EAS_REPOS_HOME, 'eas', 'Media')
        # Accept default for ADMIN_MEDIA_PREFIX
        HTCONF_FILENAME = MAD_ROOT + '/web/mad.htconf.live'
    except Exception:
        # If EAS_REPOS_HOME variable is not present, we assume we are in a deployed environment
        MAD_ROOT = '/var/www/html/eas_live'
        MEDIA_ROOT = MAD_ROOT + '/Media/'
        ADMIN_MEDIA_PREFIX = '/Media/admin/'
        HTCONF_FILENAME = MAD_ROOT + '/mad.htconf.live'
        pass
    GEOS_LIB_NAMES = ['geos_c', 'GEOS']
    TEMPLATE_DIRS = (MAD_ROOT + '/MAD/templates', MAD_ROOT + '/MAD/templates/xml',)
elif os.name == 'nt':
    try:
        KEY = 'EAS_REPOS_HOME'
        EAS_REPOS_HOME = os.environ[KEY]
    except Exception:
        sys.stdout.write('missing system variable: %s\n' % KEY)
        sys.exit(-1)
    sys.path.append(os.path.join(EAS_REPOS_HOME, 'config', 'web'))
    MAD_ROOT = os.path.join(EAS_REPOS_HOME, 'eas', 'web')
    MEDIA_ROOT = os.path.join(EAS_REPOS_HOME, 'eas', 'Media')
    # Accept default for ADMIN_MEDIA_PREFIX
    TEMPLATE_DIRS = (MAD_ROOT + '/MAD/templates', MAD_ROOT + '/MAD/templates/xml',)
    GEOS_LIB_NAMES = ['geos_c']
    HTCONF_FILENAME = MAD_ROOT + '/web/mad.htconf.live'
else:
    raise ImportError('Unsupported OS "%s"' % os.name)


# location of this import is critical
from settings_env.live.settings_deploy import *


APP_LOG_FILENAME            = MAD_ROOT + '/eas.log'

XMIT_LOG_FILENAME           = MAD_ROOT + '/eas_xmit.log'
XMIT_RUNNING_SEMAPHORE      = MAD_ROOT + '/eas_xmit_running'
XMIT_STOP_REQUEST_SEMAPHORE = MAD_ROOT + '/eas_xmit_stop_request'
XMIT_POLLING_INTERVAL = 10
XMIT_HOST = 'dbiweb.sfgov.org'
XMIT_PORT = 80
# The following are in settings_deploy.py
# XMIT_PATH

XMIT_ACCELA_LOG_FILENAME           = MAD_ROOT + '/eas_xmit_accela.log'
XMIT_ACCELA_RUNNING_SEMAPHORE      = MAD_ROOT + '/eas_xmit_accela_running'
XMIT_ACCELA_STOP_REQUEST_SEMAPHORE = MAD_ROOT + '/eas_xmit_accela_stop_request'
XMIT_ACCELA_POLLING_INTERVAL = 10
# The following are in settings_deploy.py
# XMIT_ACCELA_HOST
# XMIT_ACCELA_PORT
# XMIT_ACCELA_PATH
# XMIT_ACCELA_USER
# XMIT_ACCELA_PASSWORD

GEOS_LIBRARY_PATH = None
# Taken from libgeos.py
for geos_lib_name in GEOS_LIB_NAMES:
    geos_library_path = find_library(geos_lib_name)
    if not geos_library_path is None:
        GEOS_LIBRARY_PATH = geos_library_path
        break

if GEOS_LIBRARY_PATH is None:
    raise EnvironmentError('geos c library not found')

MEDIA_URL = 'Media/'

SECRET_KEY = '$$_ubp&8ognlj%qs83f%&t!on9ed$!478z@m9(_z$%^+onpdz4'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware'
)

VIRTUAL_DIR = ''

ROOT_URLCONF = 'urls'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.gis',      
    'MAD',
)


# This line is necessary to use the ebpub lib (Every Block), which we use for address parsing.
# todo - maybe we just need to add this to our app list?
METRO_LIST = ()

LOGGING_MAX_BYTES = 10485760
LOGGING_BACKUP_COUNT = 2
LOGGING_TO_ADDRESSES = [] # minor abuse of "constant"

for tuple in ADMINS:
    LOGGING_TO_ADDRESSES.append(tuple[1])

LOGGING = {
    'version': 1,
    'formatters': {
        'simple': {
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        }
    },

    'handlers': {
        'XMIT': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': 'INFO',
            'formatter': 'simple',
            'filename': XMIT_LOG_FILENAME,
            'backupCount': LOGGING_BACKUP_COUNT,
            'maxBytes': LOGGING_MAX_BYTES
        },
        'XMIT_ACCELA': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': 'INFO',
            'formatter': 'simple',
            'filename': XMIT_ACCELA_LOG_FILENAME,
            'backupCount': LOGGING_BACKUP_COUNT,
            'maxBytes': LOGGING_MAX_BYTES
        },
        'EAS': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': 'INFO',
            'formatter': 'simple',
            'filename': APP_LOG_FILENAME,
            'backupCount': LOGGING_BACKUP_COUNT,
            'maxBytes': LOGGING_MAX_BYTES
        }
    },
    'loggers': {
        'XMIT': {
            'handlers': ['XMIT'],
            'level': 'INFO'
        },
        'XMIT_ACCELA': {
            'handlers': ['XMIT_ACCELA'],
            'level': 'INFO'
        },
        'EAS': {
            'handlers': ['EAS'],
            'level': 'INFO'
        }
    }

}

DATETIME_FORMAT = 'm-d-Y H:i:s'
SFGIS_URL = 'http://gispub02.sfgov.org/website/sfparcel/showlotinfo.asp?LotIDValue='

TEST_RUNNER = "test_suites.runAll"

