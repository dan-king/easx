from django.conf.urls.defaults import *
from django.conf import settings
from MAD.views import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^'+settings.VIRTUAL_DIR + 'admin/', include(admin.site.urls)),

    # MAD APPLICATION:
    (r'^'+settings.VIRTUAL_DIR, include('MAD.urls'))

)
