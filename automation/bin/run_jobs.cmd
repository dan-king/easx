@echo off


If [%1]==[] goto INPUT_ERROR
If [%2]==[] goto INPUT_ERROR


set PYTHON_EXE="C:\python25\python.exe"
set PYTHON_SCRIPT="C:\svn\MAD\trunk\etl\src\job.py"
set JOBS=bay_area_cities, coast, county_line, ocean_bay_labels, poi, parcels, streets


:CHECK_ENV
set ENV=%1
IF %ENV%==DEV goto CHECK_ACTION
IF %ENV%==QA goto CHECK_ACTION
IF %ENV%==PROD goto CHECK_ACTION
goto INPUT_ERROR

:CHECK_ACTION
set ACTION=%2
IF %ACTION%==STAGE goto PROCESS_JOBS
IF %ACTION%==INIT goto PROCESS_JOBS
IF %ACTION%==COMMIT goto PROCESS_JOBS
goto INPUT_ERROR


:PROCESS_JOBS
IF %ACTION%==STAGE (
    FOR %%J IN (%JOBS%) DO %PYTHON_EXE% %PYTHON_SCRIPT% --env %ENV% --job %%J --action STAGE
    goto END_OF_SCRIPT
)
IF %ACTION%==INIT (
    FOR %%J IN (%JOBS%) DO %PYTHON_EXE% %PYTHON_SCRIPT% --env %ENV% --job %%J --action INIT
    goto END_OF_SCRIPT
)
IF %ACTION%==COMMIT (
    FOR %%J IN (%JOBS%) DO %PYTHON_EXE% %PYTHON_SCRIPT% --env %ENV% --job %%J --action COMMIT
    goto END_OF_SCRIPT
)

:INPUT_ERROR
echo ENV is %ENV%
echo ENV must in (DEV, QA, PROD)
echo ACTION is %ACTION%
echo ACTION must be in (STAGE, INIT, COMMIT)


:END_OF_SCRIPT
set ENV=
set PYTHON_EXE=
set PYTHON_SCRIPT=
set JOBS=
set ACTION=
