
from config.live import config_notifications
from connection_organizer import *
import smtplib
from email.mime.text import MIMEText
from email.MIMEBase import MIMEBase
from email.mime.multipart import MIMEMultipart

from email import Encoders

def sendEmail(env=None, subject=None, text=None, html=None, attachFiles=(), sender=None, recipientList=()):

    msg = MIMEMultipart('alternative')
    msg['Subject'] = 'Enterprise Addressing System - %s - %s' % (env, subject)

    if text:
        part = MIMEText(text, 'plain')
        msg.attach(part)

    if html:
        part = MIMEText(html, 'html')
        msg.attach(part)

    for attachFile in attachFiles:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(attachFile,"rb").read() )
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(attachFile))
        msg.attach(part)

    connectionOrganizer = ConnectionOrganizer()
    smtpHost = connectionOrganizer.smtpConnections[env]['host']
    smtpConnection = smtplib.SMTP(smtpHost)
    smtpConnection.sendmail(sender, recipientList, msg.as_string())
    smtpConnection.quit()
