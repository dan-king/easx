
import urllib, httplib
import simplejson as json
import datetime
import time

class FmeServerHelper:

    def __init__(self, host=None, user=None, password=None):
        self.host = host
        self.user = user
        self.password = password
        self.token = None
        parameters = {
            'user' : user,
            'password' : password,
            'expiration' : '10',
            'timeunit' : 'minute'
        }
        url = '/fmetoken/generate?'
        self.token = self.getResponse(url, parameters)

    @staticmethod
    def getDateTimeFormatStrings():
        return (
            '%a %b %d %H:%M:%S PST %Y',
            '%a %b %d %H:%M:%S PDT %Y',
            '%Y-%m-%dT%H:%M:%S'
        )

    @staticmethod
    def parseDateTimeString(dateTimeString):
        capturedException = None
        for formatString in FmeServerHelper.getDateTimeFormatStrings():
            try:
                dateTimeObject = datetime.datetime.strptime(dateTimeString, formatString)
                return dateTimeObject
            except Exception, e:
                capturedException = e
        raise capturedException

    @staticmethod
    def getJobId(job=None):
        if job is None:
            return None
        return job['ID']

    @staticmethod
    def getJobStatus(job=None):
        if job is None:
            return None
        return job['jobStatus']

    def getResponse(self, url=None, parameters=None):
        if parameters is None:
            parameters = {}
        if self.token:
            parameters['token'] = self.token
        contentString = ""
        httpConnection = httplib.HTTPConnection(self.host)
        try:
            encodedParameters = urllib.urlencode(parameters)
            httpConnection.request("GET", url + encodedParameters)
            response = httpConnection.getresponse()
            if response.status != 200:
                raise IOError('request failed - status: %s - message: %s' % (str (response.status), str(response.msg)))
            contentString = response.read()
        except Exception, e:
            raise
        finally:
            httpConnection.close()
        return contentString

    def doesJobExist(self, jobDict): 
        url = '/fmerest/repositories/%s.json?' % ( jobDict['repository'] )
        response = json.loads(self.getResponse(url=url))
        workspaces = response['serviceResponse']['repository']['workspaces']['workspace']
        exists = False
        for workspace in workspaces:
            if (workspace['name'] == '%s.fmw' % ( jobDict['workspace'] )):
                exists = True
        return exists

    def isJobRunning(self, jobDict):
        url = '/fmerest/jobs/running.json?'
        return len(self.getJobs(url, jobDict)) > 0

    def isJobQueued(self, jobDict):
        url = '/fmerest/jobs/queued.json?'
        return len(self.getJobs(url, jobDict)) > 0

    def getLatestCompletedJob(self, jobDict):

        lastJob = None
        url = '/fmerest/jobs/completed.json?'
        jobList = self.getJobs(url, jobDict)

        for job in jobList:
            jobDate = FmeServerHelper.parseDateTimeString(job['dateFinished'])
            if lastJob is None:
                lastJob = job
            else:
                lastJobDate = FmeServerHelper.parseDateTimeString(lastJob['dateFinished'])
                if jobDate > lastJobDate:
                    lastJob = job

        return lastJob

    def getJobs(self, url, jobDict):
        jobs = []
        response = json.loads(self.getResponse(url=url))
        allJobs = response['serviceResponse']['jobs']['job']
        for job in allJobs:
            workspacePath = job['transformationRequest']['workspacePath'][1:-1] # remove the qotes
            if workspacePath == '%s/%s/%s.fmw' % ( jobDict['repository'], jobDict['workspace'], jobDict['workspace'] ):
                jobs.append(job)
        return jobs
