
# To organize the db connections, we use a "connection group".
# A connection group is a group of DB connections that typically includes 3 environments: DEV, QA, PROD
# For the ssh connections we may use the Null Object Pattern.

import db
from db import *
from db_ssh import *
from ssh import *
from config.live.config_connections import *

class ConnectionOrganizer:

    def addConnectionGroup(self, groupName=None, dbConnGroup=None, sshConnDefs=None):
        for env in self.environments:
            connectionGroup = ConnectionGroup(
                name = groupName,
                env = env,
                sshDbPair = SshDbConnectionPair( dbConnDef=dbConnGroup[env], sshConnDef=sshConnDefs[env] )
            )
            if not self.connectionGroups.has_key(connectionGroup.name):
                self.connectionGroups[connectionGroup.name] = {}
            self.connectionGroups[connectionGroup.name][connectionGroup.env] = connectionGroup


    def __init__(self, type=None, arg=None, connectionKeys=()):
        self.connectionGroups = {}
        self.geoserverConnections = {}
        self.smtpConnections = {}
        self.environments = None
        ConnectionConfigurations(self)

    def buildEnvDict(self, values):
        if len(values) != len(self.environments):
            raise Exception( 'error in connection configuration - missing environment or value \nEnvironments%s\nValues:%s' % (str(self.environments), str(values)) )
        d = dict()
        for key, value in zip(self.environments, values):
            if key is None:
                raise Exception('error in connection configuration - missing key')
            if value is None:
                raise Exception('error in connection configuration - missing value')
            d[key] = value
        return d
