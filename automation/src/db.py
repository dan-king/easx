import sys
import ssh
import os.path

# If this gets more complicated consider creationg a separate class for each vendor
# todo - decide: do away with the useSdePort approach and instead define separate connections? seems like a wart.

class Types:
    POSTGRESQL = 'POSTGRESQL'
    DB2 = 'DB2'
    ORACLE = 'ORACLE'
    MSSQL = 'MSSQL'

class SpatialTypes:
    POSTGIS = 'POSTGIS'
    SDE = 'SDE'
    ORACLE = 'ORACLE'

TYPES = Types()
SPATIAL_TYPES = SpatialTypes()


class DbConnectionDefinition:
    def __init__(self, type=None, spatialType=None, host=None, port=None, sdePort=None, db=None, user=None, password=None, version=None, useSdePort=False ):
        # host_type is (sde, postgres, oracle, etc)
        self.type = type
        self.spatialType = spatialType
        self.host = host
        self.port = port
        self.sdePort = sdePort
        self.db = db
        self.user = user
        self.password = password
        self.version = version
        # set as needed - this is a bit of a "wart"
        self.useSdePort = useSdePort

    def __str__(self):
        return  'host: %s port: %s db: %s user: %s password: %s version: %s sdePort: %s' % (self.host, self.port, self.db, self.user, self.password, self.version, self.sdePort)

    def toStringforLogging(self):
        return  'host: %s port: %s db: %s ' % (self.host, self.port, self.db)

    def __hash__(self):
        h = hash(self.type)
        h += hash(self.spatialType)
        h += hash(self.host)
        h += hash(self.port)
        h += hash(self.sdePort)
        h += hash(self.db)
        h += hash(self.user)
        h += hash(self.password)
        h += hash(self.version)
        return h

    def __eq__(self, other):
        if self.type != other.type: return False
        if self.spatialType != other.spatialType: return False
        if self.host != other.host: return False
        if self.port != other.port: return False
        if self.sdePort != other.sdePort: return False
        if self.db != other.db: return False
        if self.user != other.user: return False
        if self.password != other.password: return False
        if self.version != other.version: return False
        if self.useSdePort != other.useSdePort: return False
        return True

    def copy(self, **kwargs):
        newInstance = DbConnectionDefinition(type=self.type, spatialType=self.spatialType, host=self.host, port=self.port, sdePort=self.sdePort, db=self.db, user=self.user, password=self.password, version=self.version, useSdePort=self.useSdePort)
        for (k, v) in kwargs.iteritems():
            setattr(newInstance, k, v)
        return newInstance

    def toPsyopg2(self):
        return "host='%s' port='%s' dbname='%s' user='%s' password='%s'" % (self.host, self.port, self.db, self.user, self.password )

    def toSqlAlchemy(self):

        connectionString = None

        if self.type == TYPES.POSTGRESQL:
            # 'postgresql://user:password@localhost:5432/sfmaps'
            connectionString = "postgresql://%s:%s@%s:%s/%s" % (self.user, self.password, self.host, self.port, self.db)
        else:
            raise Exception('only postgresql sql alchemy connections are implemented')

        return connectionString


    def toFmeParms(self, infix=None):

        # infix may be any arbitrary string that matches what we need for the FME workspace parms
        returnString = ""
        returnString += " --%s_host %s" % (infix, self.host)
        returnString += " --%s_db %s" % (infix, self.db)
        returnString += " --%s_user %s" % (infix, self.user)
        returnString += " --%s_password %s" % (infix, self.password)

        if self.useSdePort and not self.spatialType == SPATIAL_TYPES.SDE:
                raise Exception('misconfiguration in config_connections - useSdePort was true but the spatial type was not SDE')

        if self.type == TYPES.DB2:
            if self.useSdePort and self.spatialType == SPATIAL_TYPES.SDE:
                returnString += " --%s_instance port:%s" % (infix, self.sdePort)
                returnString += " --%s_version %s" % (infix, self.version)
            else:
                returnString += " --%s_instance port:%s" % (infix, self.port)
        elif self.type == TYPES.POSTGRESQL:
            if self.useSdePort and self.spatialType == SPATIAL_TYPES.SDE:
                returnString += " --%s_instance port:%s" % (infix, self.sdePort)
                returnString += " --%s_version %s" % (infix, self.version)
            else:
                returnString += " --" + infix + "_port %s" % self.port
        elif self.type == TYPES.MSSQL:
            pass
        elif self.type == TYPES.ORACLE:
            #  e.g. username/password@//10.250.60.31:1521/sfgis70
            returnString = "--%s_oracle_connection %s/%s@//%s:%s/%s" % (infix, self.user, self.password, self.host, self.port, self.db)
        elif self.type == None:
            returnString += " --" + infix + "_port %s" % self.port
        else:
            raise Exception('unsupport db connection type: ' + self.type)

        return returnString.strip()


    def getConnection(self):

        connection = None

        if self.type == TYPES.POSTGRESQL:
            connection_string = self.toPsyopg2()
            #print 'connection_string: ' + str(connection_string)
            import psycopg2
            try:
               connection = psycopg2.connect(connection_string)
            except:
               print "unable to connect to " + self.toStringforLogging()
               raise
        elif self.type == TYPES.DB2:
            import DB2
            try:
                dnsString = 'Hostname=%s;Database=%s;Port=%s;' % (self.host, self.db, self.port)
                connection = DB2.connect(dsn=dnsString, uid=self.user, pwd=self.password, autoCommit=False)
            except Exception, e:
               print "unable to connect to " + self.toStringforLogging()
               print e
               raise

        elif self.type == TYPES.MSSQL:
            # we use pymssql
            import pymssql
            try:
                connection = pymssql.connect(host=self.host, user=self.user, password=self.password, database=self.db)
            except Exception, e:
               print "unable to connect to " + self.toStringforLogging()
               print e
               raise
        elif self.type == TYPES.ORACLE:
            import cx_Oracle
            try:
                #cx_Oracle.connect("user", "password", "TNS")
                # scott/tiger@server:1521/orcl
                connectionString = '%s/%s@%s:%s/%s' % (self.user, self.password, self.host, self.port, self.db)
                connection = cx_Oracle.connect(connectionString)
            except Exception, e:
               print "unable to connect to " + self.toStringforLogging()
               print e
               raise

        else:
            message = "unsupported connection type: %s" % self.type
            print message
            raise Exception, message

        return connection


    def executeSqlStatements(self, sqlStatements):

        # We pass in a tuple of sql statements because some drivers, such as DB2 do not handle
        # a mulitple statements in one block as does psycopg2.

        connection = self.getConnection()

        try:
            cursor = connection.cursor()
            print "executing sql"
            for sql in sqlStatements:
                # debug only
                #print sql
                cursor.execute(sql)

            cursor.close()
            connection.commit()
        except Exception, e:
            print 'error while executing sql'
            print e
            raise
        finally:
            connection.close()


    def executeSql(self, sql):

        connection = self.getConnection()

        try:
            cursor = connection.cursor()
            print "executing sql"
            cursor.execute(sql)
            cursor.close()
            connection.commit()
        except Exception, e:
            print 'error while executing sql'
            print e
            raise
        finally:
            connection.close()


    def executeSqlStatement(self, cursor, sqlStatement):
        try:
            cursor.execute(sqlStatement)
        except Exception, e:
            raise
        finally:
            pass


    def standardizeException(self, exceptionImpl):
        # todo - make this more of a real factory kind of pattern (get away from using all these if statements)
        try:
            if self.type == TYPES.POSTGRESQL:
                return dict(code = exceptionImpl.pgcode, message = exceptionImpl.pgerror)
            elif self.type == TYPES.DB2:
                raise Exception('standardized exception not supported for DB2')
            elif self.type == TYPES.MSSQL:
                raise Exception('standardized exception not supported for MSSQL')
            elif self.type == TYPES.ORACLE:
                return dict(code = exceptionImpl.message.code, message = exceptionImpl.message.message)
            else:
                raise Exception('standardized exception not supported for ' + self.type)
        except Exception, e:
            print e
            raise

class DbUtils:

    @staticmethod
    def ResultIter(cursor, arraysize=1000):
        while True:
            rows = cursor.fetchmany(arraysize)
            if not rows:
                break
            for row in rows:
                yield row
