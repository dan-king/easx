
# All we want to accomplish here is to programtically open and close ssh tunnels.
#
# For now we are using a dead simple putty implemenation with os calls.
# This ties us to windows which I think is an acceptable trade off.
# This decision was for the sake of expedience - I couldn't really predict how long a platform
# independent implementation was going to take.
#
# Should we want to move to this to linux, we can create an ssh-pexecpt implementation.
#
# I put a little effort into a platform independent implementation using paramiko but I had some trouble.
# That probably comes down to python skills.
# In trying to do this with paramiko, I would frequently get "ssh connection refeused" on a connnect, after the first
# open/close. I think this is because I never properly terminated the previous connection and threads.
# Look at the history of this file to see these efforts.

import sys

class TunnelingConnectionNull:
    # null object pattern
    # I recently learned that there is a generic class that does; lokk up null object pattern in the see the python cookbook.
    def __init__(self):
        pass
    def connect(self):
        pass
    def disconnect(self):
        pass


class TunnelingConnectionPutty:

    # For the class name we are using "putty" because it is well known.
    # But the executable is plink, a member of the putty suite.
    # It's just a command line version of putty.
    #
    # I considered not using a putty defined session.
    # I decided that the use of putty sessions were a better fit for the use case and for the admin staff.
    #

    def __init__(self, sessionName=None, password=None, port=None):
        # The sessionName is the name of the putty session; this is stored in the windows registry here
        #     HKEY_CURRENT_USER\Software\SimonTatham\PuTTY\SshHostKeys
        # A rather odd thing is that it appears that plink does not use the port number saved in the session.
        # This is why we need the port number here.
        self.sessionName = sessionName
        self.password = password
        self.port = port
        self.puttyProcess = None
        from config.live import config_paths
        self.plinkExePath = config_paths.getPlinkExePath()

    def __str__(self):
        return 'sessionName: %s password: %s puttyProcess: %s plinkExePath: %s' % (self.sessionName, self.password, self.puttyProcess, self.plinkExePath)

    def connect(self):
        # Because of the way we set up the connections, we may already be connected.
        if self.puttyProcess:
            return

        command = self.plinkExePath + ' -ssh -load %s' % self.sessionName
        command +=  ' -pw %s ' %  self.password
        command +=  ' -P %s ' %  self.port
        command +=  ' -N '
        sys.stdout.write('\n')
        sys.stdout.write('opening ssh connection (%s)...\n' % self.sessionName)
        # next line is for debug only - credentials are written to stdout
        #sys.stdout.write('executing command: %s\n' % command)
        import subprocess
        self.puttyProcess = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # Weird problem with the db connections failing very quickly.
        # Could be a coincidence, but as soon as I put a sleep here the problem nearly disappeared.
        import time
        time.sleep(3)
        returnCode = self.puttyProcess.poll()
        if returnCode is not None:
            message = 'unable to open putty process - check the configuration of the putty session named %s\n' % self.sessionName
            # Some client code is not handling exceptions properly - therefore we generate output here.
            sys.stdout.write('%s\n' % message)
            raise Exception(message)
        #sys.stdout.write('returnCode: %s\n' % returnCode)
        sys.stdout.write('ssh connection open\n')

    def disconnect(self):
        import ctypes
        sys.stdout.write('closing ssh connection...\n')
        # Because of the way we set up the connections, we may already be disconnected.
        if self.puttyProcess == None:
            sys.stdout.write('ssh connection is already closed\n')
            return
        PROCESS_TERMINATE = 1
        handle = ctypes.windll.kernel32.OpenProcess(PROCESS_TERMINATE, False, self.puttyProcess.pid)
        ctypes.windll.kernel32.TerminateProcess(handle, -1)
        ctypes.windll.kernel32.CloseHandle(handle)
        self.puttyProcess = None
        sys.stdout.write('ssh connection closed\n')
        sys.stdout.write('\n')



