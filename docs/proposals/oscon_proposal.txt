title
-----
Managing Street Addresses in San Francisco with Open Source


description (limit 400 characters including white spaces)
--------------------------------------------------------
San Francisco's Street Address Management System is used by numerous agencies to edit and report on the city's geospatial street addresses.  The application helps the city reduce duplicate data maintenance work, improves the accuracy, consistency, and quality of the data - and should lower the cost of delivering services to citizens.


abstract (no limit)
-------------------
San Francisco's Street Address Management System is used by numerous agencies to edit and report on the city's geospatial street addresses.  The application helps the city reduce duplicate data maintenance work, improves the accuracy, consistency, and quality of the data - and should lower the cost of delivering services to citizens.

The system was built with nearly all open source software and uses django/geodango with postresql/postgis on the back end and extjs and openlayers for the rich javascript client. We use the city's own GIS data to create the map, which is served with geoserver and geowebcache.  The entire application is hosted on the cloud and receives periodic data updates from the city.

In this talk, we'll begin by discussing the business case for the system.  Next we review some of the organizational issues we see at the city.  We'll review the relatively simple deployment and finally discuss what we learned about the specific technologies.
This includes:

 * how (not) to use the django ORM for saving large object graphs
 
 * but still, the django ORM is cool; geodjango makes the django ORM even cooler
 
 * ETL (geospatial or otherwise) is tedious
 
 * deploying a map server with your own data, and taking the cartography seriously, is tedious
 
 * using (abusing?) the extjs data.store for the "model" on the browser


The system is expected to be in production at the end of March 2010.  The source code will soon be available under GPL3.

keywords
3tera
ajax
applogic
cloud computing
django
extract, transform, load
everyblock
extjs
feature manipulation engine
geodjango
geoserver
geospatial
json
javascript
linux
openlayers
postgresql
postgis
python
rest
unit testing
