
*****
difference between client model and server model RE the base addresses unit address
*****
todo

*****
Notes on Transaction Decorators:
*****
- After reading the following articles:
	http://docs.djangoproject.com/en/dev/topics/db/transactions/
	http://blogs.gnome.org/jamesh/2008/09/01/transaction-management-in-django/
	http://code.djangoproject.com/ticket/2227
I concluded that our problem may be nesting of transaction management decorators�

- looking at the code� I see we are adding the @transaction.commit_on_success decorator to both CrJsonModel.approve() and CrJsonModel.saveToDb()� I gather from my reading that this may be problematic� I think the solution may be to remove these decorators from these methods and add the transaction decorator to the view level methods that call these�

- removed transaction decorators from CrJsonModel methods approve() and saveToDb()� added @transaction.commit_on_success decorator to the view method saveCr()� not sure why but I've been unable to get this working either�. 

- found empirically that I could get the transaction to successfully rollback when a n exception is raised from the CrJsonModel.approve() or CrJsonModel.saveToDb() methods using the @transaction.commit_manually decorator on the view method saveCr() and mimicking the following block from the above Django doc article:
    try:
        ...
    except:
        ...
        transaction.rollback()
    else:
        transaction.commit()

- I'm not sure why I've been unable to get the @transaction.commit_on_success decorator to work�. I also tried removing these decorators and adding:
    'django.middleware.transaction.TransactionMiddleware',
to the Django middleware classes.  According to my reading, it should manage transaction commit/rollback automatically for each request� however, I could not seem to get this to work as advertised� more research will be necessary around this�

- one issue we had here was use of multiple transaction management decorators within a single request� the result is multiple transactions� any time a transaction management decorator is used, a new transaction is opened for only that method and closed when the method returns� this should only be done then when you actually want to issue multiple transactions in a single request

- It seems that the best practice is to use the transaction decorators at the level of view methods� the docs mention that you can use them elsewhere, but doing so may be problematic� using decorators at the view level wherever possible helps avoid the issue of nested transaction decorators�

- using the @transaction.commit_on_success yields unexpected results� this is still a bit mysterious to me... I'm guessing we weren't using it exactly right� I'm going to try and dig a bit deeper here


*****
from the cleint, we send stuff to the database to be saved, even if it does not need to be saved
*****
This affects addresses trigger and approve process.
Should change approve code a lot.
todo - need more detail


*****
xmit
*****
todo - lots more info needed
Here are the properties of interest to the business users:
 - xmits will occur upon an approval of an EAS Change Request
 - xmits will occur within n seconds (shall we start with 60 seconds?)
 - process can go down for an arbitrary duration and will transmit all changes, in order, upon restart
 - guaranteed to not send duplicates

Additional properties:
 - stand alone django (runs outside of web server)
 - the actual transmit occurs outside of EAS approve transaction (approve is not hung up if AVS server is down)
 - "singleton" daemon process (very hard to start a second process by accident)

*****
Notes on Logging
*****
- we are using a third party product to do logging in Django called Jogging
- typically we are using logging.info and logging.critical
- calls to logging.critical will result in a support request (page, email, or whatever)