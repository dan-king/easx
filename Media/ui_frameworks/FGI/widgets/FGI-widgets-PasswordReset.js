﻿Ext.define('FGI.widgets.PasswordResetForm', {
    extend: 'Ext.form.FormPanel',

    url: '',
    frame: true,
    title: 'Reset Password',
    submitBtnText: 'Submit Username',
    resetPwdBtnText: 'Reset my password',
    userNamePromptLabel: 'Username',
    descriptiveText: '<p style="margin-bottom:15px;">Enter your username below, and we\'ll email a new password to the email address we have on file.</p>',
    resettingPwdMask: 'Resetting your password...',
    resetSuccessMsg: 'An email has been sent with your new password.',
    resetErrorMsg: 'There was a problem resetting your password',
    formErrorMsg: 'Please correct any errors in the form.',

    initComponent: function() {

        this.addEvents({
            'passwordreset': true
        });

        this.submitButton = Ext.create('Ext.button.Button', {
            text: this.submitBtnText,
            scope: this,
            disabled: true,
            handler: this.submit
        });

        this.cancelButton = Ext.create('Ext.button.Button', {
            text: 'Cancel',
            handler: this.cancel,
            scope: this
        });

        var fieldListeners = {
            'keydown': function(textField, event, eventOptions) {
                if (textField.getValue().length > 0) {
                    this.submitButton.enable();
                } else {
                    this.submitButton.disable();
                }
                if (event.getKey() === event.ESC) {
                    this.cancel();
                } else if (event.getKey() === event.RETURN) {
                    this.submit();
                }
            },
            scope: this
        };

        Ext.apply(this, {
            bodyStyle: 'padding:5px 5px 0',
            defaults: {
                msgTarget: 'side',
                labelWidth: 150,
                enableKeyEvents: true
            },
            defaultType: 'textfield',
            buttons: [
					this.submitButton,
                    this.cancelButton
				],
            items: [
                {
                    xtype: 'container',
                    autoEl: {
                        html: this.descriptiveText
                    }
                }, {
                    fieldLabel: this.userNamePromptLabel,
                    name: 'username',
                    allowBlank: false,
                    listeners: fieldListeners
                }
            ]
        });

        this.callParent(arguments);

     },

    cancel: function() {
        this.ownerCt.close();
    },

    setFocus: function() {
        var form = this.getForm();
        var field = form.findField('username');
        if (field) {
            field.focus(true, 10);
        }
    },

    submit: function() {
        var mask = new Ext.LoadMask(this.getEl(), { msg: this.resettingPwdMask });
        mask.show();
        this.submitButton.disable();
        this.form.submit({
            url: this.url,
            method: 'POST',
            success: function(theForm, responseObj) {
                mask.hide();
                this.ownerCt.close();
                this.fireEvent('passwordreset', responseObj);
                Ext.MessageBox.alert('Password Reset!', this.resetSuccessMsg);
            },
            failure: function(theForm, responseObj) {
                var window;
                mask.hide();
                if (responseObj.result) {
                    window = Ext.MessageBox.alert(this.resetErrorMsg, responseObj.result.message);
                } else {
                    window = Ext.MessageBox.alert('', this.formErrorMsg);
                }
                // handle escape and OK
                window.on({
                    hide: this.setFocus,
                    scope: this,
                    single: true
                });
            },
            scope: this
        });
    }

});
