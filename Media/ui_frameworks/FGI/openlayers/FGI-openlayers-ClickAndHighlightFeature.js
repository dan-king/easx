﻿
Ext.namespace('FGI', 'FGI.openlayers');

FGI.openlayers.ClickAndHighlightFeature = OpenLayers.Class(OpenLayers.Control.SelectFeature, {

    renderIntent: "select",

    hoverTipFieldName: '',

    tipOffset: {
        x: 10, y: -30
    },

    showTip: false,

    featureIdField: '',

    zoomOnClick: false,

    preserveSelectedFeature: false,

    trackSelected: false,

    initialize: function(layer, options) {

        this.hover = true;

        this.hoverTip = Ext.create('Ext.tip.Tip', {
            closable: false,
            renderTo: 'map'
        });

        if (!layer.controls) {
            layer.controls = [];
        }
        layer.controls.push(this);

        if (options.clickFeature) {
            options.clickFeature = Ext.Function.createSequence(this.clickFeature, options.clickFeature);
        }
        if (options.featureIdField) {
            this.featureIdField = options.featureIdField;
        }

        OpenLayers.Control.SelectFeature.prototype.initialize.call(this, layer, options);

        // We need this handler to get the mouse events to whoever else needs to hear them.
        this.handlers = {
            feature: new OpenLayers.Handler.Feature(
                this,
                this.layer,
                this.callbacks,
                {
                    geometryTypes: this.geometryTypes,
                    stopClick: false,
                    stopDown: false,
                    stopUp: false
                }
            )
        };

    },

    // override the original clickFeature so that we can hover and click
    clickFeature: function(feature) {

        // zoom if desired
        if (this.zoomOnClick) {
            this.map.zoomToExtent(feature.geometry.bounds);
        }

        // determine if this feature is already in the selectedFeatures list
        if (this.trackSelected) {
            var selected = (OpenLayers.Util.indexOf(this.layer.selectedFeatures, feature) > -1);
            if (selected) {
                if (this.toggleSelect()) {
                    this.unselect(feature);
                } else if (!this.multipleSelect()) {
                    this.unselectAll({ except: feature });
                }
            } else {
                if (!this.multipleSelect()) {
                    this.unselectAll({ except: feature });
                }

                this.select(feature);
                //this.selectedFeatureToMaintain
            }
        }

        this.map.events.triggerEvent("featureclicked", feature);
        this.hoverTip.hide();

    },

    overFeature: function(feature) {
        if (this.showTip) {
            var message = feature.data[this.hoverTipFieldName];
            var totalCount = feature.data.count;
            if (totalCount > 1) {
                var adjustedCount = totalCount - 1;
                message += ' (' + adjustedCount + ' more)';
            }
            // http://code.google.com/p/eas/issues/detail?id=671
            message = message.replace(" ", "&nbsp;");
            this.hoverTip.update(message);
            var position = this.map.getPixelFromLonLat(feature.geometry.getBounds().getCenterLonLat());
            this.hoverTip.show();
            this.hoverTip.setPosition(position.x + this.tipOffset.x, position.y + this.tipOffset.y);
            var delayHide = new Ext.util.DelayedTask(function(){ this.hoverTip.hide() }, this);
            // http://code.google.com/p/eas/issues/detail?id=673
            delayHide.delay(3000);
        }
        this.layer.drawFeature(feature, "temporary");
    },


    outFeature: function(feature) {
        if (this.showTip) {
            this.hoverTip.hide();
        }

        // determine if this feature is already in the selectedFeatures list
        if (this.trackSelected) {
            var selected = (OpenLayers.Util.indexOf(this.layer.selectedFeatures, feature) > -1);
            if (selected) {
                this.layer.drawFeature(feature, "select");
            } else {
                this.layer.drawFeature(feature, "default");
            }
        } else {
            this.layer.drawFeature(feature, "default");
        }

    },

    selectSelectedFeature: function(feature) {

    },

    selectFeatureById: function(id) {
        for (var i in this.layer.features) {
            if (this.layer.features[i].data != undefined) {
                if (this.layer.features[i].data[this.featureIdField] == id) {
                    this.clickFeature(this.layer.features[i]);
                }
            }
        }
    },

    CLASS_NAME: "OpenLayers.Control.ClickAndHighlightFeature"

});