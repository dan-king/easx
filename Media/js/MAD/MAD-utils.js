
Ext.namespace('MAD', 'MAD.utils');

MAD.utils.getPointFromViewPortPixel = function(map, pixel) {
    var lonLat = map.getLonLatFromViewPortPx(pixel);
    return new OpenLayers.Geometry.Point(lonLat.lon, lonLat.lat);
};

MAD.utils.wktPointToPoint = function(wktPoint) {
    return new OpenLayers.Format.WKT().read(wktPoint).geometry;
};

MAD.utils.wktGeomToGeom = function(wktGeom) {
    return new OpenLayers.Format.WKT().read(wktGeom).geometry;
};

MAD.utils.lonLatToWktPoint = function(lonLat) {
    var point = new OpenLayers.Geometry.Point(lonLat.lon, lonLat.lat);
    //  todo - some duplication here
    var vectorPoint = new OpenLayers.Feature.Vector(point);
    return new OpenLayers.Format.WKT().write(vectorPoint);
};

MAD.utils.boundsToWktPolygon = function(bounds) {
    var geometryPoly = bounds.toGeometry();
    var vectorPoly = new OpenLayers.Feature.Vector(geometryPoly);
    return new OpenLayers.Format.WKT().write(vectorPoly);
};


MAD.utils.wktPointToLonLat = function(wktPoint) {
    var pointGeom = new OpenLayers.Format.WKT().read(wktPoint).geometry;
    return new OpenLayers.LonLat(pointGeom.x, pointGeom.y);
};

MAD.utils.pointToWkt = function(point) {
    var vectorPoint = new OpenLayers.Feature.Vector(point);
    return new OpenLayers.Format.WKT().write(vectorPoint);
};

MAD.utils.featureToWkt = function(feature) {
    return new OpenLayers.Format.WKT().write(feature);
};

// todo - migrate away from this and towards MAD.utils.wktToVector
MAD.utils.wktLineToVector = function(wktLine) {
    var lineGeom = new OpenLayers.Format.WKT().read(wktLine).geometry;
    return new OpenLayers.Feature.Vector(lineGeom);
};

MAD.utils.wktToVector = function(wkt) {
    var geom = new OpenLayers.Format.WKT().read(wkt).geometry;
    return new OpenLayers.Feature.Vector(geom);
};

MAD.utils.wktToMultiGeometry = function(wkt) {
    var geometryCollection = new OpenLayers.Format.WKT().read(wkt);
    return new OpenLayers.Feature.Vector(geometryCollection);
};

MAD.utils.projectBounds_2227_900913 = function(boundsIn) {
    var geometry = boundsIn.toGeometry();
    geometry.transform(new OpenLayers.Projection("EPSG:2227"), new OpenLayers.Projection("EPSG:900913"));
    return geometry.getBounds();
};

// used inside Ext key event listeners, returns true if the key press will
// result in a change in the field's state that needs to be pushed back to the
// model.  Use Ext.EventObject.getKey() method to get event key codes.
// reference: http://www.cambiaresearch.com/c4/702b8cd1-e5b0-42e6-83ac-25f0306e3e25/Javascript-Char-Codes-Key-Codes.aspx
MAD.utils.catchKey = function(key) {
    return (
        // backspace
        key === 8 ||
        // shift
        key === 16 ||
        // spacebar
        key === 32 ||
        // delete
        key === 46 ||
        // numbers (48-57)
        (key > 47 && key < 58) ||
        // alphabet (65-90)
        (key > 64 && key < 91) ||
        // numpad (96-111)
        (key > 95 && key < 112) ||
        // special characters (186-192)
        (key > 185 && key < 193) ||
        // more special characters (219-222)
        (key > 218 && key < 223) ||
        // MAD-167
        (key === undefined)
    );
};


// Command Pattern
MAD.utils.Command = function(config) {

    // user sets these
    this.fn = {};
    this.scope = {};
    this.args = [];

    Ext.apply(this, config);

    // user does not set these
    this.action = new Ext.Action({
        handler: this.fn,
        scope: this.scope,
        args: this.args
    });

};

MAD.utils.CommandSet = function() {
    // todo - clean up the way we handle args
    this.commands = [];
    this.add = function(command){
        this.commands.push(command);
    };
    this.execute = function() {
        Ext.each(this.commands, function(item, index, allItems) {
            if (item.args) {
                item.action.execute(item.args[0], item.args[1], item.args[2], item.args[3], item.args[4], item.args[5], item.args[6], item.args[7], item.args[8], item.args[9]);
            } else {
                item.action.execute();
            }
        });
    };
};

Ext.apply(Ext.util.Format, {
    clearNull: function (value) {
        if (value === null) {
            return '';
        }
        else {
            return value;
        }
    },
    clearNegativeOne: function (value) {
        if (value === -1 || value === '-1') {
            return '';
        }
        else {
            return value;
        }
    },
    clearSubaddressNum: function (value) {
        if (value === -999 || value === '-999') {
            return '';
        }
        else {
            return '#' + value;
        }
    },
    jsonDateToDisplayDate: function (v) {
        var date = Ext.Date.parse(v, "Y-m-d H:i:s");
        return Ext.util.Format.formatDateDefault(date);
    },
    tmsToShortDate: function (v) {
        var date = Ext.Date.parse(v, "Y-m-d H:i:s");
        return Ext.util.Format.formatDateShort(date);
    },
    formatDateDefault: function (v) {
        return Ext.util.Format.date(v, "F j, Y, g:i a");
    },
    formatDateShort: function (v) {
        // e.g. 2/15/2013
        return Ext.util.Format.date(v, "n/j/Y");
    }

});
