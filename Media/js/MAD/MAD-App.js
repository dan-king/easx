Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', '/Media/ui_frameworks/extjs-4.1.0/examples/ux/');
Ext.namespace('MAD', 'MAD.app');

MAD.app = {

    init: function() {

        // To support MVC properly, the MAD.app class should itself extend observable.
        // However, I could not figure out how to make MAD.app observable because of the slightly unusual way it
        // is instantiated in index.html.  Also, there is a lot of code coupled to the way it is instantiated.
        // I did want to touch that much code at this time.
        // Therefore, I am creating an event manager to handle this problem.
        this.eventManager = new Ext.Component();
        this.eventManager.addEvents('startEditAddress');
        this.eventManager.addEvents('displayAddressReport');

        this.changeRequestCommandFactory = new MAD.workflows.ChangeRequest.CommandFactory();

        Proj4js.libPath = MAD.config.Paths.Proj4jsLibPath;

        // a blank user object - gets initialized in the login function
        this.user = MAD.model.User;

        this.domainTables = {};
        this.dispositionStore = MAD.model.getDispositionDomainStore();
        this.unitTypeStore = MAD.model.getUnitTypeDomainStore();
        this.floorsDomainStore = MAD.model.getFloorsDomainStore();
        this.addressBaseNumberSuffixDomainStore = MAD.model.getAddressBaseNumberSuffixDomainStore();
        this.exceptionDataStore = MAD.model.exception.getDataStore();
        this.getDomainData();

        Ext.QuickTips.init();

        // disable showing the browser context menu on the entire site
        document.oncontextmenu = function() {
            return false;
        };

        // create the default popup
        this.popup = null;

        this.activeWorkflow = null;

        this.Search = new MAD.widgets.Search({ flex: 100 });

        this.MapContainer = Ext.create('MAD.widgets.Map', {
            includeRetired: this.Search.includeRetired,
            listeners: {
                'viewparcelsclicked': function (point) {
                    point = point.transform(new OpenLayers.Projection('EPSG:900913'),new OpenLayers.Projection('EPSG:2227'));
                    this.results.showSearchResults('PointToParcelsSearch', point.toString(), this.Search.includeRetired, false);
                },
                scope: this
            }
        });
        this.Map = this.MapContainer.map;

        this.MapContainer.contextMenu.on({
            'addNewAddressButtonClicked': function(point) {
                if (this.activeWorkflow === null) {
                    this.changeRequestWorkflow = new MAD.workflows.ChangeRequest({
                        point: point,
                        listeners: {
                            'requestapproved': this.requestApproved,
                            scope: this
                        }
                    });
                } else {
                    if (this.activeWorkflow.statusController.allowFeatureEdit()) {
                        var beforeXHRCommands = new MAD.utils.CommandSet();
                        var afterXHRCommands = new MAD.utils.CommandSet();
                        beforeXHRCommands.add(this.changeRequestCommandFactory.createShowMask({ scope: this.activeWorkflow, args: ['Loading...'] }));
                        afterXHRCommands.add(this.changeRequestCommandFactory.createHideMask({ scope: this.activeWorkflow }));
                        this.activeWorkflow.proposeBaseAddress(point, beforeXHRCommands, afterXHRCommands);
                    } else {
                        Ext.Msg.alert('Add Address', 'You cannot add addresses when reviewing a change request.');
                    }
                }
            },
            'provisionParcelButtonClicked': function(point) {
                this.showProvisionParcelPopup(point);
            },
            'googleStreetViewButtonClicked': function(point) {
                point.transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326"));
                var url = Ext.String.format(MAD.config.Urls.getGoogleStreetViewUrl(), point.y, point.x);
                window.open(url, 'easGoogleStreetView');
            }, 
            'show': function () {
                if (this.popup) {
                    this.popup.destroy();
                }
            },
            scope: this
        });

        this.Map.events.register("featureclicked", this, this.featureClicked);
        this.Map.events.register("featurerightclicked", this, this.featureRightClicked);

        this.results = new MAD.widgets.Results({
            listeners: {
                'resize': function () {
                    this.Map.updateSize();
                },
                scope: this
            }
        });

        this.loginForm = new FGI.widgets.LoginForm({
            url: MAD.config.Urls.Login,
            passwordErrorText: 'invalid username or password',
            listeners: {
                'loginsucceeded': this.login,
                'passwordhelpclicked': this.showPasswordHelp,
                scope: this
            }
        });

        this.loginWindow = Ext.create('Ext.window.Window', {
            title: '',
            layout: 'fit',
            width: 335,
            height: 150,
            items: [this.loginForm],
            modal: true,
            draggable: false,
            closeAction: 'hide',
            closable: false,
            resizable: false,
            listeners: {
                'hide': function(w) {
                    this.loginButton.toggle(false);
                },
                'show': function(w) {
                    this.loginButton.toggle(true);
                    var form = this.loginForm.getForm();
                    var field = form.findField('name');
                    if (field) {
                        field.focus(true, 10);
                    }
                },
                scope: this
            }
        });
        this.loginButton = Ext.create('Ext.button.Button', {
            text: 'Sign In',
            cls: 'button',
            toggleHandler: function(button, enabled) {
                if (enabled) {
                    this.loginWindow.show();
                } else {
                    this.loginWindow.hide();
                }
            },
            scope: this,
            enableToggle: true
        });

        this.helpDropDown = new Ext.Button({
            id: 'helpbutton',
            text: 'Help',
            cls: 'button',
            hidden: false,
            menu: new Ext.menu.Menu({
                defaults: {
                    cls: 'button',
                    scope: this
                },
                items: [{
                    id: 'wikiHelpButton',
                    text: 'Help',
                    iconCls: 'silk-page-edit',
                    handler: function() {
                        window.open(MAD.config.Urls.getWikiDocsUrl(), 'easWikiDocs');
                    }
                }, {
                    id: 'videoHelpButton',
                    text: 'Tutorials',
                    iconCls: 'silk-film',
                    handler: function() {
                        window.open(MAD.config.Urls.getVideoDocsUrl(), 'easVideoDocs');
                    }
                }, {
                    id: 'websiteButton',
                    text: 'Website',
                    iconCls: 'silk-house',
                    handler: function() {
                        window.open(MAD.config.Urls.getWebsiteUrl(), 'easWebsite');
                    }
                }, {
                    id: 'aboutButton',
                    text: 'About',
                    iconCls: 'silk-wrench',
                    handler: function() {
                        Ext.Msg.alert(this.applicationInfo.name, MAD.config.Tpls.applicationInfo.apply(this.applicationInfo));
                    }
                }]
            })
        });

        // a split button that holds the logout and change password buttons
        var passwordChangeForm = new FGI.widgets.PasswordChangeForm({ url: MAD.config.Urls.ChangePassword, title: "" });
        this.userPrefDropDown = new Ext.SplitButton({
            id: 'userprefsbutton',
            text: 'Preferences',
            cls: 'button',
            hidden: true,
            scope: this,
            menu: new Ext.menu.Menu({
                scope: this,
                items: [
                // these items will render as dropdown menu items when the arrow is clicked:
	                this.logoutButton = new Ext.menu.Item(new Ext.Action({
	                    id: 'logoutbutton',
	                    text: 'Sign Out',
                        iconCls: 'silk-key-delete',
                        cls: 'button'
	                })),
                    this.changePasswordButton = new Ext.menu.Item(new Ext.Action({
                        id: 'changepassword',
                        text: 'Change Password',
                        iconCls: 'silk-key-go',
                        cls: 'button',
                        handler: function() {
                            this.passwordChangeObj = new Ext.Window({
                                title: 'Change Password',
                                layout: 'fit',
                                width: 350,
                                height: 165,
                                items: [passwordChangeForm],
                                modal: true,
                                draggable: false,
                                closeAction: 'hide',
                                closable: false,
                                resizable: false,
                                listeners: {
                                    'show': function(w) {
                                        passwordChangeForm.resetForm();
                                    },
                                    scope: this
                                }
                            });
                            this.passwordChangeObj.show();
                        }
                    }))
                ]
            })
        });

        // logout handler
        this.logoutButton.on('click', this.logout, this);

        var titleStyleSelector = '.brand-title';
        if (MAD.app.applicationInfo.env === 'PROD') {
            titleStyleSelector += '.prod';
        }

        // create the viewport
        this.Viewport = new Ext.Viewport({
            layout: 'border',
            items: [
                // north region contains title, search widget, login/change password, and help
                {
                    region: 'north',
                    border: false,
                    bodyStyle: 'background-color:#DFE8F6;',
                    height: 40,
                    layout: {
                        type: 'hbox',
                        align: 'middle',
                        pack: 'left'
                    },
                    items: [
                        {
                            xtype: 'text',
                            styleSelector: titleStyleSelector,
                            text: 'eas',
                            degrees: 270,
                            height: 35,
                            width: 26,
                            style: 'padding-top: 1px;'
                        },
                        {
                            xtype: 'container',
                            cls: 'brand-logo',
                            width: 32,
                            height: 40
                        },
                        this.Search,
                        { xtype: 'container', flex: 1 },
                        this.TasksBar = Ext.create('MAD.widgets.TasksBar', { width: 335 }),
                        { xtype: 'container', flex: 1 },
                        this.loginButton,
                        this.userPrefDropDown,
                        { xtype: 'tbspacer', width: 10 },
                        this.helpDropDown,
                        { xtype: 'tbspacer', width: 10 }
                    ]
                },
                // center region contains the map
                {
                    region: 'center',
                    layout: 'fit',
                    border: false,
                    title: "&nbsp;",    // optional - to support lookAndFeel
                    items: [this.MapContainer]
                },
                // west region contains "search results"
                this.results,
                // south region contains change request panel
                this.changeRequestPanel = new Ext.Panel({
                    title: 'Change Request',
                    region: 'south',
                    height: 400,
                    hidden: true,
                    collapsible: true,
                    floatable: false,
                    layout: 'fit'
                })
            ],
            // hack to force loading of tiles to bottom of map container
            listeners: {
                'afterlayout': {
                    fn: function() {
                        var that = this,
                            queryString = Ext.urlDecode(window.location.search.substring(1)),
                            failureHandler = function () {
                                Ext.Msg.alert('Address Not Found', 'The address could not be found.  If you think that this is in error, please contact the system administrator.')
                            };

                        this.MapContainer.setWidth(this.MapContainer.getSize().width + 1);
                        this.MapContainer.setWidth(this.MapContainer.getSize().width - 1);
                        // get the user if they are already logged in
                        this.getUser();

                        var getAddressFromQueryString = function (addressId) {
                            Ext.Ajax.request({
                                url: MAD.config.Urls.AddressInfo + addressId,
                                disableCaching: true,
                                success: function(result) {
                                    var response =Ext.JSON.decode(result.responseText),
                                        addressJson,
                                        feature;

                                    if (response.success) {
                                        feature = new OpenLayers.Format.WKT().read(response.returnObj.geometry);
                                        this.Map.zoomToGeom(feature.geometry);
                                        this.showAddressReport(feature.geometry, addressId);
                                    } else {
                                        failureHandler();
                                    }
                                },
                                failure: failureHandler,
                                scope: that
                            });
                        }

                        if (queryString.address) {
                            getAddressFromQueryString(queryString.address);
                        } else if (queryString.query) {
                            Ext.Ajax.request({
                                url: MAD.config.Urls.AddressSearch + '?query=' + queryString.query + '&includeRetired=false&page=1&start=0&limit=1',
                                disableCaching: true,
                                success: function(result) {
                                    var response =Ext.JSON.decode(result.responseText);
                                    if (response.count > 1) {
                                        this.searchTriggerClicked("AddressSearch", queryString.query, false);
                                    } else {
                                        if (response.results[0]) {
                                            getAddressFromQueryString(response.results[0].address_base_id);
                                        } else {
                                            failureHandler();
                                        }
                                    }
                                },
                                failure: failureHandler,
                                scope: this
                            });
                        }
                    },
                    scope: this,
                    single: true
                }
            }
        });
        this.changeRequestPanel.on('searchitemselected', this.searchItemSelected, this);

        this.TasksBar.on({
            'myEditsClicked': function() {
                this.changeRequestBrowser = new MAD.widgets.ChangeRequestBrowser({
                    title: 'My Change Requests',
                    myChangeRequests: true
                });
                this.changeRequestBrowser.on('changeRequestSelected', function(requestId, reclaim) {
                    this.changeRequestWorkflow = new MAD.workflows.ChangeRequest({
                        changeRequestId: requestId, 
                        reclaimForEditing: reclaim,
                        listeners: {
                            'requestapproved': this.requestApproved,
                            scope: this
                        }
                    });
                    this.changeRequestBrowser.close();
                }, this);
                this.changeRequestBrowser.show();
            },
            'reviewEditsClicked': function() {
                this.changeRequestBrowser = new MAD.widgets.ChangeRequestBrowser({
                    title: 'Review Change Requests'
                });
                this.changeRequestBrowser.on('changeRequestSelected', function(requestId) {
                    this.changeRequestWorkflow = new MAD.workflows.ChangeRequest({
                        changeRequestId: requestId,
                        listeners: {
                            'requestapproved': this.requestApproved,
                            scope: this
                        }
                    });
                    this.changeRequestBrowser.close();
                }, this);
                this.changeRequestBrowser.show();
            },
            scope: this
        });

        this.results.on({
            'loadstore': this.updateWithResults, 
            'itemmouseenter': function(gridview, record) {
                if (gridview.ownerCt.id === 'ParcelSearchResultsGrid') {
                    this.highlightParcel(record, false);
                } else if (gridview.ownerCt.id === 'AddressSearchResultsGrid') {
                    this.addAddressSearchMarker(record, false);
                }
            },
            'itemmouseleave': function(gridview, record) {
                if (gridview.ownerCt.id === 'ParcelSearchResultsGrid') {
                    this.Map.getLayersByName('highlighted_parcel_layer')[0].runFade();
                } else if (gridview.ownerCt.id === 'AddressSearchResultsGrid') {
                    this.Map.getLayersByName('SearchedAddresses')[0].runFade();
                }
            },
            'addresslinkclicked': function(addressRecord) {
                var feature = new OpenLayers.Format.WKT().read(addressRecord.get('geometry'));
                feature.geometry.transform(new OpenLayers.Projection("EPSG:2227"), new OpenLayers.Projection("EPSG:900913"));
                this.showAddressReport(feature.geometry, addressRecord.get('address_base_id'));
            },
            scope: this
        });

        this.Search.on({
            'searchbuttonclicked': this.searchTriggerClicked, 
            'addressselected': function (record, includeRetired) {
                this.showAddressReport(record.get('geometry'), record.get('address_base_id'));
            },
            'parcelselected': function (record, includeRetired) {
                this.searchItemSelected('ParcelSearch', record.get('blk_lot'), includeRetired);
            },
            'includeretiredchanged': function (includeRetired) {
                this.MapContainer.setIncludeRetired(includeRetired);
            },
            scope: this
        });
    },
    //// init function END

    handleMarkerMouseEvent: function (record, callback) {
        var searchedAddresses = this.Map.getLayersByName('SearchedAddresses')[0];
        var key;
        for (key in searchedAddresses.markers) {
            if (searchedAddresses.markers[key].data) {
                if (searchedAddresses.markers[key].data.address_base_id === record.get('address_base_id')) {
                    callback(searchedAddresses.markers[key]);
                }
            }
        }
    },

    loadSearchResults_parcelToAddresses: function(blockLot) {
        this.results.showSearchResults('ParcelToAddressesSearch', blockLot, this.Search.includeRetired, false);
    },

    loadSearchResults_addressToParcels: function(addressBaseId) {
        this.results.showSearchResults('AddressToParcelsSearch', addressBaseId, this.Search.includeRetired, false);
    },

    highlightParcel: function (record, runFade) {
        var highlightParcelLayer = this.Map.getLayersByName('highlighted_parcel_layer')[0];
        highlightParcelLayer.destroyFeatures();
        var geometry = record.get('geometry');
        if (!geometry) {
            return;
        }
        var feature = new OpenLayers.Format.WKT().read(geometry);
        feature.geometry.transform(new OpenLayers.Projection("EPSG:2227"), new OpenLayers.Projection("EPSG:900913"));
        highlightParcelLayer.addFadeFeature(feature, runFade);
    },

    updateWithResults: function(store, records) {
        this.zoomMapToResults(store, records);
        var highlightParcelLayer = this.Map.getLayersByName('highlighted_parcel_layer')[0];
        highlightParcelLayer.destroyFeatures();
        this.Search.query('combo')[0].collapse();
        if (records && records.length > 0) {
            this.results.highlightRecord(records[0], true);
            if (store.storeId === 'AddressSearchResultsStore') {
                this.addAddressSearchMarker(records[0], true);
            } else if (store.storeId === 'ParcelSearchResultsStore') {
                this.highlightParcel(records[0], true);
            }
        }
    },

    zoomMapToResults: function(store, records) {
        // one, some, all of the geometries may be null
        if (!records || records.length === 0) {
            return;
        }
        var bounds;
        Ext.each(records, function(record) {
            var wktGeom = record.get('geometry');
            if (!wktGeom) {
                return true;
            }
            if (!bounds) {
                bounds = MAD.utils.wktGeomToGeom(wktGeom).getBounds();
            } else {
                bounds.extend(MAD.utils.wktGeomToGeom(wktGeom).getBounds());
            }
        }, this);

        if (store.storeId === 'AddressSearchResultsStore') {
            // pass
        } else if (store.storeId === 'ParcelSearchResultsStore') {
            if (bounds) {
                bounds = MAD.utils.projectBounds_2227_900913(bounds);
            }
        }

        if (bounds) {
            this.MapContainer.zoomToExtentConditional(bounds, 6);
        }

    },

    getDomainData: function() {
        Ext.Ajax.request({
            url: MAD.config.Urls.ChangeRequestDomains,
            headers: { 'Content-Type': 'application/json' },
            success: function(result, request) {
                this.domainTables = Ext.JSON.decode(result.responseText).returnObj;
                this.dispositionStore.loadData(this.domainTables["d_address_disposition"]);
                this.unitTypeStore.loadData(this.domainTables["d_unit_type"]);
                this.floorsDomainStore.loadData(this.domainTables["d_floors"]);
                this.addressBaseNumberSuffixDomainStore.loadData(this.domainTables["d_address_base_number_suffix"]);
            },
            failure: function(result, request) {
                Ext.Msg.alert('Data Retrieval Error', 'unable to retrieve application domain data');
            },
            scope: this
        });
    },

    searchTriggerClicked: function (queryType, queryValue, includeRetired) {
        this.results.showSearchResults(queryType, queryValue, includeRetired, false);
    },

    searchItemSelected: function(queryType, queryValue, includeRetired) {
        this.results.showSearchResults(queryType, queryValue, includeRetired, true);
    },

    featureClicked: function(feature) {
        if (this.popup) {
            this.popup.destroy();
        }
        if (feature.data.isClustered) {
            this.showClusterPopup(feature.geometry, feature.data);
        } else {
            this.showAddressReport(feature.geometry, feature.data.address_base_id);
        }
    },

    featureRightClicked: function(feature) {
        this.MapContainer.showContextMenu(feature.layer.controls[0].handlers.feature.evt);
    },

    showAddressLink: function (addressId) {
        var win = new Ext.Window({
            title: 'Link To Address',
            items: [
                {
                    xtype: 'container',
                    style: 'padding-bottom:10px;',
                    html: 'Press Ctrl+C to copy to clipboard.'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'URL',
                    labelWidth: 20,
                    width: '100%',
                    readOnly: true,
                    selectOnFocus: true,
                    value: 'http://' + window.location.host + window.location.pathname + '?address=' + addressId,
                    listeners: {
                        'specialkey': function(f,e){
                            if(e.getKey() == e.ENTER){
                                win.close();
                            }
                        }
                    }   
                }
            ],
            height: 130,
            width: 355,
            bodyPadding: 10,
            closable: true,
            modal: true,
            dockedItems: [{
                xtype: 'container',
                height: 30,
                dock: 'bottom',
                layout: {
                    type: 'hbox',
                    align: 'middle',
                    pack: 'end'
                },
                items: [{
                    xtype: 'button',
                    text: 'Close',
                    width: 75,
                    handler: function () {
                        win.close();
                    }
                }]
            }],
            listeners: {
                'show': function () {
                    var field = win.query('textfield')[0];
                    Ext.Function.defer(field.focus, 100, field);
                }
            }
        });
        win.show();
    },

    showClusterPopup: function(geometry, data) {
        var firstItem = { address: data.address, address_base_id: data.address_base_id, geometry: data.geometry, validation_warning_count: data.validation_warning_count, retire_tms: data.retire_tms };
        var reportData = [firstItem].concat(data.clusteredItems);

        var report = Ext.create('MAD.widgets.ClusterReport', {
            data: reportData,
            listeners: {
                'viewreportclicked': function(report, index, rec) {
                    this.showAddressReport(rec.data.geometry, rec.data.address_base_id);
                },
                scope: this
            }
        });

        this.showPopup({
            title: data.count + " Addresses",
            expandedHeight: 396,
            expandedWidth: 272,
            geometry: geometry,
            width: 272,
            height: 266,
            map: this.Map,
            items: [report]
        });
    },

    showAddressReport: function (geometry, addressId) {
        var addressReportLayer = this.Map.getLayersByName('address_report_layer')[0];
        addressReportLayer.setVisibility(true);

        if (typeof geometry === "string") {
            var feature = new OpenLayers.Format.WKT().read(geometry);
            geometry = feature.geometry;
        } else {
            geometry = geometry.clone();
        }

        var report = Ext.create('MAD.widgets.AddressReport', {
            user: this.user,
            addressId: addressId,
            geometry: geometry,
            listeners: {
                'editButtonClicked': function(addressId, isRetire) {
                    this.editAddress(addressId, isRetire);
                },
                'populateAddressReport': function(geometryStrings) {
                    addressReportLayer.stopFade();
                    addressReportLayer.destroyFeatures();
                    Ext.each(geometryStrings, function(geometryString) {
                        if (!geometryString) {
                            return true;
                        }
                        feature = MAD.utils.wktToVector(geometryString);
                        feature.geometry.transform(new OpenLayers.Projection("EPSG:2227"), new OpenLayers.Projection("EPSG:900913"));
                        addressReportLayer.addFeatures([feature]);
                    });
                    addressReportLayer.addFeatures([new OpenLayers.Feature.Vector(geometry)]);
                },
                'destroy': function() {
                    addressReportLayer.runFade();
                },
                'showaddressclicked': this.showAddressLink,
                scope: this
            }
        });
        report.on({
            'showmapdatatoggled': function (toggleState) {
                addressReportLayer.setVisibility(toggleState);
            },
            'searchitemselected': this.searchItemSelected,
            'lineagelinkclicked': function (axpId, type) {
                this.results.showLineage(axpId, type);
            },
            scope: this
        });

        this.results.showReport(report);

        // only zoom to the address if it doesn't fall within the map extent
        if (this.Map.getZoom() < 6 || !geometry.intersects(this.Map.getExtent().toGeometry())) {
            this.Map.zoomToGeom(geometry);
        }

        this.eventManager.fireEvent('displayAddressReport', addressId);
    },

    showProvisionParcelPopup: function(geometry) {
        geometry.getBounds();
        var lonlat = new OpenLayers.LonLat(geometry.bounds.left, geometry.bounds.top);
        var provisionParcelPanel = Ext.create('MAD.widgets.ProvisionParcelPanel', {
            lonlat: lonlat,
            listeners: {
                'provisionParcelExecute': function(block, lot, lonLat) {
                    if (! block || ! lot) {
                        Ext.Msg.alert(provisionParcelPanel.title, 'block and lot must be specified');
                        return;
                    }
                    var url = Ext.String.format(MAD.config.Urls.ProvisionParcel, block, lot, lonLat.lon, lonLat.lat);
                    var mask = new Ext.LoadMask(this.Viewport.container.dom, { msg: 'provisioning  parcel...' });
                    mask.show();
                    Ext.Ajax.request({
                        url: url,
                        method: 'PUT',
                        success: function(response){
                            var responseObject = Ext.decode(response.responseText);
                            if (responseObject.success) {
                                mask.hide();
                                this.popup.destroy();
                            } else {
                                mask.hide();
                                Ext.Msg.alert(provisionParcelPanel.title, responseObject.message);
                            }
                        },
                        failure: function(response){
                            mask.hide();
                            var responseObject = Ext.decode(response.responseText);
                            Ext.Msg.alert(provisionParcelPanel.title, responseObject.message);
                        },
                        scope: this
                    });
                },
                'provisionParcelCancel': function(){
                    this.popup.destroy();
                },
                scope: this
            }
        });
        
        this.showPopup({
            title: 'Provision Parcel',
            geometry: geometry,
            items: [provisionParcelPanel],
            expandable: false,
            width: 300,
            height: 250,
            map: this.Map
        });
    },

    showPopup: function (popupConfig) {
        if (this.popup) {
            this.popup.destroy();
        }

        this.popup = Ext.create('MAD.widgets.Popup', popupConfig);
        this.popup.show();
        return this.popup;
    },

    requestApproved: function (request) {
        if (this.results.getLayout().getActiveItem() === this.results.reportContainer) {
            var report = this.results.reportContainer.items.items[0];
            request.baseAddresses.each(function (baseAddress) {
                if (report.addressId === baseAddress.get('address_base_id')) {
                    var geom = new OpenLayers.Format.WKT().read(baseAddress.get('geometry_proposed')).geometry;
                    this.showAddressReport(geom, report.addressId);
                }
            }, this);
        }
    },

    editAddress: function(addressId, retireFlg) {
        this.Map.getLayersByName('highlighted_parcel_layer')[0].destroyFeatures();

        if (this.user.isRequestor()) {
            if (this.activeWorkflow == null) {
                this.eventManager.fireEvent('startEditAddress', addressId);
                this.changeRequestWorkflow = new MAD.workflows.ChangeRequest({
                    addressId: addressId,
                    retireFlg: retireFlg,
                    listeners: {
                        'requestapproved': this.requestApproved,
                        scope: this
                    }
                });
            } else if (!this.activeWorkflow.retrievingAddressData) {
                if (this.activeWorkflow.statusController.allowFeatureEdit()) {
                    this.eventManager.fireEvent('startEditAddress', addressId);
                    var beforeXHRCommands = new MAD.utils.CommandSet();
                    var afterXHRCommands = new MAD.utils.CommandSet();
                    beforeXHRCommands.add(this.changeRequestCommandFactory.createShowMask({ scope: this.activeWorkflow, args: ['Loading...'] }));
                    afterXHRCommands.add(this.changeRequestCommandFactory.createHideMask({ scope: this.activeWorkflow }));
                    this.activeWorkflow.getBaseAddress(addressId, retireFlg, beforeXHRCommands, afterXHRCommands);
                } else {
                    Ext.Msg.alert('Edit Address', 'You cannot edit addresses when reviewing a change request.');
                }
            }
        } else {
            Ext.Msg.alert('Edit Address', 'To edit an address you will need to login.');
        }
    },

    // logout the user
    logout: function() {
        Ext.Ajax.request({
            url: MAD.config.Urls.Logout,
            scope: this,
            disableCaching: false,
            success: function(response, options) {
                this.loginButton.show();
                this.userPrefDropDown.hide();
                this.user.logout();
                this.resetRoleBasedUI();
            },
            failure: function() {

            }
        });
    },

    // login the user, gets called by the listener in initComponent
    login: function(responseObj) {
        var response;
        if (responseObj.response) {
            // used when logging in with the form
            response = Ext.decode(responseObj.response.responseText);
        } else if (responseObj.responseText) {
            // used when auto loggin in using the getUser function below
            response = Ext.decode(responseObj.responseText);
        }
        this.loginButton.hide();
        this.userPrefDropDown.setText(response.returnObj.firstname);
        this.userPrefDropDown.show();
        this.user.login(response.returnObj);

        this.resetRoleBasedUI();
        if (response.returnObj.changePass === true) {
            this.passwordChangeObj = new Ext.Window({
                title: 'Update Account Information',
                layout: 'fit',
                width: 350,
                height: 380,
                items: [new FGI.widgets.PasswordChangeForm({ url: MAD.config.Urls.FirstLogin + '?user=' + response.returnObj.username, title: "", changeSecurityQuestion: true })],
                modal: true,
                draggable: false,
                closeAction: 'hide',
                closable: false
            });

            this.passwordChangeObj.show();
        }
    },

    // calls the app.login function if the user is logged in on the server
    getUser: function() {
        Ext.Ajax.request({
            url: MAD.config.Urls.GetUser,
            scope: this,
            disableCaching: false,
            success: function(response, options) {
                var ret = Ext.decode(response.responseText);
                if (ret.success) {
                    this.login(response);
                }
                // force focus on the search combo box...
                // we are doing this in getUser() because the login logic
                // was removing focus from the combobox; if we need to do
                // anything further on app load we should instead fire an
                // event here
                this.Search.query('combo')[0].focus();
            }
        });
    },

    resetRoleBasedUI: function() {
        this.TasksBar.setUser(this.user);
        this.results.setUser(this.user);
        this.MapContainer.contextMenu.setUser(this.user);
    },

    showPasswordHelp: function() {
        var passwordResetForm = new FGI.widgets.PasswordResetForm({
            url: MAD.config.Urls.ResetPassword
        });
        this.passwordHelpWindow = new Ext.Window({
            layout: 'fit',
            width: 350,
            height: 165,
            items: [ passwordResetForm ],
            modal: true,
            draggable: false,
            closeAction: 'hide',
            closable: false,
            resizable: false,
            listeners: {
                'show': function(w) {
                    passwordResetForm.setFocus();
                },
                scope: this
            }
        });
        this.passwordHelpWindow.show();
    },

    addAddressSearchMarker: function(record, runFade) {
        var addressSearch_layer = this.Map.getLayersByName('SearchedAddresses')[0];
        addressSearch_layer.clearMarkers();
        if (!record) {
            return;
        }
        var size = new OpenLayers.Size(18, 26);
        var calculateOffset = function(size) {
            return new OpenLayers.Pixel(-(size.w / 2), -size.h);
        };
        var icon = new OpenLayers.Icon(MAD.config.Images.BluePin, size, null, calculateOffset);
        addressSearch_layer.setVisibility(true);
        var geom = new OpenLayers.Format.WKT().read(record.get('geometry')).geometry;
        var lonLat = new OpenLayers.LonLat(geom.x, geom.y);
        var marker = new FGI.openlayers.SmartMarker(lonLat, icon.clone(), record.data, 'address_base_id');
        addressSearch_layer.addFadeFeature(marker, runFade);
    },

    featureCountWindow: Ext.define('MAD.widgets.featureCountWindow', {
        extend: 'Ext.Window',
        closable: false,
        hidden: true,
        border: false,
        bodyStyle: 'padding: 2px; color: #000099; background-color: #C9D9EB; font-weight: bold;',
        x: 60,
        y: 35,
        width: 150,
        maxCount: 0,
        isLoading: false,
        updateCount: function(count) {
            var title = 'Viewing ' + count + ' Addresses';
            if (count >= this.maxCount) {
                title = 'More than ' + this.maxCount + ' Addresses in this view.';
            }
            //this.setTitle(title);
            this.body.dom.innerHTML = title;
        }
    }),

    requestException: function(conn, response, options) {
       // This is a hack and awaits a more sophisticated approach.
       // If you try to use 307 in the request status, IE does not work as expected.
       // Using "401 Unauthorized" may not be technically correct but it does work.
       if (response.responseText) {
           try {
               var json = Ext.JSON.decode(response.responseText);
               if (json.status === '401' && json.message === 'MAINTENANCE_MODE') {
                   window.location.reload();
               } else {
                   Ext.Msg.alert('Unexpected Error',  'Please send the following information to support staff: ' + response.responseText);
               }
           }
           catch(err) {
               Ext.Msg.alert('Unexpected Error', 'Sorry! Something went wrong that we did not anticipate. Please send the following information to the support staff: ' + err.message);
           }
       } else {
           Ext.Msg.alert('Unexpected Error', 'Please inform the support staff that a responseText was empty.');
       }
    }

};

Ext.Ajax.on('requestexception', MAD.app.requestException);
