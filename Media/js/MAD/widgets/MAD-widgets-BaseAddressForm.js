Ext.define('MAD.widgets.BaseAddressFormPanel', {
    extend: 'Ext.FormPanel',
    alias: 'widget.baseAddressFormPanel',

    record: undefined,
    apnGridPanel: {},
    streetSegmentStore: {},
    dispositionStore: undefined,
    fieldDefaults: {
        labelAlign: 'left'
    },
    frame: false,
    title: 'Base Address',
    bodyStyle: 'background-color: #DFE8F6; padding: 5px;',
    autoScroll: true,
    layout: 'anchor',
    border: false,

    commitRecord: function () {
        this.getForm().updateRecord(this.record);
        this.record.validate();
        this.record.commit();
    },

    initComponent: function () {

        var unitTypeStore = MAD.app.unitTypeStore;
        var floorsStore = MAD.app.floorsDomainStore;

        var status = 'edit';
        if (this.record.get('retire_flg') === true) {
            status = 'retire';
            this.apnGridPanel.enableCheckColumns(false);
        }
        if (this.record.get('isNew') === true) {
            status = 'new';
        }

        Ext.apply(this, {
            items: [
                this.editingRadioGroup = new Ext.form.RadioGroup({
                    hideLabel: true,
                    columns: 2,
                    items: [
                        this.editRadio = new Ext.form.Radio({
                            boxLabel: 'Edit',
                            name: 'rb-status'
                        }),
                        this.retireRadio = new Ext.form.Radio({
                            boxLabel: 'Retire',
                            name: 'rb-status'
                        })
                    ]
                }),
                {
                    xtype: 'container',
                    style: 'padding-bottom:5px;',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        this.numTextField = new Ext.form.TextField({ 
                            fieldLabel: 'Number', 
                            name: 'num', 
                            allowBlank: false, 
                            blankText: 'this field is required', 
                            vtype: 'AddressNumber',
                            flex: 1
                        }),
                        {
                            xtype: 'container',
                            html: '',
                            width: 5
                        },
                        this.suffixTextField = new Ext.form.ComboBox({
                            fieldLabel: 'Number Suffix',
                            name: 'base_address_suffix',
                            autoSelect: false,
                            allowBlank: true,
                            editable: false,
                            triggerAction: 'all',
                            typeAhead: true,
                            width: 75,
                            queryMode: 'local',
                            displayField: 'suffix_display',
                            valueField: 'suffix_value',
                            emptyText: '',
                            forceSelection: false,
                            selectOnFocus: true,
                            store: MAD.app.addressBaseNumberSuffixDomainStore,
                            flex: 1
                        })
                    ]
                },
                this.streetsCombo = new Ext.form.ComboBox({
                    fieldLabel: 'Street',
                    anchor: '100%',
                    store: this.streetSegmentStore,
                    name: 'street_segment_id',
                    valueField: 'street_segment_id',
                    displayField: 'description',
                    queryMode: 'local',
                    emptyText: 'Select a street...',
                    editable: false,
                    triggerAction: 'all',
                    forceSelection: true,
                    allowBlank: false,
                    blankText: 'this field is required'
                }),
                new Ext.form.ComboBox({
                    store: this.dispositionStore,
                    anchor: '100%',
                    fieldLabel: 'Current Status',
                    name: 'unit_disposition',
                    typeAhead: true,
                    forceSelection: true,
                    triggerAction: 'all',
                    selectOnFocus: true,
                    valueField: 'disposition_code',
                    displayField: 'disposition_description',
                    queryMode: 'local',
                    disabled: true
                }, this),
                this.unitFieldSet = new Ext.form.FieldSet({
                    title: "Unit Information",
                    collapsed: false,
                    checkboxToggle: false,
                    items: [
                        new Ext.form.ComboBox({
                            store: unitTypeStore,
                            anchor: '100%', 
                            fieldLabel: 'Unit Type',
                            name: 'unit_type',
                            typeAhead: true,
                            forceSelection: true,
                            triggerAction: 'all',
                            selectOnFocus: true,
                            valueField: 'unit_type_id',
                            displayField: 'unit_type_description',
                            queryMode: 'local',
                            disabled: (status != 'new')
                        }),
                        new Ext.form.ComboBox({
                            store: floorsStore,
                            anchor: '100%',
                            fieldLabel: 'Floor',
                            name: 'floor_id',
                            typeAhead: true,
                            forceSelection: true,
                            triggerAction: 'all',
                            selectOnFocus: true,
                            valueField: 'floor_id',
                            displayField: 'floor_description',
                            queryMode: 'local',
                            disabled: (status != 'new')
                        }), { 
                            xtype: 'textfield', 
                            anchor: '100%',
                            fieldLabel: 'Number', 
                            name: 'unit_num', 
                            disabled: (status != 'new'),
                            vtype: 'UnitNumber', 
                            maxLength: 10 
                        }
                    ]
                })
            ]
        });

        this.unitFieldSet.on('afterrender', function () {
            this.loadData();
        }, this, { single: true });

        this.callParent(arguments);
    },

    loadData: function () {
        this.getForm().loadRecord(this.record);

        var status = 'edit';
        if (this.record.get('retire_flg') === true) {
            status = 'retire';
        }
        if (this.record.get('isNew') === true) {
            status = 'new';
        }

        if (!this.record.get('base_address_suffix')) {
            this.suffixTextField.setValue(null);
        }

        switch (status) {
            case 'new':
                this.editingRadioGroup.hide();
                break;
            case 'edit':
                this.editRadio.setValue(true);
                break;
            case 'retire':
                this.retireRadio.setValue(true);
                break;
        }

        Ext.each(this.query('textfield'), function (item, index, allItems) {
            item.on('change', function () {
                this.commitRecord();    
            }, this);
        }, this);

        Ext.each(this.query('checkbox'), function (item, index, allItems) {
            item.on('change', function () {
                this.commitRecord();
            }, this);
        }, this);

        Ext.each(this.query('combobox'), function (combobox, record, index) {
            combobox.on('select', function () {
                this.commitRecord();
            }, this);
        }, this);
    },

    disableAll: function () {
        Ext.each(this.query('textfield'), function (item, index, allItems) {
            item.disable();
        }, this);

        Ext.each(this.query('checkbox'), function (item, index, allItems) {
            item.disable();
        }, this);

        Ext.each(this.query('combo'), function (item, index, allItems) {
            item.disable();
        }, this);
        this.editRadio.disable();
        this.retireRadio.disable();
        this.apnGridPanel.enableCheckColumns(false);
    },

    reconfigureStatus: function (status) {
        // todo - more MVC needed here
        switch (status) {
            case 'edit':
                this.disableAll();
                this.editRadio.enable();
                this.retireRadio.enable();
                this.apnGridPanel.enableCheckColumns(true);
                this.record.set('retire_flg', false);
                this.commitRecord();
                break;
            case 'retire':
                this.disableAll();
                this.editRadio.enable();
                this.retireRadio.enable();
                this.record.set('retire_flg', true);
                this.commitRecord();
                break;
        }
    }

});