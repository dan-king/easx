Ext.define('MAD.widgets.ChangeRequestFormPanel', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.changeRequestFormPanel',

    record: null,
    border: false,
    user: {},

    initComponent: function () {
        this.addEvents({
            'endworkflow': true,
            'savechangerequest': true,
            'submitchangerequest': true,
            'approvechangerequest': true,
            'rejectchangerequest': true,
            'deletechangerequest': true
        });

        this.saveBtn = new Ext.Button({
            text: 'Save',
            iconCls: 'silk-database-save',
            disabled: 'True',
            iconAlign: 'top',
            handler: function () {
                this.fireEvent('savechangerequest');
            },
            scope: this,
            style: 'margin-bottom: 10px;'
        });

        this.closeBtn = new Ext.Button({
            text: 'Close',
            iconCls: 'silk-door-out',
            iconAlign: 'top',
            handler: function () {
                this.fireEvent('endworkflow');
            },
            scope: this,
            style: 'margin-bottom: 10px;'
        });

        this.discardBtn = new Ext.Button({
            text: 'Delete',
            iconCls: 'silk-database-delete',
            iconAlign: 'top',
            handler: function () {
                Ext.MessageBox.show({
                    title: "Confirm",
                    msg: "Delete this change request?",
                    fn: function (buttonId, text, opt) {
                        if (buttonId == 'ok') {
                            this.fireEvent('deletechangerequest');
                        }
                    },
                    scope: this,
                    buttons: Ext.Msg.OKCANCEL,
                    animEl: 'elId',
                    icon: Ext.MessageBox.WARNING
                });
            },
            hidden: true,
            disabled: true,
            scope: this,
            style: 'margin-bottom: 10px;'
        });

        this.submitBtn = new Ext.Button({
            text: 'Submit',
            iconCls: 'silk-database-go',
            iconAlign: 'top',
            handler: function () {
                this.fireEvent('submitchangerequest');
            },
            scope: this,
            hidden: true,
            style: 'margin-bottom: 10px;'
        });

        this.acceptBtn = new Ext.Button({
            text: 'Start<br>Review',
            iconCls: 'silk-database-edit',
            iconAlign: 'top',
            handler: function () {
                this.fireEvent('acceptchangerequestforreview');
            },
            scope: this,
            hidden: true,
            style: 'margin-bottom: 10px;'
        });

        // this.acceptWithoutReviewBtn = new Ext.Button({
        //     text: 'Approve<br>without<br>Review',
        //     iconCls: 'silk-database-edit',
        //     iconAlign: 'top',
        //     handler: function () {
        //         this.fireEvent('approvechangerequest');
        //     },
        //     scope: this,
        //     hidden: true,
        //     style: 'margin-bottom: 10px;'
        // });

        this.approveBtn = new Ext.Button({
            text: 'Approve',
            iconCls: 'silk-database-add',
            iconAlign: 'top',
            handler: function () {
                this.fireEvent('approvechangerequest');
            },
            scope: this,
            hidden: true,
            style: 'margin-bottom: 10px;'
        });

        this.rejectBtn = new Ext.Button({
            text: 'Disapprove',
            iconCls: 'silk-database-error',
            iconAlign: 'top',
            handler: function () {
                this.fireEvent('rejectchangerequest');
            },
            scope: this,
            hidden: true,
            style: 'margin-bottom: 10px;'
        });

        var buttons = [
            this.saveBtn,
            this.closeBtn,
            this.discardBtn,
            this.submitBtn,
            this.acceptBtn,
            // this.acceptWithoutReviewBtn,
            this.approveBtn,
            this.rejectBtn
        ];

        // For some mysterious reason, I am unable to successfully use xtypes in the apply below.
        // In particular, for xtype of displayfield, the fieldLabel does not render.
        // Creating instances solves the problem - but makes me sad:-(
        this.titleField = Ext.create('Ext.form.field.TextArea', {fieldLabel: 'Title', name: 'name', height: 50, maxLength: 100 });
        this.requestorComments = Ext.create('Ext.form.field.TextArea', {fieldLabel: 'Comments', name: 'requestor_comment', disabled: 'True', maxLength: 500 });
        this.reviewerComments = Ext.create('Ext.form.field.TextArea', {fieldLabel: 'Comments', name: 'reviewer_comment', disabled: 'True', maxLength: 500 });
        var fields = [
            this.titleField,
            Ext.create('Ext.form.field.Display', {fieldLabel: 'Id', name: 'change_request_id'}),
            Ext.create('Ext.form.field.Display', {fieldLabel: 'Status', name: 'status_description'}),
            Ext.create('Ext.form.field.Display', {fieldLabel: 'Requestor', name: 'requestor_name'}),
            Ext.create('Ext.form.field.Display', {fieldLabel: 'Updated', name: 'requestor_last_update'}),
            this.requestorComments,
            Ext.create('Ext.form.field.Display', {fieldLabel: 'Reviewer', name: 'reviewer_name'}),
            Ext.create('Ext.form.field.Display', {fieldLabel: 'Updated', name: 'reviewer_last_update'}),
            this.reviewerComments
        ];

        Ext.apply(this, {
            fieldDefaults: {
                labelAlign: 'left',
                anchor: '95%'
            },
            lbar: new Ext.toolbar.Toolbar({
                items: buttons,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                }
            }),
            bodyStyle: 'padding:5px 5px 0; background-color: #DFE8F6;',
            items: fields
        });

        // This is needed to enable the save button.
        this.on('afterlayout', function () {
            this.getForm().getFields().each(function (field, index, fieldCount) {
                field.on('change', function (field, newValue, oldValue, eOpts) {
                    if (this.record != null ) {
                        this.getForm().updateRecord(this.record);
                        this.record.commit();
                        field.focus();
                    }
                }, this);
            }, this);
        }, this, { single: true });

        this.callParent(arguments);
    }
});
