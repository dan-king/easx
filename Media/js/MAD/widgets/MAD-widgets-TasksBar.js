Ext.define('MAD.widgets.TasksBar', {
    extend: 'Ext.container.Container',

    layout: {
        type: 'hbox',
        align: 'top',
        pack: 'center'
    },

    initComponent: function () {
        this.addEvents({
            'myEditsClicked': true,
            'reviewEditsClicked': true
        });

        Ext.apply(this, {
            defaultType: 'button',
            defaults: {
                style: 'margin-right:14px;',
                height: 26,
                enableToggle: false,
                scope: this,
                disabled: true
            },
            items: [{
                text: 'My Changes',
                handler: function () {
                    this.fireEvent('myEditsClicked');
                },
                name: 'myEdits',
                flex: 1
            }, {
                text: 'Review Changes',
                handler: function () {
                    this.fireEvent('reviewEditsClicked');
                },
                name: 'reviewEdits',
                flex: 1
            }]
        });

        this.callParent(arguments);
    },

    getButtonByName: function (buttonName) {
        return this.query('button[name="' + buttonName + '"]')[0];
    },

    doForEachButton: function (func) {
        Ext.each(this.query('button'), func, this);
    },

    enableButton: function (buttonName) {
        this.getButtonByName(buttonName).enable();
    },

    disableButton: function (buttonName) {
        this.getButtonByName(buttonName).disable();
    },

    enableAll: function () {
        this.doForEachButton(function (button) {
            button.enable();
        });
    },

    disableAll: function () {
        this.doForEachButton(function (button) {
            button.disable();
        });
    },

    setUser: function (user) {
        if (MAD.app.activeWorkflow === null) {
            this.disableAll();
            if (user.isRequestor()) {
                this.getButtonByName("myEdits").enable();
            }
            if (user.isApprover()) {
                this.getButtonByName("reviewEdits").enable();
            }
        }
    }
});