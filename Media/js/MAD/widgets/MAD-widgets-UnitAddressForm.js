Ext.define('MAD.widgets.UnitAddressFormPanel', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.unitAddressFormPanel',

    record: undefined,
    apnGridPanel: {},
    dispositionStore: undefined,
    domainTables: undefined,
    fieldDefaults: {
        labelAlign: 'left'
    },
    frame: false,
    title: 'Unit Address',
    bodyStyle: 'padding:5px; background-color: #DFE8F6;',
    layout: 'anchor',
    border: false,

    commitRecord: function () {
        this.getForm().updateRecord(this.record);
        this.record.validate();
        this.record.commit();
    },

    initComponent: function () {

        var status = 'edit';
        if (this.record.get('retire_flg') === true) {
            status = 'retire';
            this.apnGridPanel.enableCheckColumns(false);
        }
        if (this.record.get('isNew') === true) {
            status = 'new';
        }

        var unitTypeStore = MAD.app.unitTypeStore;
        var floorsStore = MAD.app.floorsDomainStore;

        Ext.apply(this, {
            items: [
                this.editingRadioGroup = new Ext.form.RadioGroup({
                    hideLabel: true,
                    columns: 2,
                    items: [
                        this.editRadio = new Ext.form.Radio({
                            boxLabel: 'Edit',
                            name: 'rb-status'
                        }),
                        this.retireRadio = new Ext.form.Radio({
                            boxLabel: 'Retire',
                            name: 'rb-status'
                        })
                    ]
                }),
                { 
                    xtype: 'textfield', 
                    fieldLabel: 'Number', 
                    anchor: '100%', 
                    name: 'address_num', 
                    enableKeyEvents: true, 
                    disabled: (status != 'new'), 
                    vtype: 'UnitNumber', 
                    maxLength: 10 
                },
                new Ext.form.ComboBox({
                    store: unitTypeStore,
                    fieldLabel: 'Unit Type',
                    anchor: '100%',
                    name: 'unit_type',
                    typeAhead: true,
                    forceSelection: true,
                    triggerAction: 'all',
                    emptyText: 'Select a unit type...',
                    selectOnFocus: true,
                    valueField: 'unit_type_id',
                    displayField: 'unit_type_description',
                    queryMode: 'local',
                    disabled: (status != 'new')
                }),
                new Ext.form.ComboBox({
                    store: floorsStore,
                    fieldLabel: 'Floor',
                    anchor: '100%',
                    name: 'floor_id',
                    typeAhead: true,
                    forceSelection: true,
                    triggerAction: 'all',
                    emptyText: 'Select a floor...',
                    selectOnFocus: true,
                    valueField: 'floor_id',
                    displayField: 'floor_description',
                    queryMode: 'local',
                    disabled: (status != 'new')
                }),
                new Ext.form.ComboBox({
                    store: this.dispositionStore,
                    fieldLabel: 'Current Status',
                    anchor: '100%',
                    name: 'disposition',
                    typeAhead: true,
                    forceSelection: true,
                    triggerAction: 'all',
                    selectOnFocus: true,
                    valueField: 'disposition_code',
                    displayField: 'disposition_description',
                    queryMode: 'local',
                    disabled: true
                }, this)
            ]
        });

        this.on('afterrender', function () {
            this.getForm().loadRecord(this.record);

            var status = 'edit';
            if (this.record.get('retire_flg') === true) {
                status = 'retire';
            }
            if (this.record.get('isNew') === true) {
                status = 'new';
            }

            switch (status) {
                case 'new':
                    this.editingRadioGroup.hide();
                    break;
                case 'edit':
                    this.editRadio.setValue(true);
                    break;
                case 'retire':
                    this.retireRadio.setValue(true);
                    break;
            }

            Ext.each(this.query('textfield'), function (item, index, allItems) {
                item.on('change', function () {
                    this.commitRecord();
                }, this);
            }, this);

            Ext.each(this.query('checkbox'), function (item, index, allItems) {
                item.on('change', function () {
                    this.commitRecord();
                }, this);
            }, this);

            Ext.each(this.query('combobox'), function (combobox, record, index) {
                combobox.on('select', function () {
                    this.commitRecord();
                }, this);
            }, this);

        }, this, { single: true });

        this.callParent(arguments);
    },

    disableAll: function () {
        Ext.each(this.query('textfield'), function (item, index, allItems) {
            item.disable();
        }, this);

        Ext.each(this.query('checkbox'), function (item, index, allItems) {
            item.disable();
        }, this);

        Ext.each(this.query('combo'), function (item, index, allItems) {
            item.disable();
        }, this);
        this.editRadio.disable();
        this.retireRadio.disable();
        this.apnGridPanel.enableCheckColumns(false);
    },

    reconfigureStatus: function (status) {
        // todo - more MVC needed here
        switch (status) {
            case 'edit':
                this.disableAll();
                this.editRadio.enable();
                this.retireRadio.enable();
                this.apnGridPanel.enableCheckColumns(true);
                this.record.set('retire_flg', false);
                this.commitRecord();
                break;
            case 'retire':
                this.disableAll();
                this.editRadio.enable();
                this.retireRadio.enable();
                this.record.set('retire_flg', true);
                this.commitRecord();
                break;
        }
    }

});