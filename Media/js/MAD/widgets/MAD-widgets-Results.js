Ext.define('MAD.widgets.Results', {
    extend: 'Ext.panel.Panel',
    layout: 'card',
    title: 'Results',
    floatable: false,
    region: 'west',
    minWidth: 200,
    maxWidth: 900,
    width: 300,
    margins: '0 5 0 5',
    border: true,
    resizable: true,
    resizeHandles: 'e',

    pageSize: 10,
    highlightedRow: null,
    user: null,

    initComponent: function () {

        this.user = MAD.model.User;

        this.addEvents('itemmouseenter','itemmouseleave','loadstore');

        var gridDefaults = {
            pageSize: this.pageSize,
            hideHeaders: true,
            disableSelection: true,
            border: false,
            viewConfig: {
                stripeRows: false
            },
            listeners: {
                'itemmouseenter': function(gridview, record, element, index, event, eOpts) {
                    this.highlightRecord(record, false);
                    this.fireEvent('itemmouseenter', gridview, record, element, index, event, eOpts);
                },
                'itemmouseleave': function (gridview, record, element, index, event, eOpts) {
                    this.fadeHighlight();
                    this.fireEvent('itemmouseleave', gridview, record, element, index, event, eOpts);
                },
                scope: this
            }
        };

        var proxyDefaults = {
            type: 'ajax',
            method: 'GET',
            reader: {
                type: 'json',
                    root: 'results',
                    totalProperty: 'count'
            }
        };

        var storeDefaults = {
            autoLoad: false,
            pageSize: this.pageSize,
            listeners: {
                'load': function(store, records, success, eOpts) {
                    this.fireEvent('loadstore', store, records, success, eOpts);
                },
                scope: this
            }
        };

        var toolbarDefaults = {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: true
        };

        var addressStore = new Ext.data.Store(Ext.apply({
            storeId: 'AddressSearchResultsStore',
            proxy: Ext.apply({
                url: MAD.config.Urls.AddressSearch
            }, proxyDefaults),
            model: 'MAD.model.AddressSearch'
        }, storeDefaults));

        var parcelStore = new Ext.data.Store(Ext.apply({
            storeId: 'ParcelSearchResultsStore',
            proxy: Ext.apply({
                url: MAD.config.Urls.ParcelSearch
            }, proxyDefaults),
            model: 'MAD.model.LandParcelRecord'
        }, storeDefaults));

        this.reportContainer = Ext.create('Ext.container.Container', {
            layout: 'fit'
        });

        this.lineageHeader = Ext.create('Ext.panel.Panel', {
            tpl: MAD.config.Tpls.lineageReportHeader,
            dock: 'top',
            bodyStyle: 'padding:7px;',
            border: false
        });

        this.items = [
            Ext.create('Ext.grid.GridPanel', Ext.apply({
                id: 'AddressSearchResultsGrid',
                columns: [{
                    flex: 1,
                    renderer: Ext.bind(function (value, p, record) {
                        return MAD.config.Tpls.addressDetailsSearchResults.apply(Ext.apply({
                                isRequestor: this.user.isRequestor(),
                                isSearchResult: true
                            }, record.data));
                    }, this)
                }],
                store: addressStore,
                dockedItems: [Ext.apply({
                    store: addressStore
                }, toolbarDefaults)]
            }, gridDefaults)),
            Ext.create('Ext.grid.GridPanel', Ext.apply({
                id: 'ParcelSearchResultsGrid',
                columns: [{
                    flex: 1,
                    renderer: function (value, p, record) {
                        return MAD.config.Tpls.parcelSearchResults.apply(record.data);
                    }
                }],
                store: parcelStore,
                dockedItems: [Ext.apply({
                    store: parcelStore
                }, toolbarDefaults)]
            }, gridDefaults)),
            Ext.create('Ext.grid.Panel', {
                id: 'LineageGrid',
                layout: 'fit',
                disableSelection: true,
                viewConfig: { trackOver: false },
                dockedItems: [this.lineageHeader],
                emptyText: 'No records found. Lineage is not supported prior to February 15, 2012.',
                store: Ext.create('Ext.data.Store', {
                    model: 'MAD.model.AddressesFlat',
                    proxy: {
                        type: 'ajax',
                        method: 'GET',
                        reader: {
                            type: 'json',
                            root: 'addressesFlat'
                        },
                        url: MAD.config.Urls.Lineage
                    },
                    listeners: {
                        'load': function (store, records, success, operation) {
                            var changeRequest = store.proxy.reader.rawData.changeRequest;
                            this.lineageHeader.update(changeRequest);
                            Ext.each(records, function(record) {
                                record.set('report_change_request_id', changeRequest.change_request_id);
                            }, this);
                        },
                        scope: this
                    }
                }),
                columns: [
                    { text: 'APN',  dataIndex: 'blk_lot', flex: 3,
                        renderer: function (value) {
                            return '<a class="property-information-link" href="#">' + value + '</a>';
                        }
                    },
                    { text: 'Address',  dataIndex: 'full_street_name', flex: 4, 
                        renderer: function(value, metaData, record) {
                            return '<a class="address-information-link" href="#">' + record.get('base_address_num') + ' ' + value + '</a>';
                        } 
                    },
                    { text: 'Base/Unit', dataIndex: 'address_base_flg', flex: 2, 
                        renderer: function(value) {
                            if (value) {
                                return 'base';
                            } else {
                                return 'unit';
                            }

                        }
                    },
                    { text: "Unit #", dataIndex: 'unit_num', flex: 2, renderer: function(value) {
                            var num = value ? value : "";
                            return Ext.util.Format.trim(num);
                        } 
                    },
                    { header: "Linked", dataIndex: 'address_x_parcel_create_tms', flex: 5,
                        renderer: function (value, metaData, record) {
                            var displayValue = '<span style="color:green;">' + value + '</span>';
                            if (value === "1970-01-01 00:00:00") {
                                displayValue = 'initial load';
                            } else if (record.get('report_change_request_id') !== record.get('address_x_parcel_activate_change_request_id')) {
                                displayValue = '<a class="lineage-create-link" href="#">' + value + '</a>';
                            }
                            return displayValue;
                        }
                    },
                    { header: "Unlinked", dataIndex: 'address_x_parcel_retire_tms', flex: 5,
                        renderer: function (value, metaData, record) {
                            var displayValue = '<span style="color:green;">' + value + '</span>';
                            if (!value) {
                                displayValue = '';
                            } else if (record.get('report_change_request_id') !== record.get('address_x_parcel_retire_change_request_id')) {
                                displayValue = '<a class="lineage-retire-link" href="#">' + value + '</a>';
                            }
                            return displayValue;
                        }
                    }
                ],
                listeners: {
                    'itemclick': function (view, record, item, index, e) {
                        var className = e.getTarget().className;
                        if (className === "property-information-link") {
                            // include retired
                            this.showSearchResults("ParcelSearch", record.get('blk_lot'), true, true);
                        } else if (className === 'lineage-create-link' || className === 'lineage-retire-link') {
                            this.showLineage(record.get('address_x_parcel_id'), className==='lineage-create-link'?'create':'retire');
                        } else if (className === 'address-information-link') {
                            this.fireEvent('addresslinkclicked', record);
                        }
                    },
                    scope: this
                }
            }),
            this.reportContainer
        ];

        this.callParent(arguments);
    },

    switchResultsTypeTo: function (resultsType, callback) {
        var grid,
            title,
            url,
            width = 300;

        switch (resultsType) {
            case 'AddressSearch':
                url = MAD.config.Urls.AddressSearch;
                grid = this.query('#AddressSearchResultsGrid')[0];
                title = 'Addresses';
                break;
            case 'ParcelToAddressesSearch':
                url = MAD.config.Urls.ParcelToAddresses;
                grid = this.query('#AddressSearchResultsGrid')[0];
                title = 'Addresses';
                break;
            case 'ParcelSearch':
                url = MAD.config.Urls.ParcelSearch;
                grid = this.query('#ParcelSearchResultsGrid')[0];
                title = 'Parcels';
                break;
            case 'AddressToParcelsSearch':
                url = MAD.config.Urls.AddressToParcels;
                grid = this.query('#ParcelSearchResultsGrid')[0];
                title = 'Parcels';
                break;
            case 'PointToParcelsSearch':
                url = MAD.config.Urls.PointToParcels;
                grid = this.query('#ParcelSearchResultsGrid')[0];
                title = 'Parcels';
                break;
            case 'Lineage':
                url = MAD.config.Urls.Lineage;
                grid = this.query('#LineageGrid')[0];
                title = 'Change Report';
                width = 560;
                break;
        }
        this.setWidth(width);
        grid.store.proxy.url = url;
        grid.store.removeAll();
        this.reportContainer.removeAll();
        this.layout.setActiveItem(grid);
        this.setTitle(title);
        return grid;
    },

    setUser: function(user) {
        this.user = user;
        if (this.layout.getActiveItem().getView) {
            this.layout.getActiveItem().getView().refresh();
        }
    },

    showSearchResults: function(resultsType, queryValue, includeRetired, exact) {
        var grid = this.switchResultsTypeTo(resultsType);
        Ext.apply(grid.store.proxy.extraParams, {
            query: queryValue,
            includeRetired: includeRetired,
            exact: exact
        });
        grid.store.loadPage(1);
    },

    showLineage: function (axpId, type) {
        var grid = this.switchResultsTypeTo('Lineage');
        grid.store.proxy.url += axpId + '/' + type + '/';
        grid.store.load();
    },

    showReport: function (report) {
        this.reportContainer.removeAll();
        this.reportContainer.add(report);
        this.setWidth(480);
        this.layout.setActiveItem(this.reportContainer);
        this.setTitle('Address Details');
    },

    highlightRecord: function (record, autoFade) {
        var view = this.layout.getActiveItem().getView(),
            node = view.getNode(record)
            rowForHighlight = Ext.get(node).first();

        if (this.highlightedRow) {
            this.highlightedRow.stopAnimation();
        }
        rowForHighlight.setStyle('backgroundColor', '#EFEFEF');
        this.highlightedRow = rowForHighlight;
        if (autoFade) {
            this.fadeHighlight();
        }
    },

    fadeHighlight: function () {
        if (this.highlightedRow) {
            this.highlightedRow.animate({
                duration: 1500,
                to: {
                    backgroundColor: '#FFFFFF'
                },
                listeners: {
                    'afteranimate': function () {
                        if (this.highlightedRow) {
                            this.highlightedRow.setStyle('backgroundColor', '#FFFFFF');
                            this.highlightedRow = null;
                        }
                    },
                    scope: this
                }
            });
        }
    }
});
