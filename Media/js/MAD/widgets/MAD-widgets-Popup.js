﻿Ext.define('MAD.widgets.Popup', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.mad-widgets-popup',

    layout: 'fit',
    border: true,
    floating: true,
    height: 240,
    width: 300,
    expandedHeight: 400,
    expandedWidth: 500,
    expandable: true,
    baseCls: 'x-plain',
    shadow: false,
    map: null,
    lonLat: null,
    panIn: false,
    hideOnMapMove: false,
    offset: {
        x: 0,
        y: -3
    },
    header: false,

    initComponent: function () {
        this.minimizedHeight = this.height;
        this.minimizedWidth = this.width;

        if (this.geometry && !this.lonLat) {
            this.geometry.getBounds();  // forces the bounds to be calculated if they already aren't
            this.lonLat = new OpenLayers.LonLat(this.geometry.bounds.left, this.geometry.bounds.top);
        }

        this.anchor = Ext.create('Ext.toolbar.Toolbar', {
            baseCls: 'x-plain',
            cls: 'popup-anchor',
            dock: 'bottom',
            height: 16
        });

        this.contentPanel = Ext.create('Ext.panel.Panel', {
            frame: true,
            tools: [
                {
                    id: 'plus',
                    handler: function() {
                        this.expand();
                        this.contentPanel.tools.plus.hide();
                        this.contentPanel.tools.minus.show();
                    },
                    scope: this,
                    hidden: !this.expandable
                }, {
                    id: 'minus',
                    handler: function() {
                        this.minimize();
                        this.contentPanel.tools.plus.show();
                        this.contentPanel.tools.minus.hide();
                    },
                    scope: this,
                    hidden: true
                }, {
                    type: 'close',
                    handler: function() {
                        this.destroy();
                    },
                    scope: this
                }
            ],
            layout: 'fit',
            title: this.title
        });
        
        var content = this.items;
        this.items = [this.contentPanel];

        this.dockedItems = [this.anchor];

        this.callParent(arguments);

        this.on({
            show: function() {
                this.addAnchorEvents();

                new Ext.util.KeyMap(this.getEl(), {
                    key: Ext.EventObject.ESC,
                    fn: this.destroy,
                    scope: this
                });
            },
            'popupready': function () {
                this.contentPanel.add(content);
            },
            scope: this,
            single: true
        });
    },

    expand: function () {
        this.setSize(this.expandedWidth, this.expandedHeight);
    },

    minimize: function () {
        this.setSize(this.minimizedWidth, this.minimizedHeight)
    },

    hide: function(animateTarget, callback, scope, autoHide) {
        this._autoHidden = autoHide;
        this.callParent(arguments);
    },

    show: function() {
        if (!this._autoHidden) {
            this.callParent(arguments);
            this.position();
            // UGLY hack to fix issue with layout in IE when popup is on edge of map.
            // Seems to be an issue with ExtJS layouts and floating components.
            // http://code.google.com/p/eas/issues/detail?id=668
            var offset = this.getOffsetForPopupDisplay();
            if (Ext.isIE && offset.dx > 0) {
                // make popup transparent so that it doesn't flash on the
                // screen during the defer
                this.center();
                this.getEl().setStyle('filter','alpha(opacity=0)');
                Ext.defer(function() {
                    this.center();
                    Ext.defer(function() {
                        this.position();
                        // make popup opaque again, so that it is visible
                        this.getEl().setStyle('filter','');
                        this.fireEvent('popupready', this);
                    }, 1, this);
                }, 1, this);
            } else {
                this.fireEvent('popupready', this);
            }

            if(this.panIn && !this._mapMove) {
                this.panIntoView();
            } else if (this.hideOnMapMove) {
                this.addHideOnMapMoveListener();
            }
        }
    },

    position: function () {
        var pix,
            visible,
            mapbox,
            anc,
            dx,
            dy;

            if (this.map.getExtent().containsLonLat(this.lonLat)) {
                if (this._autoHidden) {
                    delete this._autoHidden;
                    this.show();
                    return;
                }
                pix = this.map.getViewPortPxFromLonLat(this.lonLat);
                mapBox = Ext.fly(this.map.div).getBox();

                //This works for positioning with the anchor on the bottom.
                dx = this.getWidth() / 2;
                dy = this.getHeight();

                this.setPosition(pix.x + mapBox.x - dx + this.offset.x, pix.y + mapBox.y - dy + this.offset.y);
            } else {
                this.hide(null, null, null, true)
            }
    },
    
    /** private: method[panIntoView]
     *  Pans the MapPanel's map so that an anchored popup can come entirely
     *  into view, with padding specified as per normal OpenLayers.Map popup
     *  padding.
     */ 
    panIntoView: function() {
        var offset = this.getOffsetForPopupDisplay();

        this.map.pan(offset.dx, offset.dy);

        if ((offset.dx !== 0 || offset.dy!== 0) && this.hideOnMapMove) {
            var addListener = function () {
                this.addHideOnMapMoveListener();
                this.map.events.un({
                    "moveend" : addListener,
                    scope : this
                });
            };
            this.map.events.on({
                "moveend" : addListener,
                scope : this            
            });
        } else if (this.hideOnMapMove) {
            this.addHideOnMapMoveListener();
        }
    },

    getOffsetForPopupDisplay: function() {
        var mapBox = Ext.fly(this.map.div).getBox(); 

        //assumed viewport takes up whole body element of map panel
        var popupPos =  this.getPosition(true);
        popupPos[0] -= mapBox.x;
        popupPos[1] -= mapBox.y;
       
        var panelSize = [mapBox.width, mapBox.height]; // [X,Y]

        var popupSize = this.getSize();

        var newPos = [popupPos[0], popupPos[1]];

        //For now, using native OpenLayers popup padding.  This may not be ideal.
        var padding = this.map.paddingForPopups;

        // X
        if(popupPos[0] < padding.left) {
            newPos[0] = padding.left;
        } else if(popupPos[0] + popupSize.width > panelSize[0] - padding.right) {
            newPos[0] = panelSize[0] - padding.right - popupSize.width;
        }

        // Y
        if(popupPos[1] < padding.top) {
            newPos[1] = padding.top;
        } else if(popupPos[1] + popupSize.height > panelSize[1] - padding.bottom) {
            newPos[1] = panelSize[1] - padding.bottom - popupSize.height;
        }

        var dx = popupPos[0] - newPos[0];
        var dy = popupPos[1] - newPos[1];

        return {dx: dx, dy: dy}
    },

    addHideOnMapMoveListener: function() {
        var hideMe = function () {
            this.hide();
            this.map.events.un({
                "move":hideMe,
                scope:this
            });
        };
        this.map.events.on({
            "move" : hideMe,
            scope : this            
        });
    },
    
    /** private: method[onMapMove]
     */
    onMapMove: function() {
        this._mapMove = true;
        if (this.isVisible() || this._autoHidden) {
            this.position();
        }
        delete this._mapMove;
    },
    
    /** private: method[addAnchorEvents]
     */
    addAnchorEvents: function() {
        this.map.events.on({
            "move" : this.onMapMove,
            scope : this            
        });
        
        this.on({
            "resize": this.position,
            "collapse": this.position,
            "expand": this.position,
            scope: this
        });
    },
    
    /** private: method[removeAnchorEvents]
     */
    removeAnchorEvents: function() {
        //stop position with feature
        this.map.events.un({
            "move" : this.onMapMove,
            scope : this
        });

        this.un("resize", this.position, this);
        this.un("collapse", this.position, this);
        this.un("expand", this.position, this);

    },

    destroy: function() {
        this.removeAnchorEvents();
        this.callParent(arguments);
    }

});
