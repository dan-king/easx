Ext.define('MAD.workflows.ChangeRequest', {
    extend: 'Ext.Component',

    // todo - need a better way to coordinate we do the showMask and hideMask 

    app: MAD.app, // the app to which we associate this workflow

    changeRequestId: null,
    reclaimForEditing: false,
    addressId: null,
    retireFlg: false,
    point: null,

    retrievingAddressData: false,
    hasChanged: false,
    currentDetailForm: null,

    initComponent: function() {
        this.app.activeWorkflow = this;

        this.mapController = new MAD.workflows.ChangeRequest.MapController({
            workflow: this,
            mapContainer: this.app.MapContainer,
            map: this.app.Map
        });

        this.initUI();

        MAD.workflows.ChangeRequest.superclass.initComponent.apply(this, arguments);
    },

    initUI: function() {
        ///// set up some use commands
        this.commandFactory = new MAD.workflows.ChangeRequest.CommandFactory();

        var disableSaveCommandSet = new MAD.utils.CommandSet();
        disableSaveCommandSet.add(this.commandFactory.createDisableSave({ scope: this }));

        var enableSaveCommandSet = new MAD.utils.CommandSet();
        enableSaveCommandSet.add(this.commandFactory.createEnableSave({ scope: this }));

        var endWorkflowCommandSet = new MAD.utils.CommandSet();
        endWorkflowCommandSet.add(this.commandFactory.createEndWorkflow({ scope: this }));

        var approveCommandSet = new MAD.utils.CommandSet();
        approveCommandSet.add(this.commandFactory.createEndWorkflow({ scope: this }));
        approveCommandSet.add(this.commandFactory.createFireApproveEvent({ scope: this }));
        /////


        var notifyOfChange = Ext.bind(function(store, record, stringOrIndex) {
            enableSaveCommandSet.execute();
            //if (console) {
            //    console.log('client defined notify');
            //}
        }, this);
        this.changeListener = new MAD.model.ChangeListener({ notify: notifyOfChange });

        // the workflow panel contains all workflow specific widgets
        this.workflowPanel = new Ext.Panel({
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            border: false,
            items: [
                this.changeRequestFormPanel = new MAD.widgets.ChangeRequestFormPanel({
                    flex: 1,
                    user: this.app.user
                }),
                this.baseAddressAccordionPanel = new MAD.widgets.BaseAddressAccordionPanel({
                    flex: 1
                }),
                {
                    // Since we change the visibility of AddressDetailPanel we use extra container here to prevent the layout from jumping around.
                    xtype: 'container',
                    flex: 1,
                    layout: 'fit',
                    border: false,
                    style: {
                        'background-color': '#DFE8F6;'
                    },
                    items: [
                        this.addressDetailPanel = Ext.create('MAD.widgets.AddressDetailPanel')
                    ]
                }
            ]
        });

        // add listener to end the workflow when change request form is completed (submitted, discarded, etc.)
        this.changeRequestFormPanel.on('endworkflow', function() {
            this.confirmEndWorkflow();
        }, this);


        ///// add listeners to change request form BEGIN

        this.changeRequestFormPanel.on('savechangerequest', function() {
            this.saveChangeRequest(MAD.config.Urls.SaveChangeRequest, disableSaveCommandSet);
        }, this);

        this.changeRequestFormPanel.on('approvechangerequest', function() {
            this.saveChangeRequest(MAD.config.Urls.ApproveChangeRequest, approveCommandSet);
        }, this);

        this.changeRequestFormPanel.on('acceptchangerequestforreview', function() {
            this.saveChangeRequest(MAD.config.Urls.AcceptChangeRequestForReview, disableSaveCommandSet);
        }, this);

        this.changeRequestFormPanel.on('submitchangerequest', function() {
            this.saveChangeRequest(MAD.config.Urls.SubmitChangeRequest, endWorkflowCommandSet);
        }, this);

        this.changeRequestFormPanel.on('rejectchangerequest', function() {
            this.saveChangeRequest(MAD.config.Urls.RejectChangeRequest, endWorkflowCommandSet);
        }, this);

        this.changeRequestFormPanel.on('deletechangerequest', function() {
            if (this.changeRequest.get('change_request_id') != null) {
                this.deleteChangeRequest(MAD.config.Urls.DeleteChangeRequest + this.changeRequest.get('change_request_id') + '/', endWorkflowCommandSet);
            } else {
                endWorkflowCommandSet.execute();
            }
        }, this);

        ///// add listeners to change request form END


        this.app.changeRequestPanel.add(this.workflowPanel);

        // reconfigure UI for change request workflow
        this.app.changeRequestPanel.expand();
        this.app.changeRequestPanel.show();

        var beforeXHRCommands = new MAD.utils.CommandSet();
        var afterXHRCommands = new MAD.utils.CommandSet();

        this.changeRequestStore = new MAD.model.ChangeRequestStore({ changeListener: this.changeListener });

        // get the request url based on the passed in id or point
        if (this.changeRequestId != null) {
            var url;
            if (this.reclaimForEditing) {
                url = Ext.String.format(MAD.config.Urls.EditChangeRequest, this.changeRequestId);
            } else {
                url = Ext.String.format(MAD.config.Urls.GetChangeRequest, this.changeRequestId);
            }
            // get the change request json and load the stores
            beforeXHRCommands.add(this.commandFactory.createShowMask({ scope: this, args: ['Loading...'] }));
            afterXHRCommands.add(this.commandFactory.createActivateChangeListener({ scope: this }));
            afterXHRCommands.add(this.commandFactory.createHideMask({ scope: this }));
            afterXHRCommands.add(this.commandFactory.createExpandTopBaseAddress({ scope: this }));
            this.getChangeRequest(url, beforeXHRCommands, afterXHRCommands);
        } else if (this.addressId != null) {

            beforeXHRCommands.add(this.commandFactory.createShowMask({ scope: this, args: ['Loading...'] }));
            afterXHRCommands.add(this.commandFactory.createActivateChangeListener({ scope: this }));
            afterXHRCommands.add(this.commandFactory.createEnableSave({ scope: this }));
            afterXHRCommands.add(this.commandFactory.createHideMask({ scope: this }));
            afterXHRCommands.add(this.commandFactory.setNewChangeRequestTitle({ scope: this }));
            this.newChangeRequest();
            this.getBaseAddress(this.addressId, this.retireFlg, beforeXHRCommands, afterXHRCommands);
        } else {
            //get new CR and add a new base address at the passed in point
            beforeXHRCommands.add(this.commandFactory.createShowMask({ scope: this, args: ['Loading...'] }));
            afterXHRCommands.add(this.commandFactory.createActivateChangeListener({ scope: this }));
            afterXHRCommands.add(this.commandFactory.createEnableSave({ scope: this }));
            afterXHRCommands.add(this.commandFactory.createHideMask({ scope: this }));
            afterXHRCommands.add(this.commandFactory.setNewChangeRequestTitle({ scope: this }));
            this.newChangeRequest();
            this.proposeBaseAddress(this.point, beforeXHRCommands, afterXHRCommands);
        }
    },

    switchChangeRequest: function(CrId) {
        var commandSet = new MAD.utils.CommandSet();
        var commandFactory = new MAD.workflows.ChangeRequest.CommandFactory();
        commandSet.add(commandFactory.createSwitchChangeRequest({ scope: this, args: [CrId] }));

        if (!this.hasChanged) {
            commandSet.execute();
            return;
        }

        Ext.Msg.show({
            title: 'Save Changes?',
            msg: 'You are about to navigate away from your current change request.  Would you like to save your changes?',
            buttons: Ext.Msg.YESNOCANCEL,
            fn: function(buttonId, text, opt) {
                if (buttonId === 'yes') {
                    this.saveChangeRequest(MAD.config.Urls.SaveChangeRequest, commandSet);
                }
                else if (buttonId === 'no') {
                    commandSet.execute();
                }
                else if (buttonId === 'cancel') {
                    // user changed their mind - do nothing
                }
            },
            animEl: 'elId',
            icon: Ext.MessageBox.QUESTION,
            scope: this
        });
    },

    showMask: function(message) {
        this.mask = new Ext.LoadMask(this.app.changeRequestPanel.body.dom, { msg: message });
        this.mask.show();
    },

    hideMask: function(delay) {
        // In most cases the delay will be zero.
        // In some cases, such as the baseAddressSelected, we use a delay to allow rendering to complete.
        // In that case, without a delay, the mask is hidden well before the rendering is completed.
        // I tried using afterrender on several different components, but this ends up being undesirably
        // complicated by the path the user takes (e.g. a second click on a panel header that is alreay open).
        // The mask here is quite important because it prevents the user from doing things like deleting
        // the base address while its details are being rendered.
        // todo - a couple of ways to implement delay - is this the best way?
        if (!delay) {
            delay = 0;
        }
        var task = new Ext.util.DelayedTask();
        task.delay(delay, function() {
            this.mask.hide();
        }, this);
    },

    alertFailure: function(title, message, callback) {
        if (!callback) {
            callback = function() { };
        }
        Ext.Msg.alert(title, message, callback);
    },

    createApnGridPanel: function(dataRecord) {
        // separate for debugging
        var parcelPickStore = dataRecord.get('parcel_pick_list');
        var linkedParcelStore = dataRecord.get('linked_parcels');
        var parcelLinkChangeStore = dataRecord.get('parcel_link_changes');
        var isProposedStatusOfficial = dataRecord.isProposedStatusOfficial();

        var apnGridPanel = new MAD.widgets.ApnGridPanel({
            id: Ext.id(),
            parcelLinkChangeStore: parcelLinkChangeStore,
            linkedParcelStore: linkedParcelStore,
            parcelPickStore: parcelPickStore,
            border: true,
            title: 'Parcels',
            allowNewParcelLinking: isProposedStatusOfficial
        });
        apnGridPanel.addListener('parcelPickChange', this.mapController.setSelectedParcels, this.mapController);
        apnGridPanel.addListener('searchitemselected', this.app.searchItemSelected, this.app);
        this.mapController.setSelectedParcels(apnGridPanel.getNetParcelPicks());
        return apnGridPanel;
    },

    validateBaseAddressMove: function(feature, baseAddressRecord) {
        var point = feature.geometry;
        var onSuccess = function(json) {
            var tempBaseAddressStore = new MAD.model.BaseAddressStore();
            tempBaseAddressStore.loadRawData(json, false);
            var tempBaseAddressRecord = tempBaseAddressStore.getRange()[0];
            var acceptNewLocation = false;

            // A move must be within the same block.
            // Previously we were exhaustively comparing the entire stores which hurt performance on large datasets.
            var parcelPickStoreThis = baseAddressRecord.get('parcel_pick_list');
            var parcelPickStoreThat = tempBaseAddressRecord.get('parcel_pick_list');
            var nearest = parcelPickStoreThis.getNearestNonServiceParcel();
            var match = false;
            var block = '0000';
            if (nearest) {
                block = nearest.getBlock();
            }
            // todo - use store.find() which might be faster
            Ext.each(parcelPickStoreThat.getRange(), function(record, index, allItems) {
                if (record.getBlock() === block) {
                    match = true;
                    return false;
                }
            });

            if (match) {
                acceptNewLocation = true;
            } else {
                // move feature back to previous location
                Ext.Msg.alert(
                    'Address Move',
                    'Sorry but this address move is not allowed.  Address moves are allowed within the same block. The address will now be moved back.',
                    function() {
                        feature.move(MAD.utils.wktPointToLonLat(baseAddressRecord.get('geometry_proposed')));
                    }
                );
            }
            if (acceptNewLocation === true) {
                // commit location
                baseAddressRecord.set('geometry_proposed', MAD.utils.pointToWkt(point));

                if (baseAddressRecord.get('isNew') === true) {
                    var tempStreetStore = tempBaseAddressRecord.get('available_streets');
                    existingStreetStore = baseAddressRecord.get('available_streets');
                    existingStreetStore.removeAll();
                    existingStreetStore.add(tempStreetStore.getRange());
                    // A move may invalidate a selected street.
                    var streetSegmentId = baseAddressRecord.get('street_segment_id');
                    if (streetSegmentId) {
                        var index = existingStreetStore.find('street_segment_id', streetSegmentId);
                        if (index === -1) {
                            Ext.Msg.alert('Address Move', 'The address move has invalidated your selected street; please reselect a street.');
                            baseAddressRecord.set('street_segment_id', undefined);
                        }
                    }
                }
            }

        };
        var beforeXHRCommands = new MAD.utils.CommandSet();
        beforeXHRCommands.add(this.commandFactory.createShowMask({ scope: this, args: ['Loading...'] }));
        var afterXHRCommands = new MAD.utils.CommandSet();
        afterXHRCommands.add(this.commandFactory.createHideMask({ scope: this }));
        this.getNewBaseAddress(point, onSuccess, beforeXHRCommands, afterXHRCommands);
    },

    proposeBaseAddress: function(point, beforeXHRCommands, afterXHRCommands) {
        // todo - would be nice to insert this function at beginning of afterXHRCommands, but my js Fu is not that good (json not in scope here).
        var onSuccess = Ext.bind(function(json) {
            // We must be within a parcel to accept the location
            // todo - encapsulate more of this in the BaseAddressStore
            var tempBaseAddressStore = new MAD.model.BaseAddressStore();
            tempBaseAddressStore.loadRawData(json, false);
            var tempBaseAddressRecord = tempBaseAddressStore.getRange()[0];
            var parcelPickStore = tempBaseAddressRecord.get('parcel_pick_list');
            if (parcelPickStore.getCount() <= 0) {
                Ext.Msg.alert(
                    'Address Create',
                    'You cannot create an address here because there are no parcels at this location.'
                );
                return;
            }
            this.changeRequestStore.addBaseAddressesFromJsonObj(json);
        }, this);
        this.getNewBaseAddress(point, onSuccess, beforeXHRCommands, afterXHRCommands);
    },

    getNewBaseAddress: function(point, onSuccess, beforeXHRCommands, afterXHRCommands) {
        beforeXHRCommands.execute();
        Ext.Ajax.request({
            url: MAD.config.Urls.AddressProposeAt + point.toString(),
            disableCaching: false,
            success: function(response) {
                var json = Ext.JSON.decode(response.responseText).returnObj;
                onSuccess(json);
                afterXHRCommands.execute();
            },
            failure: function(response) {
                this.alertFailure('Data Retrieval Error', 'unable to get address location information');
                this.hideMask();
            },
            scope: this
        });
    },

    addBaseAddressPanel: function(baseAddressRecord, initialSetup) {

        // create the base address panel for this base address
        var baseAddressPanel = new MAD.widgets.BaseAddressPanel({ dataRecord: baseAddressRecord });
        if (!initialSetup) {
            this.statusController.configureByStatus(baseAddressPanel);
        }

        // add the listener to reset the panel's title on update of record
        baseAddressPanel.dataRecord.store.on('update', function(baseAddressStore, dataRecord) {
            if (baseAddressPanel.dataRecord == dataRecord) {
                // todo - intent is so that when an address move invalidate a street pick, this is reflected in ui
                if (this.currentDetailForm && this.currentDetailForm.getXType() == "baseAddressFormPanel") {
                    this.currentDetailForm.form.loadRecord(dataRecord);
                }
                baseAddressPanel.updateTitle();
                this.mapController.showSelectedAddress(dataRecord);
                var status = baseAddressPanel.dataRecord.getStatusString();
                if (status == 'retire') {
                    baseAddressPanel.unitAddresses.disable();
                    baseAddressPanel.addUnitButton.disable();
                } else {
                    baseAddressPanel.unitAddresses.enable();
                    if (dataRecord.isProposedStatusOfficial()) {
                        baseAddressPanel.addUnitButton.enable();
                    } else {
                        baseAddressPanel.addUnitButton.disable();
                    }
                }
            }
        }, this);

        var disable = (baseAddressRecord.getStatusString() == 'retire');
        if (disable || !baseAddressRecord.isProposedStatusOfficial()) {
            baseAddressPanel.addUnitButton.disable();
        }
        // create the unit address grid panel
        var unitAddressGridPanel = new MAD.widgets.UnitAddressGridPanel({ store: baseAddressRecord.get('unit_addresses'), disabled: disable });
        baseAddressPanel.unitAddresses = unitAddressGridPanel;
        if (!initialSetup) {
            this.statusController.configureByStatus(unitAddressGridPanel);
        }

        // used to override clearing of form on collapse of base address panels during add of new unit address
        this.overrideClearOnCollapse = false;
        // add the listener to expand this panel and select the new unit row on add of new unit addresses
        baseAddressPanel.on('newUnitAddressAdded', function(baseAddressPanel, newUnitAddressRecord) {
            baseAddressPanel.unitAddresses.getSelectionModel().select(newUnitAddressRecord, false, true);
            baseAddressPanel.unitAddresses.getView().focusRow(newUnitAddressRecord);
            baseAddressPanel.unitAddresses.fireEvent('unitAddressSelected', baseAddressPanel.unitAddresses, newUnitAddressRecord);
            if (baseAddressPanel.collapsed) {
                if (this.baseAddressAccordionPanel.items.length > 1 && !this.baseAddressAccordionPanel.allCollapsed()) {
                    this.overrideClearOnCollapse = true;
                }
                baseAddressPanel.expand();
            }
        }, this);


        // add the listener to populate the addressDetailPanel with the unit address form on select of a unit address
        unitAddressGridPanel.on('unitAddressSelected', function(element, dataRecord) {
            baseAddressPanel.unselect();
            baseAddressPanel.childSelected = true;
            if (this.currentDetailForm !== null) {
                if (dataRecord == this.currentDetailForm.record) {
                    return;
                }
                this.currentDetailForm.hide();
            }
            this.showMask('Updating...');
            // If there are lots of data, the next block is CPU intensive, and the mask will lags too much, so we delay a bit.
            // While this sounds fussy, it's not, because it prevents the user from mucking with the model state while the model state may be changing.
            Ext.defer(function() {
                var apnGridPanel = this.createApnGridPanel(dataRecord);
                var dispositionCode = dataRecord.get('disposition');
                var isNew = dataRecord.get('isNew');
                var dispositionStore = this.getDispositionStore(dispositionCode, isNew, this.app.domainTables);
                var formPanel = new MAD.widgets.UnitAddressFormPanel({
                    opacity: 0.0,
                    record: dataRecord,
                    apnGridPanel: apnGridPanel,
                    dispositionStore: dispositionStore,
                    domainTables: this.app.domainTables
                });
                this.statusController.configureByStatus(formPanel);
                this.currentDetailForm = formPanel;
                this.addressDetailPanel.resetDetailPanel(formPanel, apnGridPanel, dataRecord);
                this.hideMask(500);
            }, 1, this);
        }, this);

        // add the listener to empty the addressDetailPanel on remove of a unit address
        unitAddressGridPanel.on('unitAddressRemoved', function(grid, dataRecord, selection) {
            if (selection === dataRecord) {
                baseAddressPanel.unselect();
                this.clearAddressDetail();
            }
        }, this);


        // add the listener to populate the addressDetailPanel with the base
        // address form on select of a base address
        baseAddressPanel.on('baseAddressSelected', function(panel, dataRecord) {
            this.baseAddressAccordionPanel.unselectAll();
            if (this.currentDetailForm !== null) {
                if (dataRecord == this.currentDetailForm.record) {
                    // use has clicked on the panel header that is already active/open
                    return;
                }
                this.currentDetailForm.hide();
            }
            this.showMask('Updating...');
            // If there are lots of data, the next block is CPU intensive, and the mask will lags too much, so we delay a bit.
            // While this sounds fussy, it's not, because it prevents the user from mucking with the model state while the model state may be changing.
            Ext.defer(function() {
                var apnGridPanel = this.createApnGridPanel(dataRecord);
                var dispositionCode = dataRecord.get('unit_disposition');
                var isNew = dataRecord.get('isNew');
                var dispositionStore = this.getDispositionStore(dispositionCode, isNew, this.app.domainTables);
                var formPanel = new MAD.widgets.BaseAddressFormPanel({
                    opacity: 0.0,
                    record: dataRecord,
                    streetSegmentStore: dataRecord.get('available_streets'),
                    apnGridPanel: apnGridPanel,
                    dispositionStore: dispositionStore
                });
                this.statusController.configureByStatus(formPanel);
                this.currentDetailForm = formPanel;
                this.addressDetailPanel.resetDetailPanel(formPanel, apnGridPanel, dataRecord);
                var selectionModel = baseAddressPanel.unitAddresses.getSelectionModel();
                selectionModel.deselectAll();
                baseAddressPanel.expand();
                this.mapController.showSelectedAddress(dataRecord);
                this.hideMask(500);
            }, 1, this);
        }, this);


        baseAddressPanel.on('baseAddressCollapsed', function(panel, dataRecord) {
            if (!this.overrideClearOnCollapse) {
                this.clearAddressDetail();
            } else {
                this.overrideClearOnCollapse = false;
            }
        }, this);

        // add the unitAddressGridPanel to the baseAddressPanel and the baseAddressPanel
        // to the baseAddressAccordionPanel
        baseAddressPanel.add(unitAddressGridPanel);
        this.baseAddressAccordionPanel.add(baseAddressPanel);

        // if the panel is added outside the initialization, expand it
        if (!initialSetup) {
            baseAddressPanel.expand();
        }

        return baseAddressPanel;
    },

    clearAddressDetail: function() {
        this.currentDetailForm = null;
        this.addressDetailPanel.resetDetailPanel(null, null, null);
    },

    removeBaseAddressPanel: function(baseAddressRecord) {
        // find and remove the corresponding panel
        var baseAddressPanels = this.baseAddressAccordionPanel.query('baseAddressPanel');
        Ext.each(baseAddressPanels,
            function(panel) {
                if (baseAddressRecord === panel.dataRecord) {
                    this.baseAddressAccordionPanel.remove(panel);
                }
            },
            this
        );
    },

    confirmEndWorkflow: function() {

        if (!this.hasChanged) {
            this.endWorkflow();
            return;
        }

        var commandSet = new MAD.utils.CommandSet();
        var commandFactory = new MAD.workflows.ChangeRequest.CommandFactory();
        commandSet.add(commandFactory.createEndWorkflow({ scope: this }));

        Ext.Msg.show({
            title: 'Save Changes?',
            msg: 'Would you like to save your changes?',
            buttons: Ext.Msg.YESNOCANCEL,
            fn: function(buttonId, text, opt) {
                if (buttonId === 'yes') {
                    this.saveChangeRequest(MAD.config.Urls.SaveChangeRequest, commandSet);
                }
                else if (buttonId === 'no') {
                    this.endWorkflow();
                }
                else if (buttonId === 'cancel') {
                    // user changed their mind - do nothing
                }
            },
            animEl: 'elId',
            icon: Ext.MessageBox.QUESTION,
            scope: this
        });
    },

    endWorkflow: function() {
        // restore app state
        this.app.activeWorkflow = null;
        this.mapController.deactivate();
        this.changeListener.deactivate();
        this.app.changeRequestPanel.remove(this.workflowPanel);
        this.app.changeRequestPanel.hide();
        this.app.changeRequestPanel.setTitle('Change Request');
        this.app.userPrefDropDown.enable();
        this.app.TasksBar.enableAll();
        this.app.TasksBar.setUser(this.app.user);
    },

    getChangeRequest: function(url, beforeXHRCommands, afterXHRCommands) {
        beforeXHRCommands.execute();
        // get the change request json and load the stores
        Ext.Ajax.request({
            url: url,
            headers: { 'Content-Type': 'application/json' },
            success: function(result, request) {
                var json = Ext.JSON.decode(result.responseText).returnObj;
                this.changeRequestStore.loadRawData(json, false);
                this.initChangeRequest(true);
                afterXHRCommands.execute();
            },
            failure: function(result, request) {
                this.alertFailure('Data Retrieval Error', 'unable to get change request information');
                this.endWorkflow();
                this.hideMask();
            },
            scope: this
        });
    },

    getBaseAddress: function(id, retireFlg, beforeXHRCommands, afterXHRCommands) {
        if (this.changeRequest.hasBaseAddress(id)) {
            Ext.Msg.alert('Unable to add address to change request', 'This address has already been added to the current change request.');
            return;
        }

        beforeXHRCommands.execute();
        this.app.changeRequestPanel.expand();
        // get the change request json and load the stores
        this.retrievingAddressData = true;
        Ext.Ajax.request({
            url: MAD.config.Urls.GetBaseAddrForChangeRequest + id,
            headers: { 'Content-Type': 'application/json' },
            success: function(result, request) {
                var response = Ext.JSON.decode(result.responseText);
                if (response.success) {
                    var json = response.returnObj;
                    json.base_addresses[0].retire_flg = retireFlg;
                    this.changeRequestStore.addBaseAddressesFromJsonObj(json);
                } else {
                    Ext.Msg.show({
                        title: 'Change Request Validation',
                        msg: response.message,
                        buttons: Ext.Msg.OK,
                        scope: this
                    });
                }
                afterXHRCommands.execute();
                this.retrievingAddressData = false;
            },
            failure: function(result, request) {
                this.alertFailure('Data Retrieval Error', 'unable to get base address information');
                this.endWorkflow();
                this.hideMask(0);
                this.retrievingAddressData = false;
            },
            scope: this
        });
    },

    newChangeRequest: function() {
        this.changeRequestStore.addDefaultRecord();

        this.changeRequestStore.changeRequest.set('requestor_id', this.app.user.id);
        this.changeRequestStore.changeRequest.set('requestor_name', this.app.user.firstname + ' ' + this.app.user.lastname);
        this.changeRequestStore.changeRequest.set('requestor_last_update', Ext.util.Format.formatDateDefault(new Date()));

        this.changeRequestStore.changeRequest.baseAddresses = this.changeRequestStore.baseAddressStore.data;

        this.initChangeRequest(false);
    },

    initChangeRequest: function(addBaseAddresses) {

        // disable buttons 'myEdits' and 'reviewEdits'
        this.app.TasksBar.disableButton('myEdits');
        this.app.TasksBar.disableButton('reviewEdits');

        this.app.userPrefDropDown.disable();

        // get the change request record and load it into the changeRequestFormPanel,
        // the changeRequestFormPanel itself wil handle updating the record on keyup
        this.changeRequest = this.changeRequestStore.getAt(0);
        this.changeRequestFormPanel.getForm().loadRecord(this.changeRequest);
        this.changeRequestFormPanel.record = this.changeRequest;
        //this.changeRequestFormPanel.configureToolbar();

        // get the base address store from the change request record and pass it to the
        // baseAddressAccordionPanel
        this.baseAddressStore = this.changeRequest.get("base_addresses");
        this.baseAddressAccordionPanel.baseAddressStore = this.baseAddressStore;
        if (addBaseAddresses) {
            // iterate through base addresses and create widgets
            var baseAddressIndex;
            var baseAddressPanel;
            for (baseAddressIndex = 0; baseAddressIndex < this.baseAddressStore.getCount(); baseAddressIndex++) {
                // add the baseAddressPanel for the current base address
                baseAddressPanel = this.addBaseAddressPanel(this.baseAddressStore.getAt(baseAddressIndex), true);
            }
            if (baseAddressPanel) {
                baseAddressPanel.select();
            }
        }
        // add listener to base address store to create/remove base
        // addresses panels on add/remove of records
        var addAndSelectBaseAddressPanel = function(store, records, index) {
            // assume we will have a single record
            var baseAddressPanel = this.addBaseAddressPanel(records[0], false);
            baseAddressPanel.select();
        };
        this.baseAddressStore.on({
            'add': addAndSelectBaseAddressPanel, 
            'load': addAndSelectBaseAddressPanel,
            scope: this
        });
        this.baseAddressStore.on('remove', function(store, record, index) {
            this.removeBaseAddressPanel(record);
            this.clearAddressDetail();
            this.mapController.resetControls();
        }, this);

        this.statusController = new MAD.workflows.ChangeRequest.StatusController({
            workflow: this
        });
    },
    
    deleteChangeRequest: function(url, commandSet) {
        // get the change request json and load the stores
        this.showMask('Deleting Change Request...');
        Ext.Ajax.request({
            url: url,
            method: 'DELETE',
            success: function(result, request) {
                var response = Ext.JSON.decode(result.responseText);
                if (response.success) {
                    commandSet.execute();
                } else if (response.status_code == 1) {
                    this.alertFailure('Change Request Delete Error', 'Can only delete change request you created.');
                } else {
                    this.alertFailure('Change Request Delete Error', 'Unable to delete your change request.');
                }
                this.hideMask(0);
            },
            failure: function(result, request) {
                this.alertFailure('Change Request Delete Error', 'Unable to delete your change request.');
                this.hideMask(0);
            },
            scope: this
        });
    },

    saveChangeRequest: function(url, commandSet) {

        // todo: http://code.google.com/p/eas/issues/detail?id=209&q=performance
        
        var timeout = 180000;
        var message = 'Saving Change Request.  This may take a few moments.';
        this.showMask(message);
        // use "defer" to get the mask on the screen before CPU is hogged by the json processing
        Ext.defer(function() {
            // get the json from the store
            //if (console) {
            //    console.log('serializing...');
            //}
            var json = Ext.encode(this.changeRequestStore.write());
            //if (console) {
            //    //console.log(json);
            //    console.log('serialization complete');
            //    console.log('ajax post commencing');
            //}
            // get the change request json and load the stores
            Ext.Ajax.request({
                timeout:timeout,
                url: url,
                headers: { 'Content-Type': 'application/json' },
                //params: { json: json },
                jsonData: json,
                success: function(result, request) {
                    var response = Ext.JSON.decode(result.responseText);
                    if (response.success) {
                        // todo - does this block belong in a command or commandset?
                        if (!response.returnObj.deleteCr) {
                            //if (console) {
                            //    console.log('running callback');
                            //}
                            this.changeRequest.set('concurrency_id', response.returnObj.concurrency_id);
                            if (response.returnObj.review_status_id != this.statusController.statusId) {
                                this.statusController.setChangeRequestStatus(response.returnObj.review_status_id);
                            }
                            if (this.changeRequest.get('change_request_id') === null || this.changeRequest.get('change_request_id') === "") {
                                this.changeRequest.set('change_request_id', response.returnObj.change_request_id);
                                this.changeRequestFormPanel.getForm().loadRecord(this.changeRequest);
                            }
                        }
                        commandSet.execute();
                    } else {
                        Ext.Msg.show({
                            title: 'Change Request Validation',
                            msg: response.message,
                            buttons: Ext.Msg.OK,
                            scope: this
                        });
                    }
                    this.hideMask(0);
                },
                failure: function(result, request) {
                    var response = Ext.JSON.decode(result.responseText);
                    this.alertFailure('Unable To Save', response.message);
                    this.hideMask(0);
                },
                scope: this
            });
        }, 1, this);
    },


    render: function(ct, position) {
        Ext.apply(this, {
            el: this.renderTo || document.body
        });


        MAD.workflows.ChangeRequest.superclass.render.apply(this, arguments);
    },

    onRender: function(ct, position) {
        //simply here to override the base function becuase
        //the base function fails ct.dom.insertBefore(this.el.dom, position);

        //we need this though to be able to call the base "render" function above
    },

    getDispositionStore: function(dispositionCode, isNew, domainTables) {

        // Limit the selection based on state.

        var dispositionStore = MAD.model.getDispositionDomainStore();
        dispositionStore.loadData(domainTables[dispositionStore.reader.root]);

        if (isNew === false) {
            // We go a bit of the long way round so we are a little insulated from domain changes.
            // Use disposition description because it's understandable.

            var rowIndex = dispositionStore.find('disposition_code', dispositionCode);
            var dispositionRecord = dispositionStore.getAt(rowIndex);
            var dispositionDescription = dispositionRecord.get('disposition_description');

            // Limit the selection.
            if (dispositionDescription === 'official') {
                // existing - official: limit to official
                dispositionStore.filterBy(function(record, id){
                    return (record.get('disposition_description') === 'official');
                });
            } else if (dispositionDescription === 'temporary') {
                // existing - temporary: limit to temporary
                dispositionStore.filterBy(function(record, id){
                    return (record.get('disposition_description') === 'temporary');
                });
            }
        } else {
            // new: limit to official
            dispositionStore.filterBy(function(record, id){
                return (record.get('disposition_description') === 'official');
            });
        }

        // This prevent the combobox from resetting filter.
        // http://www.sencha.com/forum/showthread.php?24269-Ext.form.ComboBox-filter-by-its-store
        delete dispositionStore.snapshot;
        return dispositionStore;
    }

});

Ext.define('MAD.workflows.ChangeRequest.MapController', {
    extend: 'Ext.Component',

    workflow: {},
    mapContainer: {},
    map: {},

    initComponent: function() {

        this.addressDragLayer = this.map.getLayersByName('addressDragLayer')[0];
        this.streetSegmentLayer = this.map.getLayersByName('streetSegmentLayer')[0];
        this.parcelLayer = this.map.getLayersByName('parcelLayer')[0];
        this.addressDragControl = this.map.getControlsBy('name', 'addressDragControl')[0];
        this.addressSelectControl = this.map.getControlsBy('name', 'selectControl')[0];

        var mapController = this;
        var workflow = this.workflow;
        this.addressDragControl.onComplete = function(feature, pixel) {
            workflow.validateBaseAddressMove(feature, feature.attributes.baseAddressRecord);            
            mapController.activateSelect();
        };


        this.mapContainer.contextMenu.on('moveAddressButtonClicked', this.moveAddressButtonClicked, this);


        MAD.workflows.ChangeRequest.MapController.superclass.initComponent.apply(this, arguments);

    },
    
    moveAddressButtonClicked: function() {
        // if the CR is in edit mode, activate the drag control, else alert the user
        if (this.workflow.statusController.allowFeatureEdit() === false) {
            Ext.Msg.alert('Edit Address', 'You cannot edit an address when reviewing a change request.');
        } else {
            this.activateDrag();
        }
    },

    zoomToAddress: function(baseAddressRecord) {
        var zoomLevel = this.map.getZoom();
        if (zoomLevel < 4) {
            this.map.zoomTo(4);
        }
        var lonLat = MAD.utils.wktPointToLonLat(baseAddressRecord.get('geometry_proposed'));
        if (!this.map.getExtent().containsLonLat(lonLat)) {
            this.map.panTo(lonLat);
        }

    },

    showSelectedAddress: function(baseAddressRecord) {
        this.activateSelect();
        this.clearAddressDragFeatures();
        this.clearStreetSegmentFeatures();

        // todo - move block into the record itself
        var lonLat;
        lonLat = MAD.utils.wktPointToLonLat(baseAddressRecord.get('geometry_proposed'));
        if (lonLat.lon === 0 || lonLat.lat === 0) {
            lonLat = MAD.utils.wktPointToLonLat(baseAddressRecord.get('geometry'));
        }

        var titleData = baseAddressRecord.getTitleData();
        var labelText = titleData.address;
        var apn = baseAddressRecord.getFirstApn();

        var point = new OpenLayers.Geometry.Point(lonLat.lon, lonLat.lat);
        var feature = new OpenLayers.Feature.Vector(point);
        feature.attributes = {
            label: labelText,
            apn: apn,
            baseAddressRecord: baseAddressRecord
        };
        this.addressDragLayer.addFeatures([feature]);

        this.showStreetSegmentOnMap(baseAddressRecord.getStreetSegmentGeometry());

        this.zoomToAddress(baseAddressRecord);
    },

    /**
    * trys to show the passed int street segement on the map
    * @param {String} wktLine: a WKT string
    * @return void
    */
    showStreetSegmentOnMap: function(wktLine) {
        if (wktLine) {
            var streetFeature = MAD.utils.wktLineToVector(wktLine);
            streetFeature.geometry.transform(new OpenLayers.Projection("EPSG:2227"), new OpenLayers.Projection("EPSG:900913"));
            this.streetSegmentLayer.addFeatures([streetFeature]);
        }
    },

    /**
    * trys to show the passed in parcel on the map
    * @param {String} wktPoly: a WKT string
    * @return void
    */
    showParcelOnMap: function(wkt) {
        this.clearParcelFeatures();
        if (wkt) {
            var parcelFeature = MAD.utils.wktToVector(wkt);
            parcelFeature.geometry.transform(new OpenLayers.Projection("EPSG:2227"), new OpenLayers.Projection("EPSG:900913"));
            this.parcelLayer.addFeatures([parcelFeature]);
        }
    },


    resetControls: function() {
        this.activateSelect();
        this.clearAddressDragFeatures();
        this.clearStreetSegmentFeatures();
        this.clearParcelFeatures();
    },

    clearAddressDragFeatures: function() {
        this.addressDragLayer.removeFeatures(this.addressDragLayer.features);
        this.addressDragLayer.destroyFeatures(this.addressDragLayer.features);
    },

    clearStreetSegmentFeatures: function() {
        this.streetSegmentLayer.removeFeatures(this.streetSegmentLayer.features);
        this.streetSegmentLayer.destroyFeatures(this.streetSegmentLayer.features);
    },

    clearParcelFeatures: function() {
        this.parcelLayer.removeFeatures(this.parcelLayer.features);
        this.parcelLayer.destroyFeatures(this.parcelLayer.features);
    },

    activateSelect: function() {
        this.addressDragControl.deactivate();
        this.addressSelectControl.activate();
    },

    activateDrag: function() {
        this.addressSelectControl.deactivate();
        this.addressDragControl.activate();
    },

    deactivate: function() {
        this.activateSelect();
        this.mapContainer.contextMenu.un('moveAddressButtonClicked', this.moveAddressButtonClicked, this);
        this.clearAddressDragFeatures();
        this.clearStreetSegmentFeatures();
        this.clearParcelFeatures();
    },

    setSelectedParcels: function(apns) {
        var json = Ext.encode(apns);
        Ext.Ajax.request({
            url: MAD.config.Urls.BaseGeometryForParcels,
            headers: { 'Content-Type': 'application/json' },
            jsonData: json,
            success: function(result, request) {
                var response = Ext.JSON.decode(result.responseText);
                this.showParcelOnMap(response.results);
            },
            failure: function(result, request) {
                Ext.Msg.alert('Data Retrieval Error', 'unable to get geometry for base parcels');
            },
            scope: this
        });
    }

});

// Change Request Status Controller
Ext.define('MAD.workflows.ChangeRequest.StatusController', {
    extend: 'Ext.Component',

    workflow: {},

    stateDependantWidgets: [
        {
            type: 'changeRequestFormPanel',
            states: {
                'default': function(widget) {
                    widget.discardBtn.show();
                    if (widget.record.data.change_request_id != null) {
                        widget.discardBtn.enable();
                    }
                    // for (i=0; i<widget.user.roles.length; i+=1){
                    //     if (widget.user.roles[i] === 'approver') {
                    //         widget.acceptWithoutReviewBtn.show();
                    //     }
                    // }
                    widget.submitBtn.show();
                    widget.acceptBtn.hide();
                    widget.approveBtn.hide();
                    widget.rejectBtn.hide();
                    widget.requestorComments.enable();
                    widget.reviewerComments.disable();
                    widget.titleField.enable();
                    widget.getForm().loadRecord(widget.record);
                },
                'submitted': function(widget) {
                    widget.discardBtn.hide();
                    widget.submitBtn.hide();
                    widget.acceptBtn.show();
                    widget.approveBtn.hide();
                    widget.rejectBtn.hide();
                    // widget.acceptWithoutReviewBtn.hide();
                    widget.requestorComments.disable();
                    widget.reviewerComments.disable();
                    widget.titleField.disable();
                    widget.getForm().loadRecord(widget.record);
                },
                'under review': function(widget) {
                    widget.discardBtn.hide();
                    widget.submitBtn.hide();
                    widget.acceptBtn.hide();
                    widget.approveBtn.show();
                    widget.rejectBtn.show();
                    // widget.acceptWithoutReviewBtn.hide();
                    widget.requestorComments.disable();
                    widget.reviewerComments.enable();
                    widget.titleField.disable();
                    widget.getForm().loadRecord(widget.record);
                }
            }
        },
        {
            type: 'baseAddressPanel',
            states: {
                'default': function(widget) {
                    if (widget.dataRecord.getStatusString() !== 'retire' && widget.dataRecord.isProposedStatusOfficial()) {
                        widget.addUnitButton.enable();
                    }
                    widget.removeBaseButton.enable();
                },
                'submitted': function(widget) {
                    widget.addUnitButton.disable();
                    widget.removeBaseButton.disable();
                },
                'under review': function(widget) {
                    widget.addUnitButton.disable();
                    widget.removeBaseButton.disable();
                }
            }
        },
        {
            type: 'unitAddressGridPanel',
            states: {
                'default': function(widget) {
                    widget.columns[0].setVisible(true);
                },
                'submitted': function(widget) {
                    widget.columns[0].setVisible(false);
                },
                'under review': function(widget) {
                    widget.columns[0].setVisible(false);
                }
            }
        },
        {
            type: 'baseAddressFormPanel',
            states: {
                'default': function(widget) {
                    widget.editRadio.on('change', function(box, checked) {
                        if (checked) {
                            this.reconfigureStatus('edit');
                        }
                    }, widget);
                    widget.retireRadio.on('change', function(box, checked) {
                        if (checked) {
                            this.reconfigureStatus('retire');
                        }
                    }, widget);
                },
                'submitted': function(widget) {
                    widget.disableAll();
                },
                'under review': function(widget) {
                    widget.disableAll();
                }
            }
        },
        {
            type: 'unitAddressFormPanel',
            states: {
                'default': function(widget) {
                    widget.editRadio.on('change', function(box, checked) {
                        if (checked) {
                            this.reconfigureStatus('edit');
                        }
                    }, widget);
                    widget.retireRadio.on('change', function(box, checked) {
                        if (checked) {
                            this.reconfigureStatus('retire');
                        }
                    }, widget);
                },
                'submitted': function(widget) {
                    widget.disableAll();
                },
                'under review': function(widget) {
                    widget.disableAll();
                }
            }
        }
    ],

    definedStates: {
        editing: 'editing',
        rejected: 'rejected',
        submitted: 'submitted',
        underReview: 'under review',
        approved: 'approved'
    },

    initComponent: function() {

        this.featureEditableStates = [
            this.definedStates.editing,
            this.definedStates.rejected
        ];

        this.statusStore = Ext.create('MAD.model.ChangeRequestStatusDomainStore');
        this.statusStore.loadData(this.workflow.app.domainTables[this.statusStore.reader.root]);

        var missingDefinedStates = [];
        var definedState;
        for (definedState in this.definedStates) {
            var index = this.statusStore.findBy(function(record, id) {
                if (record.get("status_description") === this.definedStates[definedState]) {
                    return true;
                }
            }, this);
            if (index < 0) {
                missingDefinedStates.push(this.definedStates[definedState]);
            }
        }
        if (missingDefinedStates.length > 0) {
            var states = '';
            Ext.each(missingDefinedStates, function(state) {
                states += ' "' + state + '"; ';
            }, this);
            var message = 'The following defined states were not found in the change request status domain:' + states;
            throw new Error(message);
        }

        this.statusId = this.workflow.changeRequest.get('status_id');

        this.setStatusDescription();
        this.configureAll();

        MAD.workflows.ChangeRequest.StatusController.superclass.initComponent.apply(this, arguments);
    },

    setStatusDescription: function() {
        var index = this.statusStore.findBy(function(record, id) {
            if (record.get('status_id') === this.statusId) {
                return true;
            }
        }, this);
        if (index < 0) {
            throw new Error("status row not found");
        }
        this.statusDescription = this.statusStore.getAt(index).get('status_description');
        this.workflow.changeRequest.set('status_description', this.statusDescription);
        return this.statusDescription;
    },

    configureAll: function() {
        Ext.each(this.stateDependantWidgets, function(stateDependantWidget) {
            Ext.each(this.workflow.app.Viewport.query(stateDependantWidget.type), function(widget) {
                this.configureByStatus(widget);
            }, this);
        }, this);
    },

    configureByStatus: function(widget) {
        var type = widget.getXType();
        Ext.each(this.stateDependantWidgets, function(stateDependantWidget) {
            if (stateDependantWidget.type == type) {
                if (typeof (stateDependantWidget.states[this.statusDescription]) == 'function') {
                    stateDependantWidget.states[this.statusDescription](widget);
                } else {
                    stateDependantWidget.states['default'](widget);
                }
            }
        }, this);
    },

    setChangeRequestStatus: function(status) {
        // todo - encapsulate more please
        switch (typeof status) {
            case 'number':
                this.statusId = status;
                break;
            case 'string':
                var index = this.statusStore.findBy(function(record, id){
                    if (record.get('status_description') === status) {
                        return true;
                    }
                });
                if (index < 0) {
                    throw new Exception('status row now found');
                }
                this.statusId = this.statusStore.getAt(index).get('status_id');
                break;
            default:
                return;
        }
        this.workflow.changeRequest.set('status_id', this.statusId);
        this.setStatusDescription();
        this.configureAll();
    },

    allowFeatureEdit: function() {
        var allowFeatureEdit = false;

        Ext.each(this.featureEditableStates, function(stateDescription) {
            if (this.statusDescription == stateDescription) {
                allowFeatureEdit = true;
            }
        }, this);

        return allowFeatureEdit;
    }

});

MAD.workflows.ChangeRequest.CommandFactory = function() {

    this.createDisableSave = function(config) {
        return new MAD.utils.Command({
            fn: function() {
                this.changeRequestFormPanel.saveBtn.disable();
                this.changeRequestFormPanel.discardBtn.enable();
                this.hasChanged = false;
            },
            scope: config.scope
        });
    };

    this.createEnableSave = function(config) {
        return new MAD.utils.Command({
            fn: function() {
                this.changeRequestFormPanel.saveBtn.enable();
                this.hasChanged = true;
            },
            scope: config.scope
        });
    };

    this.createEndWorkflow = function(config) {
        return new MAD.utils.Command({
            fn: function() {
                this.endWorkflow();
            },
            scope: config.scope
        });
    };

    this.createFireApproveEvent = function(config) {
        return new MAD.utils.Command({
            fn: function() {
                this.fireEvent('requestapproved', this.changeRequest);
            },
            scope: config.scope
        });
    };

    this.createShowMask = function(config) {
        return new MAD.utils.Command({
            fn: function(text) {
                this.showMask(text);
            },
            scope: config.scope,
            args: config.args
        });
    };

    this.createHideMask = function(config) {
        return new MAD.utils.Command({
            fn: function() {
                this.hideMask();
            },
            scope: config.scope
        });
    };

    this.createActivateChangeListener = function(config) {
        return new MAD.utils.Command({
            fn: function() {
                this.changeListener.activate();
            },
            scope: config.scope
        });
    };

    this.createSwitchChangeRequest = function(config) {
        return new MAD.utils.Command({
            fn: function(CrId) {
                this.changeRequestId = CrId;
                this.app.changeRequestPanel.remove(this.workflowPanel);
                this.initUI();
            },
            scope: config.scope,
            args: config.args
        });
    };

    this.createExpandTopBaseAddress = function(config) {
        return new MAD.utils.Command({
            fn: function(CrId) {
                var allCollapsed = true;
                this.baseAddressAccordionPanel.items.each(function(item, index, allItems) {
                    if (!item.collapsed) {
                        allCollapsed = false;
                    }
                }, this);
                if (allCollapsed) {
                    if (this.baseAddressAccordionPanel.getFirst()) {
                        this.baseAddressAccordionPanel.getFirst().expand();
                    }
                }
            },
            scope: config.scope,
            args: config.args
        });
    };

    this.setNewChangeRequestTitle = function(config) {
        return new MAD.utils.Command({
            fn: function() {
                if (this.changeRequest.baseAddresses.items.length > 0) {
                    var f = this.changeRequestFormPanel.getForm()
                    f.loadRecord(this.changeRequest);
                    this.changeRequestFormPanel.items.items[0].focus(true);
                }
            },
            scope: config.scope
        });
    };

};
