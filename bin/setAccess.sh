# set up groups and users

setupSsh() {
    echo setting up public key...
    pushd /home/$user
    mkdir .ssh
    pushd .ssh
    echo $publickey > authorized_keys
    popd
    popd
}

usage() {
    echo "Usage: $0 <group> <user> <public-key>"
    exit
}

if [[ ! $# == 3 ]]
then
   usage
fi

group="$1"
user="$2"
publickey="$3"
defaultpass="newpass"
password=`openssl passwd $defaultpass`


if [[ ! $? == 0 ]]
then
        echo "openssl not installed!"
        exit
fi
#Try to create the group, if it already exist, add user to group
groupadd -r $group

if [[ ! $? == 0 ]]
then
   echo "Group $group already exists, adding user: $user to group."
   useradd -g $group -p $password -d /home/$user  -m $user
   if [[ $? == 0 ]]
   then
        echo "User '$user' added to group '$group' with default password '$defaultpass'"
        setupSsh
        exit
   else
        echo "failed to create user"
        exit
   fi
else
   echo "Group $group has been created, creating user: $user ..."
   useradd -g $group -p $password -d /home/$user  -m $user
   if [[ $? == 0 ]]
   then
        echo "User '$user' added to group '$group' with default password '$defaultpass'"
        setupSsh
        exit
   else
        echo "failed to create user"
        exit
   fi
fi
