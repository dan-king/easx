
import os, glob, sys
from sys import argv
import subprocess
import osgeo.gdal
import osgeo.osr

GDAL_HOME = None
key = 'GDAL_HOME'
try:
    GDAL_HOME = os.environ[key]
except Exception:
    print 'missing system variable: %s\n' % key
    GDAL_HOME = '/usr/local/bin/'
    print 'setting GDAL_HOME to %s' % GDAL_HOME


def usage():
    print """python %s
""" % argv[0]
    sys.exit()


def retile(optFilePath=None, pyramidOutputPath=None, levels=None):
    import shutil
    if os.path.exists(pyramidOutputPath):
        shutil.rmtree(pyramidOutputPath)
    os.makedirs(pyramidOutputPath)

    # e.g. gdal_retile.py -v -r bilinear -levels 4 -ps 2048 2048 -co "TILED=YES" -co "COMPRESS=JPEG" -targetDir bmpyramid --optfile 'foo.txt'
    # gdal_retile.py -v -r bilinear -levels 4 -ps 2048 2048 -co "TILED=YES" -co "COMPRESS=JPEG" -targetDir bmpyramid bmreduced.tiff
    command = []
    command.append('python2.5')
    command.append('gdal_retile_with_alpha.py')
    command.append('-v')
    command.append('-r')
    command.append('bilinear')
    command.append('-levels')
    command.append(str(levels))
    command.append('-ps')
    command.append('2048')
    command.append('2048')
    command.append('-s_srs')
    command.append('EPSG:2227')
    command.append('-co')
    command.append('COMPRESS=JPEG')
    command.append('-co')
    command.append('TILED=YES')
    #command.append('-co')
    #command.append('BLOCKXSIZE=256')
    #command.append('-co')
    #command.append('BLOCKYSIZE=256')
    command.append('-targetDir')
    command.append(pyramidOutputPath)
    command.append('--optfile')
    command.append(optFilePath)
    print "executing command line: %s\n" % ' '.join(command)

    process = subprocess.Popen(command, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
    process.wait()
    print "command return code: " + str(process.returncode)
    if process.returncode != 0:
        sys.exit(-1)
    else:
        #print "command success"
        pass


def createRetileListFile(sourceDir, outputFilePath, fileSuffix):
    if os.path.exists(outputFilePath):
        os.remove(outputFilePath)
    inputFilePaths = glob.glob(sourceDir + "/*." + fileSuffix)
    outputFile = open(outputFilePath, 'wt')
    for inputFilePath in inputFilePaths:
        outputFile.write("%s\n" % inputFilePath)
    outputFile.close()


def createGeoFiles(filePath):
    # mostly from http://gis.stackexchange.com/questions/9421/how-to-create-tfw-and-prj-files-for-a-bunch-of-geotiffs
    source = osgeo.gdal.Open(filePath)
    sourceTransform = source.GetGeoTransform()

    source_srs = osgeo.osr.SpatialReference()
    source_srs.ImportFromWkt(source.GetProjection())
    source_srs.MorphToESRI()
    source_wkt = source_srs.ExportToWkt()

    prjFilename = os.path.splitext(filePath)[0] + '.prj'
    prjFile = open(prjFilename, 'wt')
    prjFile.write(source_wkt)
    prjFile.close()

    source = None

    twfFilename = os.path.splitext(filePath)[0] + '.tfw'
    tfwFile = open(twfFilename, 'wt')
    tfwFile.write("%0.8f\n" % sourceTransform[1])
    tfwFile.write("%0.8f\n" % sourceTransform[2])
    tfwFile.write("%0.8f\n" % sourceTransform[4])
    tfwFile.write("%0.8f\n" % sourceTransform[5])
    tfwFile.write("%0.8f\n" % sourceTransform[0])
    tfwFile.write("%0.8f\n" % sourceTransform[3])
    tfwFile.close()


def get2227():
    return """PROJCS["NAD_1983_StatePlane_California_III_FIPS_0403_Feet",
GEOGCS["GCS_North_American_1983",
    DATUM["North_American_Datum_1983",
        SPHEROID["GRS_1980",6378137,298.257222101]],
    PRIMEM["Greenwich",0],
    UNIT["Degree",0.0174532925199432955]],
PROJECTION["Lambert_Conformal_Conic_2SP"],
PARAMETER["False_Easting",6561666.666666666],
PARAMETER["False_Northing",1640416.666666667],
PARAMETER["Central_Meridian",-120.5],
PARAMETER["Standard_Parallel_1",37.06666666666667],
PARAMETER["Standard_Parallel_2",38.43333333333333],
PARAMETER["Latitude_Of_Origin",36.5],
UNIT["Foot_US",0.304800609601219241]]
"""


def fixProjectionFiles(pyramidOutputDir, levels):

    def createProjectionFile(outputFilePath):
        if os.path.exists(outputFilePath):
            os.remove(outputFilePath)
        outputFile = open(outputFilePath, 'wt')
        outputFile.write("%s" % get2227())
        outputFile.close()

    for level in range(0, levels+1):
        outputFilePath = os.path.join(pyramidOutputDir, str(level), str(level)+'.prj')
        createProjectionFile(outputFilePath)


def doRetile(imageSourceDir, pyramidOutputDir, inputImageType, levels):

    # house keeping
    if not os.path.exists(imageSourceDir):
        raise OSError('source directory not found: %s' % imageSourceDir)
    import shutil
    if os.path.exists(pyramidOutputDir):
        shutil.rmtree(pyramidOutputDir)
    os.mkdir(pyramidOutputDir)

    # retile
    optFilePath = os.path.join(os.getcwd(), 'optfile.txt')
    createRetileListFile(imageSourceDir, optFilePath, inputImageType)
    retile(optFilePath=optFilePath, pyramidOutputPath=pyramidOutputDir, levels=levels)


def main():

    imageSourceDir='/home/dev/ip_jpg/in'
    pyramidOutputDir='/home/dev/ip_jpg/out'
    inputImageType='jpg'
    levels=7

    # todo - make less lame (more automated)

    # 0) download the pictometry data
    # For now, we manually pull it off the pictometry hard drive.
    # The 2012 data is here
    # X:\CASANF12-LIB\Ortho Mosaic Tiles\CASANF12-TILES-6INCH
    # X:\CASANF12-LIB\Supplemental Data\Image Polygon Shape Files\CASANF12-TILES-6INCH
    # We want the northern set of files that excludes the airport.

    # 1) run this line first (in isolation)
    doRetile(imageSourceDir, pyramidOutputDir, inputImageType, levels)

    # 2) next, setup the image pyramid in geoserver using the admin console.

    # 3) next, stop geoserver and run this line, in isolation, under sudo
    # fixProjectionFiles(pyramidOutputDir, levels)

    # 4) then start geoserver

    print 'done!'



if __name__ == "__main__":
    main()

