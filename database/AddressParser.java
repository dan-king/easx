import java.lang.Integer;
import java.util.ArrayList;
import java.util.Iterator;
import java.lang.Exception;

/*
 
description

We've written this class for DBI to use as a function in their oracle DB.
It's used to parse address unit information.
An example would be to parse 38A into 38 and A.

how to use
    see below - look for the word "test"

todo
    use regex instead?
    
*/
class AddressParser {

    public String unitIntegerPart;
    public int unitInteger;
    public String unitStringPart;
    
    public void parseUnitString(String unitString) {
        unitIntegerPart = "";
        unitStringPart = "";
        if (unitString == null) {
            return;
        }
        unitString = unitString.trim();
        // Find the split. For example, for 38A it's between the 8 and the A.
        int index;
        for (index=0; index < unitString.length(); index++) {
            char c = unitString.charAt(index);
            try {Integer.parseInt(String.valueOf(c));}
            catch (NumberFormatException nfe) {
                break;
            }
        }
        // Do the splitting.
        unitIntegerPart = unitString.substring(0, index).trim();
        if ( ! unitIntegerPart.equals("") ) {
            unitInteger = Integer.parseInt(unitIntegerPart);
            unitIntegerPart = String.valueOf(unitInteger);
        }
        unitStringPart = unitString.substring(index).trim();
    }

    public static void testParseUnitString(String unitValue, String expectedInt, String expectedString) throws Exception {
        AddressParser addressParser;
        addressParser = new AddressParser();
        addressParser.parseUnitString(unitValue);
        if (! expectedInt.equals(addressParser.unitIntegerPart)) {
            String message = "for unit value " + unitValue + " expected '" + expectedInt + "' for integer part but got '" + addressParser.unitIntegerPart + "'";
            throw new Exception(message);
        }
        if (! expectedString.equals(addressParser.unitStringPart)) {
            String message = "for unit value " + unitValue + " expected '" + expectedString + "' for string part but got '" + addressParser.unitStringPart + "'";
            throw new Exception(message);
        }
    }

    public static void main(String args[]) throws Exception {
        testParseUnitString(null, "", "");
        testParseUnitString("0A", "0", "A");
        testParseUnitString("00038ABC", "38", "ABC");
        testParseUnitString("38A", "38", "A");
        testParseUnitString("38 A", "38", "A");
        testParseUnitString(" 38 A", "38", "A");
        testParseUnitString("A38", "", "A38");
        testParseUnitString("ABC", "", "ABC");
        testParseUnitString("38", "38", "");
        testParseUnitString(" 38", "38", "");
        testParseUnitString("38,000 A", "38", ",000 A");
    }
    
}
