# Entity Relationship Diagram (ERD)

## Legacy

This file describes how to export the ERDs of certain EAS database tables to
a PDF file.

It is not known exactly how the legacy image files (`erd_*.jpeg`) were created
but in 2018 and beyond, they are printed, or otherwise distributed, as a PDF
file with an 11"x17", landscape-oriented page.

A legacy image file was pasted into a Microsoft Word file (`*.docx`) on a
single, 11"x17" page.  The approximate date that legacy image file was created
was captured in the lower right-hand corner of the page.  Finally, the Word
document was saved as a PDF, which is easily printed and distributed.

## Future

The EAS development team is currently exploring options for producing
high-quality ERDs for the entire EAS database.  Please see EAS issue
[#214](https://bitbucket.org/sfgovdt/eas/issues/214/evaluate-er-diagram-tools) for more information.

##### END OF FILE