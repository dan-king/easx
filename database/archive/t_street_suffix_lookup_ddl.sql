-- Table: street_suffix_lookup

-- DROP TABLE street_suffix_lookup;

CREATE TABLE street_suffix_lookup
(
  code character varying(4) NOT NULL,
  description character varying(20) NOT NULL,
  commonly_used character varying(20) NOT NULL,
  CONSTRAINT street_suffix_lookup_pkey PRIMARY KEY (code, commonly_used)
)
WITH (OIDS=FALSE);

-- DROP INDEX ssl_commonly_used_idx;

CREATE INDEX ssl_commonly_used_idx
  ON street_suffix_lookup
  USING btree
  (commonly_used);


ALTER TABLE street_suffix_lookup OWNER TO postgres;
