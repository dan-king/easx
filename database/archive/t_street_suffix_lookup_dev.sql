
--delete from street_suffix_lookup

select count(*)
from streetnames

select street_type
from streetnames
where street_type = 'RIGHT OF WAY'

select *
from street_suffix_lookup

select distinct street_type
from streetnames

select
	sn.base_street_name,
	sn.street_type,
	ssl.code,
	sn.street_suffix
from streetnames sn, 
	street_suffix_lookup ssl
where ssl.commonly_used = sn.street_type	
limit 100

update streetnames
set street_suffix = ssl.code
from street_suffix_lookup ssl
where ssl.commonly_used = street_type	

update streetnames
set	street_suffix = null

ALTER TABLE streetnames DROP COLUMN street_suffix CASCADE;

ALTER TABLE streetnames
   ADD COLUMN street_suffix character varying(4);

-- should be zero records
select base_street_name, street_type
from streetnames sn
where street_type not in (
	select description
	from street_suffix_lookup
	)

