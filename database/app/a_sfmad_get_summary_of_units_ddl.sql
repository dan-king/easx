
-- drop AGGREGATE _sfmad_get_summary_of_units(text) cascade;

CREATE AGGREGATE _sfmad_get_summary_of_units(text) (
    SFUNC=array_append,
    STYPE=text[],
    FINALFUNC=_sfmad_get_summary_of_units
);
