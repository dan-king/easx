-- View: v_address_search

-- DROP VIEW v_address_search;

CREATE OR REPLACE VIEW v_address_search AS 
 SELECT ab.address_base_id as base_address_id, 
	ab.prefix AS base_address_prefix, 
	ab.base_address_num, 
	ab.suffix AS base_address_suffix, 
	a.address_id,
	a.subaddress_num_prefix as address_num_prefix, 
	a.subaddress_num as address_num, 
	a.subaddress_num_suffix as address_num_suffix, 
	sn.base_street_name as street_name,
	sn.street_type,
	sn.category as street_category, 
	sn.prefix_direction as street_prefix_direction,
	sn.suffix_direction as street_suffix_direction,
	sn.full_streetname as street_fullname,
	z.zipcode, 
	z.jurisdiction,
	ab.geometry
   FROM address_base ab, 
	addresses a, 
	streetnames sn, 
	zones z
  WHERE a.retire_tms IS NULL 
  AND ab.address_base_id = a.address_base_id 
  AND ab.street_segment_id = sn.street_segment_id 
  AND ab.zone_id = z.zone_id;

ALTER TABLE v_address_search OWNER TO postgres;
