-- Table: cr_address_x_parcels

-- DROP TABLE cr_address_x_parcels;

CREATE TABLE cr_address_x_parcels
(
  cr_address_id integer NOT NULL,
  parcel_id integer NOT NULL,
  CONSTRAINT "Key2" PRIMARY KEY (cr_address_id, parcel_id),
  CONSTRAINT "cr_addresses_X_cr_addresses_x_parcels" FOREIGN KEY (cr_address_id)
      REFERENCES cr_addresses (cr_address_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "parcels_X_cr_subaddresses" FOREIGN KEY (parcel_id)
      REFERENCES parcels (parcel_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (OIDS=FALSE);
ALTER TABLE cr_address_x_parcels OWNER TO postgres;
