-- drop FUNCTION _sfmad_get_summary_of_units(endpoint_specifier int, input_array text);
CREATE OR REPLACE FUNCTION _sfmad_get_summary_of_units(input_array text[]) RETURNS text AS $$
DECLARE
    
    /* tests
    select _Sfmad_Get_Summary_Of_Units(Array['a','b','c','d', 'e', 'f', 'g', 'h']::Text[]);
    select _Sfmad_Get_Summary_Of_Units(Array['1','2','3','4', '5', '6', '7', '8']::Text[]);
    select _Sfmad_Get_Summary_Of_Units(Array['1','2','3','4', 'A', 'B', 'C', 'D']::Text[]);
    select _Sfmad_Get_Summary_Of_Units(Array['a','b','1','2', 'a1', 'b2', '1A', '2B']::Text[]);
    select '1A,2B,a,b,1,2' = _Sfmad_Get_Summary_Of_Units(Array['a','b','1','2', 'a1', 'b2', '1A', '2B']::Text[]);
    select '1a,3c' = _sfmad_get_summary_of_units(Array['1a', '2b', '3c']::Text[]);
    select '1a,20a' = _sfmad_get_summary_of_units(Array['10a', '1a', '2a', '20a']::Text[]);
    select '2a,20a' = _sfmad_get_summary_of_units(Array['10a', '2a', '20a']::Text[]);
    select 'a1,a20' = _sfmad_get_summary_of_units(Array['a1', 'a10', 'a20']::Text[]);
    select '0,4' = _sfmad_get_summary_of_units(Array[1,2,3,0,4]::Text[]);
    select '3A,101M,J5,M11,2505,2509' = _sfmad_get_summary_of_units(Array['2505','2506','2507','2508','2509','101M','3A','3B','3-B','3C','3D','3E','7-M','7N','7P','J5','M11']::Text[]);
    select 'unable to summarize' = _sfmad_get_summary_of_units(Array[null, null, null]::Text[]);
    select 'unable to summarize' = _sfmad_get_summary_of_units(Array[null, 1]::Text[]);
    */
    
    input_array_clean text[];
    integers integer[];
    int_then_alpha text[];
    alpha_then_any text[];
    final_array text[] := ARRAY[['', '']];
    
    test_text text;
    test_int int;
    test_text_part text;
    test_int_part int;
    test_remainder_part text;
    
    current_text text;
    current_int int;
    current_int_part int;
    current_remainder_part text;
    current_text_part text;

    min_int int;
    max_int int;

    min_text text;
    max_text text;

    return_string text := '';
    

BEGIN

    -- protect against: "ERROR:  lower bound of FOR loop cannot be NULL"
    -- a casual effort yeilds no elegant and proper solution
    -- should have used python
    if input_array[array_lower(input_array, 1)] is null then
        return 'unable to summarize';
    end if;

    -- nulls in the input array are bad - but there is no good way to 

    -- clean array (remove nulls, strip white space, )
    -- As it turns out this really does not remove nulls.
    for index in array_lower(input_array, 1)..array_upper(input_array, 1) loop
        test_text := trim(input_array[index]);
        if test_text is not null and length(test_text) > 0 then
            input_array_clean := array_append(input_array_clean, test_text);
        end if;
    end loop;


    -- scan the entire array 
    --     split out 3 groups: ints, int_then_alpha, alpha_then_any
    for index in array_lower(input_array_clean, 1)..array_upper(input_array_clean, 1) loop
        begin
            integers := array_append(integers, input_array_clean[index]::int);
        exception
            when others then
                if input_array_clean[index] ~ '(^[0-9]+)' then
                    int_then_alpha := array_append(int_then_alpha, input_array_clean[index]);
                else
                    alpha_then_any := array_append(alpha_then_any, input_array_clean[index]);
                end if;
        end;
    end loop;



    -- find the extreme int_then_alpha
    if array_lower(int_then_alpha, 1) is not null then
        min_text := int_then_alpha[1];
        max_text := int_then_alpha[1];
        for index in array_lower(int_then_alpha, 1)..array_upper(int_then_alpha, 1) loop
            
            current_text := int_then_alpha[index];
            
            current_int_part := 0;
            current_int_part := substring(current_text, '(^[0-9]+)')::int; -- get the number at begining of string
            current_remainder_part := trim(leading current_int_part::text from current_text);
            
            -- min    
            test_int_part := 0;
            test_int_part := substring( min_text, '(^[0-9]+)')::int; -- get the number at begining of string
            test_remainder_part := trim(leading test_int_part::text from min_text);
            if test_int_part > current_int_part then
                min_text := current_text;
            elsif test_int_part = current_int_part then
                if test_remainder_part > current_remainder_part then
                    min_text := current_text;
                end if;
            end if;

            -- max
            test_int_part := 0;
            test_int_part := substring( max_text, '(^[0-9]+)')::int; -- get the number at begining of string
            test_remainder_part := trim(leading test_int_part::text from max_text);
            if test_int_part < current_int_part then
                max_text := current_text;
            elsif test_int_part = current_int_part then
                if test_remainder_part < current_remainder_part then
                    max_text := current_text;
                end if;
            end if;
            
        end loop;
        final_array = final_array || Array[min_text::text, max_text::text]::Text[];
   end if;




    -- find the min and max for alpha_then_any
    if array_lower(alpha_then_any, 1) is not null then
        max_text = alpha_then_any[1];
        min_text = alpha_then_any[1];
        for index in array_lower(alpha_then_any, 1)..array_upper(alpha_then_any, 1) loop
            
            current_text := alpha_then_any[index];
            
            current_text_part := SUBSTRING( current_text, '(^[^0-9]+)'); -- leading text (not int)
            current_remainder_part := trim(leading current_text_part from current_text);
            current_int_part := 0;
            current_int_part := SUBSTRING( current_remainder_part, '(^[0-9]+)')::int;
            
            -- min
            test_text_part := SUBSTRING( min_text, '(^[^0-9]+)'); -- leading text
            test_remainder_part := trim(leading test_text_part from min_text);
            test_int_part := 0;
            test_int_part := SUBSTRING( test_remainder_part, '(^[0-9]+)')::int;
            if test_text_part > current_text_part then
                min_text := current_text;
            elsif test_text_part = current_text_part then
                if test_int_part > current_int_part then
                    min_text := current_text;
                end if;
            end if;
            
            -- max
            test_text_part := SUBSTRING( max_text, '(^[^0-9]+)'); -- leading text (not int)
            test_remainder_part := trim(leading test_text_part from max_text);
            test_int_part := 0;
            test_int_part := SUBSTRING( test_remainder_part, '(^[0-9]+)')::int;
            if test_text_part < current_text_part then
                max_text := current_text;
            elsif test_text_part = current_text_part then
                if test_int_part < current_int_part then
                    max_text := current_text;
                end if;
            end if;

        end loop;
        final_array = final_array || Array[min_text::text, max_text::text]::Text[];
    end if;




    -- find the min and max for integers
    if array_lower(integers, 1) is not null then
        current_int = integers[1];
        min_int = integers[1];
        max_int = integers[1];
        for index in array_lower(integers, 1)..array_upper(integers, 1) loop
            current_int = integers[index];
            if min_int > current_int then
                min_int := current_int;
            end if;
            if max_int < current_int then
                max_int := current_int;
            end if;
        end loop;
        final_array = final_array || Array[min_int::text, max_int::text]::Text[];
    end if;

    -- return concatenated string
    for index in array_lower(final_array, 1)..array_upper(final_array, 1) loop
        if index = 1 then
            continue;
        end if;
        
        return_string := return_string || final_array[index][1] || ',' || final_array[index][2];
        if index < array_upper(final_array, 1) then
            return_string := return_string || ',';
        end if;
        
    end loop;

    return return_string;

END;
$$
LANGUAGE plpgsql;

