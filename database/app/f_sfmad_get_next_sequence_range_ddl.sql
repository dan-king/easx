
-- Function: _sfmad_get_next_sequence_range(sequence_name text, count int)

-- DROP FUNCTION _sfmad_get_next_sequence_range(sequence_name text, count int)

CREATE OR REPLACE FUNCTION _sfmad_get_next_sequence_range(sequence_name text, count int)
  RETURNS text AS
$BODY$
DECLARE
    
    /* tests
    select _sfmad_get_next_sequence_range('cr_address_x_parcels_id_seq', 1);
   */
   
    minValue int;
    maxValue int;
    
BEGIN
    maxValue = setval(sequence_name, (nextval(sequence_name) + count - 1));
    minValue = maxValue - count + 1;
    return (minValue, maxValue);
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION _sfmad_get_summary_of_units(text[]) OWNER TO postgres;
