-- temporary patch for http://code.google.com/p/eas/issues/detail?id=451

update basemap_stclines_city_staging
set label_text = 'Mission Bay Blvd South'
where cnn in (
	15019101,
	15017101,
	15021101,
	15023101,
	15022101,
	15018101,
	15020101
);

update basemap_stclines_city_staging
set label_text = 'Mission Bay Blvd North'
where cnn in (
	15019201,
	15020201,
	15023202,
	15017201,
	15023201,
	15021201,
	15022201,
	15018201
);

update basemap_stclines_city_staging
set label_text = 'Buena Vista Ave East'
where cnn in (
	3362000, 
	3363000,
	3364000,
	3365000,
	3366000
);

update basemap_stclines_city_staging
set label_text = 'Buena Vista Ave West'
where cnn in (
	3367000,
	3368000,
	3369000,
	3370000,
	3371000
);
