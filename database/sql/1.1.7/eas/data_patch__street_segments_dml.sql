update street_segments ss
set future = sss.future
from street_segments_staging sss
where ss.seg_cnn = sss.seg_cnn;

/* DATA QA

select future, count(*)
from street_segments
group by future

select future, count(*)
from street_segments_staging
group by future

select * 
from street_segments ss
left outer join street_segments_staging sss on (ss.seg_cnn = sss.seg_cnn)
where sss.seg_cnn is null;

select * 
from street_segments_staging sss
left outer join street_segments ss on (sss.seg_cnn = ss.seg_cnn)
where ss.seg_cnn is null;

select count(*) from street_segments

select count(*) from street_segments_staging

*/
