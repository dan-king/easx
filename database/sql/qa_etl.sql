


select count(*) from parcels_staging;
select count(*) from parcels;

select count(*) from basemap_citylots
union all
select count(*) from basemap_citylots_staging
union all
select count(*) from basemap_citylots_base
union all
select count(*) from basemap_citylots_base_staging;


select count(*) from etl_bboxes;
select count(*) from address_review;

select count(*) from basemap_stclines_city_staging
union all
select count(*) from basemap_stclines_arterials_staging
union all
select count(*) from basemap_stclines_freeway_staging
union all
select count(*) from basemap_stclines_highway_staging
union all
select count(*) from basemap_stclines_major_staging
union all
select count(*) from basemap_stclines_minor_staging
union all
select count(*) from basemap_stclines_non_paper_staging
union all
select count(*) from basemap_stclines_other_staging
union all
select count(*) from basemap_stclines_ramp_staging;

select count(*) from basemap_stclines_city
union all
select count(*) from basemap_stclines_arterials
union all
select count(*) from basemap_stclines_freeway
union all
select count(*) from basemap_stclines_highway
union all
select count(*) from basemap_stclines_major
union all
select count(*) from basemap_stclines_minor
union all
select count(*) from basemap_stclines_non_paper
union all
select count(*) from basemap_stclines_other
union all
select count(*) from basemap_stclines_ramp;


-- mad and sfmaps
/*
truncate etl_tables;
truncate etl_exceptions;
truncate etl_jobs cascade;
truncate etl_db_backups;
truncate etl_bboxes;
*/

-- mad truncate tables
/*
truncate parcels_staging;
truncate street_segments_staging;
truncate streetnames_staging;
truncate address_review;
*/



-- sfmaps truncate tables
/*
truncate basemap_bay_area_cities;
truncate basemap_bay_area_cities_staging;

truncate basemap_citylots;
truncate basemap_citylots_staging;

truncate basemap_citylots_base;
truncate basemap_citylots_base_staging;

truncate basemap_county_line;
truncate basemap_county_line_staging;

truncate basemap_ocean_bay_labels;
truncate basemap_ocean_bay_labels_staging;

truncate basemap_poi;
truncate basemap_poi_staging;

truncate basemap_sfmaps_coast;
truncate basemap_sfmaps_coast_staging;

truncate basemap_stclines_city;
truncate basemap_stclines_city_staging;

truncate basemap_stclines_arterials;
truncate basemap_stclines_arterials_staging;

truncate basemap_stclines_freeway;
truncate basemap_stclines_freeway_staging;

truncate basemap_stclines_highway;
truncate basemap_stclines_highway_staging;

truncate basemap_stclines_major;
truncate basemap_stclines_major_staging;

truncate basemap_stclines_minor;
truncate basemap_stclines_minor_staging;

truncate basemap_stclines_non_paper;
truncate basemap_stclines_non_paper_staging;

truncate basemap_stclines_other;
truncate basemap_stclines_other_staging;

truncate basemap_stclines_ramp;
truncate basemap_stclines_ramp_staging;
*/


