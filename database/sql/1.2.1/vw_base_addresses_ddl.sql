-- View: vw_base_addresses


-- select * from vw_base_addresses limit 1000;
-- select count(*) from vw_base_addresses;

DROP VIEW vw_base_addresses;

CREATE OR REPLACE VIEW vw_base_addresses AS
    SELECT
        ab.address_base_id,
        a.address_id,
        ab.base_address_prefix,
        ab.base_address_num,
        ab.base_address_suffix,
        st_transform(ab.geometry, 900913) AS geometry,
        sn.full_street_name,
        z.zipcode,
        z.jurisdiction,
        ss.l_f_add,
        ss.l_t_add,
        ss.r_f_add,
        ss.r_t_add,
        ab.retire_tms,
        (select count(*) from invalid_addresses ia where ia.address_id = a.address_id) as validation_warning_count,
        (CASE 
            WHEN ab.base_address_prefix IS NULL THEN '' 
            ELSE ab.base_address_prefix || ' ' 
        END || ab.base_address_num || ' ' ||
        CASE 
            WHEN ab.base_address_suffix IS NULL THEN '' 
            ELSE ab.base_address_suffix || ' ' 
        END || sn.full_street_name) as address
    FROM
        address_base ab,
        streetnames sn,
        street_segments ss,
        zones z,
        addresses a
    WHERE
        ab.zone_id = z.zone_id
        AND ab.street_segment_id = sn.street_segment_id
        AND ab.street_segment_id = ss.street_segment_id
        AND sn.category::text = 'MAP'::text
        AND ab.address_base_id = a.address_base_id
        AND a.address_base_flg = true;

ALTER TABLE vw_base_addresses OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_base_addresses TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_base_addresses TO django;
