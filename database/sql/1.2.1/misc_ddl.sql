CREATE INDEX invalid_addresses_idx_address_id
  ON invalid_addresses
  USING btree
  (address_id);