
-- We drop these because we need to alter the streetnames table.
-- All of these are needed and will be recreated later.
drop view v_base_address_search;
drop view vw_address_search;
drop view vw_addresses_flat_export;
drop view vw_addresses_flat_native;
drop view vw_base_addresses;
drop view vw_invalid_addresses;
drop view vw_street_segments_map;

----- we do not restore these - they are obsolete for various reasons
drop view vw_addresses_unqadds;
drop view vw_streets_for_new_address;
drop view vw_streetnames_distinct;
drop view vw_streets_for_address;

