-- Function: _eas_correct_etl_street_segment_splits()

-- DROP FUNCTION _eas_correct_etl_street_segment_splits();

CREATE OR REPLACE FUNCTION _eas_correct_etl_street_segment_splits()
  RETURNS text AS
$BODY$
DECLARE

    _address_base_id int;
    _retired_street_segment_id int;
    _unretired_street_segment_id int;
    _base_address_num int;
    _base_address_suffix character varying(10);
    _full_street_name character varying(255);

BEGIN

    alter table address_base disable trigger _eas_address_base_after;

    ----- Cursor through invalid addresses that are invalid because of a street retirement.
    declare cursorInvalidAddresses cursor for
        SELECT
            ab.address_base_id,
            ab.street_segment_id,
            ab.base_address_num,
            ab.base_address_suffix,
            sn.full_street_name
        FROM address_base ab
        inner join addresses a on (ab.address_base_id = a.address_base_id)
        inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
        inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
        WHERE 1 = 1
        and sn.category = 'MAP'
        and a.address_base_flg = true
        and ss.date_dropped is not null
        and ab.retire_tms is null
        order by
            sn.base_street_name,
            sn.street_type,
            ab.base_address_num,
            ab.base_address_suffix,
            a.unit_num;
    begin
        open cursorInvalidAddresses;
        loop
            fetch cursorInvalidAddresses into _address_base_id, _retired_street_segment_id, _base_address_num, _base_address_suffix, _full_street_name;
            if not found then
                exit;
            end if;

            -- RAISE NOTICE '_address_base_id: %, _retired_street_segment_id: %, _base_address_num: %, _base_address_suffix: %, _full_street_name: %', _address_base_id, _retired_street_segment_id, _base_address_num, _base_address_suffix, _full_street_name;

            -- Look for the nearest unretired street that has the same name and is within the specified distance.
            -- We do not check to see if the address range fit is correct.
            select ss.street_segment_id into _unretired_street_segment_id
            from
                address_base ab,
                street_segments ss,
                streetnames sn
            where ss.street_segment_id = sn.street_segment_id
            and sn.category = 'MAP'
            and st_distance(ab.geometry, ss.geometry) < 500
            and ab.address_base_id = _address_base_id
            and sn.full_street_name = _full_street_name
            and ss.street_segment_id != _retired_street_segment_id
            and ss.date_dropped is null
            order by st_distance(ab.geometry, ss.geometry) asc
            limit 1;


            if _unretired_street_segment_id is not null then

                -- RAISE NOTICE 'update to street segment to _unretired_street_segment_id: %', _unretired_street_segment_id;

                update address_base
                set street_segment_id = _unretired_street_segment_id
                where address_base_id = _address_base_id;

            end if;


        end loop;
        close cursorInvalidAddresses;
    end;
    -----

    alter table address_base enable trigger _eas_address_base_after;

    return 'success!';


END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _eas_correct_etl_street_segment_splits() OWNER TO eas_dbo;
