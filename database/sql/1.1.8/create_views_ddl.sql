-- View: v_base_address_search

-- DROP VIEW v_base_address_search;

CREATE OR REPLACE VIEW v_base_address_search AS
 SELECT
    ab.address_base_id,
    ab.base_address_prefix,
    ab.base_address_num,
    ab.base_address_suffix,
    sn.base_street_name AS street_name,
    sn.street_type,
    sn.category AS street_category,
    sn.pre_direction AS street_pre_direction,
    sn.post_direction AS street_post_direction,
    sn.full_street_name,
    sn.street_segment_id, z.zipcode,
    z.jurisdiction,
    ad.disposition_description::text AS disposition,
    st_transform(ab.geometry, 900913) AS geometry,
    a.create_tms,
    a.last_change_tms,
    a.retire_tms
   FROM address_base ab, addresses a, streetnames sn, zones z, d_address_disposition ad
  WHERE 1 = 1 AND ab.address_base_id = a.address_base_id
   AND a.retire_tms IS NULL AND a.address_base_flg = true
   AND ab.zone_id = z.zone_id AND ab.street_segment_id = sn.street_segment_id
    AND a.disposition_code = ad.disposition_code;

ALTER TABLE v_base_address_search OWNER TO eas_dbo;
GRANT ALL ON TABLE v_base_address_search TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE v_base_address_search TO django;


-- View: vw_address_search

-- DROP VIEW vw_address_search;

CREATE OR REPLACE VIEW vw_address_search AS
 SELECT b.address_id, a.geometry, a.address_base_id, a.base_address_prefix AS base_prefix, a.base_address_num, a.base_address_suffix AS base_suffix, b.unit_num, c.full_street_name, c.category, d.zipcode, d.jurisdiction
   FROM address_base a, addresses b, streetnames c, zones d
  WHERE b.retire_tms IS NULL AND a.address_base_id = b.address_base_id AND a.street_segment_id = c.street_segment_id AND a.zone_id = d.zone_id;

ALTER TABLE vw_address_search OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_address_search TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_address_search TO django;




-- View: vw_addresses_flat_export
-- DROP VIEW vw_addresses_flat_export;
-- select * from vw_addresses_flat_export where street_full_street_name = 'MISSION BAY BLVD NORTH';
CREATE OR REPLACE VIEW vw_addresses_flat_export AS
 SELECT (((ab.address_base_id::text || '-'::text) || a.address_id::text) || '-'::text) ||
        CASE
            WHEN axp.id IS NULL THEN '0'::text
            ELSE axp.id::text
        END AS id,
        ab.address_base_id AS eas_address_base_id,
        ab.base_address_num,
        ab.base_address_suffix,
        sn.base_street_name AS street_name,
        sn.street_type,
        sn.post_direction AS street_post_direction,
        ss.seg_cnn AS street_cnn,
        sn.full_street_name AS street_full_street_name,
        ab.create_tms AS base_address_create_tms,
        ab.retire_tms AS base_address_retire_tms,
        a.address_id AS eas_unit_address_id,
        a.address_base_flg AS unit_address_base_flg,
        a.unit_num AS unit_address,
        a.create_tms AS unit_address_create_tms,
        a.retire_tms AS unit_address_retire_tms,
        dap.disposition_description,
        p.map_blk_lot AS map_block_lot,
        p.blk_lot AS block_lot,
        p.date_map_add AS parcel_date_map_add,
        p.date_map_drop AS parcel_date_map_drop,
        axp.id AS eas_address_x_parcel_id,
        axp.create_tms AS address_x_parcel_create_tms,
        axp.retire_tms AS address_x_parcel_retire_tms,
        z.zipcode,
        x(transform(ab.geometry,4326)) AS longitude,
        y(transform(ab.geometry,4326)) AS latitude, ab.geometry
   FROM address_base ab
   JOIN addresses a ON ab.address_base_id = a.address_base_id
   JOIN streetnames sn ON ab.street_segment_id = sn.street_segment_id
   JOIN street_segments ss ON ab.street_segment_id = ss.street_segment_id
   JOIN zones z ON z.zone_id = ab.zone_id
   JOIN d_address_disposition dap ON a.disposition_code = dap.disposition_code
   LEFT JOIN address_x_parcels axp ON axp.address_id = a.address_id
   LEFT JOIN parcels p ON axp.parcel_id = p.parcel_id
  WHERE 1 = 1 AND sn.category::text = 'MAP'::text;

ALTER TABLE vw_addresses_flat_export OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_addresses_flat_export TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_addresses_flat_export TO django;
GRANT SELECT ON TABLE vw_addresses_flat_export TO geoserver;





-- View: vw_addresses_flat_native
-- DROP VIEW vw_addresses_flat_native;
-- select * from vw_addresses_flat_native where full_street_name = 'MISSION BAY BLVD NORTH';
CREATE OR REPLACE VIEW vw_addresses_flat_native AS
 SELECT (((ab.address_base_id::text || '-'::text) || a.address_id::text) || '-'::text) ||
        CASE
            WHEN axp.id IS NULL THEN '0'::text
            ELSE axp.id::text
        END AS id,
        ab.address_base_id,
        ab.base_address_num,
        ab.base_address_suffix,
        sn.base_street_name,
        sn.street_type,
        sn.post_direction,
        sn.full_street_name,
        ss.seg_cnn,
        ss.street_segment_id,
        ab.create_tms AS base_address_create_tms,
        ab.retire_tms AS base_address_retire_tms,
        a.address_id,
        a.address_base_flg,
        a.unit_num,
        a.unit_type_id,
        a.floor_id,
        a.disposition_code,
        dap.disposition_description,
        a.create_tms AS unit_address_create_tms,
        a.retire_tms AS unit_address_retire_tms,
        p.map_blk_lot,
        p.blk_lot,
        p.date_map_add AS parcel_date_map_add,
        p.date_map_drop AS parcel_date_map_drop,
        axp.id AS address_x_parcel_id,
        axp.create_tms AS address_x_parcel_create_tms,
        axp.retire_tms AS address_x_parcel_retire_tms,
        z.zipcode,
        ab.geometry
   FROM address_base ab
   JOIN addresses a ON ab.address_base_id = a.address_base_id
   JOIN streetnames sn ON ab.street_segment_id = sn.street_segment_id
   JOIN street_segments ss ON ab.street_segment_id = ss.street_segment_id
   JOIN zones z ON z.zone_id = ab.zone_id
   JOIN d_address_disposition dap ON a.disposition_code = dap.disposition_code
   LEFT JOIN address_x_parcels axp ON axp.address_id = a.address_id
   LEFT JOIN parcels p ON axp.parcel_id = p.parcel_id
  WHERE 1 = 1 AND sn.category::text = 'MAP'::text;

ALTER TABLE vw_addresses_flat_native OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_addresses_flat_native TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_addresses_flat_native TO django;
GRANT SELECT ON TABLE vw_addresses_flat_native TO geoserver;





-- View: vw_base_addresses

-- DROP VIEW vw_base_addresses;

CREATE OR REPLACE VIEW vw_base_addresses AS
 SELECT a.address_base_id, a.base_address_prefix, a.base_address_num, a.base_address_suffix, st_transform(a.geometry, 900913) AS geometry, c.full_street_name, d.zipcode, d.jurisdiction
   FROM address_base a, streetnames c, zones d
  WHERE a.retire_tms IS NULL AND a.zone_id = d.zone_id AND a.street_segment_id = c.street_segment_id AND c.category::text = 'MAP'::text
  ORDER BY c.base_street_name, c.street_type, a.base_address_num;

ALTER TABLE vw_base_addresses OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_base_addresses TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_base_addresses TO django;




-- View: vw_invalid_addresses

-- DROP VIEW vw_invalid_addresses;

CREATE OR REPLACE VIEW vw_invalid_addresses AS
 SELECT ia.invalid_address_id, ia.message, ab.address_base_id, ab.base_address_num, ab.base_address_suffix, ab.geometry, sn.full_street_name, a.unit_num, a.address_base_flg, diat.invalid_type_desc
   FROM invalid_addresses ia, addresses a, address_base ab, streetnames sn, d_invalid_address_types diat
  WHERE 1 = 1 AND ia.address_id = a.address_id AND a.address_base_id = ab.address_base_id AND sn.category::text = 'MAP'::text AND ab.street_segment_id = sn.street_segment_id AND diat.invalid_type_id = ia.invalid_type_id;

ALTER TABLE vw_invalid_addresses OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_invalid_addresses TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_invalid_addresses TO django;
GRANT SELECT ON TABLE vw_invalid_addresses TO geoserver;






-- View: vw_street_segments_map

-- DROP VIEW vw_street_segments_map;

CREATE OR REPLACE VIEW vw_street_segments_map AS
 SELECT ss.street_segment_id, ss.seg_cnn, ss.str_seg_cnn, sn.base_street_name, sn.street_type, ss.l_f_add, ss.l_t_add, ss.r_f_add, ss.r_t_add, ss.f_st, ss.t_st, ss.f_node_cnn, ss.t_node_cnn, ss.date_added, ss.gds_chg_id_add, ss.date_dropped, ss.gds_chg_id_dropped, ss.date_altered, ss.gds_chg_id_altered, ss.zip_code, ss.district, ss.accepted, ss.jurisdiction, ss.n_hood, ss.layer, ss.active, ss.future, ss.toggle_date, ss.create_tms, ss.update_tms, ss.geometry
   FROM street_segments ss, streetnames sn
  WHERE ss.street_segment_id = sn.street_segment_id AND sn.category::text = 'MAP'::text;

ALTER TABLE vw_street_segments_map OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_street_segments_map TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_street_segments_map TO django;




-- View: vw_streets_for_address

-- DROP VIEW vw_streets_for_address;

CREATE OR REPLACE VIEW vw_streets_for_address AS
 SELECT ss.street_segment_id, ss.geometry, ss.date_dropped, ((((sn.full_street_name::text || ' ('::text) || LEAST(sar.right_from_address, sar.right_to_address, sar.left_from_address, sar.left_to_address)) || ' - '::text) || GREATEST(sar.right_from_address, sar.right_to_address, sar.left_from_address, sar.left_to_address)) || ')'::text AS description
   FROM street_segments ss, streetnames sn, street_address_ranges sar
  WHERE ss.street_segment_id = sn.street_segment_id AND ss.street_segment_id = sar.street_segment_id AND sn.category::text = 'MAP'::text;

ALTER TABLE vw_streets_for_address OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_streets_for_address TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_streets_for_address TO django;




