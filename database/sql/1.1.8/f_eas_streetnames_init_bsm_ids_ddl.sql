
-- DROP FUNCTION _eas_streetnames_init_bsm_ids();
-- select _eas_streetnames_init_bsm_ids();

CREATE OR REPLACE FUNCTION _eas_streetnames_init_bsm_ids()
  RETURNS text AS
$BODY$
DECLARE

BEGIN

    -- This proc automatically droppped immedaitely after the 1.1.8 release.

    if (
        (
            select count(*)
            from streetnames_staging sns1,
                streetnames_staging sns2
            where 1=1
            and sns1.bsm_streetnameid <> sns2.bsm_streetnameid
            and sns1.base_street_name = sns2.base_street_name
            and coalesce(sns1.street_type, '') = coalesce(sns2.street_type, '')
            and coalesce(sns1.post_direction, '') = coalesce(sns2.post_direction, '')
            and sns1.category = sns2.category
        ) > 0
    ) then
        raise exception 'There are functionally duplicate records in the streetnames_staging table. This indicates a data error or that a post-provisioning cleanup is needed.';
    end if;

    ------- clean up the streetnames in 2 steps
    -- step 1: if the bsm_streetnameid exists, use it to update the other fields
    update streetnames sn
    set 
        base_street_name = a.base_street_name,
        street_type = a.street_type,
        post_direction = a.post_direction,
        category = a.category
    from 
        (
            select distinct 
                base_street_name,
                street_type,
                category,
                bsm_streetnameid,
                pre_direction,
                post_direction
            from streetnames_staging 
        ) a
    where sn.bsm_streetnameid is not null
    and sn.bsm_streetnameid = a.bsm_streetnameid;

    -- step 2: set the bsm_streetnameid
    update streetnames sn
    set bsm_streetnameid = sns.bsm_streetnameid
    from streetnames_staging sns
    where sn.base_street_name = sns.base_street_name
    and coalesce(sn.street_type, '') = coalesce(sns.street_type, '')
    and coalesce(sn.post_direction, '') = coalesce(sns.post_direction, '')
    and sn.category = sns.category;
    -----

    -- select * from streetnames order by bsm_streetnameid

    return 'success!';


END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _eas_streetnames_init_bsm_ids() OWNER TO eas_dbo;
