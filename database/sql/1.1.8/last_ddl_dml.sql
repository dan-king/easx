
DROP FUNCTION _eas_streetnames_init_bsm_ids();

ALTER TABLE streetnames
    ADD CONSTRAINT bsmstreetnameid_segcnn_unq
    UNIQUE (bsm_streetnameid, seg_cnn);
