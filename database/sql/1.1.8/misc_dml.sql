
delete from geometry_columns
where f_table_name = 'test_street_address_geocode'
and f_table_schema = 'public';

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'street_segments_staging', 'geometry', 2, 2227, 'MULTILINESTRING'
where not exists (
	select 1 from geometry_columns where f_table_schema = 'public'	and f_table_name = 'street_segments_staging'
);
