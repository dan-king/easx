alter table address_base drop column unq_adds_id;
alter table address_base_history drop column unq_adds_id;
alter table addresses drop column unq_adds_id;
alter table addresses_history drop column unq_adds_id;
alter table address_x_parcels drop column confidence_score;
alter table address_x_parcels_history drop column confidence_score;

alter table change_requests add CONSTRAINT "change_request_fk_request_user" FOREIGN KEY (requestor_user_id)
REFERENCES auth_user (id) MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT;

alter table change_requests add CONSTRAINT "change_request_fk_review_user" FOREIGN KEY (reviewer_user_id)
REFERENCES auth_user (id) MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT;

alter table cr_change_requests add CONSTRAINT "change_request_fk_request_user" FOREIGN KEY (requestor_user_id)
REFERENCES auth_user (id) MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT;

alter table cr_change_requests add CONSTRAINT "change_request_fk_review_user" FOREIGN KEY (reviewer_user_id)
REFERENCES auth_user (id) MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT;

-- We put this trigger validation into the streets etl proc.
DROP TRIGGER _eas_streetnames_before ON streetnames;
DROP FUNCTION _eas_streetnames_before();
DROP INDEX streetnames_unq_category;

drop table etl_exceptions;
