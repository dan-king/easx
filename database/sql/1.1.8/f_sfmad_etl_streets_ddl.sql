
DROP FUNCTION _sfmad_etl_streets(character);

-- Function: _sfmad_etl_streets(character)

CREATE OR REPLACE FUNCTION _sfmad_etl_streets(IN job_id_in character, OUT success boolean, OUT messagetext text)
  RETURNS record AS
$BODY$
DECLARE

BEGIN

  truncate street_segments_work;
  truncate streetnames_work;
  success = false;

  ----- general preparation and validation BEGIN

  -- If we include POI (points of interest) application behavior is undefined.
  delete from streetnames_staging where category = 'POI';
  
  -- there are two records in the street segments data that are need to be deleted.  i dont know why (ABL)
  delete from street_segments_staging where seg_cnn = 0;

  -- If the staging table has fewer records than the target, we have an issue.
  -- Exclude the 'UNKNOWN' segment because it will never be in the DPW data.
  if (select count(*) from street_segments_staging) < (select count(*) from street_segments where seg_cnn != -1) then
    success = false;
    messageText = 'ETL failed. There are more rows in street_segments than in street_segments_staging.';
    return;
  end if;

  --check that each each street_segments_staging record has a unique seg_cnn value
  if (select count(*) from street_segments_staging) <> (select count(*) from (select distinct (seg_cnn) from street_segments_staging) a) then
    success = false;
    messageText = 'ETL failed. seg_cnn values in street_segments_staging are not unique.';
    return;
  end if;
  ----- general preparation and validation END




  ----- street names preparation and validation BEGIN
  if (
        (
            select count(*)
            from streetnames_staging sns1,
                streetnames_staging sns2
            where 1=1
            and sns1.bsm_streetnameid <> sns2.bsm_streetnameid
            and sns1.base_street_name = sns2.base_street_name
            and coalesce(sns1.street_type, '') = coalesce(sns2.street_type, '')
            and coalesce(sns1.post_direction, '') = coalesce(sns2.post_direction, '')
            and sns1.category = sns2.category
        ) > 0
    ) then
        success = false;
        messageText = 'There are functionally duplicate records in the streetnames_staging table. This indicates a data error or that a post-provisioning cleanup is needed.';
        return;
  end if;

  if (
        (
            select count(*)
            from streetnames sn1,
                streetnames sn2
            where 1=1
            and sn1.bsm_streetnameid <> sn2.bsm_streetnameid
            and sn1.base_street_name = sn2.base_street_name
            and coalesce(sn1.street_type, '') = coalesce(sn2.street_type, '')
            and coalesce(sn1.post_direction, '') = coalesce(sn2.post_direction, '')
            and sn1.category = sn2.category
        ) > 0
    ) then
        success = false;
        messageText = 'There are functionally duplicate records in the streetnames table. This should never happen.';
        return;
  end if;
  ----- street names preparation and validation END




  --insert new street segment records into main table...generate street_segment_id value
  -- store the new street segment records
  insert into street_segments_work (
      seg_cnn,
      str_seg_cnn,
      l_f_add,
      l_t_add,
      r_f_add,
      r_t_add,
      f_st,
      t_st,
      f_node_cnn,
      t_node_cnn,
      date_added,
      gds_chg_id_add,
      date_dropped,
      gds_chg_id_dropped,
      date_altered,
      gds_chg_id_altered,
      zip_code,
      district,
      accepted,
      jurisdiction,
      n_hood,
      layer,
      active,
      future,
      toggle_date,
      geometry,
      status
  )
  select
    sss.seg_cnn,
    sss.str_seg_cnn,
    sss.l_f_add,
    sss.l_t_add,
    sss.r_f_add,
    sss.r_t_add,
    sss.f_st,
    sss.t_st,
    sss.f_node_cnn,
    sss.t_node_cnn,
    sss.date_added,
    sss.gds_chg_id_add,
    sss.date_dropped,
    sss.gds_chg_id_dropped,
    sss.date_altered,
    sss.gds_chg_id_altered,
    sss.zip_code,
    sss.district,
    sss.accepted,
    sss.jurisdiction,
    sss.n_hood,
    sss.layer,
    sss.active,
    sss.future,
    sss.toggle_date,
    st_transform(sss.geometry, 2227),
    'NEW'
  FROM street_segments_staging sss
  left join street_segments b on sss.seg_cnn = b.seg_cnn
  where sss.seg_cnn <> 0
  and b.seg_cnn is null;


  -- insert new street segments records in street_segments, generate a new street_segment_id
  insert into street_segments (
    seg_cnn,
    str_seg_cnn,
    l_f_add,
    l_t_add,
    r_f_add,
    r_t_add,
    f_st,
    t_st,
    f_node_cnn,
    t_node_cnn,
    date_added,
    gds_chg_id_add,
    date_dropped,
    gds_chg_id_dropped,
    date_altered,
    gds_chg_id_altered,
    zip_code,
    district,
    accepted,
    jurisdiction,
    n_hood,
    layer,
    active,
    future,
    toggle_date,
    create_tms,
    geometry
  )
  Select
    a.seg_cnn,
    a.str_seg_cnn,
    a.l_f_add,
    a.l_t_add,
    a.r_f_add,
    a.r_t_add,
    a.f_st,
    a.t_st,
    a.f_node_cnn,
    a.t_node_cnn,
    a.date_added,
    a.gds_chg_id_add,
    a.date_dropped,
    a.gds_chg_id_dropped,
    a.date_altered,
    a.gds_chg_id_altered,
    a.zip_code,
    a.district,
    a.accepted,
    a.jurisdiction,
    a.n_hood,
    a.layer,
    a.active,
    a.future,
    a.toggle_date,
    now(),
    a.geometry
  from street_segments_work a
  where a.status = 'NEW';


  --update business values of all parcels based on the latest data without changing street_segment_id

  --store records that have a newly created drop/retire date
  insert into street_segments_work (
      seg_cnn,
      str_seg_cnn,
      l_f_add,
      l_t_add,
      r_f_add,
      r_t_add,
      f_st,
      t_st,
      f_node_cnn,
      t_node_cnn,
      date_added,
      gds_chg_id_add,
      date_dropped,
      gds_chg_id_dropped,
      date_altered,
      gds_chg_id_altered,
      zip_code,
      district,
      accepted,
      jurisdiction,
      n_hood,
      layer,
      active,
      future,
      toggle_date,
      geometry,
      status
  ) select
      sss.seg_cnn,
      sss.str_seg_cnn,
      sss.l_f_add,
      sss.l_t_add,
      sss.r_f_add,
      sss.r_t_add,
      sss.f_st,
      sss.t_st,
      sss.f_node_cnn,
      sss.t_node_cnn,
      sss.date_added,
      sss.gds_chg_id_add,
      sss.date_dropped,
      sss.gds_chg_id_dropped,
      sss.date_altered,
      sss.gds_chg_id_altered,
      sss.zip_code,
      sss.district,
      sss.accepted,
      sss.jurisdiction,
      sss.n_hood,
      sss.layer,
      sss.active,
      sss.future,
      sss.toggle_date,
      st_transform(sss.geometry, 2227),
      'RETIRED'
  from
    street_segments ss,
    street_segments_staging sss
  where ss.seg_cnn = sss.seg_cnn
  AND COALESCE(ss.date_dropped, '1900-01-01') <> COALESCE(sss.date_dropped, '1900-01-01');


  update street_segments a
  set date_dropped = (
    select b.date_dropped
    from street_segments_work b
    where a.seg_cnn = b.seg_cnn
  )
  from street_segments_work c
  where a.seg_cnn = c.seg_cnn
  and c.status = 'RETIRED';


  -- store records that have a newly created alter dates
  insert into street_segments_work (
      seg_cnn,
      str_seg_cnn,
      l_f_add,
      l_t_add,
      r_f_add,
      r_t_add,
      f_st,
      t_st,
      f_node_cnn,
      t_node_cnn,
      date_added,
      gds_chg_id_add,
      date_dropped,
      gds_chg_id_dropped,
      date_altered,
      gds_chg_id_altered,
      zip_code,
      district,
      accepted,
      jurisdiction,
      n_hood,
      layer,
      active,
      future,
      toggle_date,
      geometry,
      status
  ) select
      sss.seg_cnn,
      sss.str_seg_cnn,
      sss.l_f_add,
      sss.l_t_add,
      sss.r_f_add,
      sss.r_t_add,
      sss.f_st,
      sss.t_st,
      sss.f_node_cnn,
      sss.t_node_cnn,
      sss.date_added,
      sss.gds_chg_id_add,
      sss.date_dropped,
      sss.gds_chg_id_dropped,
      sss.date_altered,
      sss.gds_chg_id_altered,
      sss.zip_code,
      sss.district,
      sss.accepted,
      sss.jurisdiction,
      sss.n_hood,
      sss.layer,
      sss.active,
      sss.future,
      sss.toggle_date,
      st_transform(sss.geometry, 2227),
      'ALTERED'
  from
    street_segments ss,
    street_segments_staging sss
  where ss.seg_cnn = sss.seg_cnn
  and (
       coalesce(ss.l_f_add, 0)                 <> coalesce(sss.l_f_add, 0)
    or coalesce(ss.l_t_add, 0)                 <> coalesce(sss.l_t_add, 0)
    or coalesce(ss.r_f_add, 0)                 <> coalesce(sss.r_f_add, 0)
    or coalesce(ss.r_t_add, 0)                 <> coalesce(sss.r_t_add, 0)
    or coalesce(ss.date_added)                 <> coalesce(sss.date_added)
    or coalesce(ss.date_dropped)               <> coalesce(sss.date_dropped)
    or coalesce(ss.date_altered, '1900-01-01') <> coalesce(sss.date_altered, '1900-01-01')
    or (
        ss.geometry is not null
        AND
        sss.geometry is not null
        AND
        ST_HausdorffDistance(ss.geometry, sss.geometry) > 0.05
	)
  );


  -- todo
  -- Decide where you want to keep the left-from, left-to, etc and keep it in one single place.
  -- In the meantime, we maintain wherever we see it.
  update street_segments ss
  set
    date_altered = ssw.date_altered,
    geometry = coalesce(st_transform(ssw.geometry, 2227), ss.geometry),
    l_f_add = ssw.l_f_add,
    l_t_add = ssw.l_t_add,
    r_f_add = ssw.r_f_add,
    r_t_add = ssw.r_t_add
  from street_segments_work ssw
  where ss.seg_cnn = ssw.seg_cnn
  and ssw.status = 'ALTERED';


  ----- BEGIN street address ranges
  insert into street_address_ranges (left_from_address, left_to_address, right_from_address, right_to_address, create_tms, street_segment_id)
  select
    a.l_f_add,
    a.l_t_add,
    a.r_f_add,
    a.r_t_add,
    now(),
    b.street_segment_id
  from  street_segments_work a,
        street_segments b
  where a.seg_cnn = b.seg_cnn
  and a.status = 'NEW';

  update street_address_ranges sar
  set
    left_from_address   = ssw.l_f_add,
    left_to_address     = ssw.l_t_add,
    right_from_address  = ssw.r_f_add,
    right_to_address    = ssw.r_t_add,
    update_tms          = now()
  from  street_segments ss,
        street_segments_work ssw
  where ss.seg_cnn = ssw.seg_cnn
  and ssw.status = 'ALTERED'
  and ss.street_segment_id = sar.street_segment_id;
  ----- END street address ranges


  -- apply an update_tms value to every record that got retired or altered
  update street_segments a
  set update_tms =  now()
  from (
    select ssw.seg_cnn as seg_cnn
    from street_segments_work ssw
    where ssw.status in ('ALTERED', 'RETIRED')
    group by ssw.seg_cnn
  ) b
  where b.seg_cnn = a.seg_cnn;




  ----- BEGIN streetnames processing
  -- streetnames_work is used for processing and reporting

  -- preparation
  insert into streetnames_work (
      seg_cnn,
      bsm_streetnameid,
      pre_direction,
      base_street_name,
      street_type,
      post_direction,
      full_street_name,
      category,
      create_tms,
      status
  )
  select
      sns.seg_cnn,
      sns.bsm_streetnameid,
      sns.pre_direction,
      sns.base_street_name,
      sns.street_type,
      sns.post_direction,
      sns.full_street_name,
      sns.category,
      now(),
      'INSERT'
  from streetnames_staging sns
  left join streetnames sn on (sns.bsm_streetnameid = sn.bsm_streetnameid and sns.seg_cnn = sn.seg_cnn)
  where sn.streetname_id is null and sn.seg_cnn is null;

  insert into streetnames_work (
      seg_cnn,
      bsm_streetnameid,
      pre_direction,
      base_street_name,
      street_type,
      post_direction,
      full_street_name,
      category,
      create_tms,
      status
  )
  select
      sns.seg_cnn,
      sns.bsm_streetnameid,
      sns.pre_direction,
      sns.base_street_name,
      sns.street_type,
      sns.post_direction,
      sns.full_street_name,
      sns.category,
      now(),
      'UPDATE'
  from streetnames_staging sns
  join streetnames sn on (sns.bsm_streetnameid = sn.bsm_streetnameid and sns.seg_cnn = sn.seg_cnn)
  where sns.base_street_name            <> sn.base_street_name
  or coalesce(sns.street_type, '')      <> coalesce(sn.street_type, '')
  or coalesce(sns.post_direction, '')   <> coalesce(sn.post_direction, '')
  or sns.category                       <> sn.category;

  insert into streetnames_work (
      seg_cnn,
      bsm_streetnameid,
      pre_direction,
      base_street_name,
      street_type,
      post_direction,
      full_street_name,
      category,
      create_tms,
      status
  )
  select
      sn.seg_cnn,
      sn.bsm_streetnameid,
      sn.pre_direction,
      sn.base_street_name,
      sn.street_type,
      sn.post_direction,
      sn.full_street_name,
      sn.category,
      now(),
      'DELETE'
  from streetnames sn
  left join streetnames_staging sns on (sns.bsm_streetnameid = sn.bsm_streetnameid and sns.seg_cnn = sn.seg_cnn)
  where sns.bsm_streetnameid is null and sns.seg_cnn is null
  and sn.bsm_streetnameid not in(-1, 0);

  -- select * from streetnames_work

  -- Update the street_segment_id here so the above SQL has one less join and is easier to read.
  update streetnames_work snw
  set street_segment_id = ss.street_segment_id
  from street_segments ss
  where snw.seg_cnn = ss.seg_cnn;

  -- inserts, updates, deletes
  INSERT INTO streetnames (
    seg_cnn,
    bsm_streetnameid,
    base_street_name,
    street_type,
    category,
    street_segment_id,
    pre_direction,
    post_direction,
    create_tms
  )
  select
    seg_cnn,
    bsm_streetnameid,
    base_street_name,
    street_type,
    category,
    street_segment_id,
    pre_direction,
    post_direction,
    now()
  from streetnames_work snw
  where snw.status = 'INSERT';

  update streetnames sn
  set
    seg_cnn             = snw.seg_cnn,
    bsm_streetnameid    = snw.bsm_streetnameid,
    base_street_name    = snw.base_street_name,
    street_type         = snw.street_type,
    category            = snw.category,
    street_segment_id   = snw.street_segment_id,
    pre_direction       = snw.pre_direction,
    post_direction      = snw.post_direction,
    update_tms          = now()
  from streetnames_work snw
  where snw.bsm_streetnameid = sn.bsm_streetnameid
  and snw.seg_cnn = sn.seg_cnn
  and snw.status = 'UPDATE';

  delete 
  from streetnames sn
  where exists (
	select 1
	from streetnames_work snw
	where snw.bsm_streetnameid = sn.bsm_streetnameid
	and snw.seg_cnn = sn.seg_cnn
	and snw.status = 'DELETE'
  );

  update streetnames
  set full_street_name = (
      base_street_name
      ||
      CASE
          WHEN street_type is null THEN ''
          ELSE ' ' || street_type
      END
      ||
      CASE
          WHEN post_direction is null THEN ''
          ELSE ' ' || post_direction
      END
  );

  -- validations
  -- In an effort to make things simple we do the validations last.
  -- We could discuss weather this is the best way. If something is invalid, we have to roll back the transaction.
  -- Our system is not busy enough for us to worry about this.
  -- Because these data are carefully maintained by DPW, I expect invalid data at the rate of 1 day out of 500.
  --
  -- We should have exactly one MAP row per segment.
  -- To test this, we have 2 steps.
  -- Step 1: check for more than one MAP row per segment.
  if (
        (
            select count(*) from (
		    select 1
		    from streetnames
		    where category = 'MAP'
		    group by category, seg_cnn
		    having count(*) > 1
	    ) a
        ) > 0
  ) then
    success = false;
    messageText = 'ETL halted. Multiple MAP rows found in the streetnames table for a single street segment.';
    return;
  end if;
  -- Step 2: check for street segments that have no map row.
  if (
        (
            select count(*)
            from street_segments ss
            where not exists (
                select 1
                from streetnames sn
                where sn.seg_cnn = ss.seg_cnn
                and sn.category = 'MAP'
            )
        ) > 0
  ) then
    success = false;
    success = false;
    messageText = 'ETL halted. One or more street_segments records lacks a streetnames record with category = MAP.';
    return;
  end if;
  ----- END streetnames processing


  perform _eas_correct_etl_street_segment_splits();


  perform _eas_validate_addresses_after_etl('STREETS');


  messageText = 'Streets ETL Successful.';
  success = true;
  return;


END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _sfmad_etl_streets(character) OWNER TO postgres;
