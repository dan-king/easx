﻿
/*
select * 
from street_segments 
where l_f_add is null 
or l_t_add is null
or r_f_add is null
or r_t_add is null;
*/


select 
	ss1.seg_cnn, 
	sn1.base_street_name,
	sn1.street_type, 
	ss1.l_f_add, 
	ss1.l_t_add,
	ss1.r_f_add, 
	ss1.r_t_add
from 
	street_segments ss1,
	streetnames sn1
where ss1.seg_cnn = sn1.seg_cnn
and sn1.category = 'MAP'
and (ss1.l_f_add != 0 and ss1.l_t_add != 0 and ss1.r_f_add != 0 and ss1.r_t_add != 0)
and ss1.date_dropped is null
and exists (
	select 1
	from 
		street_segments ss2,
		streetnames sn2
	where ss2.seg_cnn = sn2.seg_cnn
	and sn2.category = 'MAP'
	and ss2.seg_cnn <> ss1.seg_cnn
	and ss2.date_dropped is null
	and sn1.base_street_name = sn2.base_street_name
	and coalesce(sn1.street_type, '') = coalesce(sn2.street_type, '')
	and (ss2.l_f_add != 0 and ss2.l_t_add != 0 and ss2.r_f_add != 0 and ss2.r_t_add != 0)
	and (
		(
			-- left
			(
				ss2.l_f_add between ss1.l_f_add and ss1.l_t_add
				or
				ss2.l_t_add between ss1.l_f_add and ss1.l_t_add
			)
			or
			(
			
				ss2.l_f_add between ss1.l_f_add and ss1.l_t_add
				or
				ss2.l_t_add between ss1.l_f_add and ss1.l_t_add
			)
		)
		or
		(
			-- right
			(
				ss2.r_f_add between ss1.r_f_add and ss1.r_t_add
				or
				ss2.r_t_add between ss1.r_f_add and ss1.r_t_add
			)
			or
			(
				ss2.r_f_add between ss1.r_f_add and ss1.r_t_add
				or
				ss2.r_t_add between ss1.r_f_add and ss1.r_t_add
			)
		)
	)
)
order by sn1.base_street_name, sn1.street_type, sn1.seg_cnn, ss1.l_f_add, ss1.l_t_add, ss1.r_f_add, ss1.r_t_add;
