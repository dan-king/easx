-- drop procedure calc_string_similarity;
-- JARO_WINKLER and JARO_WINKLER_SIMILARITY cause "SQL Error: Io exception: Software caused connection abort: socket write error" on simple update statements when the row count is > say about 10000.
-- http://forums.oracle.com/forums/thread.jspa?threadID=669739&tstart=405
-- But if you cursor through the table it works fine.
create or replace
PROCEDURE calc_string_similarity IS

    v_unq_adds_id number;

    cursor street_cur is
    select unq_adds_id
    from unq_adds_nn;
    --where addr_streetname_std is not null
    --and streetname_std is not null;

    cursor suffix_cur is
    select unq_adds_id
    from unq_adds_nn;
    --where addr_street_suffix_std is not null
    --and st_addr_suffix_std is not null;

BEGIN

    update unq_adds_nn
    set jws_streetname = 0,
        jws_suffix = 0;

    open street_cur;
        loop

            fetch street_cur into v_unq_adds_id;
            exit when street_cur%NOTFOUND;

            update unq_adds_nn
            set jws_streetname = utl_match.JARO_WINKLER_SIMILARITY(addr_streetname, streetname)
            where unq_adds_id = v_unq_adds_id;

        end loop;
        commit;
    close street_cur;

    open suffix_cur;
        loop
            fetch suffix_cur into v_unq_adds_id;
            exit when suffix_cur%NOTFOUND;

            update unq_adds_nn
            set jws_suffix = utl_match.JARO_WINKLER_SIMILARITY(addr_street_suffix, st_addr_suffix)
            where unq_adds_id = v_unq_adds_id;

        end loop;
        commit;
    close suffix_cur;

END calc_string_similarity;