--these views are needed within Oracle to create objects with data needed for ouput to eas
CREATE OR REPLACE FORCE VIEW "ADDRESS_PROCESSING"."VW_ADDRS_X_PARCELS_OUT" AS
  SELECT a."UNQ_ADDS_ID",
    a."BLKLOT",
    a."SCORE",
    a."VALID_SPATIAL",
    b.subaddr_to,
    b.addr_number,
    b.addr_unit,
    b.addr_streetname,
    b.addr_streetsuffix,
    b.addr_zip_spatial
  FROM addrs_x_parcels a,
    unq_adds b
  WHERE a.unq_adds_id = b.unq_adds_id
  ORDER BY a.unq_adds_id;
  
CREATE OR REPLACE FORCE VIEW VW_AVS_RETIRES_OUT
AS
  SELECT a."UNQ_ADDS_ID",
    a."END_DATE",
    b.subaddr_to,
    b.addr_number,
    b.addr_unit,
    b.addr_streetname,
    b.addr_streetsuffix,
    b.addr_zip_spatial
  FROM avs_retires a,
    unq_adds b
  WHERE a.unq_adds_id = b.unq_adds_id
  ORDER BY a.unq_adds_id;
  
CREATE OR REPLACE FORCE VIEW "ADDRESS_PROCESSING"."VW_AXP_RETIRES_OUT" AS
  SELECT a."UNQ_ADDS_ID",
    a."EAS_ADDRESS_ID",
    a."BLOCKLOT",
    a."END_DATE",
    b.subaddr_to,
    b.addr_number,
    b.addr_unit,
    b.addr_streetname,
    b.addr_streetsuffix,
    b.addr_zip_spatial
  FROM axp_retires a,
    unq_adds b
  WHERE a.unq_adds_id = b.unq_adds_id
  ORDER BY a.unq_adds_id;  