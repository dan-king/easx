
--Defines a view that isolates those unq_adds that need to be looked at in street seg and parcel analysis
--create and populate a column in unq_adds that indicates association to a record in EAS
--relies on a table existing in the db called unq_adds_in_eas created by fme workspace

CREATE OR REPLACE FORCE VIEW "ADDRESS_PROCESSING"."VW_UNQ_ADDS_TO_PROCESS" 
AS
  SELECT *
  FROM unq_adds
  WHERE SRC_DBIAVS2_ID IS NOT NULL
  AND eas_address_id   IS NULL;

--populates ua.cnn_final, and ua.dist_to_seg with results of nearest neighbor street seg query   
--for performance, create a temporary table that stores the values we want with a unq_adds_id
--my best guess is that this approach will take about 210 minutes for the nearest neighbor query to run 



--create a table that holds the info necessary to determine which segment is the right one for each address point
drop table unq_adds_nn;

create table unq_adds_nn (
  nn_id int,
  unq_adds_id int,
  cnn int,
  dist_in_meters decimal(20),
  addr_streetname varchar(100),
  addr_street_suffix varchar(50),
  streetname varchar(155),
  st_addr_suffix varchar(50),
  addr_number int,
  min_addr_range float(126),
  max_addr_range float(126),
  addr_range_dif int,
  exclude_flag varchar2(1 byte),
  pick_flag varchar2(1 byte),
  pick_id varchar2(1 byte),
  --addr_streetname_std varchar2(100 byte),
  --addr_street_suffix_std varchar2(50 byte),
  --streetname_std varchar2(155 byte),
  --st_addr_suffix_std varchar2(50 byte),
  jws_streetname number(38,0),
  jws_suffix number(38,0)
);

----- use this section to accomodate "changing your mind"
-- alter table "ADDRESS_PROCESSING"."UNQ_ADDS_NN" drop column   addr_streetname_std
-- alter table "ADDRESS_PROCESSING"."UNQ_ADDS_NN" drop column   addr_street_suffix_std 
-- alter table "ADDRESS_PROCESSING"."UNQ_ADDS_NN" drop column   streetname_std 
-- alter table "ADDRESS_PROCESSING"."UNQ_ADDS_NN" drop column   st_addr_suffix_std 
-- alter table "ADDRESS_PROCESSING"."UNQ_ADDS_NN" drop column     jws_streetname
-- alter table "ADDRESS_PROCESSING"."UNQ_ADDS_NN" drop column     jws_suffix 

-- alter table UNQ_ADDS_NN add(addr_streetname_std varchar2(100 byte));
-- alter table UNQ_ADDS_NN add(addr_street_suffix_std varchar2(50 byte));
-- alter table UNQ_ADDS_NN add(streetname_std varchar2(155 byte));
-- alter table UNQ_ADDS_NN add(st_addr_suffix_std varchar2(50 byte));
-- alter table UNQ_ADDS_NN add(jws_streetname integer);
-- alter table UNQ_ADDS_NN add(jws_suffix integer);

-- alter table UNQ_ADDS_NN add(exclude_flag varchar2(1 byte));
-- alter table UNQ_ADDS_NN add(pick_flag varchar2(1 byte));
-- alter table UNQ_ADDS_NN add(pick_id varchar2(1 byte));

--this should take about 15 minutes to run.
-- truncate table unq_adds_nn;
insert into unq_adds_nn NOLOGGING (nn_id,unq_adds_id, cnn, dist_in_meters, addr_streetname, addr_street_suffix, streetname,
                          st_addr_suffix,addr_number,min_addr_range,max_addr_range) 

-- select count(*) from vw_unq_adds_to_process                        

SELECT /*+ORDERED USE_NL (t2, TBL_STREETS_ALL_SIDX)*/
    rownum,
    ua.unq_adds_id, 
    t2.cnn,
    sdo_nn_distance(1) as dist_in_meters,
    ua.addr_streetname_std,
    ua.addr_streetsuffix_std,
    t2.streetname,
    t2.streettype,
    ua.addr_number,
    (CASE
      WHEN t2.L_F_ADD >= t2.R_F_ADD THEN t2.R_F_ADD 
      ELSE t2.L_F_ADD
    END) as min_addr_range,
    (CASE
      WHEN t2.L_T_ADD <= t2.R_T_ADD THEN t2.R_T_ADD 
      ELSE t2.L_T_ADD
    END) as max_addr_range
FROM  
    vw_unq_adds_to_process ua,
    TBL_STREETS_ALL t2
WHERE 1=1
    and SDO_NN(t2.geometry, ua.geometry, 'sdo_num_res=10', 1) = 'TRUE'
    -- exclude retired streets
    and t2.date_dropp is null
ORDER BY ua.unq_adds_id, dist_in_meters;
commit;  

create index unq_adds_nn_idx1 on unq_adds_nn (unq_adds_id);
create index unq_adds_nn_idx2 on unq_adds_nn (cnn);
create index unq_adds_nn_idx3 on unq_adds_nn (nn_id);


-- patch the crazy '[NULLS]' construct
update unq_adds_nn set addr_streetname = null where addr_streetname = '[NULL]';
update unq_adds_nn set addr_street_suffix = null where addr_street_suffix = '[NULL]';
update unq_adds_nn set streetname = null where streetname = '[NULL]';
update unq_adds_nn set st_addr_suffix = null where st_addr_suffix = '[NULL]';
commit;

----- BEGIN - calc fields

-- calculate string similarity for street name and street suffix
-- JARO_WINKLER and JARO_WINKLER_SIMILARITY cause "SQL Error: Io exception: Software caused connection abort: socket write error" when you try to run an update staement for a table with > 10000 rows.
-- See http://forums.oracle.com/forums/thread.jspa?threadID=669739&tstart=405.
-- But if you cursor through the table it works fine - yipee!.
-- For simplicity we include all string similarity calcs in this proc.
-- This has been taking about 5 minutes to run.
call calc_string_similarity();
commit;

-- address range

-- these records fall within range.  Difference between range and housenumber is zero.
update unq_adds_nn
set addr_range_dif = null;
commit;

update unq_adds_nn
set addr_range_dif = 0
where addr_number between min_addr_range and max_addr_range;
commit;

--these records fall below the range. Output calculates difference.
update unq_adds_nn
set addr_range_dif = min_addr_range - addr_number
where addr_number < min_addr_range
and addr_number is not null;
commit;

--these addresses fall above the address range.  Output calculates difference.
update unq_adds_nn
set addr_range_dif = addr_number - max_addr_range
where addr_number > max_addr_range
and addr_number is not null;
commit;

----- END - calc fields



call select_street_segment();

create index unq_adds_nn_idx4 on unq_adds_nn (pick_flag);

-- join the values we want into the unq_adds table
-- select count(*) from unq_adds ua where cnn_final is null;
-- select count(*) from unq_adds ua where cnn_final is not null;

update unq_adds ua
set cnn_final =
  (SELECT uan.cnn
   FROM  unq_adds_nn uan
   WHERE ua.unq_adds_id = uan.unq_adds_id
   and uan.pick_flag = 'Y')
WHERE src_dbiavs2_id is not null
and ua.cnn_final is null;
commit;
