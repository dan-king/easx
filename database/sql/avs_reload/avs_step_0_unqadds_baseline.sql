--Rolls back content of unq_adds to whatever was there during beta implementation

--DROP TABLE UNQ_ADDS_BAK;
--CREATE TABLE UNQ_ADDS_BAK as
--Select * from unq_adds;

DROP TABLE UNQ_ADDS;

CREATE TABLE "ADDRESS_PROCESSING"."UNQ_ADDS"
  (
    "ADDR_NUMBER"       VARCHAR2(20 BYTE),
    "ADDR_UNIT"         VARCHAR2(10 BYTE),
    "ADDR_STREETNAME"   VARCHAR2(100 BYTE),
    "ADDR_STREETSUFFIX" VARCHAR2(14 BYTE),
    "ADDR_ZIP_SPATIAL"  VARCHAR2(14 BYTE),
    "ADDR_ZIP_ORIG"     VARCHAR2(5 BYTE),
    "ADDR_CONCATENATE"  VARCHAR2(255 BYTE),
    "SRC_SFADDRS_ID"    NUMBER(10,0),
    "SRC_DWG_ID"        NUMBER(10,0),
    "SRC_DBIAVS_ID"     NUMBER(10,0),
    "SRC_DPW_ID"        NUMBER(10,0),
    "SRC_PLANNING_ID"   NUMBER(10,0),
    "SUBADDR_TO"        VARCHAR2(38 BYTE),
    "GEOMETRY" "MDSYS"."SDO_GEOMETRY" ,
    "ZONE_ID"          NUMBER,
    "XCOORD"           VARCHAR2(25 BYTE),
    "YCOORD"           VARCHAR2(25 BYTE),
    "SRC_DBIAVS2_ID"   NUMBER(10,0),
    "UNQ_ADDS_ID"      NUMBER(38,0),
    "EAS_ADDRESS_ID"   NUMBER(38,0),
    "CNN_FINAL"        VARCHAR2(20 BYTE),
    "ADDR_STREETNAME_STD"	VARCHAR2(100 BYTE),
    "ADDR_STREETSUFFIX_STD"	VARCHAR2(14 BYTE)
    );
alter table "ADDRESS_PROCESSING"."UNQ_ADDS" add constraint UNQ_ADDS_PK primary key("UNQ_ADDS_ID") ;


insert into unq_adds(		
	ADDR_NUMBER	,
	ADDR_UNIT	,
	ADDR_STREETNAME	,
	ADDR_STREETSUFFIX	,
	ADDR_ZIP_SPATIAL	,
	ADDR_ZIP_ORIG	,
	ADDR_CONCATENATE	,
	SRC_SFADDRS_ID	,
	SRC_DWG_ID	,
	SRC_DBIAVS_ID	,
	SRC_DPW_ID	,
	SRC_PLANNING_ID	,
	SUBADDR_TO	,
	GEOMETRY	,
	ZONE_ID	,
	XCOORD	,
	YCOORD	,
	SRC_DBIAVS2_ID	,
	UNQ_ADDS_ID	,
	CNN_FINAL	)

select		
	ADDR_NUMBER	,
	ADDR_UNIT	,
	ADDR_STREETNAME	,
	ADDR_STREETSUFFIX	,
	ADDR_ZIP_SPATIAL	,
	ADDR_ZIP_ORIG	,
	ADDR_CONCATENATE	,
	SRC_SFADDRS_ID	,
	SRC_DWG_ID	,
	SRC_DBIAVS_ID	,
	SRC_DPW_ID	,
	SRC_PLANNING_ID	,
	SUBADDR_TO	,
	GEOMETRY	,
	ZONE_ID	,
	XCOORD	,
	YCOORD	,
	SRC_DBIAVS2_ID	,
	UNQ_ADDS_ID	,
	SWS_SEG_CNN	
	from unq_adds_orig;
commit;

--clean up some garbage from etl zero
--select * from unq_adds where cnn_final < 0;
update unq_adds
set cnn_final = null
where cnn_final < 0;
commit;

update unq_adds
set addr_streetname_std = addr_streetname,
    addr_streetsuffix_std = addr_streetsuffix;
commit;

drop INDEX unq_adds_idx_101;
drop INDEX unq_adds_idx_102;
drop INDEX unq_adds_idx_103;
drop INDEX unq_adds_idx_104;
drop INDEX unq_adds_idx_105;
drop index unq_adds_idx_106;

CREATE INDEX unq_adds_idx_101 ON UNQ_ADDS(ADDR_NUMBER);
CREATE INDEX unq_adds_idx_102 ON UNQ_ADDS(addr_streetname_std);
CREATE INDEX unq_adds_idx_103 ON UNQ_ADDS(addr_streetsuffix_std);
CREATE INDEX unq_adds_idx_104 ON UNQ_ADDS(ADDR_UNIT);
CREATE INDEX unq_adds_idx_105 ON UNQ_ADDS(ADDR_ZIP_SPATIAL);
create index unq_adds_idx_106 on unq_adds(eas_address_id);


----- BEGIN - standardize street and suffix values

-- when streetname ends in ' ST', remove it from streetname and put 'STREET' into the suffix
-- select * from unq_adds where trim(addr_streetname_std) like '% ST';
update unq_adds 
set addr_streetname_std = rtrim(addr_streetname_std, ' ST'),
    addr_streetsuffix = 'STREET'
where trim(addr_streetname_std) like '% ST';
commit;

-- when streetname ends in ' CR', remove it from streetname and put 'CIRCLE' into the suffix
-- select * from unq_adds where trim(addr_streetname_std) like '% CR';
update unq_adds 
set addr_streetname_std = rtrim(addr_streetname_std, ' CR'),
    addr_streetsuffix_std = 'CIRCLE'
where trim(addr_streetname_std) like '% CR';
commit;

-- when streetname ends in ' PLACE', remove it from streetname and put 'PLACE' into the suffix
-- select * from unq_adds where trim(avs_street_name) like '% PLACE';
update unq_adds
set addr_streetname_std = rtrim(addr_streetname_std, ' PLACE'),
    addr_streetsuffix_std = 'PLACE'
where trim(addr_streetname_std) like '% PLACE';
commit;

-- when streetname ends in ' AVE', remove it from streetname and put 'AVENUE' into the suffix
-- select * from unq_adds where trim(addr_streetname_std) like '% AVE';
update unq_adds
set addr_streetname_std = rtrim(addr_streetname_std, ' AVE'),
    addr_streetsuffix_std = 'AVENUE'
where trim(addr_streetname_std) like '% AVE';
commit;

-- remove periods
-- select * from unq_adds where instr(addr_streetname_std, '.') > 0;
update unq_adds
set addr_streetname_std = replace(addr_streetname_std, '.')
where instr(addr_streetname_std, '.') > 0;
commit;

-- remove dashes
-- select * from unq_adds where instr(addr_streetname_std, '.') > 0;
update unq_adds
set addr_streetname_std = replace(addr_streetname_std, '-')
where addr_streetname_std like '%-%';
commit;

-- remove apostrophes
-- select * from unq_adds where addr_streetname_std like '%''%';
update unq_adds
set addr_streetname_std = replace(addr_streetname_std, '''')
where addr_streetname_std like '%''%';
commit;

-- strip leading zeros
-- select COUNT(*) from unq_adds where trim(addr_streetname_std) like '0%';
update unq_adds
set addr_streetname_std = ltrim(addr_streetname_std, '0')
where addr_streetname_std like '0%';
commit;

----- remove T.I. and TI from avs_street_name
-- select count(*) from unq_adds where addr_streetname_std like '%(T.I.)';
update unq_adds
set addr_streetname_std = rtrim(addr_streetname_std, '- TI')
where addr_streetname_std like '% - TI';
commit;

update unq_adds
set addr_streetname_std = rtrim(addr_streetname_std, '(T.I.)')
where addr_streetname_std like '%(T.I.)';
commit;

update unq_adds
set addr_streetname_std = trim(both ' ' from addr_streetname_std)
where addr_streetname_std like ' %'
or addr_streetname_std like '% ';
commit;

----- multiple consecutive whitespaces
-- select count(*) from unq_adds where addr_streetname_std like '%  %';
update unq_adds
set addr_streetname_std = REGEXP_REPLACE(addr_streetname_std, '( ){2,}', ' ');
commit;

update unq_adds
set ADDR_STREETSUFFIX_STD = REGEXP_REPLACE(ADDR_STREETSUFFIX_STD, '( ){2,}', ' ');
commit;

-- should be zero rows
--select count(*) 
--from unq_adds
--where 
--(instr(addr_streetname_std, '.') > 0
--or addr_streetname_std like '%-%'
--or addr_streetname_std like '%''%'
--or addr_streetname_std like '% - TI'
--or addr_streetname_std like '%(T.I.)'
--or addr_streetname_std like '0%')
--or addr_streetname_std like '%  %';

----- END - standardize street and suffix values



---- n=571149
--select count(*) from unq_adds;

---- n=571028
--select count(*) from ( select
--  ADDR_NUMBER,
--  --ADDR_STREETNAME,
--  ADDR_STREETNAME_STD,
--  --ADDR_STREETSUFFIX,
--  ADDR_STREETSUFFIX_STD,
--  ADDR_UNIT,
--  ADDR_ZIP_SPATIAL
--from unq_adds
--group by ADDR_NUMBER,
--  --ADDR_STREETNAME,
--  ADDR_STREETNAME_STD,
--  --ADDR_STREETSUFFIX,
--  ADDR_STREETSUFFIX_STD,
--  ADDR_UNIT,
--  ADDR_ZIP_SPATIAL );


------- BEGIN - prove that we have "duplicates"
--
---- drop table unq_adds_temp;
--
---- This table will have rows if we have duplicates.
--create table unq_adds_temp as
--select ADDR_NUMBER, ADDR_STREETNAME_STD, ADDR_STREETSUFFIX_STD, ADDR_UNIT, ADDR_ZIP_SPATIAL
--from unq_adds
--GROUP BY ADDR_NUMBER, ADDR_STREETNAME_STD, ADDR_STREETSUFFIX_STD, ADDR_UNIT, ADDR_ZIP_SPATIAL
--having count(*) > 1
--order by ADDR_STREETNAME_STD, ADDR_STREETSUFFIX_STD, ADDR_NUMBER, ADDR_UNIT, ADDR_ZIP_SPATIAL;
--
--
--select * from unq_adds_temp;
--
---- see all of the orignal 242 rows
--select ua1.*
--from
--  unq_adds ua1,
--  unq_adds_temp ua2
--where ua1.ADDR_NUMBER = ua2.ADDR_NUMBER
--and ua1.ADDR_STREETNAME_STD = ua2.ADDR_STREETNAME_STD
--and ua1.ADDR_STREETSUFFIX_STD = ua2.ADDR_STREETSUFFIX_STD
--and ua1.ADDR_UNIT = ua2.ADDR_UNIT
--and ua1.ADDR_ZIP_SPATIAL = ua2.ADDR_ZIP_SPATIAL
--order by ua1.ADDR_STREETNAME_STD, ua1.ADDR_STREETSUFFIX_STD, ua1.ADDR_NUMBER, ua1.ADDR_UNIT, ua1.ADDR_ZIP_SPATIAL;
------- END - prove that we have "duplicates"



----- Delete the "duplicates".
-- About here is where Adam would like to preserve the SRC fields.
-- Currently we just toss them out.
delete unq_adds
where unq_adds_id in (
  select min(unq_adds_id)
  from unq_adds
  GROUP BY ADDR_NUMBER, ADDR_STREETNAME_STD, ADDR_STREETSUFFIX_STD, ADDR_UNIT, ADDR_ZIP_SPATIAL
  having count(*) > 1
);


CREATE INDEX "ADDRESS_PROCESSING"."UNQ_ADDS_SIDX" ON "ADDRESS_PROCESSING"."UNQ_ADDS" ("GEOMETRY")
INDEXTYPE IS "MDSYS"."SPATIAL_INDEX";

drop INDEX UNQ_ADDS_UIDX;
CREATE UNIQUE INDEX UNQ_ADDS_UIDX ON UNQ_ADDS
(ADDR_NUMBER, ADDR_UNIT, ADDR_ZIP_SPATIAL, ADDR_STREETNAME_STD, ADDR_STREETSUFFIX_STD);


----- BEGIN populate eas_address_id
drop index unq_adds_in_eas_idx1;
create index unq_adds_in_eas_idx1 on unq_adds_in_eas(unq_adds_id);

update unq_adds a
set eas_address_id = (
    select address_id
    from unq_adds_in_eas b
    where a.unq_adds_id=b.unq_adds_id
);
commit;
----- END populate eas_address_id

