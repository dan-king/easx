--identifies addr to parcel relationships that are valid both in avs and in spatial query
drop index parcels_poly_blklot_idx;
create index parcels_poly_blklot_idx on parcels_poly(blk_lot);
drop index unq_adds_sidx;
create index unq_adds_sidx on unq_adds(geometry) indextype is mdsys.spatial_index;

--create a table with all of the spatially related unq_adds to blkots for those unq_adds that dont exist in eas
drop table ua_x_parcels_spatial;
create table ua_x_parcels_spatial (unq_adds_id int, blklot varchar(10));

--this spatial intersection query  should take 16.5 minutes
insert into ua_x_parcels_spatial (unq_adds_id , blklot)
select 
    a.unq_adds_id,
    b.blk_lot
from
    vw_unq_adds_to_process a,
    parcels_poly b
where 1=1
AND sdo_anyinteract(b.geometry, a.geometry)='TRUE';
commit;


create index ua_x_parcels_spatial_idx1 on ua_x_parcels_spatial(unq_adds_id);
create index ua_x_parcels_spatial_idx2 on ua_x_parcels_spatial(blklot);


--creates a select set of addr_x_parcel relationships with scores and indication of a spatial match
--filters out records with a score less than 30, and that dont touch respective parcel
DROP TABLE ADDRS_X_PARCELS;

CREATE TABLE ADDRS_X_PARCELS (
    unq_adds_id int,
    blklot varchar(10),
    score int,
    valid_spatial varchar(1),
    exception_text varchar(128)
);
insert into ADDRS_X_PARCELS (unq_adds_id , blklot , score , valid_spatial)
SELECT UNQ_ADDS_ID, BLKLOT, SCORE, SPATIAL
FROM (
    SELECT
        b."UNQ_ADDS_ID",
        b."BLKLOT",
        sum(c.points) as score,
        max(valid_spatial) as spatial
    FROM
        vw_unq_adds_to_process a,
        (
            select unq_adds_id, blklot, sourcename, '0' as valid_spatial from ua_src_link
            union
            select unq_adds_id, blklot, 'spatial' as sourcename, '1' as valid_spatial from ua_x_parcels_spatial
         ) b,
        sources c
    WHERE 1=1
    AND a.unq_adds_id = b.unq_adds_id
    AND b.sourcename = c.sourcename
    group by b."UNQ_ADDS_ID", b."BLKLOT")
WHERE 1=1
AND SCORE >= 30
AND SPATIAL <> 0;
COMMIT;
 
--delete records where the blklot is retired. 
DELETE FROM ADDRS_X_PARCELS
WHERE 1=1
AND blklot not in (
    select blk_lot from parcels_pt
    where DATE_MAP_DROP is null
);
commit;                     
