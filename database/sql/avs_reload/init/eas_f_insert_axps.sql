-- Function: avs_reload.insert_axps()

DROP FUNCTION IF EXISTS avs_reload.insert_axps(
    _unq_adds_id int,
    _address_id int,
    _current_tms timestamp without time zone,
    _ACTION character(6),                       -- INSERT or EXCEPT
    _MESSAGE character varying(128)
);

CREATE OR REPLACE FUNCTION avs_reload.insert_axps(
    _unq_adds_id int,
    _address_id int,
    _current_tms timestamp without time zone,
    _ACTION character(6),                       -- INSERT or EXCEPT
    _MESSAGE character varying(128)
)
RETURNS void AS
$BODY$
DECLARE 

    _block_lot character varying(10);

BEGIN

    if _ACTION not in ('INSERT', 'EXCEPT') then
        raise exception 'programming error - invalid domain for _ACTION: %', _ACTION;
    end if;

    if _ACTION = 'EXCEPT' then
        update avs_reload.axp_inserts set exception_text = _MESSAGE where unq_adds_id = _unq_adds_id;
        return;
    end if;

    declare cursorAddressXParcel cursor for
        select blklot
        from avs_reload.axp_inserts
        where unq_adds_id::int = _unq_adds_id
        and exception_text is null
        and success_text is null;
    begin
        open cursorAddressXParcel;
        loop
            fetch cursorAddressXParcel into _block_lot;
            if not found then
                exit;
            end if;

            perform avs_reload.insert_axp(_unq_adds_id, _block_lot, _address_id, _current_tms, _ACTION, _MESSAGE);

        end loop;
        close cursorAddressXParcel;
    end;

    return;


END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs_reload.insert_axps(
    _unq_adds_id int,
    _address_id int,
    _current_tms timestamp without time zone,
    _ACTION character(6),                       -- INSERT or EXCEPT
    _MESSAGE character varying(128)
)
OWNER TO postgres;

