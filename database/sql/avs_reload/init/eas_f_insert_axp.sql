-- Function: avs_reload.insert_axp()

DROP FUNCTION IF EXISTS avs_reload.insert_axp(
    _unq_adds_id int,
    _block_lot character varying(10),
    _address_id int,
    _current_tms timestamp without time zone,
    _ACTION character(6),                       -- INSERT or EXCEPT
    _MESSAGE character varying(128)
);

CREATE OR REPLACE FUNCTION avs_reload.insert_axp(
    _unq_adds_id int,
    _block_lot character varying(10),
    _address_id int,
    _current_tms timestamp without time zone,
    _ACTION character(6),                       -- INSERT or EXCEPT
    _MESSAGE character varying(128)
)
RETURNS void AS
$BODY$
DECLARE 

    _parcel_id int = null;
    _parcel_retire_tms timestamp without time zone = null;
    _address_retire_tms timestamp without time zone = null;
    _address_x_parcel_id integer = null;

BEGIN

    if _ACTION not in ('INSERT', 'EXCEPT') then
        raise exception 'programming error - invalid domain for _ACTION: %', _ACTION;
    end if;

    if _ACTION = 'EXCEPT' then
        update avs_reload.axp_inserts set exception_text = _MESSAGE where unq_adds_id = _unq_adds_id and blklot = _block_lot;
        return;
    end if;

    BEGIN
        
        
        select parcel_id, date_map_drop::timestamp without time zone into _parcel_id, _parcel_retire_tms from parcels where blk_lot = _block_lot;

        if _parcel_id is null then
            update avs_reload.axp_inserts set exception_text = 'unable to link address with parcel because there is no matching parcel row' where unq_adds_id = _unq_adds_id and blklot = _block_lot;
            return;
        end if;

        if _parcel_retire_tms = null then
            update avs_reload.axp_inserts set exception_text = 'unable to link address with parcel because parcel is retired' where unq_adds_id = _unq_adds_id and blklot = _block_lot;
            return;
        end if;

        select into _address_retire_tms retire_tms from addresses where address_id = _address_id;

        if _address_retire_tms is not null then
            update avs_reload.axp_inserts set exception_text = 'unable to link address with parcel because address is retired' where unq_adds_id = _unq_adds_id and blklot = _block_lot;
            return;
        end if;


        select id into _address_x_parcel_id from address_x_parcels where address_id = _address_id and parcel_id = _parcel_id and retire_tms is null;
        
        if _address_x_parcel_id is null then
            insert into address_x_parcels ( address_id, parcel_id, activate_change_request_id, create_tms, last_change_tms )
            values ( _address_id, _parcel_id, 0, _current_tms, _current_tms );

            update avs_reload.axp_inserts set success_text = 'inserted address parcel link' where unq_adds_id = _unq_adds_id and blklot = _block_lot;
            return;
        else
            update avs_reload.axp_inserts set success_text = 'existing address parcel link' where unq_adds_id = _unq_adds_id and blklot = _block_lot;
            return;
        end if;

        update avs_reload.axp_inserts set success_text = 'programming error' where unq_adds_id = _unq_adds_id and blklot = _block_lot;


    EXCEPTION
        when others then
            update avs_reload.axp_inserts set exception_text = SQLERRM where unq_adds_id = _unq_adds_id and blklot = _block_lot;
    END;

    return;


END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs_reload.insert_axp(
    _unq_adds_id int,
    _block_lot character varying(10),
    _address_id int,
    _current_tms timestamp without time zone,
    _ACTION character(6),                       -- INSERT or EXCEPT
    _MESSAGE character varying(128)
)
OWNER TO postgres;

