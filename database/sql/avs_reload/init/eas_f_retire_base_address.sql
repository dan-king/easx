-- Function: avs_reload.retire_base_address()

DROP FUNCTION IF EXISTS avs_reload.retire_base_address(
    _address_base_id    int,
    _current_tms        timestamp without time zone
);

CREATE OR REPLACE FUNCTION avs_reload.retire_base_address(
    _address_base_id    int,
    _current_tms        timestamp without time zone
)
RETURNS void AS
$BODY$
DECLARE 
    _address_id int;
BEGIN

    -- Retire the member unit addresses first because of foreign key constraint.
    perform avs_reload.retire_unit_addresses(_address_base_id, _current_tms);

    -- Retire the AXPs for the base address next because of foreign key constraint.
    select address_id into _address_id from addresses where address_base_id = _address_base_id and address_base_flg = true;
    perform avs_reload.retire_axps(_address_id, _current_tms);

    -- Retire the base address itself.
    update address_base
    set retire_tms = _current_tms,
        last_change_tms = _current_tms
    where address_base_id = _address_base_id;


END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs_reload.retire_base_address(
    _address_base_id    int,
    _current_tms        timestamp without time zone
)
OWNER TO postgres;
