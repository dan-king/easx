
CREATE SCHEMA avs_reload;


-- ALTER TABLE avs_reload.addr_inserts ALTER exception_text TYPE character varying(256);
-- ALTER TABLE avs_reload.addr_inserts ALTER success_text TYPE character varying(256);
-- ALTER TABLE avs_reload.addr_retires ALTER exception_text TYPE character varying(256);
-- ALTER TABLE avs_reload.addr_retires ALTER success_text TYPE character varying(256);
-- ALTER TABLE avs_reload.axp_inserts ALTER exception_text TYPE character varying(256);
-- ALTER TABLE avs_reload.axp_inserts ALTER success_text TYPE character varying(256);
-- ALTER TABLE avs_reload.axp_retires ALTER exception_text TYPE character varying(256);
-- ALTER TABLE avs_reload.axp_retires ALTER success_text TYPE character varying(256);


DROP VIEW IF EXISTS avs_reload.vw_load_summary;
DROP VIEW IF EXISTS avs_reload.vw_load_report;
DROP VIEW IF EXISTS avs_reload.vw_eas_addresses;


DROP TABLE IF EXISTS avs_reload.addr_inserts;
DROP TABLE IF EXISTS avs_reload.addr_retires;
DROP TABLE IF EXISTS avs_reload.axp_inserts;
DROP TABLE IF EXISTS avs_reload.axp_retires;

-----
CREATE TABLE avs_reload.addr_inserts
(
  addr_number character varying(20),
  addr_unit character varying(10),
  addr_streetname character varying(100),
  addr_streetsuffix character varying(14),
  addr_zip_spatial character varying(14),
  addr_zip_orig character varying(5),
  addr_concatenate character varying(255),
  addr_base_flg boolean,
  src_sfaddrs_id double precision,
  src_dwg_id double precision,
  src_dbiavs_id double precision,
  src_dpw_id double precision,
  src_planning_id double precision,
  subaddr_to character varying(38),
  zone_id double precision,
  xcoord character varying(25),
  ycoord character varying(25),
  src_dbiavs2_id double precision,
  unq_adds_id double precision,
  eas_address_id double precision,
  cnn_final character varying(20),
  addr_streetname_std character varying(100),
  addr_streetsuffix_std character varying(14),
  cnn double precision,
  dist_in_meters double precision,
  addr_range_dif double precision,
  jws_streetname double precision,
  jws_suffix double precision,
  pick_id character varying(1),
  geometry geometry,
  exception_text character varying(256),
  success_text character varying(256)
  CONSTRAINT enforce_dims_geometry CHECK (ndims(geometry) = 2),
  CONSTRAINT enforce_geotype_geometry CHECK (geometrytype(geometry) = 'POINT'::text OR geometry IS NULL),
  CONSTRAINT enforce_srid_geometry CHECK (srid(geometry) = 2227)
)
WITH (OIDS=TRUE);
ALTER TABLE avs_reload.addr_inserts OWNER TO postgres;

CREATE INDEX addr_inserts_idx_unq_adds_id
   ON avs_reload.addr_inserts USING btree (unq_adds_id);

CREATE INDEX addr_inserts_idx_subaddr_to
   ON avs_reload.addr_inserts USING btree (subaddr_to);

delete 
from geometry_columns
where f_table_schema = 'avs_reload'
and f_table_name = 'addr_inserts';

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'avs_reload', 'addr_inserts', 'geometry', 2, 2227, 'POINT'
where not exists (
    select 1
    from geometry_columns
    where f_table_schema = 'avs_reload'
    and f_table_name = 'addr_inserts'
);
-----




-----
CREATE TABLE avs_reload.addr_retires
(
  unq_adds_id double precision,
  end_date character varying(255),
  subaddr_to double precision,
  addr_number character varying(20),
  addr_unit character varying(10),
  addr_streetname character varying(100),
  addr_streetsuffix character varying(14),
  addr_zip_spatial character varying(14),
  exception_text character varying(256),
  success_text character varying(256),
  cascade_retire boolean,
  addr_base_flg boolean
)
WITH (OIDS=FALSE);
ALTER TABLE avs_reload.addr_retires OWNER TO postgres;
-----




-----
CREATE TABLE avs_reload.axp_inserts
(
  unq_adds_id double precision,
  blklot character varying(10),
  score double precision,
  valid_spatial character varying(1),
  subaddr_to double precision,
  addr_number character varying(20),
  addr_unit character varying(10),
  addr_streetname character varying(100),
  addr_streetsuffix character varying(14),
  addr_zip_spatial character varying(14),
  exception_text character varying(256),
  success_text character varying(256)
)
WITH (OIDS=FALSE);
ALTER TABLE avs_reload.axp_inserts OWNER TO postgres;

-- DROP INDEX avs_reload.axp_inserts_idx_unq_adds_id;

CREATE INDEX axp_inserts_idx_unq_adds_id
  ON avs_reload.axp_inserts
  USING btree
  (unq_adds_id);
-----




-----
CREATE TABLE avs_reload.axp_retires
(
  unq_adds_id double precision,
  subaddr_to double precision,
  eas_address_id double precision,
  blocklot character varying(255),
  end_date character varying(255),
  addr_number character varying(20),
  addr_unit character varying(10),
  addr_streetname character varying(100),
  addr_streetsuffix character varying(14),
  addr_zip_spatial character varying(14),
  exception_text character varying(256),
  cascade_retire boolean,
  success_text character varying(256),
  addr_base_flg boolean
)
WITH (OIDS=FALSE);
ALTER TABLE avs_reload.axp_retires OWNER TO postgres;
-----




----- set up reporting objects
-- drop view avs_reload.vw_eas_addresses;
CREATE OR REPLACE VIEW avs_reload.vw_eas_addresses AS
    SELECT
        b.unq_adds_id,
        b.address_base_flg,
        b.address_id,
        a.geometry,
        a.address_base_id,
        a.base_address_prefix AS base_prefix,
        a.base_address_num,
        a.base_address_suffix AS base_suffix,
        b.unit_num,
        c.base_street_name,
        c.street_type,
        d.zipcode,
        d.jurisdiction
    FROM
        address_base a,
        addresses b,
        streetnames c,
        zones d
    WHERE a.address_base_id = b.address_base_id
    AND a.street_segment_id = c.street_segment_id
    AND a.zone_id = d.zone_id
    AND c.category = 'MAP';

ALTER TABLE avs_reload.vw_eas_addresses OWNER TO postgres;




-----
-- DROP VIEW avs_reload.vw_load_report;
-- This could be refactored; it will be hard to maintain.
-- But it might be short lived so I am leaving as is for the time being.
-- The idea with the sort order is that you can find any given address easily.
-- So '100 main' should be easy to find in this report.
-- After that you want the user to be able to discern what happened with this address.
-- And so...
--    insert precedes retire
--    base precedes unit
--    unit precedes axp
CREATE OR REPLACE VIEW avs_reload.vw_load_report as

-- base and unit inserts
SELECT
    ai.addr_number::int,
    ai.addr_unit,
    ai.addr_streetname,
    ai.addr_streetsuffix,
    ai.addr_zip_spatial,
    case
        when ai.addr_base_flg = true then 'BASE'
        else 'UNIT'
    end::char(4) as type,
    'INSERT'::char(6) as action,
    null as blocklot,
    ai.exception_text,
    ai.success_text,
    ai.unq_adds_id::int,
    ai.subaddr_to::int,
    ai.cnn_final::int,
    ai.cnn::int,
    null::boolean as cascade_retire,
    case
        when ai.addr_base_flg = true then 1
        else 2
    end::int as sort_order_addr_type,
    case
        when ai.addr_unit = '-1' then '0'
        else ai.addr_unit
    end::varchar(10) as sort_order_unit,
    0 as sort_order_action
FROM avs_reload.addr_inserts ai


UNION 


-- axp inserts
SELECT
    axpi.addr_number::int,
    axpi.addr_unit,
    axpi.addr_streetname,
    axpi.addr_streetsuffix,
    axpi.addr_zip_spatial,
    'AXP'::char(4) as type,
    'INSERT'::char(6) as action,
    axpi.blklot as blocklot,
    axpi.exception_text,
    axpi.success_text,
    axpi.unq_adds_id::int,
    axpi.subaddr_to::int,
    null::int as cnn_final,
    null::int as cnn,
    null::boolean as cascade_retire,
    3 as sort_order_addr_type,
    case
        when axpi.addr_unit = '-1' then '0'
        else axpi.addr_unit
    end::varchar(10) as sort_order_unit,
    0 as sort_order_action
FROM avs_reload.axp_inserts axpi


UNION


-- addr retires
SELECT
    addr_number::int as addr_number,
    addr_unit as addr_unit,
    addr_streetname as addr_streetname,
    addr_streetsuffix as addr_street_suffix,
    addr_zip_spatial as addr_zip_spatial,
    case
        when addr_base_flg = true then 'BASE'
        else 'UNIT'
    end::char(4) as type,
    'RETIRE'::char(6) as action,
    null::varchar(10) as blocklot,
    exception_text,
    success_text,
    unq_adds_id::int,
    subaddr_to::int,
    null::int as cnn_final,
    null::int as cnn,
    cascade_retire,
    case
        when addr_base_flg = true then 1
        else 2
    end::int as sort_order_addr_type,
    case
        when addr_unit = '-1' then '0'
        else addr_unit
    end::varchar(10) as sort_order_unit,
    1 as sort_order_action
FROM avs_reload.addr_retires ar


UNION


-- axp retires
SELECT
    axpr.addr_number::int,
    axpr.addr_unit,
    axpr.addr_streetname,
    axpr.addr_streetsuffix,
    axpr.addr_zip_spatial,
    'AXP'::char(4) as addr_type,
    'RETIRE'::char(6) as action,
    axpr.blocklot,
    axpr.exception_text,
    axpr.success_text,
    axpr.unq_adds_id::int,
    axpr.subaddr_to::int,
    null::int as cnn_final,
    null::int as cnn,
    axpr.cascade_retire,
    3 as sort_order_addr_type,
    case
        when axpr.addr_unit = '-1' then '0'
        else axpr.addr_unit
    end::varchar(10) as sort_order_unit,
    1 as sort_order_action
FROM avs_reload.axp_retires axpr

ORDER BY
    addr_number::int,
    addr_streetname,
    sort_order_unit,
    sort_order_action,
    sort_order_addr_type,
    unq_adds_id::int,
    addr_streetsuffix;


ALTER TABLE avs_reload.vw_load_report OWNER TO postgres;
-----


-----
-- drop view avs_reload.vw_load_summary;
-- select * from avs_reload.vw_load_summary;
CREATE OR REPLACE VIEW avs_reload.vw_load_summary AS
    select 
        type,
        action,
        success_text, 
        substring(exception_text from 1 for 95) as exception_text,
        count(*),
        case
            when exception_text is null then 0
            else 1
        end::int as sort_order_success,
        case
            when type = 'BASE' then 0
            when type = 'UNIT' then 1
            when type = 'AXP' then 2
            else 9
        end::int as sort_order_type,
        case
            when action = 'INSERT' then 0
            when action = 'RETIRE' then 1
            else 9
        end::int as sort_order_action
    from avs_reload.vw_load_report
    group by     
        type,
        action,
        success_text, 
        substring(exception_text from 1 for 95),
        sort_order_success,
        sort_order_type,
        sort_order_action
    order by         
        sort_order_success,
        --sort_order_action,
        --sort_order_type,
        count(*) desc;
ALTER TABLE avs_reload.vw_load_summary OWNER TO postgres;
-----
