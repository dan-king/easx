-- Function: avs_reload.insert_unit_address()

DROP FUNCTION IF EXISTS avs_reload.insert_unit_address(
    _unq_adds_id int,
    _addr_unit character varying(10),
    _address_base_id int,
    _current_tms timestamp without time zone,
    _ACTION character(6),                       -- INSERT or EXCEPT
    _MESSAGE character varying(256)
);

CREATE OR REPLACE FUNCTION avs_reload.insert_unit_address(
    _unq_adds_id int,
    _addr_unit character varying(10),
    _address_base_id int,
    _current_tms timestamp without time zone,
    _ACTION character(6),                       -- INSERT or EXCEPT
    _MESSAGE character varying(256)
)
RETURNS void AS
$BODY$
DECLARE 
    _unit_address_ids integer[];
    _unit_address_id integer;
BEGIN


    if _ACTION not in ('INSERT', 'EXCEPT') then
        raise exception 'programming error - invalid domain for _ACTION: %', _ACTION;
    end if;

    if _ACTION = 'EXCEPT' then
        update avs_reload.addr_inserts set exception_text = _MESSAGE where unq_adds_id = _unq_adds_id;
        perform avs_reload.insert_axps(_unq_adds_id, null, _current_tms, 'EXCEPT', 'parent row insert failed');
        return;
    end if;

    BEGIN
        select into _unit_address_ids public._eas_find_equiv_unit_address(_address_base_id::int, _addr_unit::text);

        if array_upper(_unit_address_ids, 1) is null then
            -- no equivalent records found - the nominal path
            select into _unit_address_id nextval('addresses_address_id_seq');
            -- domain values; unit type 0: other; floor 105: 'unknown'; disposition code 1: official
            insert into addresses ( address_id, address_base_id, unit_num, unit_type_id, floor_id, disposition_code, address_base_flg, unq_adds_id, activate_change_request_id, create_tms, last_change_tms)
            values ( _unit_address_id, _address_base_id, _addr_unit, 0, 105, 1, false, _unq_adds_id, 0, _current_tms, _current_tms );

            update avs_reload.addr_inserts set success_text = 'inserted unit address' where unq_adds_id = _unq_adds_id;

        else
            -- equivalent record(s) found
            if array_upper(_unit_address_ids, 1) > 1 then
                raise exception 'multiple equivalent unit address records already exist';
            end if;
            _unit_address_id = _unit_address_ids[1];
            update avs_reload.addr_inserts set success_text = 'existing equivalent unit address' where unq_adds_id = _unq_adds_id;

        end if;

        perform avs_reload.insert_axps(_unq_adds_id, _unit_address_id, _current_tms, 'INSERT', null);

    EXCEPTION
        when others then
            update avs_reload.addr_inserts set exception_text = substring(SQLERRM from 1 for 256) where unq_adds_id = _unq_adds_id;
            perform avs_reload.insert_axps(_unq_adds_id, _unit_address_id, _current_tms, 'EXCEPT', 'parent row insert failed');
    END;

END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs_reload.insert_unit_address(
    _unq_adds_id int,
    _addr_unit character varying(10),
    _address_base_id int,
    _current_tms timestamp without time zone,
    _ACTION character(6),                       -- INSERT or EXCEPT
    _MESSAGE character varying(256)
)
OWNER TO postgres;
