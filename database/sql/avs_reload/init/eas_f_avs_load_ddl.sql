
-- DROP FUNCTION IF EXISTS avs_reload.load();

CREATE OR REPLACE FUNCTION avs_reload.load()
  RETURNS text AS
$BODY$
DECLARE

    -- The Big Picture
    -- 1) insert base addresses
    -- 2) insert unit addresses
    -- 3) insert address_x_parcels
    -- 4) prepare for retirement processing (insert cascade retires)
    -- 5) retire address_x_parcels
    -- 6) retire unit addresses
    -- 7) retire base addresses
    --
    -- This entire procedure should be treated as a single transaction by the calling process.
    --
    -- When processing the source tables:
    --      avs_reload.addr_inserts
    --      avs_reload.addr_retires
    --      avs_reload.axp_inserts
    --      avs_reload.axp_retires
    -- we use a field called "exception_text" to...
    --      exclude rows from insertion and from further processing
    --      record exceptions for that row for subsequent reporting
    -- and we use a field called "success_text" to...
    --      simplify reporting
    --      help to assert that things are working properly
    --
    -- todo - add uniqueness constraints to unq_adds_id in EAS itself (we rows have non-unique values [nulls])
    -- todo - should we do much maintenance here, it would be very nice to follow a naming convention...even for something as mundane as block - lot. I mean really.

    _address_base_id    integer;
    _address_id         integer;
    _parcel_id          integer;
    _block_lot          varchar(10);
    _addr_number        integer;
    _addr_unit          varchar(10);
    _zone_id            double precision;
    _geometry           geometry;
    _unq_adds_id        integer;
    _street_segment_id  integer;
    _current_tms        timestamp without time zone = now();
    _date               date;
    _retire_change_request_id int;
    
    -- Set _debug to true to test exception processing code
    _debug              boolean = false;
    _exception_text     varchar(64);

BEGIN

    ----- base address logic in one place
    update avs_reload.addr_inserts ai
    set addr_base_flg = false;

    update avs_reload.addr_inserts ai
    set addr_base_flg = true
    where (
        unq_adds_id::int = coalesce(subaddr_to::int, -1)
        or
        subaddr_to is null
    );
    -----


    ----- BEGIN - insert address_base row and single corresponding addresses row

    -- While the authoritative duplicate check is the address_base trigger, we add several additional
    -- duplicate checks to give a a better sense of the overall quality of the load.

    update avs_reload.addr_inserts ai
    set exception_text = 'base address not inserted: input has null geometry but it also appears that insert would have been a duplicate'
    where 1=1
    and addr_base_flg = true
    and exception_text is null
    and success_text is null
    and ai.geometry is null
    and exists (
        select 1
        from
            address_base ab,
            street_segments ss,
            d_street_type st
        where 1 = 1
        and ab.street_segment_id = ss.street_segment_id
        and ss.st_type = st.abbreviated
        and ab.base_address_num::int = ai.addr_number::int
        and coalesce(ab.base_address_suffix, '-1') = ai.addr_unit
        and ss.st_name = ai.addr_streetname
        and st.unabbreviated = ai.addr_streetsuffix
        -- We can't use zone id because there was no geometry to being with!
        -- Please leave this next comment in place for documentation.
        -- and ab.zone_id::int = (coalesce(ai.zone_id, -1))::int
        and ab.retire_tms is null
    );


    update avs_reload.addr_inserts ai
    set exception_text = 'base address not inserted because it would have created a duplicate'
    from public.street_segments ss_1
    where 1=1
    and addr_base_flg = true
    and exception_text is null
    and success_text is null
    and ai.cnn_final = ss_1.str_seg_cnn
    and ai.geometry is not null
    and exists (
        select 1
        from
            address_base ab,
            street_segments ss_2
        where 1 = 1
        and ab.retire_tms is null
        and ab.street_segment_id = ss_2.street_segment_id
        and ab.base_address_num = ai.addr_number::int
        and coalesce(ab.base_address_suffix, '-1') = ai.addr_unit
        and ss_1.st_name = ss_2.st_name
        and coalesce(ss_1.st_type, '') = coalesce(ss_2.st_type, '')
        and ab.zone_id::int = (coalesce(ai.zone_id, -1))::int
    );

    -- qa runs:y reviewed:
    update avs_reload.addr_inserts
    set exception_text = 'base address not inserted because the geometry was null'
    where 1=1
    and exception_text is null
    and success_text is null
    and geometry is null
    and addr_base_flg = true;


    -- qa runs:y reviewed: pem
    update avs_reload.addr_inserts ai
    set exception_text = 'base address not inserted because there was no matching row in the street_segments table'
    where 1=1
    and exception_text is null
    and success_text is null
    and addr_base_flg = true
    and (
        ai.cnn_final is null
        or
        not exists (
            select 1
            from public.street_segments ss
            where coalesce(ai.cnn_final, '') = ss.str_seg_cnn
        )
    );

    -- qa runs:? reviewed
    update avs_reload.addr_inserts ai
    set exception_text = 'base address not inserted because there was a similar unit address row'
    from public.street_segments ss_1
    where 1=1
    and addr_base_flg = true
    and exception_text is null
    and success_text is null
    and ai.cnn_final = ss_1.str_seg_cnn
    and ai.geometry is not null
    and exists (
        select 1
        from
            address_base ab,
            addresses a,
            street_segments ss_2
        where 1 = 1
        and ab.retire_tms is null
        and ab.street_segment_id = ss_2.street_segment_id
        and ab.address_base_id = a.address_base_id
        and ab.base_address_num = ai.addr_number::int
        and coalesce(a.unit_num, '') = ai.addr_unit
        and ss_1.st_name = ss_2.st_name
        and coalesce(ss_1.st_type, '') = coalesce(ss_2.st_type, '')
        and ab.zone_id::int = (coalesce(ai.zone_id, -1))::int
    );

    -- qa runs:y reviewed:
    declare cursorBaseAddressInsert cursor for
        select
            ai.addr_number::int,
            nullif(ai.addr_unit, '-1'),
            ai.zone_id,
            st_transform(ai.geometry, 2227),
            ai.unq_adds_id::int,
            ss.street_segment_id
        from
             avs_reload.addr_inserts ai,
             public.street_segments ss
        where 1=1
        and ai.addr_base_flg = true
        and ai.exception_text is null
        and ai.success_text is null
        and ai.cnn_final::int = ss.seg_cnn::int;
    begin
        open cursorBaseAddressInsert;
        loop
            fetch cursorBaseAddressInsert into _addr_number, _addr_unit, _zone_id, _geometry, _unq_adds_id, _street_segment_id;
            if not found then
                exit;
            end if;
            BEGIN
                if _debug then
                    raise exception 'debug exception in cursorBaseAddressInsert';
                end if;

                select into _address_base_id nextval('address_base_address_base_id_seq');
                _exception_text = 'base address not inserted';

                -- note: unq_adds_id will be going away in the address base table
                insert into address_base ( address_base_id, base_address_prefix, base_address_num, base_address_suffix, zone_id, geometry, street_segment_id, create_tms, retire_tms, last_change_tms, unq_adds_id, distance_to_segment )
                 values( _address_base_id, null, _addr_number, _addr_unit, _zone_id, _geometry, _street_segment_id, _current_tms, null, _current_tms, _unq_adds_id, null );

                select into _address_id nextval('addresses_address_id_seq');
                _exception_text = 'unit address for base address not inserted';

                insert into addresses ( address_id, address_base_id, unit_type_id, floor_id, unit_num, disposition_code, address_base_flg, unq_adds_id, activate_change_request_id, update_change_request_id, retire_change_request_id, create_tms, retire_tms, last_change_tms, concurrency_id, mailable_flg )
                values ( _address_id, _address_base_id, 0, 105, null, 1, true, _unq_adds_id, 0, 0, 0, _current_tms, null, _current_tms, 0, false );

                update avs_reload.addr_inserts
                set success_text = 'inserted base address'
                where unq_adds_id = _unq_adds_id;

            EXCEPTION
                when others then
                    update avs_reload.addr_inserts
                    set exception_text = _exception_text || ' - ' || SQLERRM
                    where unq_adds_id = _unq_adds_id;

            END;
        end loop;
        close cursorBaseAddressInsert;
    end;
    ----- END - base address inserts


    ----- BEGIN - unit address inserts
    -- qa runs:? reviewed: n
    update avs_reload.addr_inserts ai
    set exception_text = 'unit address not inserted; value in addr_unit is -1 which is not valid'
    where 1=1
    and ai.subaddr_to is not null
    and addr_base_flg = false
    and ai.exception_text is null
    and ai.success_text is null
    and ai.addr_unit = '-1';


    -- qa runs:y reviewed:n
    update avs_reload.addr_inserts ai
    set exception_text = 'unit address not inserted because the corresponding base address is retired'
    where 1=1
    and ai.addr_base_flg = false
    and ai.exception_text is null
    and ai.success_text is null
    and exists (                            -- the parent base address exists
        select 1
        from address_base ab
        where ab.unq_adds_id = ai.subaddr_to::int
    )
    and not exists (                        -- the parent base address is not retired
        select 1
        from address_base ab
        where ab.unq_adds_id = ai.subaddr_to::int
        and ab.retire_tms is null
    );

    -- qa runs:y reviewed:
    update avs_reload.addr_inserts ai
    set exception_text = 'unit address not inserted because we could not find a corresponding base address'
    where 1=1
    and ai.exception_text is null
    and ai.success_text is null
    and ai.subaddr_to is not null
    and ai.addr_base_flg = false
    and not exists (                                              -- the parent base address does not exist
        select 1
        from address_base ab
        where ab.unq_adds_id::int = ai.subaddr_to::int
    );


    declare cursorUnitAddressInsert cursor for
        -- qa:y reviewed:
        select
            ab.address_base_id,
            ai.addr_unit,
            ai.unq_adds_id
        from
            avs_reload.addr_inserts ai,
            address_base ab
        where 1=1
        and ai.subaddr_to is not null
        and ai.addr_base_flg = false
        and ai.subaddr_to::int = ab.unq_adds_id::int     -- has a row in address_base
        and ai.exception_text is null
        and ai.success_text is null;
    begin
        open cursorUnitAddressInsert;
        loop
            fetch cursorUnitAddressInsert into _address_base_id, _addr_unit, _unq_adds_id;
            if not found then
                exit;
            end if;
            BEGIN

                -- domain values
                -- unit type 0: other
                -- floor 105: 'unknown'
                -- disposition code 1: official
                if _debug then
                    raise exception 'debug exception in cursorUnitAddressInsert';
                end if;
                insert into addresses ( address_base_id, unit_num, unit_type_id, floor_id, disposition_code, address_base_flg, unq_adds_id, activate_change_request_id, create_tms, last_change_tms)
                values ( _address_base_id, _addr_unit, 0, 105, 1, false, _unq_adds_id, 0, _current_tms, _current_tms );

                update avs_reload.addr_inserts
                set success_text = 'inserted unit address'
                where unq_adds_id = _unq_adds_id;

            EXCEPTION

                when others then
                    update avs_reload.addr_inserts
                    set exception_text = 'unit address not inserted - ' || SQLERRM
                    where unq_adds_id = _unq_adds_id;

            END;
        end loop;
        close cursorUnitAddressInsert;
    end;
    ----- END - unit address inserts


    ----- BEGIN - address x parcel inserts
    -- qa runs:y reviewed:n
    update avs_reload.axp_inserts axpi
    set exception_text = 'unable to link address with parcel because there is no matching address row'
    where not exists (
        select 1
        from addresses a
        where a.unq_adds_id::int = axpi.unq_adds_id::int
    )
    and axpi.exception_text is null
    and axpi.success_text is null;

    -- test runs:y reviewed:n
    update avs_reload.axp_inserts axpi
    set exception_text = 'unable to link address with parcel because there is no matching parcel row'
    where not exists (
        select 1
        from parcels p
        where axpi.blklot = p.blk_lot
    )
    and axpi.exception_text is null
    and axpi.success_text is null;

    -- qa runs:y reviewed:n
    update avs_reload.axp_inserts axpi
    set exception_text = 'unable to link address with parcel because address is retired'
    from addresses a
    where axpi.unq_adds_id = a.unq_adds_id
    and a.retire_tms is not null
    and axpi.exception_text is null
    and axpi.success_text is null;

    -- qa runs:y reviewed:n
    update avs_reload.axp_inserts axpi
    set exception_text = 'unable to link address with parcel because parcel is retired'
    from parcels p
    where axpi.blklot = p.blk_lot
    and p.date_map_drop is not null
    and axpi.exception_text is null
    and axpi.success_text is null;

    -- inserts - the address_x_parcel insert trigger enforces integrity - catch and record exceptions it raises
    declare cursorAddressXParcelInsert cursor for
        select
            axpi.unq_adds_id,
            axpi.blklot,
            a.address_id,
            p.parcel_id
        from
            avs_reload.axp_inserts axpi,
            public.addresses a,
            public.parcels p
        where axpi.unq_adds_id::int = a.unq_adds_id::int
        and axpi.blklot = p.blk_lot
        and axpi.exception_text is null
        and axpi.success_text is null;
    begin
        open cursorAddressXParcelInsert;
        loop
            fetch cursorAddressXParcelInsert into _unq_adds_id, _block_lot, _address_id, _parcel_id;
            if not found then
                exit;
            end if;
            BEGIN
                if _debug then
                    raise exception 'debug exception in cursorAddressXParcelInsert';
                end if;

                insert into address_x_parcels ( address_id, parcel_id, activate_change_request_id, create_tms, last_change_tms )
                values ( _address_id, _parcel_id, 0, _current_tms, _current_tms );

                update avs_reload.axp_inserts
                set success_text = 'inserted address parcel link'
                where unq_adds_id = _unq_adds_id
                and blklot = _block_lot;

            EXCEPTION
                when others then
                    update avs_reload.axp_inserts
                    set exception_text = SQLERRM
                    where unq_adds_id = _unq_adds_id
                    and blklot = _block_lot;
            END;
        end loop;
        close cursorAddressXParcelInsert;
    end;
    ----- END - address x parcel inserts




    ----- BEGIN - prepare for retirement processing
    -- Before we can start retirement of base address, addresses, and address_x_parcels, we must do some preparation.
    -- Here's why.
    -- We have 2 source tables for retires:
    --      avs_reload.addr_retires
    --      avs_reload.axp_retires
    -- Now, say we have a base address in avs_reload.addr_retires.
    -- Retiring a base address can lead to the retirement of it's underlying addresses and address_x_parcels.
    -- Since an exception can occur on any of these updates, we need a place to record that exception to prevent the underlying updates from occurring.
    -- That place is in the "source tables".
    -- What we do here is take the records that underlie a higher level retire and add them to the source tables.

    -- set some defaults
    update avs_reload.addr_retires set cascade_retire = false;
    update avs_reload.axp_retires set cascade_retire = false;

    insert into avs_reload.addr_retires (unq_adds_id, subaddr_to, cascade_retire)
    select a1.unq_adds_id, ab1.unq_adds_id, true
    from addresses a1
        ,address_base ab1
    where 1=1
    and a1.address_base_id = ab1.address_base_id
    and a1.address_base_id in (
        select a2.address_base_id
        from addresses a2,
            avs_reload.addr_retires r
        where a2.unq_adds_id = r.unq_adds_id
        and a2.address_base_flg = true
    )
    and a1.address_base_flg = false
    and a1.unq_adds_id not in (
        select r2.unq_adds_id
        from avs_reload.addr_retires r2
    );

    update avs_reload.addr_retires set addr_base_flg = false;

    update avs_reload.addr_retires
    set addr_base_flg = true
    where subaddr_to is null
    or unq_adds_id = subaddr_to;


    -- insert underlying address_x_parcels
    -- qa runs:y reviewed:n
    insert into avs_reload.axp_retires (unq_adds_id, end_date, eas_address_id, blocklot, cascade_retire)
    select a.unq_adds_id, null, a.address_id, p.blk_lot, true
    from addresses a,
        address_x_parcels axp,
        parcels p,
        avs_reload.addr_retires r
    where axp.address_id = a.address_id
    and axp.parcel_id = p.parcel_id
    and axp.retire_tms is null
    and r.unq_adds_id = a.unq_adds_id;

    -- insert change request
    -- qa runs:y reviewed:n
    INSERT INTO public.change_requests(cr_change_request_id, requestor_user_id, requestor_comment, reviewer_user_id, reviewer_comment, create_tms)
    VALUES (0, 1, 'avs load', 1, 'avs_load', _current_tms);
    select into _retire_change_request_id last_value from public.change_requests_change_request_id_seq;
    ----- END - prepare for retirement processing




    ----- BEGIN - address x parcels retires
    -- qa runs:y reviewed:n
    update avs_reload.axp_retires axpr
    set exception_text = 'unable to retire address parcel link - unable to find matching address parcel link record'
    where 1=1
    and exception_text is null
    and success_text is null
    and not exists (
        select 1
        from    public.address_x_parcels axp,
                public.parcels p,
                public.addresses a
        where axp.parcel_id = p.parcel_id
        and axp.address_id = a.address_id
        and axp.address_id = axpr.eas_address_id
        and p.blk_lot = axpr.blocklot
    );

    -- qa runs:y reviewed:n
    update avs_reload.axp_retires axpr
    set exception_text = 'unable to retire address parcel link - address parcel link record is already retired'
    where 1=1
    and exception_text is null
    and success_text is null
    and exists (
        select 1
        from    public.address_x_parcels axp,
                public.parcels p,
                public.addresses a
        where axp.parcel_id = p.parcel_id
        and axp.address_id = a.address_id
        and axp.retire_tms is not null
        and axp.address_id = axpr.eas_address_id
        and p.blk_lot = axpr.blocklot
    );


    -- retire address_x_parcels rows - a trigger enforces integrity - we catch and record exceptions it raises
    -- qa runs:y reviewed:n
    declare cursorAddressXParcelRetire cursor for
        select
            unq_adds_id::int,
            eas_address_id::int,
            blocklot
        from avs_reload.axp_retires
        where exception_text is null
        and success_text is null;
    begin
        open cursorAddressXParcelRetire;
        loop
            fetch cursorAddressXParcelRetire into _unq_adds_id, _address_id, _block_lot;
            if not found then
                exit;
            end if;
            BEGIN
                if _debug then
                    raise exception 'debug exception in cursorAddressXParcelRetire';
                end if;

                update address_x_parcels axp
                set retire_tms = _current_tms,
                    retire_change_request_id = _retire_change_request_id
                where exists (
                    select 1
                    from    public.parcels p,
                            public.addresses a
                    where axp.parcel_id = p.parcel_id
                    and axp.address_id = a.address_id
                    and axp.retire_tms is null
                    and axp.address_id = _address_id
                    and p.blk_lot = _block_lot
                );

                update avs_reload.axp_retires
                set success_text = 'retired address parcel link'
                where unq_adds_id = _unq_adds_id
                and blocklot = _block_lot
                and eas_address_id = _address_id;

            EXCEPTION
                when others then
                    update avs_reload.axp_retires
                    set exception_text = SQLERRM
                    where unq_adds_id = _unq_adds_id
                    and blocklot = _block_lot
                    and eas_address_id = _address_id;
            END;
        end loop;
        close cursorAddressXParcelRetire;
    end;
    ----- END - address x parcels retires


    ----- BEGIN - retire unit addresses
    -- qa runs:y reviewed:n
    update avs_reload.addr_retires r
    set exception_text = 'unable to retire unit address - no matching address record'
    where 1=1
    and addr_base_flg = false
    and exception_text is null
    and success_text is null
    and not exists (
        select 1
        from addresses a
        where a.unq_adds_id::int = r.unq_adds_id::int
        and a.address_base_flg = false
    );

    -- qa runs:y reviewed:n
    update avs_reload.addr_retires r
    set exception_text = 'unable to retire unit address - address is already retired'
    where 1=1
    and addr_base_flg = false
    and exception_text is null
    and success_text is null
    and exists (
        select 1
        from addresses a
        where a.unq_adds_id::int = r.unq_adds_id::int
        and a.retire_tms is not null
        and a.address_base_flg = false
    );

    -- qa runs:y reviewed:n
    update avs_reload.addr_retires r
    set exception_text = 'unable to retire unit address - one or more underlying address_x_parcels rows is not retired'
    where 1=1
    and addr_base_flg = false
    and exception_text is null
    and success_text is null
    and exists (
        select 1
        from addresses a,
            address_x_parcels axp
        where a.unq_adds_id::int = r.unq_adds_id::int
        and a.retire_tms is not null
        and a.address_id = axp.address_id
        and a.address_base_flg = false
        and axp.retire_tms is null
    );

    -- retire the rows
    declare cursorUnitAddressRetire cursor for
        -- qa runs:n reviewed:n
        select
            r.unq_adds_id::int,
            a.address_id
        from
            avs_reload.addr_retires r,
            public.addresses a
        where r.exception_text is null
        and r.success_text is null
        and addr_base_flg = false
        and a.unq_adds_id = r.unq_adds_id;
    begin
        open cursorUnitAddressRetire;
        loop
            fetch cursorUnitAddressRetire into _unq_adds_id, _address_id;
            if not found then
                exit;
            end if;
            BEGIN
                if _debug then
                    raise exception 'debug exception in cursorUnitAddressRetire';
                end if;

                update public.addresses
                set retire_tms = _current_tms,
                    retire_change_request_id = _retire_change_request_id
                where address_id = _address_id;

                update avs_reload.addr_retires
                set success_text = 'retired unit address'
                where unq_adds_id = _unq_adds_id;

            EXCEPTION
                when others then
                    update avs_reload.addr_retires
                    set exception_text = SQLERRM
                    where unq_adds_id = _unq_adds_id;
            END;
        end loop;
        close cursorUnitAddressRetire;
    end;
    ----- END - address retires


    ----- BEGIN - address base retires
    -- qa runs:y reviewed:n
    update avs_reload.addr_retires r
    set exception_text = 'unable to retire base address - no matching record'
    where 1=1
    and exception_text is null
    and success_text is null
    and addr_base_flg = true
    and not exists (
        select 1
        from addresses a
        where a.unq_adds_id::int = r.unq_adds_id::int
        and a.address_base_flg = true
    );

    -- qa runs:y reviewed:n
    update avs_reload.addr_retires r
    set exception_text = 'unable to retire base address - already retired'
    where 1=1
    and addr_base_flg = true
    and exception_text is null
    and success_text is null
    and exists (
        select 1
        from addresses a
        where a.unq_adds_id::int = r.unq_adds_id::int
        and a.retire_tms is not null
        and a.address_base_flg = true
    );

    -- qa runs:y reviewed:n
    update avs_reload.addr_retires r
    set exception_text = 'unable to retire base address - one or more underlying unit addresses is not retired'
    where 1=1
    and addr_base_flg = true
    and r.exception_text is null
    and r.success_text is null
    and exists (
        -- filter for unretired unit addresses that underlie the base address that we want to retire
        select 1
        from addresses a1,
            addresses a2
        where a1.unq_adds_id::int = r.unq_adds_id::int
        and a1.retire_tms is null
        and a1.address_base_flg = true
        and a1.address_base_id = a2.address_base_id
        and a2.address_base_flg = false
        and a2.retire_tms is null
    );

    -- retire the rows
    -- qa runs:? reviewed:n
    -- paul - fix this getting both success and exception fields filled in: "unable to retire base address - no matching record";"retired unit address"
    declare cursorBaseAddressRetire cursor for
        select
            r.unq_adds_id::int,
            a.address_id
        from
            avs_reload.addr_retires r,
            public.addresses a
        where 1=1
        and r.exception_text is null
        and r.success_text is null
        and a.unq_adds_id::int = r.unq_adds_id::int
        and addr_base_flg = true;
    begin
        open cursorBaseAddressRetire;
        loop
            fetch cursorBaseAddressRetire into _unq_adds_id, _address_id;
            if not found then
                exit;
            end if;
            BEGIN
                if _debug then
                    raise exception 'debug exception in cursorBaseAddressRetire';
                end if;

                update public.addresses
                set retire_tms = _current_tms,
                    retire_change_request_id = _retire_change_request_id
                where address_id = _address_id;

                update public.address_base ab
                set retire_tms = _current_tms
                where address_base_id in (
                    select address_base_id
                    from addresses a
                    where a.address_id = _address_id
                    limit 1
                );

                update avs_reload.addr_retires
                set success_text = 'retired base address'
                where unq_adds_id = _unq_adds_id;

            EXCEPTION
                when others then
                    update avs_reload.addr_retires
                    set exception_text = SQLERRM
                    where unq_adds_id = _unq_adds_id;
            END;
        end loop;
        close cursorBaseAddressRetire;
    end;
    ----- END - address base retires


    return 'end of avs_reload.load()';

END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE STRICT
COST 100;
ALTER FUNCTION avs_reload.load() OWNER TO postgres;
