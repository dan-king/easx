-- Function: avs_reload.insert_unit_addresses()

DROP FUNCTION IF EXISTS avs_reload.insert_unit_addresses(
    _address_base_id int,
    _subaddr_to_id int,
    _current_tms timestamp without time zone,
    _ACTION character(6),                       -- INSERT or EXCEPT
    _MESSAGE character varying(256)
);

CREATE OR REPLACE FUNCTION avs_reload.insert_unit_addresses(
    _address_base_id int,
    _subaddr_to_id int,
    _current_tms timestamp without time zone,
    _ACTION character(6),                       -- INSERT or EXCEPT
    _MESSAGE character varying(256)
)
RETURNS void AS
$BODY$
DECLARE 

    _unit_address_ids integer[];
    _unit_address_id integer;
    _addr_unit character varying(10);
    _unq_adds_id int;

BEGIN

    if _ACTION not in ('INSERT', 'EXCEPT') then
        raise exception 'programming error - invalid domain for _ACTION: %', _ACTION;
    end if;

    declare cursorUnitAddress cursor for
        select
            ai.addr_unit,
            ai.unq_adds_id::int
        from
            avs_reload.addr_inserts ai
        where 1=1
        and ai.subaddr_to is not null
        and ai.addr_base_flg = false
        and ai.subaddr_to::int = _subaddr_to_id
        and ai.exception_text is null
        and ai.success_text is null;
    begin

        open cursorUnitAddress;
        loop
            fetch cursorUnitAddress into _addr_unit, _unq_adds_id;
            if not found then
                exit;
            end if;

            perform avs_reload.insert_unit_address( _unq_adds_id, _addr_unit, _address_base_id, _current_tms, 'INSERT', null::text);

        end loop;
        close cursorUnitAddress;
    end;


END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs_reload.insert_unit_addresses(
    _address_base_id int,
    _subaddr_to_id int,
    _current_tms timestamp without time zone,
    _ACTION character(6),                       -- INSERT or EXCEPT
    _MESSAGE character varying(256)
)
OWNER TO postgres;

