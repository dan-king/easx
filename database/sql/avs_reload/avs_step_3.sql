
--BEGIN 
--populate a table of distinct addresses (this function was formerly filled with a view)
drop table SRC_DBIAVS2_AGG;

CREATE TABLE SRC_DBIAVS2_AGG (
  "ID_FGI"            INT,
  "ADDR_NUMBER"       VARCHAR2(20 BYTE),
  "ADDR_UNIT"         VARCHAR2(10 BYTE),
  "ADDR_STREETNAME"   VARCHAR2(100 BYTE),
  "ADDR_STREETSUFFIX" VARCHAR2(14 BYTE),
  "ADDR_ZIP_SPATIAL"  VARCHAR2(14 BYTE)
);
--Paul hates this use of '[NULL]', but we need some substitute for a true null to support joins
insert into SRC_DBIAVS2_AGG (
  ID_FGI            ,
  ADDR_NUMBER       ,
  ADDR_UNIT         ,
  ADDR_STREETNAME   ,
  ADDR_STREETSUFFIX ,
  ADDR_ZIP_SPATIAL
) (
  SELECT MIN(ID_FGI),
    STREET_NUMBER,
    NVL(unit, -1),
    UPPER(AVS_STREET_NAME),
    UPPER(NVL(AVS_STREET_TYPE, '[NULL]')),
    NVL(zip_spatial, 0)
  FROM src_dbiavs2
 -- where src_dbiavs2.end_date is null
  group by STREET_NUMBER, NVL(unit, -1), UPPER(AVS_STREET_NAME), UPPER(NVL(AVS_STREET_TYPE, '[NULL]')), NVL(zip_spatial, 0)
);
commit;

-- add indices on SRC_DBIAVS2_AGG
-- DROP INDEX SRC_DBIAVS2_AGG_IDX_0;
-- DROP INDEX SRC_DBIAVS2_AGG_IDX_1;
-- DROP INDEX SRC_DBIAVS2_AGG_IDX_2;
-- DROP INDEX SRC_DBIAVS2_AGG_IDX_3;
-- DROP INDEX SRC_DBIAVS2_AGG_IDX_4;

CREATE INDEX SRC_DBIAVS2_AGG_IDX_0 ON SRC_DBIAVS2_AGG (ADDR_NUMBER);
CREATE INDEX SRC_DBIAVS2_AGG_IDX_1 ON SRC_DBIAVS2_AGG (ADDR_STREETNAME);
CREATE INDEX SRC_DBIAVS2_AGG_IDX_2 ON SRC_DBIAVS2_AGG (ADDR_STREETSUFFIX);
CREATE INDEX SRC_DBIAVS2_AGG_IDX_3 ON SRC_DBIAVS2_AGG (ADDR_ZIP_SPATIAL);
CREATE INDEX SRC_DBIAVS2_AGG_IDX_4 ON SRC_DBIAVS2_AGG (ADDR_UNIT);
--END


-- BEGIN - update and insert unq_adds from src_dbiavs2
-- Efforts to make this query (or similar) run reasonably fast have proven difficult.
-- Therefore we are going to kludge below. Runs in ~60 seconds.

-- n=227730
create table src_dbiavs2_update NOLOGGING as
select avs.id_fgi, 
       ua.unq_adds_id
from  SRC_DBIAVS2_AGG avs, 
      unq_adds ua
where ua.addr_number = avs.addr_number
and ua.addr_unit = avs.addr_unit
and ua.addr_streetname_std = avs.addr_streetname
and ua.addr_streetsuffix_std = avs.addr_streetsuffix
and ua.addr_zip_spatial = avs.addr_zip_spatial;
commit;
-- select count(*) from src_dbiavs2_update;

drop index src_dbiavs2_update_idx;
create index src_dbiavs2_update_idx on src_dbiavs2_update(unq_adds_id);
 
UPDATE UNQ_ADDS ua
SET src_dbiavs2_id = (
   SELECT src_dbiavs2_update.id_fgi
   FROM src_dbiavs2_update
   WHERE src_dbiavs2_update.unq_adds_id = ua.unq_adds_id);
commit;
-- select count(*) from unq_adds where src_dbiavs2_id is not null;
drop table src_dbiavs2_update;


--insert records that didn't find a match
--runs in ~67 seconds
insert into unq_adds (
  ADDR_NUMBER,
  ADDR_UNIT,
  ADDR_STREETNAME,
  ADDR_STREETSUFFIX,
  ADDR_STREETNAME_STD,
  ADDR_STREETSUFFIX_STD,
  ADDR_ZIP_SPATIAL,
  SRC_DBIAVS2_ID
) (
  SELECT
	  avs.ADDR_NUMBER,
	  avs.ADDR_UNIT,
	  avs.ADDR_STREETNAME,
	  avs.ADDR_STREETSUFFIX,
	  avs.ADDR_STREETNAME,    -- not a typo
	  avs.ADDR_STREETSUFFIX,  -- not a typo
	  avs.ADDR_ZIP_SPATIAL,
	  avs.ID_FGI
  FROM src_dbiavs2_agg avs
	left join unq_adds ua on avs.id_fgi = ua.src_dbiavs2_id
  WHERE ua.src_dbiavs2_id is null); 
commit;

-- END - update and insert unq_adds from src_dbiavs2


----- BEGIN- Add new records to ua_src_link table
--Creation of SRC_DBIAVS2_LINK is a processing step
CREATE TABLE SRC_DBIAVS2_LINK (
  "ID_FGI"            INT,
  "ADDR_NUMBER"       VARCHAR2(20 BYTE),
  "ADDR_UNIT"         VARCHAR2(10 BYTE),
  "ADDR_STREETNAME"   VARCHAR2(100 BYTE),
  "ADDR_STREETSUFFIX" VARCHAR2(14 BYTE),
  "ADDR_ZIP_SPATIAL"  VARCHAR2(14 BYTE),
  BLOCKLOT			  VARCHAR2(50 BYTE)
);

insert into SRC_DBIAVS2_LINK (
  ID_FGI            ,
  ADDR_NUMBER       ,
  ADDR_UNIT         ,
  ADDR_STREETNAME   ,
  ADDR_STREETSUFFIX ,
  ADDR_ZIP_SPATIAL	,
  BLOCKLOT
) (
  SELECT ID_FGI,
    STREET_NUMBER,
    NVL(unit, -1),
    UPPER(AVS_STREET_NAME),
    UPPER(NVL(AVS_STREET_TYPE, '[NULL]')),
    NVL(zip_spatial, 0),
	BLOCKLOT
  FROM src_dbiavs2);
commit;

-- add indices on SRC_DBIAVS2_LINK
--DROP INDEX SRC_DBIAVS2_LINK_IDX_0;
--DROP INDEX SRC_DBIAVS2_LINK_IDX_1;
--DROP INDEX SRC_DBIAVS2_LINK_IDX_2;
--DROP INDEX SRC_DBIAVS2_LINK_IDX_3;
--DROP INDEX SRC_DBIAVS2_LINK_IDX_4;

CREATE INDEX SRC_DBIAVS2_LINK_IDX_0 ON SRC_DBIAVS2_LINK (ADDR_NUMBER);
CREATE INDEX SRC_DBIAVS2_LINK_IDX_1 ON SRC_DBIAVS2_LINK (ADDR_STREETNAME);
CREATE INDEX SRC_DBIAVS2_LINK_IDX_2 ON SRC_DBIAVS2_LINK (ADDR_STREETSUFFIX);
CREATE INDEX SRC_DBIAVS2_LINK_IDX_3 ON SRC_DBIAVS2_LINK (ADDR_ZIP_SPATIAL);
CREATE INDEX SRC_DBIAVS2_LINK_IDX_4 ON SRC_DBIAVS2_LINK (ADDR_UNIT);

----- BEGIN - delete test records from the ua_src_link
-- this insert statement adds data to the ua_src_link table
--190 seconds to run
create index ua_src_link_idx10 on ua_src_link(sourcename);

delete from ua_src_link
where sourcename = 'src_dbiavs2';
commit;

drop index ua_src_link_idx10;
----- END - delete test records from the ua_src_link


--Add new records to ua_src_link table
--Creation of SRC_DBIAVS2_LINK is a processing step
-- select * from ua_src_link where rownum < 100;
insert into ua_src_link (unq_adds_id, src_id_fgi, blklot, sourcename)
select
    a.unq_adds_id,
    b.id_fgi,
    b.blocklot,
    'src_dbiavs2'
from  unq_adds a, 
      SRC_DBIAVS2_LINK b
where 1=1 AND
  a.addr_number = b.addr_number and
  a.addr_streetname_std = b.addr_streetname and
  a.addr_streetsuffix_std = b.addr_streetsuffix and
  a.addr_unit = b.addr_unit and
  a.addr_zip_spatial = b.addr_zip_spatial;
commit;

drop table SRC_DBIAVS2_LINK;

----- END - Add new records to ua_src_link table


----- BEGIN - add geometry to the new records in unq_adds
-- This table will temporarily store the geometries of interest
-- Note that geom was transformed to SRID 40978 in the parcel FMW.
-- see http://download.oracle.com/docs/html/B10826_01/sdo_cs_ref.htm for more info on cs transformation in Oracle

create table src_dbiavs2_geom as
select 
    a.id_fgi,
    b.unq_adds_id,
    c.geometry
from 
    src_dbiavs2 a, 
    unq_adds b, 
    parcels_pt c
where a.id_fgi = b.src_dbiavs2_id
and a.blocklot = c.blk_lot;
commit;
-- select count(*) from src_dbiavs2_geom;

create index src_dbiavs_geom_idx on src_dbiavs2_geom (unq_adds_id);
DROP INDEX "ADDRESS_PROCESSING"."UNQ_ADDS_SIDX";

-- select count(*) from unq_adds where geometry is null;

update unq_adds ua
set geometry =  (
    select b.geometry 
    from  src_dbiavs2_geom b
    where ua.unq_adds_id = b.unq_adds_id
)
where geometry is null;   
commit;
	
CREATE INDEX "ADDRESS_PROCESSING"."UNQ_ADDS_SIDX" ON "ADDRESS_PROCESSING"."UNQ_ADDS" ("GEOMETRY") 
   INDEXTYPE IS "MDSYS"."SPATIAL_INDEX" ;  

drop table src_dbiavs2_geom;
-----END --add geometry to the new records in unq_adds

----- BEGIN - populate zone value where necesary
--select count(*) from unq_adds where zone_id is null and geometry is not null;
--use anyinteract for speed (runs in 358 seconds)
update unq_adds a
set zone_id = (
  select min(z.z_zone_id)
  from zones z
  where SDO_ANYINTERACT(z.geometry, a.geometry) = 'TRUE'
)
WHERE 1=1
  AND geometry is not null
  AND zone_id is null;
COMMIT;

--use nearest neighbor to clean up stragglers (4 seconds)
update unq_adds a
set zone_id = (
  select z.z_zone_id
  from zones z
  where SDO_NN(z.geometry, a.geometry, 'sdo_num_res=1') = 'TRUE'
)
WHERE 1=1
  AND geometry is not null
  AND zone_id is null;
COMMIT;


----- END - populate zone value where necesary


----- BEGIN - isolate records that should be retired in EAS
-- create a table of avs "addresses" (as eas defines them) that are permanemently retired
-- this populates the table with all addresses that were ever defined as retired in avs
DROP TABLE avs_retires_temp;
DROP TABLE avs_retires;

CREATE TABLE avs_retires_temp as
SELECT
  a.unq_adds_id,
  b.end_date
FROM
  UA_SRC_LINK a,
  SRC_DBIAVS2 b
WHERE 1=1
AND a.sourcename = 'src_dbiavs2'
AND a.src_id_fgi = b.id_fgi
GROUP BY a.unq_adds_id, b.end_date
ORDER BY a.unq_adds_id, b.end_date;
commit; 

--this deletes records where the same address (as address is defined by EAS) has been reinstated as active
DELETE FROM avs_retires_temp
WHERE 1=1
  AND unq_adds_id IN 
      (SELECT unq_adds_id
       FROM avs_retires_temp
       WHERE end_date is null);
COMMIT;       

CREATE TABLE avs_retires as
SELECT
  unq_adds_id,
  max(end_date) as end_date
FROM avs_retires_temp
WHERE 1=1
GROUP BY unq_adds_id
ORDER BY unq_adds_id;
commit; 
----- END -isolate records that should be retired in EAS

----- BEGIN - isolate axp's that should be retired in EAS
--list all axp combos
DROP TABLE axp_retires_temp;
DROP TABLE axp_retires;

CREATE TABLE axp_retires_temp as
SELECT
  a.unq_adds_id,
  b.blocklot,
  b.end_date
FROM
  UA_SRC_LINK a,
  SRC_DBIAVS2 b
WHERE 1=1
AND a.sourcename = 'src_dbiavs2'
AND a.src_id_fgi = b.id_fgi;
commit; 

--this deletes records where the axp combo is active based on null end_date
--This approach ensures that if a combo was set to retired, then reactivated, that it will be deleted from this list.
DELETE FROM axp_retires_temp
WHERE 1=1
  AND unq_adds_id IN 
      (SELECT unq_adds_id
       FROM axp_retires_temp
       WHERE 1=1
        AND end_date is null);
COMMIT;       

CREATE TABLE axp_retires as
SELECT
  a.unq_adds_id,
  b.eas_address_id,
  a.blocklot,
  max(end_date) as end_date
FROM axp_retires_temp a, unq_adds b
WHERE 1=1
  AND a.unq_adds_id = b.unq_adds_id
group by a.unq_adds_id, b.eas_address_id, a.blocklot
ORDER BY unq_adds_id;
commit; 
-- select count(*) from axp_retires;

