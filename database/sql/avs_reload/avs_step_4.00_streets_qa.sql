-- drop view v_unq_adds_nn_qa;
CREATE or replace VIEW v_unq_adds_nn_qa  AS
select 
    UNQ_ADDS_ID            ,
    DIST_IN_METERS         ,
    ADDR_NUMBER            ,
    MIN_ADDR_RANGE         ,
    MAX_ADDR_RANGE         ,
    ADDR_RANGE_DIF         ,
    --ADDR_STREETNAME        ,
    --ADDR_STREET_SUFFIX     ,
    --STREETNAME             ,
    --ST_ADDR_SUFFIX         ,
    ADDR_STREETNAME_STD    ,
    ADDR_STREET_SUFFIX_STD ,
    STREETNAME_STD         ,
    ST_ADDR_SUFFIX_STD     ,
    JWS_STREETNAME         ,
    JWS_SUFFIX             ,
    EXCLUDE_FLAG           ,
    PICK_FLAG              ,
    PICK_ID                
from unq_adds_nn
order by UNQ_ADDS_ID;

select count(*)
from v_unq_adds_nn_qa
where pick_flag = 'Y';

select 218630/10 - 18959
from dual;


-- QA the update statements in the prod select_street_segment
select *
from v_unq_adds_nn_qa
where 1=1
and unq_adds_id in (
  select unq_adds_id
  from unq_adds_nn
  group by unq_adds_id, pick_id
  --having pick_id in ('A', 'B', 'C', 'D', 'E', 'F')
  having pick_id in ('A')
)
order by unq_adds_id asc, jws_streetname desc, dist_in_meters asc;

-- n=218630
select count(*) from v_unq_adds_nn_qa;

-- sets from which we have selected a segment
-- n=189590
--select count(*)
select * 
from v_unq_adds_nn_qa 
where 1=1
and unq_adds_id in (
   select distinct unq_adds_id
    from v_unq_adds_nn_qa
    where pick_flag = 'Y'
)
order by unq_adds_id asc, jws_streetname desc, dist_in_meters asc;


-- sets that have not been selected
-- n=60
--select count(*)
select * 
from v_unq_adds_nn_qa 
where 1=1
and unq_adds_id not in (
   select distinct unq_adds_id
    from v_unq_adds_nn_qa
    where pick_flag = 'Y'
)
and exclude_flag = 'N'
order by unq_adds_id asc, jws_streetname desc, dist_in_meters asc;

-- sets that have been excluded
-- n=28980
--select count(*)
select * 
from v_unq_adds_nn_qa 
where 1=1
and unq_adds_id in (
   select unq_adds_id
    from v_unq_adds_nn_qa
    group by unq_adds_id, exclude_flag
    having exclude_flag = 'Y'
)
order by unq_adds_id asc, jws_streetname desc, dist_in_meters asc;


select 189590 + 28980 + 60  from dual;


-- specify your jws threshhold here
--and unq_adds_id in (
--   select unq_adds_id
--    from v_unq_adds_nn_qa
--    group by unq_adds_id, jws_streetname
--    having max(jws_streetname) >= 90 and max(jws_streetname) <= 99
--)




select * 
from unq_adds_nn 
where unq_adds_id in (
   select unq_adds_id
    from unq_adds_nn
    group by unq_adds_id, exclude_flag
    having exclude_flag = 'Y'
);


select *
from unq_adds_nn
where ltrim(trim(streetname), '0') = trim(addr_streetname)
and substr(trim(streetname), 1, 1) = '0';




SELECT 
  addr_streetname_std,
  streetname_std,
  addr_street_suffix_std,
  st_addr_suffix_std
from unq_adds_nn
where addr_streetname_std is null
or streetname_std is null
or addr_street_suffix_std is null
or st_addr_suffix_std is null;

