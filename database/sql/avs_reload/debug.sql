
-- select avs_reload.insert_records();
-- select avs_reload.retire_records();


SELECT                  -- A flattened view of addresses.
	ab.address_base_id,
    ab.unq_adds_id,
	a.address_id,
    a.unq_adds_id,
	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	ab.zone_id,
	asewkt(ab.geometry),
	a.address_base_flg,
	a.unit_num,
	sn.base_street_name,
    sn.street_type,
    p.blk_lot,
	ab.create_tms as ab_create_tms,
	a.create_tms as a_create_tms,
    axp.create_tms as axp_create_tms,
	ab.retire_tms as ab_retire_tms,
	a.retire_tms as a_retire_tms,
    axp.retire_tms as axp_retire_tms,
    ab.street_segment_id,
    ss.st_name
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id)
WHERE 1 = 1
and sn.category = 'MAP'
and sn.base_street_name like ( '%10TH%')
and street_type = 'ST'
and ab.base_address_num = 0
--and a.address_id = 102984
--and ab.address_base_id = 102984
--and ab.unq_adds_id in (249654)
--and p.blk_lot = '0052119'
--and a.unit_num = '103'
order by
	ss.st_name,
	ss.st_type,
	a.address_base_flg desc,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num;








































select * 
from avs_reload.axp_inserts axpi,
    public.addresses a
where axpi.unq_adds_id = a.unq_adds_id
and axpi.exception_text is null
and axpi.success_text is null

select * 
from avs_reload.axp_inserts axpi
where 1=1
and axpi.exception_text is null
and axpi.success_text is null





select * from avs_reload.addr_inserts where unq_adds_id = 1417928
select * from avs_reload.axp_inserts where unq_adds_id = 1417928
select * from addresses where unq_adds_id = 1421350
select * from address_base where unq_adds_id = 1417928



SELECT
	ab.address_base_id,
	a.address_id,
	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num,
	ss.st_name,
    ss.st_type
FROM
	address_base ab,
	addresses a,
	street_segments ss
WHERE 1 = 1
AND ab.address_base_id = a.address_base_id
AND ab.street_segment_id = ss.street_segment_id
--and ss.st_name like 'HARV%'
-- and ab.base_address_num = 10
--and ab.unq_adds_id = 41889
and a.address_id = 422296
order by 	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num,
	ss.st_name,
	ss.st_type
;



select * 
from street_segments 
where 1=1
-- and st_name like 'HARV%'
and seg_cnn = 6692000;

select * 
from streetnames
where base_street_name like 'HARV%'







----- BEGIN load report
select * from avs_reload.addr_retires;
select count(*) from avs_reload.axp_retires where subaddr_to is null;
select count(*) from avs_reload.axp_retires where subaddr_to is not null;
select count(*) from avs_reload.axp_retires;

select * from avs_reload.vw_load_report;
select * from avs_reload.vw_load_summary;
select * from avs_reload.addr_inserts order by addr_number::int, addr_streetname, addr_streetsuffix, addr_unit;

select asewkt(transform(geometry, 4326)) from avs_reload.addr_inserts where unq_adds_id = 239384;

select 
    exception_text, 
    success_text, 
    count(*) 
from avs_reload.addr_inserts 
group by 
    exception_text, 
    success_text;

select 
    exception_text, 
    success_text, 
    count(*) 
from avs_reload.axp_inserts 
group by 
    exception_text, 
    success_text;

----- END load report




----- BEGIN retires
update avs_reload.addr_retires set cascade_retire = false;
update avs_reload.axp_retires set cascade_retire = false;

insert into avs_reload.addr_retires (unq_adds_id, subaddr_to, cascade_retire)
select a1.unq_adds_id, ab1.unq_adds_id, true
from addresses a1
    ,address_base ab1
where 1=1
and a1.address_base_id = ab1.address_base_id
and a1.address_base_id in (
    select a2.address_base_id
    from addresses a2,
        avs_reload.addr_retires r
    where a2.unq_adds_id = r.unq_adds_id
    and a2.address_base_flg = true
)
and a1.address_base_flg = false
and a1.unq_adds_id not in (
    select r2.unq_adds_id
    from avs_reload.addr_retires r2
);



select * from avs_reload.vw_retires_report;



----- END retires



update avs_reload.addr_retires 
set exception_text = null, success_text = null;

update avs_reload.axp_retires 
set exception_text = null, success_text = null;


select * from avs_reload.vw_insert_report;
select * from avs_reload.addr_inserts where unq_adds_id = 1187693;
select * from avs_reload.vw_axp_inserts_report where exception_text = 'unable to link address with parcel because there is no matching address row';
select * from avs_reload.vw_addr_retires_report;
select * from avs_reload.vw_axp_retires_report;

-----

select exception_text, success_text, count(*)
from avs_reload.vw_addr_retires_report
group by exception_text, success_text;

select exception_text, success_text, count(*)
from avs_reload.vw_axp_retires_report
group by exception_text, success_text;

-----

select count(*) from avs_reload.vw_addr_inserts_report where success_text is not null
union all
select count(*) from avs_reload.vw_axp_inserts_report where success_text is not null
union all
select count(*) from avs_reload.vw_addr_retires_report where success_text is not null
union all
select count(*) from avs_reload.vw_axp_retires_report where success_text is not null;






select count(*)
from addresses
where unit_num = '9'
and address_base_id = 226675
and address_base_flg = false
and retire_tms is null;




-- null geom
    update avs_reload.addr_inserts ai
    set exception_text = 'base address not inserted because the geometry was null'
    where 1=1
    and exception_text is null
    and success_text is null
    and ai.geometry is null
    and (ai.subaddr_to is null or ai.unq_adds_id::int = ai.subaddr_to::int );  -- is a base address


-- duplicates
    update avs_reload.addr_inserts ai
    set exception_text = 'base address not inserted because it would have created a duplicate'
    from public.street_segments ss_1
    where 1=1
    and (ai.subaddr_to is null or ai.unq_adds_id::int = ai.subaddr_to::int )  -- is a base address
    and exception_text is null
    and success_text is null
    and ai.cnn_final = ss_1.str_seg_cnn
    and exists (
        select 1
        from
            address_base ab,
            street_segments ss_2
        where 1 = 1
        and ab.retire_tms is null
        and ab.street_segment_id = ss_2.street_segment_id
        and ab.base_address_num = ai.addr_number::int
        and coalesce(ab.base_address_suffix, '-1') = ai.addr_unit
        and ss_1.st_name = ss_2.st_name
        and coalesce(ss_1.st_type, '') = coalesce(ss_2.st_type, '')
        and ab.zone_id::int = (coalesce(ai.zone_id, -1))::int
    );


select *
from d_street_type 

select count(*)
from d_street_type st,
    street_segments ss
where ss.st_type = st.abbreviated

select count(*)
from avs_reload.addr_inserts ai
where 1=1 
and ai.geometry is null
and exists (
    select 1
    from
        address_base ab,
        street_segments ss,
        d_street_type st
    where 1 = 1
    and ab.street_segment_id = ss.street_segment_id
    and ss.st_type = st.abbreviated
    and ab.base_address_num::int = ai.addr_number::int
    and coalesce(ab.base_address_suffix, '-1') = ai.addr_unit
    and ss.st_name = ai.addr_streetname
    and st.unabbreviated = ai.addr_streetsuffix
    -- We can't use zone id because there was no geometry to being with.
    --and ab.zone_id::int = (coalesce(ai.zone_id, -1))::int
    and ab.retire_tms is null
)

-----
select * from avs_reload.addr_inserts order by exception_text, success_text;

select *
from parcels
where blk_lot = '2719C004'


select * from avs_reload.vw_load_report where exception_text is null and success_text is null;
select * from streetnames where base_street_name like '%AQUA%'




select * from avs_reload.vw_load_summary;
-----


CREATE TABLE debug
(
  msg text
);


delete from debug
update avs_reload.addr_inserts set exception_text = null, success_text = null;

select * from debug


        select
            ai.addr_number::int,
            nullif(ai.addr_unit, '-1'),
            ai.addr_streetname, 
            ai.addr_streetsuffix, 
            ai.zone_id,
            z.zipcode,
            st_transform(ai.geometry, 2227),
            ai.unq_adds_id::int
        from avs_reload.addr_inserts ai
        left outer join zones z on ai.zone_id = z.zone_id
        left outer join street_segments ss on ss.seg_cnn::int = ai.cnn_final::int
        where 1=1
        and ai.addr_base_flg = true
        and ai.exception_text is null
        and ai.success_text is null;

        select count(*)
        from avs_reload.addr_inserts ai
        left outer join zones z on ai.zone_id = z.zone_id
        left outer join street_segments ss on ss.seg_cnn::int = ai.cnn_final::int
        where 1=1
        and ai.addr_base_flg = false



select avs_reload.find_equiv_base_address(520, null, 'CHESTNUT', 'STREET', '94133');
select avs_reload.find_equiv_base_address(1, null, 'SOUTH PARK', null, null);

select 
    ai.addr_number::int,
    nullif(ai.addr_unit, '-1'),
    ai.addr_streetname, 
    nullif(ai.addr_streetsuffix, '[NULL]'), -- you gotta love this
    ai.zone_id,
    z.zipcode,
    st_transform(ai.geometry, 2227),
    ai.unq_adds_id::int,
    ss.street_segment_id,
    sn.base_street_name, 
    st.unabbreviated
from avs_reload.addr_inserts as ai
left outer join zones as z on ai.zone_id = z.zone_id
left outer join street_segments as ss on ss.seg_cnn::int = ai.cnn_final::int
left outer join streetnames as sn on (sn.street_segment_id = ss.street_segment_id and sn.category = 'MAP')
left outer join d_street_type st on sn.street_type = st.abbreviated
where 1=1
and ai.addr_base_flg = true
and ai.addr_streetname = 'BROADWAY'
and ai.addr_number = '999';

select avs_reload.find_equiv_base_address(999, null, 'ROBERT C LEVY', 'TUNNEL', '94133');


select avs_reload._eas_insert_base_address( null::text, 999::int, null::text, 23, null::geometry, 13121, now()::timestamp without time zone, 259908::int);

select * from avs_reload.addr_inserts where exception_text like 'insert or update on table%'


select * 
from avs_reload.addr_inserts ai1
where unq_adds_id = 236133

        select distinct
            axpi.addr_number::int,
            nullif(axpi.addr_unit, '-1') as addr_unit,
            axpi.addr_streetname, 
            nullif(axpi.addr_streetsuffix, '[NULL]') as addr_streetsuffix, -- you gotta love this
            st.unabbreviated as street_suffix_unabbreviated,
            axpi.unq_adds_id::int
        from avs_reload.axp_inserts as axpi
        inner join streetnames as sn on (sn.base_street_name = axpi.addr_streetname and sn.category = 'MAP')
        inner join d_street_type st on (sn.street_type = st.abbreviated)
        where 1=1




select *
from street_segments ss
where not exists (
    select 1
    from streetnames sn
    where ss.street_segment_id = sn.street_segment_id
);
