-- DROP TABLE xmit_queue_accela;
-- DROP TABLE xmit_accela_queue;

CREATE TABLE xmit_accela_queue
(
  id serial NOT NULL,
  address_base_history_id integer NOT NULL,
  address_history_id integer,
  address_x_parcel_history_id integer,
  last_change_tms timestamp without time zone NOT NULL,
  xmit_tms timestamp without time zone,
  sort_order integer NOT NULL,
  CONSTRAINT "xmit_accela_queue_PK" PRIMARY KEY (id),
  CONSTRAINT "xmit_accela_queue_FK_address_base_history_id" FOREIGN KEY (address_base_history_id)
      REFERENCES address_base_history (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "xmit_accela_queue_FK_address_history_id" FOREIGN KEY (address_history_id)
      REFERENCES addresses_history (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "xmit_accela_queue_FK_address_x_parcel_history_id" FOREIGN KEY (address_x_parcel_history_id)
      REFERENCES address_x_parcels_history (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "xmit_accela_queue_UNQ" UNIQUE (address_base_history_id, address_history_id, address_x_parcel_history_id, last_change_tms)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE xmit_accela_queue OWNER TO eas_dbo;
GRANT ALL ON TABLE xmit_accela_queue TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE xmit_accela_queue TO django;

-- Index: "xmit_accela_queue_IDX_address_base_history_id"

-- DROP INDEX "xmit_accela_queue_IDX_address_base_history_id";

CREATE INDEX "xmit_accela_queue_IDX_address_base_history_id"
  ON xmit_accela_queue
  USING btree
  (address_base_history_id);

-- Index: "xmit_accela_queue_IDX_address_history_id"

-- DROP INDEX "xmit_accela_queue_IDX_address_history_id";

CREATE INDEX "xmit_accela_queue_IDX_address_history_id"
  ON xmit_accela_queue
  USING btree
  (address_history_id);

-- Index: "xmit_accela_queue_IDX_address_x_parcel_history_id"

-- DROP INDEX "xmit_accela_queue_IDX_address_x_parcel_history_id";

CREATE INDEX "xmit_accela_queue_IDX_address_x_parcel_history_id"
  ON xmit_accela_queue
  USING btree
  (address_x_parcel_history_id);

-- Index: "xmit_accela_queue_IDX_last_change_tms"

-- DROP INDEX "xmit_accela_queue_IDX_last_change_tms";

CREATE INDEX "xmit_accela_queue_IDX_last_change_tms"
  ON xmit_accela_queue
  USING btree
  (last_change_tms);

-- Index: "xmit_accela_queue_IDX_xmit_tms"

-- DROP INDEX "xmit_accela_queue_IDX_xmit_tms";

CREATE INDEX "xmit_accela_queue_IDX_xmit_tms"
  ON xmit_accela_queue
  USING btree
  (xmit_tms);

ALTER TABLE xmit_accela_queue_id_seq OWNER TO eas_dbo;
GRANT ALL ON TABLE xmit_accela_queue_id_seq TO eas_dbo;
GRANT ALL ON TABLE xmit_accela_queue_id_seq TO django;
