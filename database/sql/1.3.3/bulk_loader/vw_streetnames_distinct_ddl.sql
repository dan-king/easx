-- View: vw_streetnames_distinct

-- DROP VIEW vw_streetnames_distinct;

CREATE OR REPLACE VIEW vw_streetnames_distinct AS 

select distinct 
    base_street_name, 
    st.abbreviated as abbreviated_suffix, 
    st.unabbreviated as unabbreviated_suffix
from streetnames sn
left outer join d_street_type st on sn.street_type = st.abbreviated;

ALTER TABLE vw_streetnames_distinct OWNER TO postgres;



