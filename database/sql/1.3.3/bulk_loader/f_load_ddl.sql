
DROP FUNCTION IF EXISTS bulkloader.load(
    _offset int,
    _limit int
);
CREATE OR REPLACE FUNCTION bulkloader.load(
    _offset int,
    _limit int
)
  RETURNS text AS
$BODY$
DECLARE

    _bulkloader_id                 int;
    -- In this load we choose an arbitrary create date
    _current_tms            timestamp without time zone = now();
    _address_base_id        int;
    _address_id             int;
    _address_x_parcel_id    int;
    _exception_text         character varying(256);
    _sort_order             int;
    _success                boolean;
    _offset                 int = _offset;
    _limit                  int = _limit;
    _std_end_date_patch     date;

BEGIN


    ---- bulkloader
    -- Avoid joins here until we have a performance problem. The reason behind this is simplicity and readability.
    -- When we run into duplicates the "first one in wins".
    -- The "order by" is important because
    --    we use bla.id to stabilize the sort so it can be used with offset and limit
    -- We use offset and limit on the cursor query to help reduce the size of the transaction and to help with progress indication.
    -- The use of offset is inefficient but I am not "feeling" that effect here - probably the rest of the processing takes so long.
    -- See discussion http://postgresql.1045698.n5.nabble.com/Indexing-problem-with-OFFSET-LIMIT-td1906950.html
    -- Total time to process about 350000 rows is 30 minutes.
    declare cursorBulkload cursor for
        select
          bla.id,
          bla.exception_text
        from bulkloader.address_extract bla
        where bla.load_tms is null
        order by bla.id
        offset _offset
        limit _limit;
    begin
        open cursorBulkload;
        loop
            -- We discard _sort_order.
            fetch cursorBulkload into _bulkloader_id, _exception_text;

            if not found then
                exit;
            end if;


                -- Skip rows that have an exception.
                -- We do this here instead of in the select cursor to simplify QA and because we update exception_text during the cursor operation.
                if _exception_text is not null then
                    continue;
                end if;
                -- base address
                begin
                    select into _success bulkloader.process_address_base(_bulkloader_id, _current_tms);
                    if not _success then
                        continue;
                    end if;
                exception
                    when others then
                        update bulkloader.address_extract set exception_text = substring(SQLERRM from 1 for 256) where id = _bulkloader_id;
                        continue;
                end;


                -- unit address
                begin
                    perform bulkloader.process_address_unit(_bulkloader_id, _current_tms);
                exception
                    when others then
                        update bulkloader.address_extract set exception_text = substring(SQLERRM from 1 for 256) where id = _bulkloader_id;
                        continue;
                end;


                -- address parcel link
                begin
                    perform bulkloader.process_address_parcel_link(_bulkloader_id, _current_tms);
                exception
                    when others then
                        update bulkloader.address_extract set exception_text = substring(SQLERRM from 1 for 256) where id = _bulkloader_id;
                end;

                -- mark row as loaded
                update bulkloader.address_extract bla
                set load_tms = now()
                where id = _bulkloader_id and exception_text is null;


        end loop;
        close cursorBulkload;
    end;
    ----


    return 'success!';


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION bulkloader.load(
    _offset int,
    _limit int
) OWNER TO postgres;
