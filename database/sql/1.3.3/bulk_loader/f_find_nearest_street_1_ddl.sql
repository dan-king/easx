
DROP FUNCTION IF EXISTS bulkloader.find_nearest_street_1(
    _bulkloader_id         int,
    _street_name    character varying(64),
    _street_type    character varying(32),
    _parcel_id      int,
    _std_end_date   character varying(10),
    out _street_segment_id int
);

CREATE OR REPLACE FUNCTION bulkloader.find_nearest_street_1(
    _bulkloader_id         int,
    _street_name    character varying(64),
    _street_type    character varying(32),
    _parcel_id      int,
    _std_end_date   character varying(10),
    out _street_segment_id int
)
  RETURNS int AS
$BODY$
DECLARE

    _row_count int;
    _street_segment_id_1 int;
    _street_segment_id_2 int;
    _street_type_unabbreviated character varying(64);
    _exception_text character varying(256);

BEGIN

        begin
            -- First try category='MAP'.
            select into _street_segment_id bulkloader.find_nearest_street_2(_bulkloader_id, _street_name, _street_type, _parcel_id, _std_end_date, 'MAP');
            if _street_segment_id is null then
                -- Then try category='ALIAS'.
                select into _street_segment_id bulkloader.find_nearest_street_2(_bulkloader_id, _street_name, _street_type, _parcel_id, _std_end_date, 'ALIAS');
            end if;

            if _street_segment_id is null then
                -- For 1/10000 of the input records, we will need to expand our search.
                -- We do this for large parcels - where the parcel centroid is too far from the street segments.
                -- We don't do this up front because it is expensive.
                -- e.g. 35 marist st
                insert into bulkloader.streets_nearest (parcel_id, street_segment_id, distance)
                select p.parcel_id, ss.street_segment_id, st_distance(p.geometry, ss.geometry)
                from parcels p
                inner join public.street_segments ss on st_dwithin(p.geometry, ss.geometry, 500)
                where parcel_id = _parcel_id
                and (parcel_id, street_segment_id) not in (
                    select parcel_id, street_segment_id
                    from bulkloader.streets_nearest
                    where parcel_id = _parcel_id
                );

                -- Now we try again.
                -- TODO ? this block is the same as above and is a candidate for refactor.
                -- First try category='MAP'.
                select into _street_segment_id bulkloader.find_nearest_street_2(_bulkloader_id, _street_name, _street_type, _parcel_id, _std_end_date, 'MAP');
                if _street_segment_id is null then
                    -- Then try category='ALIAS'.
                    select into _street_segment_id bulkloader.find_nearest_street_2(_bulkloader_id, _street_name, _street_type, _parcel_id, _std_end_date, 'ALIAS');
                end if;

            end if;

        exception
            when others then
                update bulkloader.address_extract set exception_text = substring(SQLERRM from 1 for 256) where id = _bulkloader_id;
        end;


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION bulkloader.find_nearest_street_1(
    _bulkloader_id         int,
    _street_name    character varying(64),
    _street_type    character varying(32),
    _parcel_id      int,
    _std_end_date   character varying(10),
    out _street_segment_id int
) OWNER TO postgres;
