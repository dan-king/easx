
DROP FUNCTION IF EXISTS bulkloader.process_address_base(    
    _bulkloader_id int,
    _create_tms timestamp without time zone
);


CREATE OR REPLACE FUNCTION bulkloader.process_address_base( 
    _bulkloader_id int,
    _create_tms timestamp without time zone
)
  RETURNS boolean AS
$BODY$
DECLARE
    _street_number          int;
    _std_street_number_suffix   character varying(10);
    _street_name            character varying(64);
    _street_type            character varying(32);
    _std_end_date           character varying(10);
    _geometry               geometry;
    _zone_id                int;
    _row_count              int;
    _street_segment_id      int;
    _distance               double precision;
    _new_ids                int[];
    _address_id             int;
    _parcel_id              int;
    _block                  character varying(6);
    _lot                    character varying(5);
    _blocks                 character varying(6)[];
    _lots                   character varying(5)[];
    _address_base_id        int = null;
    _address_base_ids       int[];
    _disposition_code       int;
    _exception_text         character varying(256);
    _change_request_id      int;
    _load_source            character varying(32);
    _source_id              int;
    _geocode                boolean;
    _bla_geometry           geometry;
BEGIN

    select 
        bla.street_number,
        bla.std_street_number_sfx,
        bla.street_name,
        bla.std_street_type,
        bla.std_end_date,
        bla.block,
        bla.lot,
        bla.load_source,
        bla.source_id,
        bla.geometry,
        p.parcel_id,
        pp.geometry
    into
        _street_number,
        _std_street_number_suffix,
        _street_name,
        _street_type,
        _std_end_date,
        _block,
        _lot,
        _load_source,
        _source_id,
        _bla_geometry,
        _parcel_id,
        _geometry
    from bulkloader.address_extract bla
    inner join public.parcels p on bla.blocklot = p.blk_lot
    inner join bulkloader.parcel_points pp on pp.parcel_id = p.parcel_id
    where id = _bulkloader_id;

    _geocode = FALSE;

    GET DIAGNOSTICS _row_count = ROW_COUNT;
    if _row_count > 1 then
        raise exception 'expected one row or less but got %', _row_count::text;
    elsif _row_count = 1 and _bla_geometry is not null then
        _geometry := _bla_geometry;
    elsif _row_count = 1 and _bla_geometry is null then
        _geocode := TRUE;
    elsif _row_count = 0 then
        select 
            bla.street_number,
            bla.std_street_number_sfx,
            bla.street_name,
            bla.std_street_type,
            bla.std_end_date,
            bla.block,
            bla.lot,
            bla.load_source,
            bla.source_id,
            bla.geometry
        into
            _street_number,
            _std_street_number_suffix,
            _street_name,
            _street_type,
            _std_end_date,
            _block,
            _lot,
            _load_source,
            _source_id,
            _geometry
        from bulkloader.address_extract bla
        where id = _bulkloader_id;
    end if;


    if _street_name = 'UNKNOWN' then
        -- disposition is "placeholder"
        _disposition_code := 2;
        -- DBI is using "NO STREET SUFFIX" - we use null.
        _street_type := null;
    else
        -- disposition is official
        _disposition_code = 1;
    end if;

    ----- First we look for a base address that matches the bulkloader address.
    select into _address_base_ids bulkloader.find_address_base(_street_number, _std_street_number_suffix, _street_name, _street_type, _bulkloader_id);

    if array_upper(_address_base_ids, 1) is not null then
        -- found one or more
        if array_upper(_address_base_ids, 1) > 1 then
            -- found more than one
            update bulkloader.address_extract set exception_text = 'multiple matching base address records already exist' where id = _bulkloader_id;
            return false;
        end if;
        -- found one
        _address_base_id = _address_base_ids[1];
        update bulkloader.address_extract set address_base_id = _address_base_id where id = _bulkloader_id;
        return true;
    end if;
    -----
    -- At this point we know there is no matching address.
    if _geocode = FALSE then
        select result_id into strict _street_segment_id from
                bulkloader.find_street_segment_by_geometry(
                    _street_number,
                    _street_name,
                    _street_type,
                    _std_end_date::date,
                    TRUE,
                    _geometry);

            if (_street_segment_id is null) then
                update bulkloader.address_extract set exception_text = 'no street segment found' where id = _bulkloader_id;
                return false;
            end if;

            if (
                (select st_distance(
                    _geometry, 
                    (select geometry from public.street_segments where street_segment_id = _street_segment_id)
                )) > 500
            ) then
                update bulkloader.address_extract set exception_text = 'steet segment is greater than 500 feet from the address point' where id = _bulkloader_id;
                return false;
            end if;

    else
    ----- street segment (and geometry)
    -- Is this a service address?  If so, process as such.
        if ( _block = '9999' or _block = '0000') and ( _lot = '999' or _lot = '000') then
           
            select address_point, segment_id into strict _geometry, _street_segment_id from
                bulkloader.geocode_street_address(
                    _street_number,
                    _street_name,
                    _street_type,
                    _std_end_date::date,
                    TRUE);

            if (_geometry is null) or (_street_segment_id is null) then
                update bulkloader.address_extract set exception_text = 'could not geocode service address' where id = _bulkloader_id;
                return false;
            end if;
            
            -- Confirm that the resulting geocoded point is in the service parcel, and only there.
            select array_agg(block_num), array_agg(lot_num) into _blocks, _lots
                from parcels
                where ST_Intersects(_geometry, geometry);
            
            if array_upper(_blocks, 1) is null or array_upper(_lots, 1) is null then
                update bulkloader.address_extract set exception_text = 'service address does not geocode to within any parcel' where id = _bulkloader_id;
                return false;
            end if;
            
            if array_upper(_blocks, 1) > 1 or array_upper(_lots, 1) > 1 then
                update bulkloader.address_extract set exception_text = 'service address geocodes to multiple parcels' where id = _bulkloader_id;
                return false;
            end if;
            
            if _blocks[1] <> '0000' or _lots[1] <> '000' then
                update bulkloader.address_extract set exception_text = 'service address codes to a non-service parcel' where id = _bulkloader_id;
                return false;
            end if;

        else -- not a service address - find street segment
            select into _street_segment_id bulkloader.find_nearest_street_1(_bulkloader_id, _street_name, _street_type, _parcel_id, _std_end_date);
        end if;
    end if;


    if _street_segment_id is null then
        -- using the pipe as a delimiter hack
        select substring('unable to find nearby street | nearest: ' || string_agg(distinct full_street_name, ', ') from 1 for 255)
        into _exception_text
        from (
            select (base_street_name || ' ' || coalesce(street_type, ''))::character varying(64) as full_street_name
            from bulkloader.vw_streets_nearest
            where parcel_id = _parcel_id
            and category = 'MAP'
            and date_dropped is null
            order by distance asc
            limit 5
        ) vtab;

        update bulkloader.address_extract set exception_text = _exception_text where id = _bulkloader_id and exception_text is null;
        return false;
    end if;

    update bulkloader.address_extract set street_segment_id = _street_segment_id where id = _bulkloader_id;
    -----


    ----- We find the nearest zone because the address point geometry may be out in the bay (by hunters point) where there are no zone polygons.
    -- We discard the distance variable.
    select zone_id, st_distance(_geometry, z.geometry) into _zone_id, _distance  as distance
    from zones z
    order by distance asc
    limit 1;

    if (_zone_id is null) then
        update bulkloader.address_extract set exception_text = 'could not find zone - should not happen' where id = _bulkloader_id;
        return false;
    else
        update bulkloader.address_extract set zone_id = _zone_id where id = _bulkloader_id;
    end if;
    -----

    select value into _change_request_id from bulkloader.metadata where key = 'change_request_id';
    -- For the bulkloader data we assume that base addresses do not have unit information at the base address level.
    select into _new_ids public._eas_insert_base_address( null::text, _street_number::int, _std_street_number_suffix::text, _zone_id::int, _geometry::geometry, _street_segment_id::int, _create_tms::timestamp without time zone, _disposition_code, _change_request_id);
    _address_base_id = _new_ids[1];
    -- We discard _address_id.
    _address_id = _new_ids[2];


    update bulkloader.address_extract set address_base_id = _address_base_id where id = _bulkloader_id;

    insert into public.address_sources (eas_id, eas_table, source_id, source_system) values(_address_base_id, 'address_base', _source_id, _load_source);

    return true;

END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION bulkloader.process_address_base(
    _bulkloader_id int,
    _create_tms timestamp without time zone
) OWNER TO postgres;