-- Function: public.insert_base_address()

DROP FUNCTION IF EXISTS public._eas_insert_base_address(
    _base_address_prefix    character varying(10),
    _base_address_num       int, 
    _base_address_suffix    character varying(10),
    _zone_id                int, 
    _geometry               geometry, 
    _street_segment_id      int, 
    _current_tms            timestamp without time zone,
    _disposition_code       int,
    _change_request_id      int,
    out _new_ids            int[]
);

CREATE OR REPLACE FUNCTION public._eas_insert_base_address(
    _base_address_prefix    character varying(10),
    _base_address_num       int, 
    _base_address_suffix    character varying(10),
    _zone_id                int, 
    _geometry               geometry, 
    _street_segment_id      int, 
    _current_tms            timestamp without time zone,
    _disposition_code       int,
    _change_request_id      int,
    out _new_ids            int[]
)
RETURNS int[] AS
$BODY$
DECLARE 

    _address_base_id int;
    _address_id int;

BEGIN

    if _geometry is null then
        raise exception 'unable to insert base address - no geometry';
    end if;

    if _street_segment_id is null then
        raise exception 'unable to insert base address - no street segment id';
    end if;

    if _zone_id is null then
        raise exception 'unable to insert base address - no zone id';
    end if;

    select into _address_base_id nextval('address_base_address_base_id_seq');

    insert into address_base ( 
        address_base_id, 
        base_address_prefix, 
        base_address_num, 
        base_address_suffix, 
        zone_id, 
        geometry, 
        street_segment_id, 
        create_tms, 
        retire_tms, 
        last_change_tms, 
        distance_to_segment 
    )
    values( 
        _address_base_id, 
        _base_address_prefix, 
        _base_address_num, 
        _base_address_suffix, 
        _zone_id, 
        _geometry, 
        _street_segment_id, 
        _current_tms, 
        null, 
        _current_tms, 
        null 
    );


    select into _address_id nextval('addresses_address_id_seq');

    insert into addresses ( 
        address_id, 
        address_base_id, 
        unit_type_id,  
        floor_id,  
        unit_num,
        disposition_code,
        address_base_flg,  
        activate_change_request_id,  
        update_change_request_id,  
        retire_change_request_id,  
        create_tms,  
        retire_tms,  
        last_change_tms,  
        concurrency_id,  
        mailable_flg 
    )
    values (  
        _address_id,  
        _address_base_id,  
        0,  
        105,  
        null,
        _disposition_code,
        true,  
        _change_request_id,  
        _change_request_id,  
        _change_request_id,  
        _current_tms,  
        null,  
        _current_tms,  
        0,  
        false 
    );

    -- todo - make more structured (like use multple OUT parms) (I found myself loosing time trying to do this the right way.)
    _new_ids = array[_address_base_id, _address_id];


END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION public._eas_insert_base_address(
    _base_address_prefix    character varying(10),
    _base_address_num       int, 
    _base_address_suffix    character varying(10),
    _zone_id                int, 
    _geometry               geometry, 
    _street_segment_id      int, 
    _current_tms            timestamp without time zone,
    _disposition_code       int,
    _change_request_id      int,
    out _new_ids            int[]
)
OWNER TO postgres;
