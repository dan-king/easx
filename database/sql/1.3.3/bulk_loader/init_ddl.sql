DROP SCHEMA IF EXISTS bulkloader CASCADE;

CREATE SCHEMA bulkloader;


CREATE TABLE bulkloader.metadata
(
  key character varying(256) primary key,
  value character varying(256)
)
WITH (OIDS=FALSE);
ALTER TABLE bulkloader.metadata OWNER TO postgres;

INSERT INTO bulkloader.metadata(key, value)
    VALUES ('change_request_id', null);

CREATE TABLE bulkloader.parcel_points
(
  parcel_id int NOT NULL,
  geometry geometry,
  CONSTRAINT enforce_dims_geometry CHECK (ndims(geometry) = 2),
  CONSTRAINT enforce_geotype_geometry CHECK (geometrytype(geometry) = 'POINT'::text),
  CONSTRAINT enforce_srid_geometry CHECK (srid(geometry) = 2227)
)
WITH (OIDS=FALSE);
ALTER TABLE bulkloader.parcel_points OWNER TO postgres;

CREATE INDEX parcel_points_idx
  ON bulkloader.parcel_points
  USING btree (parcel_id);

CREATE INDEX parcel_points_sidx
  ON bulkloader.parcel_points
  USING gist (geometry);


CREATE TABLE bulkloader.blocks_nearest
(
  block_num character varying(12),
  address_extract_id int,
  distance double precision
)
WITH (OIDS=FALSE);
ALTER TABLE bulkloader.blocks_nearest OWNER TO postgres;

CREATE INDEX blocks_nearest_idx1
  ON bulkloader.blocks_nearest
  USING btree (block_num);

CREATE INDEX blocks_nearest_idx2
  ON bulkloader.blocks_nearest
  USING btree (address_extract_id);


CREATE TABLE bulkloader.streets_nearest
(
  parcel_id int not null,
  street_segment_id int not null,
  distance double precision
)
WITH (OIDS=FALSE);
ALTER TABLE bulkloader.streets_nearest OWNER TO postgres;

CREATE INDEX streets_nearest_idx1
  ON bulkloader.streets_nearest
  USING btree (parcel_id);

CREATE INDEX streets_nearest_idx2
  ON bulkloader.streets_nearest
  USING btree (street_segment_id);


CREATE TABLE bulkloader.parcels_without_geometry
(
  blk_lot character(9),
  geometry geometry,
  CONSTRAINT enforce_dims_geometry CHECK (ndims(geometry) = 2),
  CONSTRAINT enforce_srid_geometry CHECK (srid(geometry) = 2227)
)
WITH (OIDS=FALSE);
ALTER TABLE bulkloader.parcels_without_geometry OWNER TO postgres;
-- DROP INDEX bulkloader.parcels_without_geometry_idx_blk_lot
CREATE INDEX parcels_without_geometry_idx_blk_lot
  ON bulkloader.parcels_without_geometry
  USING btree
  (blk_lot);
-----


CREATE TABLE bulkloader.address_extract
(
  id serial primary key,
  blocklot character varying(10),
  block character varying(256),
  lot character varying(256),
  unit character varying(256),
  unit_sfx character varying(256),
  floor character varying(256),
  street_number character varying(256),
  street_number_sfx character varying(256),
  street_name character varying(256),
  street_sfx character varying(256),
  street_type character varying(256),
  load_source character varying(32),
  source_id character varying(32),
  geometry geometry,
    -- columns below this line are not from bulkloader csv and are used for EAS processing
  std_street_type character varying(32),
  std_end_date character varying(10),
  std_unit character varying(10),
  std_street_number_sfx character(1),
  street_segment_id int,
  zone_id int, 
  address_base_id int,
  address_id int,
  address_x_parcel_id int,
  exception_text character varying(256),
  sql character varying(1024),
  load_tms timestamp without time zone
)
WITH (OIDS=FALSE);
ALTER TABLE bulkloader.address_extract OWNER TO postgres;


DROP VIEW IF EXISTS bulkloader.vw_load_summary;
CREATE OR REPLACE VIEW bulkloader.vw_load_summary AS
    select 
        exception_text_general, 
        count, 
        round(count/count_total::numeric * 100, 4) as percentage
    from (
        select 
          (case
            when position('|' in bla.exception_text) > 0 then substring(bla.exception_text from 1 for position('|' in bla.exception_text) - 1)
            when position(':' in bla.exception_text) > 0 then substring(bla.exception_text from 1 for position(':' in bla.exception_text) - 1)
            when position('.' in bla.exception_text) > 0 then substring(bla.exception_text from 1 for position('.' in bla.exception_text) - 1)
            else substring(bla.exception_text from 1 for 95)
          end) as exception_text_general,
            count(*) as count,
            (select count(*) from bulkloader.address_extract) as count_total
        from bulkloader.address_extract bla
        group by exception_text_general
        order by count(*) desc
    ) v;
ALTER TABLE bulkloader.vw_load_summary OWNER TO postgres;



DROP VIEW IF EXISTS bulkloader.vw_load_results;
CREATE OR REPLACE VIEW bulkloader.vw_load_results AS
    select 
        bla.id, bla.block, bla.lot,
        bla.street_number, bla.street_number_sfx, bla.street_name, bla.street_sfx, bla.street_type, 
        bla.unit, bla.unit_sfx,
        substring(bla.exception_text from 1 for 95) as exception_text,
        bla.sql, bla.std_street_type,
        bla.std_end_date, bla.street_segment_id, bla.zone_id, bla.address_base_id, bla.address_id, bla.address_x_parcel_id
    from bulkloader.address_extract bla
    order by
        bla.street_number,
        bla.street_name,
        bla.std_street_type,
        bla.street_number_sfx,
        bla.unit,
        bla.unit_sfx;
ALTER TABLE bulkloader.vw_load_results OWNER TO postgres;


CREATE TABLE bulkloader.qa
(
  description varchar(255),
  pass boolean
)
WITH (OIDS=FALSE);
ALTER TABLE bulkloader.qa OWNER TO postgres;

DROP VIEW IF EXISTS bulkloader.vw_streets_nearest;
CREATE OR REPLACE VIEW bulkloader.vw_streets_nearest AS
    select
        snear.street_segment_id,
        snear.distance,
        snames.base_street_name as base_street_name,
        st.unabbreviated as street_type,
        snames.street_type as street_type_abbreviated,
        snames.category,
        snear.parcel_id,
        ss.date_added,
        ss.date_dropped
    from bulkloader.streets_nearest snear
    inner join public.street_segments ss on ss.street_segment_id = snear.street_segment_id
    inner join public.streetnames snames on snames.street_segment_id = snear.street_segment_id
    left outer join d_street_type st on snames.street_type = st.abbreviated
    where 1=1
    -- Exclude certain streets that will not have addresses (such as stockton tunnel)
    -- If this gets more complex we'll create an exclude table.
    and snear.street_segment_id not in (13412);
ALTER TABLE bulkloader.vw_streets_nearest OWNER TO postgres;


