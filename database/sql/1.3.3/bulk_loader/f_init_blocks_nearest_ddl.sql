
-- DROP FUNCTION IF EXISTS bulkloader.init_blocks_nearest();

CREATE OR REPLACE FUNCTION bulkloader.init_blocks_nearest()
  RETURNS text AS
$BODY$
DECLARE

BEGIN


    delete from bulkloader.blocks_nearest;

	insert into bulkloader.blocks_nearest (block_num, address_extract_id, distance)
	select pp.block_num, bla.id, st_distance(pp.geometry, bla.geometry)
	from bulkloader.address_extract bla
	inner join public.parcels pp on st_dwithin(pp.geometry, bla.geometry, 500)
	where bla.geometry is not null 
	and pp.block_num != '0000'
	and pp.date_map_drop is null;

    return 'success!';


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE STRICT
COST 100;
ALTER FUNCTION bulkloader.init_blocks_nearest() OWNER TO postgres;
