
-- drop table address_base_street_provisioning;

create table address_base_street_provisioning (
    address_base_id integer,
    address_id integer,
    provisioned_street_segment_id integer,
    provisioned_street_name character varying(60),
    provisioned_street_type character varying(6),
    provisioned_street_geometry geometry,
    dpw_street_segment_id integer,
    diff_metric integer,
    sql_update_street_segment varchar(256),
    sql_update_invalid_address varchar(256)
);
