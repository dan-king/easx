-- Function: _sfmad_etl_parcels()
--
-- Use non-temp tables to allow 2 phase commit.
--

DROP FUNCTION IF EXISTS _sfmad_etl_parcels(
  job_id_in char(36),
  out success boolean,
  out messageText text,
  out newParcelsCount int,
  out alteredParcelsCount int,
  out retiredParcelsCount int,
  out parcelWorkCount int
);

DROP FUNCTION IF EXISTS _sfmad_etl_parcels();

CREATE OR REPLACE FUNCTION _sfmad_etl_parcels(
  _job_uuid varchar(36),
  out success boolean,
  out messageText text
)
RETURNS record AS
$BODY$
DECLARE 

  BEGIN

    truncate parcels_work;

    --check that the source table (parcels_staging) has more records than the target table (parcels)
    if (select count(*) from parcels where parcel_id not in (select parcel_id from parcels_provisioning where provisioned_tms is null))
        >
       (select count(*) from parcels_staging) then
        success := false;
        messageText := 'ETL failed. Parcels table (target) has more records than the parcels_staging (source) table.';
        return;
    end if;

    --check that each parcels_staging record has a unique blk_lot value
	if (select count(*) from parcels_staging) <> (select count(*) from (select distinct (blk_lot) from parcels_staging) a) then
        success := false;
        messageText := 'ETL failed. Blk_lot values in parcels_staging are not unique.';
		return;
    end if;

    -- If the staging table contains values that will bring existing blocklots out of reitrement, we record an
    -- exception, and delete the staged rows. This is caused by an upstream clerical error.
    -- When you see the exception report, notify DPW, they will make some fixes, which will correct the problem
    -- for the next time the job runs
  
    insert into etl_exceptions (job_uuid, message)
    select _job_uuid, 'illegal parcel transition from retired to unretired: ' || ps.blk_lot
    from parcels p
    inner join parcels_staging ps on p.blk_lot = ps.blk_lot
    where p.date_map_drop is not null
    and ps.date_map_drop is null;

    delete from parcels_staging ps
    using parcels p
    where p.blk_lot = ps.blk_lot
    and p.date_map_drop is not null
    and ps.date_map_drop is null;


    -- prepare new parcels
    insert into parcels_work (
        map_blk_lot,
        blk_lot,
        block_num,
        lot_num,
        date_rec_add,
        date_rec_drop,
        date_map_add,
        date_map_drop,
        date_map_alt,
        project_id_add,
        project_id_drop,
        project_id_alt,
        exception_notes,
        create_tms,
        geometry,
        status
    )
    select
        a.map_blk_lot,
        a.blk_lot,
        a.block_num,
        a.lot_num,
        a.date_rec_add,
        a.date_rec_drop,
        a.date_map_add,
        a.date_map_drop,
        a.date_map_alt,
        a.project_id_add,
        a.project_id_drop,
        a.project_id_alt,
        a.exception_notes,
        now(),
        st_transform(a.geometry, 2227) as geometry,
        'NEW'
    from parcels_staging a
    left join parcels b on a.blk_lot = b.blk_lot
    where b.blk_lot is null;


    -- prepare updated parcels
    -- We look for any parcel that has changed.
    -- The ETL processing changes the geometry data ever so slightly so we cannot use a simple st_equals to find the geometries that have changed.
    -- Therefore we use an approximation
    --     st_area(ST_symDifference(ps.geometry, p.geometry)) / st_perimeter(ps.geometry) > 0.05
    -- for geometry change that I arrived at after about an hour of experimentation.
    -- The threshold of 0.05 represents the place where there seems to be a break in the trend
    -- and is about where you can't see the difference with your eye on the screen.
    insert into parcels_work (
        map_blk_lot,
        blk_lot,
        block_num,
        lot_num,
        date_rec_add,
        date_rec_drop,
        date_map_add,
        date_map_drop,
        date_map_alt,
        project_id_add,
        project_id_drop,
        project_id_alt,
        exception_notes,
        create_tms,
        geometry,
        status
    )
    select
        ps.map_blk_lot,
        ps.blk_lot,
        ps.block_num,
        ps.lot_num,
        ps.date_rec_add,
        ps.date_rec_drop,
        ps.date_map_add,
        ps.date_map_drop,
        ps.date_map_alt,
        ps.project_id_add,
        ps.project_id_drop,
        ps.project_id_alt,
        ps.exception_notes,
        now(),
        ps.geometry,
        'UPDATED'
    from
        parcels p,
        parcels_staging ps
    WHERE p.blk_lot = ps.blk_lot
    AND (
           coalesce(p.map_blk_lot, '')                  != coalesce(ps.map_blk_lot, '')
        OR coalesce(p.block_num, '')                    != coalesce(ps.block_num, '')
        OR coalesce(p.lot_num, '')                      != coalesce(ps.lot_num, '')
        OR coalesce(p.date_rec_add::date, '1-1-1970'::date)   != coalesce(ps.date_rec_add::date, '1-1-1970'::date)
        OR coalesce(p.date_rec_drop::date, '1-1-1970'::date)  != coalesce(ps.date_rec_drop::date, '1-1-1970'::date)
        OR coalesce(p.date_map_add::date, '1-1-1970'::date)   != coalesce(ps.date_map_add::date, '1-1-1970'::date)
        OR coalesce(p.date_map_drop::date, '1-1-1970'::date)  != coalesce(ps.date_map_drop::date, '1-1-1970'::date)
        OR coalesce(p.date_map_alt::date, '1-1-1970'::date)   != coalesce(ps.date_map_alt::date, '1-1-1970'::date)
        OR coalesce(p.project_id_add, '')               != coalesce(ps.project_id_add, '')
        OR coalesce(p.project_id_drop, '')              != coalesce(ps.project_id_drop, '')
        OR coalesce(p.project_id_alt, '')               != coalesce(ps.project_id_alt, '')
        OR (
            ps.geometry is not null
            AND
            p.geometry is not null
            AND
            st_area(ST_symDifference(ps.geometry, p.geometry)) / st_perimeter(ps.geometry) > 0.05
        )
    );


    ----- Now we work on the parcels table.
    insert into parcels (
        map_blk_lot,
        blk_lot,
        block_num,
        lot_num,
        date_rec_add,
        date_rec_drop,
        date_map_add,
        date_map_drop,
        date_map_alt,
        project_id_add,
        project_id_drop,
        project_id_alt,
        exception_notes,
        create_tms,
        geometry
    ) select
        map_blk_lot,
        blk_lot,
        block_num,
        lot_num,
        date_rec_add,
        date_rec_drop,
        date_map_add,
        date_map_drop,
        date_map_alt,
        project_id_add,
        project_id_drop,
        project_id_alt,
        exception_notes,
        create_tms,
        geometry
    from parcels_work
    where status = 'NEW';

    update parcels p
    set 
        map_blk_lot = pw.map_blk_lot,
        blk_lot = pw.blk_lot,
        block_num = pw.block_num,
        lot_num = pw.lot_num,
        date_rec_add = pw.date_rec_add,
        date_rec_drop = pw.date_rec_drop,
        date_map_add = pw.date_map_add,
        date_map_drop = pw.date_map_drop,
        date_map_alt = pw.date_map_alt,
        project_id_add = pw.project_id_add,
        project_id_drop = pw.project_id_drop,
        project_id_alt = pw.project_id_alt,
        exception_notes = pw.exception_notes,
        create_tms = pw.create_tms,
        geometry = st_transform(pw.geometry,2227),
        update_tms =  now()
    from parcels_work pw
    where p.blk_lot = pw.blk_lot
    and pw.status = 'UPDATED';

    -- Here we do some special processing for the service parcel geometry.
    -- The incoming geometry may be invalid because of the way it is produced; in this step we make it valid.
    -- If we were using postgis 2.0 we could use st_makevalid; alas we are using 1.5 so we must use this st_buffer hack.
    update parcels
    set geometry = (
        select st_buffer(geometry, 0)
        from parcels_staging
        where blk_lot = '0000000'
    )
    where blk_lot = '0000000';

    
    -- This validation flags addresses that are linked to retired parcels.
    perform _eas_validate_addresses_after_etl('PARCELS');


    -- mark parcel provisioning rows
    update parcels_provisioning
    set provisioned_tms = now()
    where parcel_id in (
        select parcel_id
        from
            parcels p,
            parcels_work pw
        where p.blk_lot = pw.blk_lot
        and pw.status = 'UPDATED'
    );


    -- Record bounding boxes for map cache refresh.
    -- We do not use these as yet.
    -- It is part of a tile cache refesh optimization.
    -- But there is an issue:
    -- http://code.google.com/p/eas/issues/detail?id=179&q=geoserver
    /*
    insert into etl_bboxes (tms, geometry)
    select distinct now(), st_envelope(geometry)
    from parcels_work
    where geometry is not null;
    */

    success := true;
    messageText := 'Parcel ETL Successful.';
    return;

END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION _sfmad_etl_parcels(
  _job_uuid varchar(36),
  out success boolean,
  out messageText text
)
OWNER TO postgres;
