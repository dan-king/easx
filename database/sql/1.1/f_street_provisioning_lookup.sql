/*

select _eas_street_provisioning_lookup();

select * from address_base_street_provisioning;

select * from vw_street_segments_map limit 10;

address_base_id,provisioned_street_segment_id,dpw_street_segment_id
440075;18910;18936
440074;18910;18936

-- update statements
update address_base set street_segment_id = 18943 where address_base_id = 482990;
update address_review set resolution_tms = '2012-04-09 08:44:42.262829' where address_id = 724305 and review_type_id = 5;

*/

-- DROP FUNCTION _eas_street_provisioning_lookup();

CREATE OR REPLACE FUNCTION _eas_street_provisioning_lookup() RETURNS text AS
$BODY$
DECLARE 

    _address_base_id                int;
    _address_id                     int;
    _provisioned_street_segment_id  int;
    _provisioned_street_name        character varying(60);
    _provisioned_street_type        character varying(6);
    _provisioned_geometry           geometry;
    _dpw_street_segment_id          integer;
    _diff_metric                    integer;
    _now                            timestamp without time zone = now();
    _ADDRESS_INVALID_TYPE_STREET_RETIRED int = 5;

BEGIN

    truncate table address_base_street_provisioning;

    -- Insert base addresses that are using provisioned streets.
    -- We may find that we want to exclude retired base addresses in which case we will add this to the where clause: and ab.retire_tms is null
    insert into address_base_street_provisioning (
        address_base_id, 
        address_id, 
        provisioned_street_segment_id, 
        provisioned_street_name, 
        provisioned_street_type, 
        provisioned_street_geometry
    )
    select 
        ab.address_base_id,
        a.address_id,
        ss.street_segment_id,
        ss.base_street_name,
        ss.street_type,
        ss.geometry
    from vw_street_segments_map ss
    join address_base ab on ab.street_segment_id = ss.street_segment_id
    join addresses a on ab.address_base_id = a.address_base_id
    where ss.seg_cnn > 99000000
    and a.address_base_flg = true
    and ab.retire_tms is null;


    -- For each of these base addresses, find the nearest BSM/DPW street segment.
    declare cursorProvisionedStreet cursor for
        select address_base_id, address_id, provisioned_street_segment_id, provisioned_street_name, provisioned_street_type, provisioned_street_geometry from address_base_street_provisioning;
    begin
        open cursorProvisionedStreet;
        loop
            fetch cursorProvisionedStreet into _address_base_id, _address_id, _provisioned_street_segment_id, _provisioned_street_name, _provisioned_street_type, _provisioned_geometry;
            if not found then
                exit;
            end if;

            -- Find the best matching DPW street.
            select street_segment_id, st_distance(st_centroid(geometry), st_centroid(_provisioned_geometry))::int into _dpw_street_segment_id, _diff_metric
            from vw_street_segments_map
            where seg_cnn < 99000000
            and st_dwithin(st_centroid(geometry), st_centroid(_provisioned_geometry), 100)
            and base_street_name = _provisioned_street_name
            and coalesce(street_type, '') = coalesce(_provisioned_street_type, '')
            and date_dropped is null
            order by st_distance(st_centroid(geometry), st_centroid(_provisioned_geometry)) asc
            limit 1;
    
            update address_base_street_provisioning
            set dpw_street_segment_id = _dpw_street_segment_id,
                diff_metric = _diff_metric, 
                sql_update_street_segment = 'update address_base set street_segment_id = ' || _dpw_street_segment_id::text || ' where address_base_id = ' || _address_base_id::text || ';',
                sql_update_invalid_address = $$update address_review set resolution_tms = '$$ || _now::text || $$' where address_id = $$ || _address_id::text || ' and review_type_id = ' || _ADDRESS_INVALID_TYPE_STREET_RETIRED::text || ';'
            where address_base_id = _address_base_id
            and provisioned_street_segment_id = _provisioned_street_segment_id;

        end loop;
        close cursorProvisionedStreet;


    end;

    return 'SUCCESS!';

END;

$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

ALTER FUNCTION _eas_street_provisioning_lookup() OWNER TO eas_dbo;
