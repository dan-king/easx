
BEGIN;

LOCK TABLE cr_address_base, address_base IN ACCESS EXCLUSIVE MODE NOWAIT;

alter table address_base disable trigger _eas_address_base_before;
alter table address_base disable trigger _eas_address_base_after;

update cr_address_base set base_address_num = 999999 where base_address_num > 999999;

update address_base set base_address_num = 999999 where base_address_num > 999999;

/*
alter table cr_address_base
    DROP CONSTRAINT cr_address_base__base_address_num_cc;
*/
alter table cr_address_base
    add CONSTRAINT cr_address_base__base_address_num_cc CHECK (base_address_num between 0 and 999999);

/*
alter table address_base
    DROP CONSTRAINT address_base__base_address_num_cc;
*/
alter table address_base
    add CONSTRAINT address_base__base_address_num_cc CHECK (base_address_num between 0 and 999999);


alter table address_base enable trigger _eas_address_base_before;
alter table address_base enable trigger _eas_address_base_after;

COMMIT;