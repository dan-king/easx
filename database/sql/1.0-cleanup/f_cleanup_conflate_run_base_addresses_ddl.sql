
create or replace function _eas_cleanup_conflate_run_base_addresses()
  returns text as
$body$

declare

    _address_number     int;
    _street_name        character varying(60);
    _street_suffix      character varying(6);
    _geometry           geometry;
    _address_base_ids   int[];
    _address_base_id    int;
    _unit_count         int;
    _base_count         int;
    _conflation_count   int;
    _current_tms        timestamp without time zone = now();
    _base_count_threshold int = 4;
    _message            character varying(256);
    _index              int;

begin

    declare cursorBaseAddressToConflate cursor is
        select
            ccba.base_address_num,
            ccba.base_street_name,
            ccba.street_type,
            ccba.geometry,
            ARRAY(
                select address_base_id
                from cleanup_conflate_base_addresses ccba2
                where ccba.base_address_num = ccba2.base_address_num
                and ccba.base_street_name = ccba2.base_street_name
                and ccba.street_type = ccba2.street_type
                and ccba.geometry = ccba2.geometry
            ),
            count(*)
        from cleanup_conflate_base_addresses ccba
        where processed_flg = false
        group by
            ccba.base_address_num,
            ccba.base_street_name,
            ccba.street_type,
            ccba.geometry
        order by ccba.base_address_num, ccba.base_street_name, ccba.street_type;
    begin
        open cursorBaseAddressToConflate;
        loop
            fetch cursorBaseAddressToConflate into _address_number, _street_name, _street_suffix, _geometry, _address_base_ids, _base_count;
            if not found then
                exit;
            end if;
            begin

                if _base_count <= _base_count_threshold then

                    -- If there are fewer than an arbitrarily specified number of base addresses we do nothing.
                    -- We do this because an apartment or condo building with 4 exterior doors is common; one with 150 exterior doors is uncommon.

                    update cleanup_conflate_base_addresses
                    set message = 'below threshold - skipping',
                        processed_flg = true
                    where base_address_num = _address_number
                    and base_street_name = _street_name
                    and street_type = _street_suffix;

                    continue;

                end if;

                ----- If there units under any of the base addresses, skip. 
                select count(*) into _unit_count
                from addresses 
                where 1 = 1 
                and address_base_flg = false
                and address_base_id in (select _eas_array_to_series(_address_base_ids));
                
                if _unit_count != 0 then

                    _message = 'found unit addresses in this set  - skipping - unit count: ' || _unit_count::text;

                    update cleanup_conflate_base_addresses
                    set message = _message,
                        processed_flg = true
                    where base_address_num = _address_number
                    and base_street_name = _street_name
                    and street_type = _street_suffix;

                    continue;

                end if;
                -----

                -- We expect _eas_conflate_base_address to be used beyond being called from here.
                -- Therefore, we use the more general and user friendly signature:
                --      (_address_number, _street_name, _street_suffix, _geometry)
                -- rather than:
                --      (_address_base_ids)

                select into _conflation_count _eas_conflate_base_address(_address_number, _street_name, _street_suffix, _geometry, _current_tms);

                for _index in array_lower(_address_base_ids, 1)..array_upper(_address_base_ids, 1) loop
                    begin
                        
                        _address_base_id = _address_base_ids[_index];
                        
                        perform _eas_retire_base_address(_address_base_id, _current_tms);

                        update cleanup_conflate_base_addresses
                        set message = 'conflate and retire',
                            processed_flg = true
                        where base_address_num = _address_number
                        and base_street_name = _street_name
                        and street_type = _street_suffix;

                    end;
                end loop;

            end;
        end loop;
        close cursorBaseAddressToConflate;
    end;


    return 'success!';


end;
$body$
  language 'plpgsql' volatile strict
  cost 100;
alter function _eas_cleanup_conflate_run_base_addresses() owner to postgres;
