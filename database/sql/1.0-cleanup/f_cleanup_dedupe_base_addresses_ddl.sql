
create or replace function _eas_cleanup_dedupe_base_addresses()
  returns text as
$body$

declare

    _full_addr          character varying(100);
    _address_base_ids   int[];
    _address_base_id    int;
    _current_tms        timestamp without time zone = now();

begin

    --in flag field:
    -- -1: human needed
    -- 0: not processed
    -- 1: processed, keep
    -- 2: processed, retire

    declare cursorBaseAddressDupes cursor is
        select distinct(full_addr)
        from cleanup_address_base_dupes;

    begin
        open cursorBaseAddressDupes;
        loop
            fetch cursorBaseAddressDupes into _full_addr;
            if not found then
                exit;
            end if;
            begin


                ----- If there is a base address that has more units than any other, choose it.
                select into _address_base_ids ARRAY(
                    select distinct address_base_id
                    from cleanup_address_base_dupes
                    where full_addr = _full_addr
                    and addresses_records_cnt = (
                        select max(addresses_records_cnt)
                        from cleanup_address_base_dupes
                        where full_addr = _full_addr
                    )
                );

                if array_upper(_address_base_ids, 1) = 1 then

                    update cleanup_address_base_dupes
                    set flag = '1'
                    where full_addr = _full_addr
                    and address_base_id = _address_base_ids[1];

                    update cleanup_address_base_dupes
                    set flag = '2'
                    where full_addr = _full_addr
                    and address_base_id != _address_base_ids[1];

                    continue;

                end if;
                -----


                ----- If there is a base address that has more AXPs than any other, choose it.
                -- Note: axp_records_cnt should be the AXP count for the base address plus all of the unit addresses
                select into _address_base_ids ARRAY(
                    select distinct address_base_id
                    from cleanup_address_base_dupes
                    where full_addr = _full_addr
                    and axp_records_cnt = (
                        select max(axp_records_cnt)
                        from cleanup_address_base_dupes
                        where full_addr = _full_addr
                    )
                );

                if array_upper(_address_base_ids, 1) = 1 then

                    update cleanup_address_base_dupes
                    set flag = '1'
                    where full_addr = _full_addr
                    and address_base_id = _address_base_ids[1];

                    update cleanup_address_base_dupes
                    set flag = '2'
                    where full_addr = _full_addr
                    and address_base_id != _address_base_ids[1];

                    continue;

                end if;
                -----


                ----- If there is a base address that has a lower distance to street segment than any other, choose it.
                select into _address_base_ids ARRAY(
                    select distinct address_base_id
                    from cleanup_address_base_dupes
                    where full_addr = _full_addr
                    and dist_to_segment = (
                        select min(dist_to_segment)
                        from cleanup_address_base_dupes
                        where full_addr = _full_addr
                    )
                );

                if array_upper(_address_base_ids, 1) = 1 then

                    update cleanup_address_base_dupes
                    set flag = '1'
                    where full_addr = _full_addr
                    and address_base_id = _address_base_ids[1];

                    update cleanup_address_base_dupes
                    set flag = '2'
                    where full_addr = _full_addr
                    and address_base_id != _address_base_ids[1];

                    continue;

                end if;
                -----


                ----- For the few remaining duplicates, it appears that we can arbitrarily keep the first and retire the others.
                select into _address_base_ids ARRAY(
                    select distinct address_base_id
                    from cleanup_address_base_dupes
                    where full_addr = _full_addr
                );

                update cleanup_address_base_dupes
                set flag = '1'
                where full_addr = _full_addr
                and address_base_id = _address_base_ids[1];

                update cleanup_address_base_dupes
                set flag = '2'
                where full_addr = _full_addr
                and address_base_id != _address_base_ids[1];
                -----


            end;
        end loop;
        close cursorBaseAddressDupes;
    end;



    declare cursorBaseAddressRetire cursor is
        select address_base_id
        from cleanup_address_base_dupes
        where flag = '2';

    begin
        open cursorBaseAddressRetire;
        loop
            fetch cursorBaseAddressRetire into _address_base_id;
            if not found then
                exit;
            end if;
            begin
    
                perform _eas_retire_base_address(_address_base_id, _current_tms);

            end;
        end loop;
        close cursorBaseAddressRetire;
    end;
    -----


    return 'success!';


end;
$body$
  language 'plpgsql' volatile strict
  cost 100;
alter function _eas_cleanup_dedupe_base_addresses() owner to postgres;
