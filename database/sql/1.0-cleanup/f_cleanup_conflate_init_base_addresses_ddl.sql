
create or replace function _eas_cleanup_conflate_init_base_addresses()
  returns void as
$body$

declare

begin

    insert into cleanup_conflate_base_addresses (
      address_base_id,
      base_address_prefix,
      base_address_num,
      base_address_suffix,
      geometry,
      base_street_name,
      street_type,
      message,
      processed_flg
    )
    select 
        ab.address_base_id, 
        ab.base_address_prefix, 
        ab.base_address_num, 
        ab.base_address_suffix, 
        ab.geometry,
        sn.base_street_name,
        sn.street_type,
        ''::character varying(256),
        false
    from
        address_base ab,
        addresses a,
        street_segments ss,
        streetnames sn
    where ab.address_base_id = a.address_base_id
    and a.address_base_flg = true
    and ab.retire_tms is null
    and a.retire_tms is null
    and ss.street_segment_id = ab.street_segment_id
    and ss.street_segment_id = sn.street_segment_id
    and sn.category = 'MAP'
    and ab.address_base_id not in (
        select address_base_id
        from cleanup_conflate_base_addresses
        where processed_flg = false
    )
    and exists ( 
        select 1
        from
            address_base ab1,
            street_segments ss1,
            streetnames sn1
        where ab1.street_segment_id = ss1.street_segment_id
        and ss1.street_segment_id = sn1.street_segment_id
        and ab1.address_base_id != ab.address_base_id
        and ab1.base_address_num = ab.base_address_num
        and ab1.geometry = ab.geometry
        and ab1.base_address_suffix != ab.base_address_suffix
        and sn1.base_street_name = sn.base_street_name
        and sn1.street_type = sn.street_type
    )
    order by sn.full_street_name, ab.base_address_num, ab.base_address_suffix;


end;
$body$
  language 'plpgsql' volatile strict
  cost 100;
alter function _eas_cleanup_conflate_init_base_addresses() owner to postgres;
