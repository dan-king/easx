
/*

select * 
from cleanup_address_base_dupes 
--where flag = '0'
order by full_addr;

*/

-- select _eas_cleanup_dedupe_base_addresses();


SELECT                  -- A flattened view of addresses.
	ab.address_base_id,
	a.address_base_flg,
    ab.unq_adds_id,
	a.address_id,
    a.unq_adds_id,
	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num,
	sn.base_street_name,
    sn.street_type,
    p.blk_lot,
    p.parcel_id,
	ab.create_tms as ab_create_tms,
	a.create_tms as a_create_tms,
    axp.create_tms as axp_create_tms,
	ab.retire_tms as ab_retire_tms,
	a.retire_tms as a_retire_tms,
    axp.retire_tms as axp_retire_tms,
    ab.street_segment_id,
    '-----',
    cabd.*
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
inner join cleanup_address_base_dupes cabd on cabd.address_base_id = a.address_base_id
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id)
WHERE 1 = 1
and sn.category = 'MAP'
order by
	ab.base_address_num,
	ss.st_name,
	ss.st_type,
	a.address_base_flg desc,
	ab.base_address_suffix,
	a.unit_num;


/* should be zero afterwards


*/
