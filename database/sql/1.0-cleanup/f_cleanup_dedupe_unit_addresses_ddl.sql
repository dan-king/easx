
create or replace function _eas_cleanup_dedupe_unit_addresses()
  returns text as
$body$

declare

    _unq_unit_addr character varying(100);
    _address_ids   int[];
    _address_id    int;
    _current_tms   timestamp without time zone = now();

begin

    --in flag field:
    -- -1: human needed
    -- 0: not processed
    -- 1: processed, keep
    -- 2: processed, retire

    declare cursorUnitAddressDupes cursor is
        select distinct(unq_unit_addr)
        from cleanup_address_unit_dupes;

    begin
        open cursorUnitAddressDupes;
        loop
            fetch cursorUnitAddressDupes into _unq_unit_addr;
            if not found then
                exit;
            end if;
            begin


                ----- If there is a unit address that has more AXPs than any other, choose it.
                select into _address_ids ARRAY(
                    select distinct address_id
                    from cleanup_address_unit_dupes
                    where unq_unit_addr = _unq_unit_addr
                    and axp_records_cnt = (
                        select max(axp_records_cnt)
                        from cleanup_address_unit_dupes
                        where unq_unit_addr = _unq_unit_addr
                    )
                );

                if array_upper(_address_ids, 1) = 1 then

                    update cleanup_address_unit_dupes
                    set flag = '1'
                    where unq_unit_addr = _unq_unit_addr
                    and address_id = _address_ids[1];

                    update cleanup_address_unit_dupes
                    set flag = '2'
                    where unq_unit_addr = _unq_unit_addr
                    and address_id != _address_ids[1];

                    continue;

                end if;
                -----


                ----- For the remaining duplicates, it appears that we can arbitrarily keep the first and retire the others.
                select into _address_ids ARRAY(
                    select distinct address_id
                    from cleanup_address_unit_dupes
                    where unq_unit_addr = _unq_unit_addr
                );

                update cleanup_address_unit_dupes
                set flag = '1'
                where unq_unit_addr = _unq_unit_addr
                and address_id = _address_ids[1];

                update cleanup_address_unit_dupes
                set flag = '2'
                where unq_unit_addr = _unq_unit_addr
                and address_id != _address_ids[1];
                -----


            end;
        end loop;
        close cursorUnitAddressDupes;
    end;



    declare cursorUnitAddressRetire cursor is
        select address_id
        from cleanup_address_unit_dupes
        where flag = '2';

    begin
        open cursorUnitAddressRetire;
        loop
            fetch cursorUnitAddressRetire into _address_id;
            if not found then
                exit;
            end if;
            begin
    
                perform _eas_retire_unit_address(_address_id, _current_tms);

            end;
        end loop;
        close cursorUnitAddressRetire;
    end;


    return 'success!';


end;
$body$
  language 'plpgsql' volatile strict
  cost 100;
alter function _eas_cleanup_dedupe_unit_addresses() owner to postgres;
