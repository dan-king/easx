-- Function: _eas_address_x_parcels_before()

-- DROP FUNCTION _eas_address_x_parcels_before();

CREATE OR REPLACE FUNCTION _eas_address_x_parcels_before()
  RETURNS trigger AS
$BODY$
DECLARE
    _address_number integer;
    _full_street_name character varying(64);
    _block_lot character varying(10);
    _block_num character varying(10);
    _block_nums character varying(64);
    _distance int;
    _message character varying(255);
BEGIN

    -- All the constraints in one place.

    IF (TG_OP = 'DELETE') THEN
        RAISE EXCEPTION 'delete rejected - address_x_parcels id=% - deletes are not allowed', OLD.id;
    ELSIF (TG_OP = 'UPDATE') THEN

        IF NEW.retire_tms IS NULL THEN
            RAISE EXCEPTION 'update rejected  - address_x_parcels id=% - retire_tms must not be null', NEW.id;
        END IF;
        IF NEW.retire_change_request_id IS NULL THEN
            RAISE EXCEPTION 'update rejected  - address_x_parcels id=% - retire_change_request_id must not be null', NEW.id;
        END IF;
        IF NEW.create_tms > NEW.retire_tms THEN
            RAISE EXCEPTION 'update rejected - address_x_parcels id=% - create_tms is greater than retire_tms', NEW.id;
        END IF;

    ELSE
        -- INSERT

        IF (NEW.retire_tms IS NOT NULL) THEN
            RAISE EXCEPTION 'insert rejected - address_x_parcels - retire_tms must be null';
        end if;

        -- Do not allow multiple live rows.
        perform 1
        from address_x_parcels
        where address_id = NEW.address_id
        and parcel_id = NEW.parcel_id
        and retire_tms is null;

        IF FOUND THEN
            RAISE EXCEPTION 'insert rejected - address_x_parcels - existing active row was found';
        END IF;

        -- Do not allow overlapping date ranges.
        perform 1
        from address_x_parcels
        where address_id = NEW.address_id
        and parcel_id = NEW.parcel_id
        and retire_tms > NEW.create_tms;

        IF FOUND THEN
            RAISE EXCEPTION 'insert rejected - address_x_parcels - overlapping date range';
        END IF;

        -- Allow linking to a parcel only if the parcel is within 500 feet of the address point
        -- and the APN of the parcel is in the same block as the parcel(s) under the address point.

        -- Performance matters!

        -- This will be our path 9999 times out of 10000. It's also fastest.
        -- If the parcel contains the address point all other constraints should also be met.
        perform 1
        from
           address_base ab,
           addresses a,
           parcels p
        where a.address_id = NEW.address_id
        and a.address_base_id = ab.address_base_id
        and NEW.parcel_id = p.parcel_id
        and contains(p.geometry, ab.geometry);

        if FOUND then
            RETURN NEW;
        end if;
        --


        perform 1
        from
            address_base ab,
            addresses a,
            parcels p1
        where a.address_id = NEW.address_id
        and a.address_base_id = ab.address_base_id
        and NEW.parcel_id = p1.parcel_id
        and st_distance(p1.geometry, ab.geometry) <= 500.0
        and (p1.block_num in (
            select distinct p2.block_num
                from parcels p2
                where st_contains(p2.geometry, ab.geometry)) 
            or p1.block_num in (
                select p3.block_num from (
                    select (st_distance(geometry, ab.geometry)::int) as "distance", p2.block_num
                        from parcels p2
                        where ((ST_DWithin(p2.geometry, ab.geometry, 500)) and NOT (p2.block_num = '0000')) order by "distance" asc limit 1
                ) as p3
            )
        or p1.block_num = '0000');


        if not FOUND then
            ----- Gather common information for reporting to user.
            select ab.base_address_num, sn.full_street_name, blk_lot, block_num, st_distance(p1.geometry, ab.geometry)::int
            into _address_number, _full_street_name, _block_lot, _block_num, _distance
            from
                address_base ab,
                addresses a, 
                streetnames sn,
                parcels p1
            where a.address_id = NEW.address_id
            and a.address_base_id = ab.address_base_id
            and ab.street_segment_id = sn.street_segment_id
            and sn.category = 'MAP'
            and p1.parcel_id = NEW.parcel_id;
            -----


            ----- check block num
            perform 1
            from
                address_base ab,
                addresses a,
                parcels p1
            where a.address_id = NEW.address_id
            and a.address_base_id = ab.address_base_id
            and NEW.parcel_id = p1.parcel_id
            and (p1.block_num not in (
                select distinct p2.block_num
                    from parcels p2
                    where st_contains(p2.geometry, ab.geometry)) 
                or p1.block_num not in (
                    select p3.block_num from (
                        select (st_distance(geometry, ab.geometry)::int) as "distance", p2.block_num
                            from parcels p2
                            where ((ST_DWithin(p2.geometry, ab.geometry, 500)) and NOT (p2.block_num = '0000')) order by "distance" asc limit 1
                    ) as p3
                )
                or p1.block_num != '0000');

            -- It's less confusing to interpret the results of the exception report if this exception precedes distance exception.
            -- Why? It's mostly because addresses can be retired after they are loaded and we can't (yet) see retired addresses in the UI.
            IF FOUND THEN
                select string_agg(distinct p1.block_num, ', ') into _block_nums
                from
                    address_base ab,
                    addresses a,
                    parcels p1
                where a.address_id = NEW.address_id
                and a.address_base_id = ab.address_base_id
                and st_contains(p1.geometry, ab.geometry);

                _message = 'ADDRESS: ' || _address_number::text || ' ' || _full_street_name || ' APN: ' || _block_lot || ' BLOCK_NUMs: ' || _block_nums;
                RAISE EXCEPTION 'insert rejected - address_x_parcels - the parcel is not within the block number range: %', _message;
            END IF;
            -----

            IF _distance > 500.0 THEN
                _message = 'ADDRESS: ' || _address_number::text || ' ' || _full_street_name || ' APN: ' || _block_lot || ' DISTANCE: ' || _distance;
                RAISE EXCEPTION 'insert rejected - address_x_parcels - the parcel is not within within 500 feet of the address: %', _message;
            END IF;

            _message = 'ADDRESS: ' || _address_number::text || ' ' || _full_street_name || ' APN: ' || _block_lot;
            RAISE EXCEPTION 'insert rejected - address_x_parcels - dang - looks like there is a flaw in our validation process %', _message;

        END IF;

        
    END IF;


    RETURN NEW;

  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _eas_address_x_parcels_before()
  OWNER TO eas_dbo;