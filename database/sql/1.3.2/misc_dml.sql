INSERT INTO address_change_notification_history (last_notification_tms) VALUES (now());

UPDATE parcels_provisioning pp 
SET 
	suspended_comment = 'CCNR''s were never recorded',
	suspended_tms = now()
FROM parcels p 
WHERE p.blk_lot IN ('1364075', '1364076', '1364077', '1364078', '1364079', '1364080', '1364081') 
AND pp.parcel_id = p.parcel_id;

UPDATE parcels 
SET date_map_drop = now() 
WHERE blk_lot IN ('1364075', '1364076', '1364077', '1364078', '1364079', '1364080', '1364081');
