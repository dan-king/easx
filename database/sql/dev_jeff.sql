-- Tests whether records have been altered, dropped or added to DPW's parcel and street spatial data.
DECLARE @tms smalldatetime;
SET @tms = '2010-04-07 16:29:43';
SELECT count(*)
FROM [sde].[dtisgis].[sfgis_Mstr_Lots]
    where dateMAP_ALT >= @tms
          or dateMap_DROP  >= @tms
          or  dateMap_ADD >=  @tms;
SELECT count(*)
FROM [sde].dtisgis.sfgis_All_Stclines_Mstr
    where DATE_ALTERED >= @tms
          or DATE_DROPPED  >= @tms
          or   DATE_ADDED >=  @tms;
SELECT count(*)
FROM [sde].dtisgis.sfgis_All_Nodes_Mstr
    where DATE_ALTERED >= @tms
          or DATE_DROPPED  >= @tms
          or   DATE_ADDED >=  @tms;

/*

select count(*)
from "SFGIS"."BASEMAP_STCLINES_ARTERIALS_STAGING";
select count(*)
from "SFGIS"."BASEMAP_STCLINES_CITY_STAGING";
select count(*)
from "SFGIS"."BASEMAP_STCLINES_FREEWAY_STAGING";
select count(*)
from"SFGIS"."BASEMAP_STCLINES_HIGHWAY_STAGING";
select count(*)
from "SFGIS"."BASEMAP_STCLINES_MAJOR_STAGING";
select count(*)
from "SFGIS"."BASEMAP_STCLINES_MINOR_STAGING";
select count(*)
from "SFGIS"."BASEMAP_STCLINES_NON_PAPER_STAGING";
select count(*)
from "SFGIS"."BASEMAP_STCLINES_OTHER_STAGING";
select count(*)
from "SFGIS"."BASEMAP_STCLINES_RAMP_STAGING";
select count(*)
from SFGIS.STREETNAMES_STAGING;
select count(*)
from SFGIS.street_segments_staging;
select count(*)
from sfgis.baseclines_not_in_all_stclines_mstr
/*
SFGIS.STREETNAMES_STAGING                                                19172
SFGIS.basemap_stclines_arterials_staging                                  2612
SFGIS.basemap_stclines_city_staging                                      10845
SFGIS.basemap_stclines_freeway_staging                                      98
SFGIS.basemap_stclines_highway_staging                                     203
SFGIS.basemap_stclines_major_staging                                      1537
SFGIS.basemap_stclines_minor_staging                                      2392
SFGIS.basemap_stclines_non_paper_staging                                 15188
SFGIS.basemap_stclines_other_staging                                      1164
SFGIS.basemap_stclines_ramp_staging                                        113
SFGIS.street_segments_staging                                            16675
sfgis.baseclines_not_in_all_stclines_mstr                                    4
*/


Select ADDR_STREETSUFFIX, COUNT (ADDR_STREETSUFFIX) c
from unq_adds
group by ADDR_STREETSUFFIX
GO

Select STREETTYPE, COUNT (STREETTYPE) c
from TBL_STREETS_ALL
group by STREETTYPE



Select ADDR_STREETSUFFIX, COUNT (ADDR_STREETSUFFIX) c
from unq_adds

where
ADDR_STREETSUFFIX IN
('CENTER',
'HILLS',
'LANDING',
'NORTH',
'PARKS',
'PARKWAY',
'SOUTH',
'UNION',
'WALKS')
group by ADDR_STREETSUFFIX
