
            select  abh.id, null, null, abh.last_change_tms
            from address_base_history abh
            where 1=1--abh.history_action in ('insert', 'update', 'retire')
            and not exists(
                select 1
                from addresses_history ah
                where abh.address_base_id = ah.address_base_id
                and abh.last_change_tms = ah.last_change_tms
                and ah.history_action in ('insert', 'update', 'retire')
            )
            and not exists (
                select 1
                from addresses_history ah,
                address_x_parcels_history axph
                where abh.address_base_id = ah.address_base_id
                and abh.last_change_tms = ah.last_change_tms
                and ah.address_id = axph.address_id
                and axph.last_change_tms = ah.last_change_tms
            )

select polygon '((0,0),(1,1))' ~= polygon '((1,2),(0,0))'
select asewkt(abh1.geometry), asewkt(abh2.geometry)
select abh1.geometry <-> abh2.geometry
select abh1.geometry ~= abh2.geometry
from address_base_history abh1, address_base_history abh2
where abh1.id = 11 and abh2.id = 11


truncate xmit_queue;
truncate address_x_parcels_history cascade;
truncate addresses_history cascade;
truncate address_base_history cascade;

select * from xmit_queue
select * from address_base_history;
select * from addresses_history;
select * from address_x_parcels_history;

truncate xmit_queue

select * from xmit_queue 
where xmit_tms is null
order by last_change_tms

----- base address changes that have no unit address changes and have no APX changes
SELECT  abh.id, null, null, abh.last_change_tms
FROM address_base_history abh
where not exists(
	select 1
	from addresses_history ah
	where abh.address_base_id = ah.address_base_id 
	and abh.last_change_tms = ah.last_change_tms
	and ah.history_action in ('insert', 'update', 'retire')
)
and not exists (
	select 1
	from addresses_history ah,
	address_x_parcels_history axph
	where abh.address_base_id = ah.address_base_id 
	and abh.last_change_tms = ah.last_change_tms
	and ah.address_id = axph.address_id
	and axph.last_change_tms = ah.last_change_tms
)
AND NOT EXISTS (
	SELECT  1
	FROM    xmit_queue xq
	WHERE 	xq.address_base_history_id = abh.id
	AND     xq.last_change_tms = abh.last_change_tms
	and 	xq.address_history_id is null
	and 	xq.address_x_parcel_history_id is null
);


----- unit address changes that have no APX changes
SELECT  abh.id, ah.id, null, abh.last_change_tms
FROM address_base_history abh
INNER JOIN addresses_history ah ON (abh.address_base_id = ah.address_base_id and abh.last_change_tms = ah.last_change_tms) 
WHERE ah.history_action in ('insert', 'update', 'retire')
and not exists (
	select 1
	from address_x_parcels_history axph
	where ah.address_id = axph.address_id
	and axph.last_change_tms = ah.last_change_tms
)
AND NOT EXISTS (
	SELECT  1
	FROM    xmit_queue xq
	WHERE 	xq.address_base_history_id = abh.id
	AND     xq.last_change_tms = abh.last_change_tms
	and 	xq.address_history_id = ah.id
	and 	xq.address_x_parcel_history_id is null
);


----- APX changes
SELECT abh.id, ah.id, axph.id, abh.last_change_tms
FROM address_base_history abh
INNER JOIN addresses_history ah ON (abh.address_base_id = ah.address_base_id and abh.last_change_tms = ah.last_change_tms) 
INNER JOIN address_x_parcels_history axph ON (ah.address_id = axph.address_id and ah.last_change_tms = axph.last_change_tms)
AND NOT EXISTS (
	SELECT  1
	FROM    xmit_queue xq
	WHERE 	xq.address_base_history_id = abh.id
	AND     xq.last_change_tms = abh.last_change_tms
	and 	xq.address_history_id = ah.id
	and 	xq.address_x_parcel_history_id = axph.id
);





DROP FUNCTION _sfmad_address_base_history() cascade;
DROP FUNCTION _sfmad_addresses_history() cascade;
DROP FUNCTION _sfmad_address_x_parcels_history() cascade;


truncate address_base_history;
truncate addresses_history;
truncate address_x_parcels_history;



-- select changes that have occurred in the unit address itself, or in links between the unit address and parcels
	SELECT  ah.address_id,
		ah.id as address_history_id,
		ah.history_action as address_history_action,
		axph.address_x_parcel_id as address_x_parcel_id,
		axph.id as address_x_parcel_history_id,
		axph.history_action as axph_action,
		ah.last_change_tms
    FROM    addresses_history ah
    LEFT OUTER JOIN address_x_parcels_history axph ON (ah.address_id = axph.address_id and ah.last_change_tms = axph.last_change_tms)
    where (ah.history_action in ('insert', 'update', 'retire') or axph.id is not null)
    AND NOT EXISTS (
	SELECT  1
	FROM    xmit_queue xq
	WHERE 	xq.address_history_id = ah.id
	AND     COALESCE(xq.address_x_parcel_history_id, 0) = COALESCE(axph.id, 0)
	AND     xq.last_change_tms = ah.last_change_tms
    );

SELECT * FROM addresses_history;
SELECT * FROM address_x_parcels_history;
