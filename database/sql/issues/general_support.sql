﻿
select count(*) from avs.avs_addresses where exception_text is not null;

select * from avs.avs_addresses where block = '3723' and lot = '001';

select * from parcels where block_num = '3723' and lot_num = '001';


SELECT                  -- A flattened view of addresses.
	ab.address_base_id,
    ab.unq_adds_id,
	a.address_id,
    a.unq_adds_id,
	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num,
	sn.base_street_name,
    sn.street_type,
    p.blk_lot,
    p.parcel_id,
	ab.create_tms as ab_create_tms,
	a.create_tms as a_create_tms,
    axp.create_tms as axp_create_tms,
	ab.retire_tms as ab_retire_tms,
	a.retire_tms as a_retire_tms,
    axp.retire_tms as axp_retire_tms,
    ab.street_segment_id,
    ab.zone_id
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id)
WHERE 1 = 1
and sn.category = 'MAP'
--and a.unit_num is not null
--and a.retire_tms is null
and sn.base_street_name = ( 'CLAY')
and ab.base_address_num = 1037
--and a.address_id = 276516
-- and ab.address_base_id = 21962
--and ab.unq_adds_id = 200095
--and p.blk_lot = '0052119'
--and a.unit_num = '103'
order by
	ss.st_name,
	ss.st_type,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num
limit 1000;
