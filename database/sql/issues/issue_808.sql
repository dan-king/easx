﻿
select ss.seg_cnn, ss.st_name, ss.st_type, '<<<>>>', sss.st_name, sss.st_type
from street_segments_staging sss, 
street_segments ss
where ss.seg_cnn = sss.seg_cnn
and ss.st_name <> sss.st_name
--or (ss.st_name = sss.st_name and ss.st_type <> sss.st_type)
order by ss.st_name, ss.st_type



select ss.seg_cnn, ss.st_name, ss.st_type, ST_equals(ss.geometry, sss.geometry), sss.st_name, sss.st_type
from street_segments_staging sss, 
street_segments ss
where ss.seg_cnn = sss.seg_cnn
and ST_equals(ss.geometry, sss.geometry)


select ss.seg_cnn, ss.st_name, ss.st_type, ST_HausdorffDistance(ss.geometry, sss.geometry), sss.st_name, sss.st_type
from street_segments_staging sss, 
street_segments ss
where ss.seg_cnn = sss.seg_cnn
order by ST_HausdorffDistance(ss.geometry, sss.geometry) desc
limit 1000


select ss.seg_cnn, ss.st_name, ss.st_type
from street_segments ss
where not ST_isvalid(ss.geometry)

select 
	p.blk_lot, 
	ST_HausdorffDistance(p.geometry, ps.geometry)
from 
	parcels_staging ps, 
	parcels p
where p.blk_lot = ps.blk_lot
and p.block_num != '0000'
and p.geometry is not null 
and ps.geometry is not null
order by ST_HausdorffDistance(p.geometry, ps.geometry) desc
limit 10000




select p.blk_lot
from parcels p
where p.block_num = '0000'

select *
from parcels
where geometry is null 
and  blk_lot in (
'3752129',
'0710C061',
'4104018',
'1065033',
'4228002',
'1269081',
'0753004',
'1561012',
'0067004',
'1152004'
)
