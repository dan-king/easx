-- RE http://code.google.com/p/eas/issues/detail?id=343

-- 4 == ADDRESS_NOT_INSIDE_PARCEL

insert into address_review (address_id, review_type_id, message)
select distinct a1.address_id, 4, 'apn:' || p1.blk_lot
from
    address_base ab1,
    addresses a1,
    address_x_parcels axp1,
    parcels p1
where ab1.address_base_id = a1.address_base_id
and ab1.retire_tms is null
and a1.retire_tms is null
and a1.address_id = axp1.address_id
and axp1.parcel_id = p1.parcel_id
and axp1.retire_tms is null
and p1.date_map_drop is null
and not exists (
    select 1
    from
        parcels p2,
        address_base ab2,
        addresses a2
    where contains(p2.geometry, ab2.geometry)
    and ab2.address_base_id = ab1.address_base_id
    and a2.address_id = a1.address_id
    and p2.blk_lot = p1.blk_lot
)
and not exists (
    select 1
    from address_review ar
    where ar.address_id = a1.address_id
    and ar.review_type_id = 4
)
and 'PARCELS' in ('PARCELS', 'ALL')

