
-- n=333236
select count(*) from avs.avs_addresses

/*
34218;"ALTERNATE"
20383;"ALIAS"
2;"INVALID"
278099;"PRIMARY"
534;"ADDITIONAL"
*/
select count(*), address_kind
from avs.avs_addresses
where 1=1
group by address_kind;


-- primary addresses that have no other kinds of addresses 
-- n=220573
select *
from avs.avs_addresses
where 1=1
and address_kind = 'PRIMARY'
and blocklot not in (
    select blocklot
    from avs.avs_addresses
    where address_kind <> 'PRIMARY'
    --where address_kind in ('ADDITIONAL', 'ALTERNATE', 'ALIAS', 'INVALID', 'PRIMARY', 'ADDITIONAL')
)
order by blocklot, address_kind;



-- primary addresses that have other kinds of addresses (other kinds of addresses listed) 
-- n=112425
-- first half of union grabs the PRIMARY rows
select *
from avs.avs_addresses
where 1=1
and blocklot in (
    select blocklot
    from avs.avs_addresses
    where address_kind <> 'PRIMARY'
    --where address_kind in ('ADDITIONAL', 'ALTERNATE', 'ALIAS', 'INVALID', 'PRIMARY', 'ADDITIONAL')
)
and address_kind = 'PRIMARY'
union all
-- second half of union grabs the rows other than the PRIMARY rows
select *
from avs.avs_addresses
where 1=1
and blocklot in (
    select blocklot
    from avs.avs_addresses
    where address_kind = 'PRIMARY'
)
and address_kind <> 'PRIMARY'
--and address_kind in ('ADDITIONAL', 'ALTERNATE', 'ALIAS', 'INVALID', 'PRIMARY', 'ADDITIONAL')
order by blocklot, address_kind;


-- addresses that have no primary address
-- n=238
select *
from avs.avs_addresses
where 1=1
and blocklot not in (
    select blocklot
    from avs.avs_addresses
    where address_kind = 'PRIMARY'
)
order by blocklot, address_kind;


-- value = 333236
select 220573 + 112425 + 238;


/*
addresses with muplitple primary rows
I was thinking that think this should not be allowed (we can add checks in EAS).
Why?
I _think_ that Vivian said a parcel should have one primary address (number, street, APN).
The data strongly suggest this is not the case - so I beleive I misunderstood Vivian.
For example, APN 00110002 has the following AVS rows:
"0010002";"0010";"002";"0";"PRIMARY";"ASSESSOR";455;"";"JEFFERSON";"ST"
"0010002";"0010";"002";"0";"PRIMARY";"ASSESSOR";461;"";"JEFFERSON";"ST"
"0010002";"0010";"002";"0";"PRIMARY";"ASSESSOR";465;"";"JEFFERSON";"ST"
"0010002";"0010";"002";"0";"PRIMARY";"ASSESSOR";471;"";"JEFFERSON";"ST"
"0010002";"0010";"002";"0";"PRIMARY";"ASSESSOR";475;"";"JEFFERSON";"ST"
"0010002";"0010";"002";"0";"PRIMARY";"ASSESSOR";485;"";"JEFFERSON";"ST"
"0010002";"0010";"002";"0";"PRIMARY";"ASSESSOR";495;"";"JEFFERSON";"ST"
"0010002";"0010";"002";"0";"PRIMARY";"ASSESSOR";680;"";"BEACH";"ST"

Moreover, there are 118525 rows where a single APN has muliple PRIMARY Addresses (see query below).

Therefore, I think what PRIMARY may mean is that for a given address such as 455 JEFFERSON, there should be one PRIMARY.


*/
-- n=118525
select *
from avs.avs_addresses
where 1=1
and address_kind = 'PRIMARY'
and blocklot in (
    select blocklot
    from avs.avs_addresses
    where 1=1
    and address_kind = 'PRIMARY'
    group by blocklot
    having count(*) > 1
)
order by blocklot;


-- This query is not perfect but with it you can find rows that have matching addresses and that have multiple PRIMARY (address_kind) rows.
/*
Here are some rows that have the same address, the same APN and have more than one row with address_kind value of PRIMARY.
"1644034";"1644";"034";"1";"ALTERNATE";"MULTIUNIT";1;"";"CABRILLO";"ST";"STREET";"V";""
"1644034";"1644";"034";"1";"PRIMARY";"ASSESSOR";1;"";"CABRILLO";"ST";"STREET";"V";""
"1644034";"1644";"034";"0";"PRIMARY";"ASSESSOR";1;"";"CABRILLO";"ST";"STREET";"V";"0"
*/
select *
from avs.avs_addresses
where 1=1
and blocklot in (
    --select count(*), avsa1.blocklot
    select avsa1.blocklot
    from avs.avs_addresses avsa1, 
        avs.avs_addresses avsa2
    where 1=1
    and avsa1.street_number = avsa2.street_number
    and avsa1.avs_street_name = avsa2.avs_street_name
    and avsa1.avs_street_type = avsa2.avs_street_type
    and avsa1.address_kind = 'PRIMARY'
    and avsa2.address_kind <> 'PRIMARY'
    and avsa1.id <> avsa2.id
    group by avsa1.blocklot
    having count(*) > 1
)
order by street_number, avs_street_name, avs_street_type, blocklot;




