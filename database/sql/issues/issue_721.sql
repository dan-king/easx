﻿

----- Here is the algorithm and the sql that needs to be added to the street etl proc.


----- for each invalid address that is invalid because of a street retirement
SELECT
	ab.address_base_id,
	ab.street_segment_id,
	ab.base_address_num,
	ab.base_address_suffix,
	sn.full_street_name,
	ab.retire_tms,
	ss.date_dropped,
	st_distance(ab.geometry, ss.geometry) * 2 as distance
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
WHERE 1 = 1
and sn.category = 'MAP'
and a.address_base_flg = true
and ss.date_dropped is not null
and ab.retire_tms is null
order by
	ss.st_name,
	ss.st_type,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num;



-----now get the nearest unretired street that has the same name and is within that distance
select st_distance(ab.geometry, ss.geometry), ss.*
from
	address_base ab,
	street_segments ss,
	streetnames sn
where 1=1
and ss.street_segment_id = sn.street_segment_id
and sn.category = 'MAP'
--- these all vary with each invalid address
and st_distance(ab.geometry, ss.geometry) < (620 * 2)
and ab.address_base_id = 468996
and base_street_name = 'TENNY'
and street_type = 'PL'
and ss.street_segment_id != 8169
and ss.date_dropped is null
order by st_distance(ab.geometry, ss.geometry) asc
limit 10

-- if there is not one, there is nothing we can do and it probably realy is invalid


-- if there is one...do the update

-- disable the address base after trigger
alter table address_base disable trigger _eas_address_base_after;

-- update the base address so its using the new segment
update address_base
set street_segment_id = 18921
where address_base_id = 478044;

-- enable the address base after trigger
alter table address_base enable trigger _eas_address_base_after;


-- Done!







