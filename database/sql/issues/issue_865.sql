﻿
-- select * from parcels_provisioning order by provisioned_tms;

update parcels_provisioning
set provisioned_tms = now()
where parcel_id in (
	select parcel_id
	from parcels
	where blk_lot in ('0063037', '0063038')
);

update parcels_provisioning
set provisioned_tms = null
where parcel_id in (
	select parcel_id
	from parcels
	where blk_lot in ('0063037', '0063038')
);
