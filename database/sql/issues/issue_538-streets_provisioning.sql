
-- support provisioning of streets when DPW turn around is too slow 	
-- http://code.google.com/p/eas/issues/detail?id=538

/*

Jeff and Paul and Judy agreed that we would use the block of seg_cnn values from 99,000,001 through 99,999,999
to indicate a provisional street segment.

*/

-- identify base addresses that are using DT provisioned segments.
select *
from address_base ab
left outer join street_segments ss on ss.street_segment_id = ab.street_segment_id
where ss.seg_cnn > 99000000;


-- for each segment in that list find the best replacment.
-- If there is no single segment that is obviously the best fit, flag it so we can deal with it manually.
select 
    ssm_prov.seg_cnn, 
    ssm_prov.base_street_name, 
    ssm_prov.street_type,
    st_distance(st_centroid(ssm_prov.geometry), st_centroid(ssm_dpw.geometry)),
    ssm_dpw.seg_cnn, 
    ssm_dpw.base_street_name, 
    ssm_dpw.street_type
from 
    vw_street_segments_map ssm_prov,
    vw_street_segments_map ssm_dpw
where ssm_prov.seg_cnn > 99000000
and ssm_dpw.seg_cnn < 99000000
and st_dwithin(st_centroid(ssm_prov.geometry), st_centroid(ssm_dpw.geometry), 1000)
and ssm_prov.base_street_name = ssm_dpw.base_street_name
and coalesce(ssm_prov.street_type, '') = coalesce(ssm_dpw.street_type, '')
order by ssm_prov.seg_cnn, ssm_prov.base_street_name, ssm_prov.street_type, st_distance;



select 
    ssm_prov.seg_cnn, 
    ssm_prov.base_street_name, 
    ssm_prov.street_type,
    st_distance(st_centroid(ssm_prov.geometry), st_centroid(ssm_dpw.geometry)),
    ssm_dpw.seg_cnn, 
    ssm_dpw.base_street_name, 
    ssm_dpw.street_type
from 
    vw_street_segments_map ssm_prov,
    vw_street_segments_map ssm_dpw
where ssm_prov.seg_cnn > 99000000
and ssm_dpw.seg_cnn < 99000000
and st_dwithin(st_centroid(ssm_prov.geometry), st_centroid(ssm_dpw.geometry), 1000)
and ssm_prov.base_street_name = ssm_dpw.base_street_name
and coalesce(ssm_prov.street_type, '') = coalesce(ssm_dpw.street_type, '')
order by ssm_prov.seg_cnn, ssm_prov.base_street_name, ssm_prov.street_type, st_distance;






select *
from street_segments
where seg_cnn > 99000000;

select *
from street_segments ss1
join streetnames sn1 on ss1.street_segment_id = sn1.street_segment_id
where sn1.category = 'MAP'
and sn1.base_street_name = 'AVOCET'
and sn1.street_type = 'WAY';

select *
from street_segments
where 1=1
and st_name = 'AVOCET'
