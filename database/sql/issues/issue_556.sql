
SELECT
    /*
    ss.st_name,
	ss.st_type,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num,
    count(*)
    */
	ab.address_base_id,
    ab.unq_adds_id,
	a.address_id,
    a.unq_adds_id,
	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num,
	sn.base_street_name,
    sn.street_type,
    p.blk_lot,
    p.parcel_id,
	ab.create_tms as ab_create_tms,
	a.create_tms as a_create_tms,
    axp.create_tms as axp_create_tms,
	ab.retire_tms as ab_retire_tms,
	a.retire_tms as a_retire_tms,
    axp.retire_tms as axp_retire_tms,
    ab.street_segment_id,
    ab.zone_id
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id) 
WHERE 1 = 1
and sn.category = 'MAP'

--and sn.base_street_name = 'PINE'
--and ab.base_address_num = 1000

--/* unit addresses linked to retired parcels
and (
    a.address_base_flg = false
    and p.date_map_drop is not null
    and a.retire_tms is null
    and axp.retire_tms is null
)
--*/

/* base addresses linked to retired parcels
and (
    a.address_base_flg = true
    and p.date_map_drop is not null
    and a.retire_tms is null
)
*/

/* base addresses linked to retired streets
and (
    a.address_base_flg = true
    and ss.date_dropped is not null
    and a.retire_tms is null
)
*/

/*
group by 
    ss.st_name,
	ss.st_type,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num
*/
order by
	ss.st_name,
	ss.st_type,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num
limit 1000;



