
select * from streetnames_work;


  insert into streetnames_work (
      seg_cnn,
      bsm_streetnameid,
      prefix_direction,
      prefix_type,
      base_street_name,
      street_type,
      suffix_direction,
      suffix_type,
      full_street_name,
      category,
      street_segment_id,
      create_tms
  )
  select
      sns.seg_cnn,
      sns.bsm_streetnameid,
      sns.prefix_direction,
      sns.prefix_type,
      sns.base_street_name,
      sns.street_type,
      sns.suffix_direction,
      sns.suffix_type,
      sns.full_street_name,
      sns.category,
      ss.street_segment_id,
      now()
  from streetnames_staging sns
  join street_segments ss on (sns.seg_cnn = ss.seg_cnn)
  left join streetnames sn on (sns.seg_cnn = sn.seg_cnn and sns.full_street_name = sn.full_street_name and sns.category = sn.category)
  where 1=1
  and sn.streetname_id is null;



  select   snw.base_street_name, 
      snw.street_type, 
      snw.full_street_name, 
      snw.bsm_streetnameid
    from streetnames sn, streetnames_work snw
  where sn.category = 'MAP' 
  and snw.category = 'MAP'
  and sn.seg_cnn = snw.seg_cnn
  and (sn.base_street_name <> snw.base_street_name or sn.street_type <> snw.street_type or sn.full_street_name <> snw.full_street_name);


  -- First we update matching MAP rows.
-- n=49
  update streetnames sn
  set base_street_name = snw.base_street_name, 
      street_type = snw.street_type, 
      full_street_name = snw.full_street_name, 
      bsm_streetnameid = snw.bsm_streetnameid
  from streetnames_work snw
  where sn.category = 'MAP' 
  and snw.category = 'MAP'
  and sn.seg_cnn = snw.seg_cnn
  and (sn.base_street_name <> snw.base_street_name or sn.street_type <> snw.street_type or sn.full_street_name <> snw.full_street_name);


select * from streetnames where base_street_name like '%CEC%'


  select *
  from streetnames
  where streetname_id in (
    -- Find the ALIAS rows in streetnames that match the MAP rows.
    -- DEBUG select sn_map.seg_cnn, sn_map.base_street_name, sn_map.street_type, sn_map.category, '<<< >>>', sn_alias.category, sn_alias.seg_cnn, sn_alias.base_street_name, sn_alias.street_type
    select sn_alias.streetname_id
    from streetnames sn_map,
         streetnames sn_alias
    where sn_map.category = 'MAP' 
    and sn_alias.category = 'ALIAS'
    and sn_map.seg_cnn = sn_alias.seg_cnn
    and sn_map.base_street_name = sn_alias.base_street_name 
    and sn_map.street_type = sn_alias.street_type
    order by sn_map.seg_cnn, sn_map.base_street_name, sn_map.street_type
  );


  delete 
  from streetnames
  where streetname_id in (
    -- Find the ALIAS rows in streetnames that match the MAP rows.
    -- DEBUG select sn_map.seg_cnn, sn_map.base_street_name, sn_map.street_type, sn_map.category, '<<< >>>', sn_alias.category, sn_alias.seg_cnn, sn_alias.base_street_name, sn_alias.street_type
    select sn_alias.streetname_id
    from streetnames sn_map,
         streetnames sn_alias
    where sn_map.category = 'MAP' 
    and sn_alias.category = 'ALIAS'
    and sn_map.seg_cnn = sn_alias.seg_cnn
    and sn_map.base_street_name = sn_alias.base_street_name 
    and sn_map.street_type = sn_alias.street_type
    order by sn_map.seg_cnn, sn_map.base_street_name, sn_map.street_type
  );



  insert into streetnames (
      seg_cnn,
      bsm_streetnameid,
      prefix_direction,
      prefix_type,
      base_street_name,
      street_type,
      suffix_direction,
      suffix_type,
      full_street_name,
      category,
      street_segment_id,
      create_tms
  )
  select
      seg_cnn,
      bsm_streetnameid,
      prefix_direction,
      prefix_type,
      base_street_name,
      street_type,
      suffix_direction,
      suffix_type,
      full_street_name,
      category,
      street_segment_id,
      create_tms
  from streetnames_work snw
  where category = 'ALIAS'
and seg_cnn = 15127000
  and not exists (
    select 1
    from streetnames sn
    where sn.full_street_name = snw.full_street_name
    and sn.category = snw.category
    and sn.seg_cnn = snw.seg_cnn
  );

select * from streetnames where base_street_name like '%EMERALD%'



select *
from streetnames_staging
where base_street_name like '%EMERALD%'

select * from streetnames_staging where seg_cnn = 15127000
select * from streetnames where seg_cnn = 15127000
select * from streetnames_work where seg_cnn = 15127000

  select
      seg_cnn,
      bsm_streetnameid,
      prefix_direction,
      prefix_type,
      base_street_name,
      street_type,
      suffix_direction,
      suffix_type,
      full_street_name,
      category,
      street_segment_id,
      create_tms
  from streetnames_work snw
  where category = 'MAP'
  and not exists (
    select 1
    from streetnames sn
    where sn.full_street_name = snw.full_street_name
    and sn.category = snw.category
    and sn.seg_cnn = snw.seg_cnn
  );

  insert into streetnames (
      seg_cnn,
      bsm_streetnameid,
      prefix_direction,
      prefix_type,
      base_street_name,
      street_type,
      suffix_direction,
      suffix_type,
      full_street_name,
      category,
      street_segment_id,
      create_tms
  )
  select
      seg_cnn,
      bsm_streetnameid,
      prefix_direction,
      prefix_type,
      base_street_name,
      street_type,
      suffix_direction,
      suffix_type,
      full_street_name,
      category,
      street_segment_id,
      create_tms
  from streetnames_work snw
  where category = 'MAP'
  and not exists (
    select 1
    from streetnames sn
    where sn.full_street_name = snw.full_street_name
    and sn.category = snw.category
    and sn.seg_cnn = snw.seg_cnn
  );

  insert into streetnames (
      seg_cnn,
      bsm_streetnameid,
      prefix_direction,
      prefix_type,
      base_street_name,
      street_type,
      suffix_direction,
      suffix_type,
      full_street_name,
      category,
      street_segment_id,
      create_tms
  )
  select
      seg_cnn,
      bsm_streetnameid,
      prefix_direction,
      prefix_type,
      base_street_name,
      street_type,
      suffix_direction,
      suffix_type,
      full_street_name,
      category,
      street_segment_id,
      create_tms
  from streetnames_work snw
  where category = 'ALIAS'
  and not exists (
    select 1
    from streetnames sn
    where sn.full_street_name = snw.full_street_name
    and sn.category = snw.category
    and sn.seg_cnn = snw.seg_cnn
  );
