
-- http://code.google.com/p/eas/issues/detail?id=466

select *
from avs.avs_addresses
where exception_text is null

update avs.avs_addresses
set exception_text = null

select *
from avs.avs_addresses
where exception_text like '%programming%'

delete
from avs.avs_addresses
where exception_text is null


select avs.delete_addresses();


select avs.load(
    _offset int,
    _limit int,
    _end_dated boolean


select avs.load(0, 100000, True);
select avs.load(0, 10000, False);

select p.parcel_id, avsa.*
from avs.avs_addresses avsa, 
    public.parcels p
where avsa.blocklot = p.blk_lot


select substring('unable to find nearby street | nearest: ' || string_agg(distinct full_street_name, ', ') from 1 for 255)
from (
    select (base_street_name || ' ' || coalesce(street_type, ''))::character varying(64) as full_street_name
    from avs.vw_streets_nearest
    where parcel_id = 201854
    and category = 'MAP'
    and date_dropped is null
    order by distance asc
    limit 5
) foo



select * 
wher e