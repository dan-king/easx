﻿SELECT * from parcels_staging where blk_lot = '5650054' or blk_lot = '5650053'

SELECT * from parcels limit 1

-- 218052
SELECT count(*) from parcels_staging

-- 218054
SELECT count(*) from parcels


--delete
select *
from parcels p
where parcel_id in (
	-- eas parcels that are now missing in parcels staging
	select parcel_id
	from parcels p
	left outer join parcels_staging ps on ps.blk_lot = p.blk_lot
	where ps.blk_lot is null
) and not exists (
	-- with no AXP records
	select 1
	from address_x_parcels axp
	where axp.parcel_id = p.parcel_id
);


select *
from parcels
where block_num = '5650' and (lot_num = '033' or  lot_num = '034' or lot_num = '053' or lot_num = '054')


select *
from parcels
where map_blk_lot in ('5650053', '5650033')

select *
from parcels
where blk_lot <> (block_num || lot_num);

select *
from parcels 
where 
(blk_lot = '5650054' and block_num = '5650' and lot_num = '034')
or 
(blk_lot = '5650053' and block_num = '5650' and lot_num = '033')


-- 1778500
-- 2864999
select *
from parcels
where 1=1
and blk_lot = '2864999'
--and block_num like '2864%'
order by block_num, lot_num

select *
from parcels 
where map_blk_lot is null

----- TEST CASES

-- TEST 1: Parcel in EAS is no longer being staged and IS NOT linked to an address
-- RESULT: should succeed and delete 9999999 parcel record from parcels
INSERT INTO parcels(
            map_blk_lot, blk_lot, block_num, lot_num, create_tms)
    VALUES ('9999999', '9999999', '9999', '999', now());

-- TEST 2: Parcel in EAS is no longer being staged and IS linked to an address
-- RESULT: should fail saying parcels has records not found in parcels staging
DELETE FROM parcels_staging WHERE blk_lot = '7548017';

-- TEST 3: Parcel in EAS is no longer being staged and IS NOT linked to an address and IS being provisioned
-- RESULT: should succeed and preserve provisioned 9999999 parcel
INSERT INTO parcels(
            map_blk_lot, blk_lot, block_num, lot_num, create_tms)
    VALUES ('9999999', '9999999', '9999', '999', now());

INSERT INTO parcels_provisioning(
            parcel_id, 
            create_user_id, create_tms)
    VALUES ((select parcel_id from parcels where blk_lot = '9999999'),
	10, now());

-- TEST 4: Staged Parcel has nulls in blk_lot/block_num/lot_num data
-- RESULT: should fail saying parcels_staging has bad data
INSERT INTO parcels_staging(
            map_blk_lot)
    VALUES ('9999999');

-- TEST 5: Staged Parcel has inconsistent in blk_lot/block_num/lot_num data
-- RESULT: should fail saying parcels_staging has bad data
INSERT INTO parcels_staging(
            map_blk_lot, blk_lot, block_num, lot_num)
    VALUES ('9999999', '9999999', '9991', '991');

-- TEST 6: Staging table has two or more parcels with identical blk_lot values
-- RESULT: should fail saying parcels_staging has bad data
INSERT INTO parcels_staging(
            map_blk_lot, blk_lot, block_num, lot_num)
    VALUES ('9999999', '9999999', '9999', '999'),
           ('9999999', '9999999', '9999', '999');

-- PRE TEST SQL
-- temporary work around for parcel missing in staging
INSERT INTO parcels_staging(map_blk_lot, blk_lot, block_num, lot_num, date_map_add, date_map_drop, geometry)
select map_blk_lot, blk_lot, block_num, lot_num, date_map_add, date_map_drop, geometry FROM parcels WHERE blk_lot = '8705009'
and not exists (
	select 1 from parcels_staging where blk_lot = '8705009'
);

-- TEST SQL
SELECT _sfmad_etl_parcels('1');

-- CLEAN UP SQL
DELETE FROM parcels_provisioning WHERE parcel_id = (SELECT parcel_id FROM parcels WHERE blk_lot = '9999999');
DELETE FROM address_x_parcels WHERE parcel_id = (SELECT parcel_id FROM parcels WHERE blk_lot = '9999999');
DELETE FROM parcels_staging WHERE blk_lot = '9999999' OR map_blk_lot = '9999999';
DELETE FROM parcels WHERE blk_lot = '9999999';
INSERT INTO parcels_staging(map_blk_lot, blk_lot, block_num, lot_num, date_map_add, date_map_drop, geometry)
select map_blk_lot, blk_lot, block_num, lot_num, date_map_add, date_map_drop, geometry FROM parcels WHERE blk_lot = '7548017'
and not exists (
	select 1 from parcels_staging where blk_lot = '7548017'
);

-- CLEAN UP CHECKS
SELECT count(*) FROM parcels_provisioning WHERE parcel_id = (SELECT parcel_id FROM parcels WHERE blk_lot = '9999999');
SELECT count(*) FROM address_x_parcels WHERE parcel_id = (SELECT parcel_id FROM parcels WHERE blk_lot = '9999999');
SELECT count(*) FROM parcels_staging WHERE blk_lot = '9999999';
SELECT count(*) FROM parcels WHERE blk_lot = '9999999';
