
-- http://code.google.com/p/eas/issues/detail?id=368
-- This will output SQL that can be used to update AVS with correct street name suffixes.
/*

select *, '', '', '', '', '', '', '', '', '', ''
from avs.avs_addresses
where avs_street_name = 'EMERALD COVE'
and avs_street_type = 'TERRACE';

select * from parcels where blk_lot = '4991600';

select avs.find_nearest_street_3(_street_name, _street_type, false, _parcel_id, _std_end_date, _category);
select avs.find_nearest_street_3('EMERALD COVE', 'TERRACE', false, 147286, null, 'MAP');
-- street_segment_id = 969
select * from streetnames where street_segment_id = 969;

select * from avs.vw_streets_nearest where street_segment_id = 969 and category = 'MAP' limit 1;
select * from avs.vw_streets_nearest where street_segment_id = 969 and category = 'MAP' order by date_dropped desc nulls first limit 1;

select *
from vw_streetnames_distinct
where base_street_name = 'EMERALD COVE';

sele

*/

select
    id,
    (avs_street_sfx)::varchar(32) as street_suffix_existing,
    (avs_street_type_new)::varchar(32) as street_suffix_new,
    (street_number::text || ' ' || avs_street_name || ' ' || avs_street_sfx || ' will become ' || street_number::text || ' ' || avs_street_name || ' ' || avs_street_type_new )::varchar(256) as comment
from (
    select 
        id, 
        exception_text, 
        street_number,
        street_number_sfx,
        avs_street_name,
        avs_street_sfx,
        trim(both ' ' from split_part(exception_text , '| try ', 2)) as avs_street_type_new
    from avs.avs_addresses
    where 1=1
    and exception_text like 'your street suffix appears to be incorrect%'
) vtab;
