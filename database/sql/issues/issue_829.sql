﻿
-- AXP changes 

-- within the last 7 days

-- not in the same change request

-- scenario A
-- with parcels thattouching

-- scenario B
-- using the same parcel

-- scenario C
-- with the same base address

select 
	base_address_num, 
	full_street_name, 
	address_base_flg,
	unit_num,
	blk_lot,
	address_x_parcel_create_tms,
	address_x_parcel_retire_tms,
	'create' as action,
        cr.*
        into foo
from vw_addresses_flat_native af
join vw_change_request cr on (cr.change_request_id = address_x_parcel_activate_change_request_id)
where address_x_parcel_id is not null
and address_x_parcel_create_tms > now()::date - 7
and address_x_parcel_retire_tms is null
--and not (address_x_parcel_activate_change_request_id = 1 and address_x_parcel_retire_change_request_id = 1)

union all

select 
	base_address_num, 
	full_street_name, 
	address_base_flg,
	unit_num,
	blk_lot,
	address_x_parcel_create_tms,
	address_x_parcel_retire_tms,
	'retire' as action,
        cr.*
from vw_addresses_flat_native af
join vw_change_request cr on (cr.change_request_id = address_x_parcel_retire_change_request_id)
where address_x_parcel_id is not null
and address_x_parcel_retire_tms > now()::date - 7
and address_x_parcel_retire_tms is not null
--and not (address_x_parcel_activate_change_request_id = 1 and address_x_parcel_retire_change_request_id = 1)

order by 
	base_address_num, 
	full_street_name, 
	address_base_flg,
	unit_num,
	blk_lot,
	action;


	