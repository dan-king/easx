-- DROP TABLE invalid_addresses;
-- drop VIEW vw_invalid_addresses;
-- drop TABLE d_invalid_address_types;

CREATE TABLE d_invalid_address_types
(
  invalid_type_id serial NOT NULL,
  invalid_type_desc character varying(64),
  CONSTRAINT d_invalid_address_pk PRIMARY KEY (invalid_type_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE d_invalid_address_types OWNER TO eas_dbo;
GRANT ALL ON TABLE d_invalid_address_types TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE d_invalid_address_types TO django;


----------

INSERT INTO d_invalid_address_types(invalid_type_id, invalid_type_desc) VALUES (1, 'parcel retirement');
INSERT INTO d_invalid_address_types(invalid_type_id, invalid_type_desc) VALUES (4, 'distance between address and parcel is greater than 500 feet');
INSERT INTO d_invalid_address_types(invalid_type_id, invalid_type_desc) VALUES (5, 'street retirement');

----------

DROP VIEW vw_address_reviews;
DROP TABLE address_review;
DROP TABLE d_address_review_types;

----------



----------


CREATE TABLE invalid_addresses
(
  invalid_address_id serial NOT NULL,
  address_id integer NOT NULL,
  invalid_type_id integer NOT NULL,
  message character varying(256),
  create_tms timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT invalid_address_pk PRIMARY KEY (invalid_address_id),
  CONSTRAINT "invalid_address_X_d_invalid_address_types" FOREIGN KEY (invalid_type_id)
      REFERENCES d_invalid_address_types (invalid_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT address_x_invalid_address FOREIGN KEY (address_id)
      REFERENCES addresses (address_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE invalid_addresses OWNER TO eas_dbo;
GRANT ALL ON TABLE invalid_addresses TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE invalid_addresses TO django;


----------


CREATE OR REPLACE VIEW vw_invalid_addresses AS
  SELECT
    ia.invalid_address_id,
    ia.message,
    ab.address_base_id,
    ab.base_address_num,
    ab.base_address_suffix,
    ab.geometry,
    sn.full_street_name,
    a.unit_num,
    a.address_base_flg,
    diat.invalid_type_desc
  FROM
    invalid_addresses ia,
    addresses a,
    address_base ab,
    streetnames sn,
    d_invalid_address_types diat
  WHERE 1 = 1
  AND ia.address_id = a.address_id
  AND a.address_base_id = ab.address_base_id
  AND sn.category::text = 'MAP'::text
  AND ab.street_segment_id = sn.street_segment_id
  AND diat.invalid_type_id = ia.invalid_type_id;

ALTER TABLE vw_invalid_addresses OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_invalid_addresses TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_invalid_addresses TO django;
GRANT SELECT ON TABLE vw_invalid_addresses TO geoserver;


----------


select _eas_validate_addresses_after_etl('PARCELS');
select _eas_validate_addresses_after_etl('STREETS');
