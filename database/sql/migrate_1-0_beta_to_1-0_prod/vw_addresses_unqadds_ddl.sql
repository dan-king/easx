-- View: vw_addresses_unqadds

-- DROP VIEW vw_addresses_unqadds;

CREATE OR REPLACE VIEW vw_addresses_unqadds AS
 SELECT addresses.address_id, addresses.unq_adds_id
   FROM addresses
  WHERE addresses.unq_adds_id IS NOT NULL;

ALTER TABLE vw_addresses_unqadds OWNER TO postgres;

