
DROP FUNCTION IF EXISTS public._eas_insert_unit_address(
    _address_base_id int,
    _unit_num character varying(10),
    _unit_type_id int,
    _floor_id int,
    _disposition_code int,
    _unq_adds_id int,
    _activate_change_request_id int,
    _current_tms timestamp without time zone
);

CREATE OR REPLACE FUNCTION public._eas_insert_unit_address(
    _address_base_id int,
    _unit_num character varying(10),
    _unit_type_id int,
    _floor_id int,
    _disposition_code int,
    _unq_adds_id int,
    _activate_change_request_id int,
    _current_tms timestamp without time zone
)
RETURNS int AS
$BODY$
DECLARE 

    _unit_address_id int;
    -- domain values; unit type 0: other; floor 105: 'unknown'; disposition code 1: official
    _unit_type_id int := coalesce(_unit_type_id, 0);
    _floor_id int := coalesce(_floor_id, 105);
    _disposition_code int := coalesce(_disposition_code, 1);

BEGIN

    select into _unit_address_id nextval('addresses_address_id_seq');

    insert into addresses ( address_id, address_base_id, unit_num, unit_type_id, floor_id, disposition_code, address_base_flg, unq_adds_id, activate_change_request_id, create_tms, last_change_tms)
    values ( _unit_address_id, _address_base_id, _unit_num, _unit_type_id, _floor_id, _disposition_code, false, _unq_adds_id, _activate_change_request_id, _current_tms, _current_tms);

    return _unit_address_id;

END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION public._eas_insert_unit_address(
    _address_base_id int,
    _unit_num character varying(10),
    _unit_type_id int,
    _floor_id int,
    _disposition_code int,
    _unq_adds_id int,
    _activate_change_request_id int,
    _current_tms timestamp without time zone
)
OWNER TO postgres;
