
-- Table: d_street_type

-- DROP TABLE d_street_type;

CREATE TABLE d_street_type
(
  id serial NOT NULL,
  abbreviated character varying(32) NOT NULL,
  unabbreviated character varying(32) NOT NULL,
  domain character(4) NOT NULL,
  CONSTRAINT "d_street_type_PK" PRIMARY KEY (id),
CONSTRAINT "d_street_type_UNQ1"
    UNIQUE (unabbreviated)
)
WITH (OIDS=FALSE);
ALTER TABLE d_street_type OWNER TO postgres;

CREATE INDEX "d_street_type_IDX1"
  ON d_street_type
  USING btree (abbreviated);

CREATE INDEX "d_street_type_IDX2"
  ON d_street_type
  USING btree (unabbreviated);


-- The first block of inserts are from 
--    http://www.usps.com/ncsc/lookups/abbr_suffix.txt
-- to create these inserts:
-- wget the file from USPS (see above URL)
-- awk '{print "INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES (\47"$1"\47, \47"$3"\47, \47USPS\47);"}' abbr_suffix.txt | sort | uniq > foo
-- \47 is the single quote
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('ALLEY', 'ALY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('ANNEX', 'ANX', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('ARCADE', 'ARC', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('AVENUE', 'AVE', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('BAYOO', 'BYU', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('BEACH', 'BCH', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('BEND', 'BND', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('BLUFF', 'BLF', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('BLUFFS', 'BLFS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('BOTTOM', 'BTM', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('BOULEVARD', 'BLVD', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('BRANCH', 'BR', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('BRIDGE', 'BRG', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('BROOK', 'BRK', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('BROOKS', 'BRKS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('BURG', 'BG', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('BURGS', 'BGS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('BYPASS', 'BYP', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CAMP', 'CP', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CANYON', 'CYN', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CAPE', 'CPE', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CAUSEWAY', 'CSWY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CENTER', 'CTR', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CENTERS', 'CTRS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CIRCLE', 'CIR', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CIRCLES', 'CIRS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CLIFF', 'CLF', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CLIFFS', 'CLFS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CLUB', 'CLB', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('COMMON', 'CMN', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CORNER', 'COR', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CORNERS', 'CORS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('COURSE', 'CRSE', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('COURT', 'CT', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('COURTS', 'CTS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('COVE', 'CV', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('COVES', 'CVS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CREEK', 'CRK', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CRESCENT', 'CRES', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CREST', 'CRST', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CROSSING', 'XING', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CROSSROAD', 'XRD', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('CURVE', 'CURV', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('DALE', 'DL', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('DAM', 'DM', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('DIVIDE', 'DV', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('DRIVE', 'DR', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('DRIVES', 'DRS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('ESTATE', 'EST', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('ESTATES', 'ESTS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('EXPRESSWAY', 'EXPY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('EXTENSION', 'EXT', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('EXTENSIONS', 'EXTS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FALL', 'FALL', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FALLS', 'FLS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FERRY', 'FRY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FIELD', 'FLD', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FIELDS', 'FLDS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FLAT', 'FLT', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FLATS', 'FLTS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FORD', 'FRD', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FORDS', 'FRDS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FOREST', 'FRST', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FORGE', 'FRG', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FORGES', 'FRGS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FORK', 'FRK', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FORKS', 'FRKS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FORT', 'FT', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('FREEWAY', 'FWY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('GARDEN', 'GDN', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('GARDENS', 'GDNS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('GATEWAY', 'GTWY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('GLEN', 'GLN', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('GLENS', 'GLNS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('GREEN', 'GRN', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('GREENS', 'GRNS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('GROVE', 'GRV', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('GROVES', 'GRVS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('HARBOR', 'HBR', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('HARBORS', 'HBRS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('HAVEN', 'HVN', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('HEIGHTS', 'HTS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('HIGHWAY', 'HWY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('HILL', 'HL', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('HILLS', 'HLS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('HOLLOW', 'HOLW', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('INLET', 'INLT', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('ISLAND', 'IS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('ISLANDS', 'ISS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('ISLE', 'ISLE', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('JUNCTION', 'JCT', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('JUNCTIONS', 'JCTS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('KEY', 'KY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('KEYS', 'KYS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('KNOLL', 'KNL', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('KNOLLS', 'KNLS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('LAKE', 'LK', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('LAKES', 'LKS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('LANDING', 'LNDG', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('LAND', 'LAND', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('LANE', 'LN', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('LIGHT', 'LGT', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('LIGHTS', 'LGTS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('LOAF', 'LF', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('LOCK', 'LCK', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('LOCKS', 'LCKS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('LODGE', 'LDG', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('LOOP', 'LOOP', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('MALL', 'MALL', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('MANOR', 'MNR', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('MANORS', 'MNRS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('MEADOW', 'MDW', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('MEADOWS', 'MDWS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('MEWS', 'MEWS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('MILL', 'ML', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('MILLS', 'MLS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('MISSION', 'MSN', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('MOTORWAY', 'MTWY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('MOUNTAIN', 'MTN', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('MOUNTAINS', 'MTNS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('MOUNT', 'MT', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('NECK', 'NCK', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('ORCHARD', 'ORCH', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('OVAL', 'OVAL', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('OVERPASS', 'OPAS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PARK', 'PARK', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PARKS', 'PARK', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PARKWAY', 'PKWY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PARKWAYS', 'PKWY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PASSAGE', 'PSGE', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PASS', 'PASS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PATH', 'PATH', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PIKE', 'PIKE', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PINE', 'PNE', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PINES', 'PNES', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PLACE', 'PL', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PLAIN', 'PLN', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PLAINS', 'PLNS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PLAZA', 'PLZ', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('POINT', 'PT', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('POINTS', 'PTS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PORT', 'PRT', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PORTS', 'PRTS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('PRAIRIE', 'PR', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('RADIAL', 'RADL', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('RAMP', 'RAMP', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('RANCH', 'RNCH', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('RAPID', 'RPD', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('RAPIDS', 'RPDS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('REST', 'RST', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('RIDGE', 'RDG', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('RIDGES', 'RDGS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('RIVER', 'RIV', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('ROAD', 'RD', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('ROADS', 'RDS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('ROUTE', 'RTE', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('ROW', 'ROW', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('RUE', 'RUE', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('RUN', 'RUN', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('SHOAL', 'SHL', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('SHOALS', 'SHLS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('SHORE', 'SHR', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('SHORES', 'SHRS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('SKYWAY', 'SKWY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('SPRING', 'SPG', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('SPRINGS', 'SPGS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('SPUR', 'SPUR', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('SPURS', 'SPUR', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('SQUARE', 'SQ', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('SQUARES', 'SQS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('STATION', 'STA', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('STRAVENUE', 'STRA', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('STREAM', 'STRM', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('STREETS', 'STS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('STREET', 'ST', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('SUMMIT', 'SMT', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('TERRACE', 'TER', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('THROUGHWAY', 'TRWY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('TRACE', 'TRCE', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('TRACK', 'TRAK', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('TRAFFICWAY', 'TRFY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('TRAIL', 'TRL', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('TUNNEL', 'TUNL', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('TURNPIKE', 'TPKE', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('UNDERPASS', 'UPAS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('UNIONS', 'UNS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('UNION', 'UN', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('VALLEYS', 'VLYS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('VALLEY', 'VLY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('VIADUCT', 'VIA', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('VIEWS', 'VWS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('VIEW', 'VW', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('VILLAGES', 'VLGS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('VILLAGE', 'VLG', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('VILLE', 'VL', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('VISTA', 'VIS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('WALKS', 'WALK', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('WALK', 'WALK', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('WALL', 'WALL', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('WAYS', 'WAYS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('WAY', 'WAY', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('WELLS', 'WLS', 'USPS');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('WELL', 'WL', 'USPS');


-- To create these CITY domain inserts:
-- select *
-- from streetnames
-- where street_type not in (
--    select abbreviated
--    from d_street_type
-- ) and category = 'MAP';
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('STAIRWAY', 'STWY', 'CITY');
INSERT INTO d_street_type(unabbreviated, abbreviated, domain) VALUES ('STEPS', 'STEP', 'CITY');

-- Make some updates where USPS has weird values.
update d_street_type set abbreviated = 'PARKS', domain = 'CITY' where unabbreviated  = 'PARKS';
update d_street_type set abbreviated = 'PKWYS', domain = 'CITY' where unabbreviated  = 'PARKWAYS';
update d_street_type set abbreviated = 'SPURS', domain = 'CITY' where unabbreviated  = 'SPURS';
update d_street_type set abbreviated = 'WALKS', domain = 'CITY' where unabbreviated  = 'WALKS';

ALTER TABLE d_street_type add CONSTRAINT "d_street_type_UNQ2" UNIQUE (abbreviated);
