

-- select * from d_address_base_number_suffix;

-- drop TABLE d_address_base_number_suffix;

CREATE TABLE d_address_base_number_suffix
(
  id serial NOT NULL,
  suffix_value char(1),
  suffix_display character varying(4) NOT NULL,
  CONSTRAINT d_address_base_number_suffix_pk PRIMARY KEY (id)
)
WITH (OIDS=FALSE);
ALTER TABLE d_address_base_number_suffix OWNER TO postgres;


CREATE INDEX "d_address_base_number_suffix_unq"
  ON d_address_base_number_suffix
  USING btree (suffix_value);

insert into d_address_base_number_suffix (suffix_value, suffix_display) values (null, 'None');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values (chr(188), '1/4');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values (chr(189), '1/2');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values (chr(190), '3/4');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('A', 'A');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('B', 'B');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('C', 'C');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('D', 'D');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('E', 'E');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('F', 'F');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('G', 'G');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('H', 'H');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('I', 'I');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('J', 'J');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('K', 'K');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('L', 'L');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('M', 'M');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('N', 'N');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('O', 'O');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('P', 'P');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('Q', 'Q');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('R', 'R');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('S', 'S');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('T', 'T');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('U', 'U');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('V', 'V');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('W', 'W');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('X', 'X');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('Y', 'Y');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('Z', 'Z');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('0', '0');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('1', '1');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('2', '2');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('3', '3');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('4', '4');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('5', '5');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('6', '6');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('7', '7');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('8', '8');
insert into d_address_base_number_suffix (suffix_value, suffix_display) values ('9', '9');
