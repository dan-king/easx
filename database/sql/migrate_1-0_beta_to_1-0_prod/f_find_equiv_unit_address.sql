-- Function: _eas_find_equiv_unit_address()

DROP FUNCTION IF EXISTS _eas_find_equiv_unit_address(
    _address_base_id int, 
    _unit_num character varying(10),
    out _address_ids integer[]
);

CREATE OR REPLACE FUNCTION _eas_find_equiv_unit_address(
    _address_base_id int, 
    _unit_num character varying(10),
    out _address_ids integer[]
)
RETURNS integer[] AS
$BODY$
DECLARE 


BEGIN

-- TEST
-- select _eas_find_equiv_unit_address(...)
-- This was witten for the avs reload.

select into _address_ids ARRAY(
    select distinct a.address_id
    from addresses a
    where (
        (_unit_num is null and a.unit_num is null)
        or
        (_unit_num is not null and a.unit_num = _unit_num)
    )
    and a.address_base_id = _address_base_id
    and a.address_base_flg = false
    and a.retire_tms is null
);  


END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION _eas_find_equiv_unit_address(
    _address_base_id int, 
    _unit_num character varying(10),
    out _address_ids integer[]
)
OWNER TO postgres;

