CREATE INDEX streetnames_idx_base_street_name
    ON streetnames USING btree (base_street_name);

CREATE UNIQUE INDEX streetnames_unq_category
    ON streetnames ( seg_cnn)
    WHERE category = 'MAP';
