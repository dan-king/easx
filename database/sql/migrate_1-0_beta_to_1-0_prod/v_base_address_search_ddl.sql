
-- View: v_base_address_search

-- DROP VIEW v_base_address_search;

-- README
-- The first half of the UNION returns an aggregate row for the case where there are no unit addresses.
-- If this is the case, nothing is returned by the second half of the union
-- The sql in the first half includes
--    HAVING count(*) = 1
-- but does not include 
--    AND a.address_base_flg = true
-- The HAVING line is the trick - we want only rows from this select if there is only one base address row
--
-- The second half of the UNION returns an aggregate row for the case where there are unit addresses.
-- If this is the case, nothing is returned by the first half of the union
-- Conversly, the second half of the union has
--    HAVING count(*) >= 1
-- and
--    AND a.address_base_flg = false
-- This will return the aggregate only when there there are units addresses rows.
--
-- History
--     Removed 
--         AND sn.category = 'MAP'
--     from where clause so we will get road aliases such as "5th ave" instead of having to type "05th ave"
--
--    Added 
--        AND a.retire_tms is null
--    to exclude retired addresses
--

CREATE OR REPLACE VIEW v_base_address_search AS
SELECT
  ab.address_base_id,
  ab.base_address_prefix,
  ab.base_address_num,
  ab.base_address_suffix,
  ab.create_tms,
  st_transform(ab.geometry, 900913) AS geometry,
  sn.base_street_name AS street_name,
  sn.street_type,
  sn.category AS street_category,
  sn.prefix_direction AS street_prefix_direction,
  sn.suffix_direction AS street_suffix_direction,
  sn.full_street_name,
  z.zipcode, z.jurisdiction,
  '' AS summary_of_units,
  0 AS count_of_units,
  min(ad.disposition_description::text) AS disposition_min,
  max(ad.disposition_description::text) AS disposition_max,
  0 as geocoding_score
FROM
  address_base ab,
  addresses a,
  streetnames sn,
  zones z,
  d_address_disposition ad
WHERE 1 = 1
AND ab.address_base_id = a.address_base_id
AND ab.retire_tms is null
AND a.retire_tms is null
AND ab.zone_id = z.zone_id
AND ab.street_segment_id = sn.street_segment_id
AND a.disposition_code = ad.disposition_code
--AND sn.category = 'MAP'
GROUP BY
  ab.address_base_id,
  ab.base_address_prefix,
  ab.base_address_num,
  ab.base_address_suffix,
  ab.create_tms,
  ab.geometry,
  sn.base_street_name,
  sn.street_type,
  sn.category,
  sn.prefix_direction,
  sn.suffix_direction,
  sn.full_street_name,
  z.zipcode,
  z.jurisdiction
HAVING count(*) = 1
UNION ALL
SELECT 
 ab.address_base_id,
 ab.base_address_prefix,
 ab.base_address_num,
 ab.base_address_suffix,
 ab.create_tms,
 st_transform(ab.geometry, 900913) AS geometry,
 sn.base_street_name AS street_name,
 sn.street_type,
 sn.category AS street_category,
 sn.prefix_direction AS street_prefix_direction,
 sn.suffix_direction AS street_suffix_direction,
 sn.full_street_name,
 z.zipcode,
 z.jurisdiction,
 _sfmad_get_summary_of_units((COALESCE(a.unit_num, ''::character varying)::text)) AS summary_of_units,
 count(*) AS count_of_units,
 min(ad.disposition_description::text) AS disposition_min,
 max(ad.disposition_description::text) AS disposition_max,
 0 as geocoding_score
FROM 
 address_base ab,
 addresses a,
 streetnames sn,
 zones z,
 d_address_disposition ad
WHERE 1 = 1
AND ab.address_base_id = a.address_base_id
AND ab.retire_tms is null
AND a.retire_tms is null
AND ab.zone_id = z.zone_id
AND ab.street_segment_id = sn.street_segment_id
AND a.disposition_code = ad.disposition_code
AND a.address_base_flg = false
--AND sn.category = 'MAP'
GROUP BY 
 ab.address_base_id,
 ab.base_address_prefix,
 ab.base_address_num,
 ab.base_address_suffix,
 ab.create_tms,
 ab.geometry,
 sn.base_street_name,
 sn.street_type,
 sn.category,
 sn.prefix_direction,
 sn.suffix_direction,
 sn.full_street_name,
 z.zipcode,
 z.jurisdiction
HAVING count(*) >= 1;

ALTER TABLE v_base_address_search OWNER TO postgres;
