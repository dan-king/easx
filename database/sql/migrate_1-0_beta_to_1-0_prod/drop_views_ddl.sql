
-- We need to drop these views so we can alter a column in addresses.
-- See 
--    t_addresses_ddl-dml.sql
--    d_address_review_types_dml.sql
-- for details.
-- All of these views should be recreated later in this deploy.
DROP VIEW v_base_address_search;
DROP VIEW vw_address_reviews;
DROP VIEW vw_address_search;
DROP VIEW vw_base_address_x_parcels;
DROP VIEW vw_blocklot_x_address;
-- drop view avs_reload.vw_eas_addresses;
