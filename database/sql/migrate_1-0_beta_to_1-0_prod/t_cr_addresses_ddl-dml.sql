
ALTER TABLE cr_addresses DROP COLUMN unit_num_prefix;
ALTER TABLE cr_addresses DROP COLUMN unit_num_suffix;
ALTER TABLE cr_addresses ALTER column unit_num TYPE character varying(10);
