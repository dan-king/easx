
ALTER TABLE cr_address_x_parcels
   ADD COLUMN address_x_parcel_id integer;

ALTER TABLE cr_address_x_parcels
   ADD COLUMN link boolean;

ALTER TABLE cr_address_x_parcels
   ADD COLUMN unlink boolean;

ALTER TABLE cr_address_x_parcels
   ADD CONSTRAINT "cr_address_x_parcels_FK_address_x_parcel" FOREIGN KEY(address_x_parcel_id)
   REFERENCES address_x_parcels (id) MATCH SIMPLE
   ON UPDATE RESTRICT ON DELETE RESTRICT;
