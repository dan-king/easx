-- Function: _eas_smart_find_equiv_base_address()

DROP FUNCTION IF EXISTS _eas_find_equiv_base_address_smart(
    _number int, 
    _number_suffix character varying(20), 
    _street_name character varying(60), 
    _street_suffix character varying(14),
    _zip_code character varying(10),
    out _address_base_ids integer[]
);

CREATE OR REPLACE FUNCTION _eas_find_equiv_base_address_smart(
    _number int, 
    _number_suffix character varying(20), 
    _street_name character varying(60), 
    _street_suffix character varying(14),
    _zip_code character varying(10),
    out _address_base_ids integer[]
)
RETURNS integer[] AS
$BODY$
DECLARE 


BEGIN

    select into _address_base_ids _eas_find_equiv_base_address(_number, _number_suffix,  _street_name, _street_suffix,   _zip_code);
    if array_upper(_address_base_ids, 1) is not null then
        return;
    end if;

    select into _address_base_ids _eas_find_equiv_base_address(_number, _number_suffix,  _street_name, _street_suffix,   null);
    if array_upper(_address_base_ids, 1) is not null then
        return;
    end if;

    select into _address_base_ids _eas_find_equiv_base_address(_number, null,     _street_name, _street_suffix,   null);
    if array_upper(_address_base_ids, 1) is not null then
        return;
    end if;

    select into _address_base_ids _eas_find_equiv_base_address(_number, null,     _street_name, null,             null);

    return;

END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION _eas_find_equiv_base_address_smart(
    _number int, 
    _number_suffix character varying(20), 
    _street_name character varying(60), 
    _street_suffix character varying(14),
    _zip_code character varying(10),
    out _address_base_ids integer[]
)
OWNER TO postgres;
