-- View: vw_street_type
-- This view is to help document the model and the fact that these data are from the USPS
--     http://www.usps.com/ncsc/lookups/abbr_suffix.txt

-- DROP VIEW vw_street_type;

CREATE OR REPLACE VIEW vw_street_type AS 
	select
	  dst.unabbreviated,
	  dsta.alias,
	  dst.abbreviated,
	  dst.domain
	from d_street_type dst,
	     d_street_type_aliases dsta
	where  dst.id = dsta.street_type_id
	order by dst.unabbreviated, dst.domain;

ALTER TABLE vw_street_type OWNER TO postgres;
