-- Function: public._eas_validate_addresses_after_etl()

DROP FUNCTION IF EXISTS public._eas_validate_addresses_after_etl(
    VALIDATION_SCOPE character varying(10)
);

CREATE OR REPLACE FUNCTION public._eas_validate_addresses_after_etl(
    VALIDATION_SCOPE character varying(10)
)
RETURNS text AS
$BODY$
DECLARE 

    -- There is probably some _simple_ and cool way to handle domain constants - maybe _you_ can show us?
    -- select * from d_address_review_types;
    PARCEL_RETIRED int = 1; -- parcel is retired
    STREET_RETIRED int = 5; -- street is retired
    ADDRESS_PARCEL_DISTANCE_GT_500 int = 4; -- address is not inside parcel

BEGIN


    if VALIDATION_SCOPE is null
       or
       VALIDATION_SCOPE not in ('PARCELS', 'STREETS', 'ALL') then
            raise exception 'expected one of (PARCELS, STREETS, ALL) but got %', coalesce(VALIDATION_SCOPE, 'NULL');
    end if;


	-- parcel is retired
	insert into address_review (address_id, review_type_id, message)
    select axp.address_id, PARCEL_RETIRED, 'apn: ' || p.blk_lot
    from
        address_x_parcels axp,
        parcels p
    where 1=1
    and axp.parcel_id = p.parcel_id
    and axp.retire_tms is null
    and p.date_map_drop is not null
    and not exists (
        select 1
        from address_review ar
        where ar.address_id = axp.address_id
        and ar.review_type_id = PARCEL_RETIRED
    )
    and VALIDATION_SCOPE in ('PARCELS', 'ALL');



    -- street is retired
    insert into address_review (address_id, review_type_id, message)
    select distinct a.address_id, STREET_RETIRED, 'street: ' || sn.full_street_name
    from
        addresses a,
        address_base ab,
        street_segments ss,
        streetnames sn
    where ss.street_segment_id = ab.street_segment_id
    and ab.address_base_id = a.address_base_id
    and a.address_base_flg = 'TRUE'
    and ab.street_segment_id = sn.street_segment_id
    and sn.category = 'MAP'
    and ss.date_dropped is not null
    and ab.retire_tms is null
    and not exists (
        select 1
        from address_review ar
        where ar.address_id = a.address_id
        and ar.review_type_id = STREET_RETIRED
    )
    and VALIDATION_SCOPE in ('STREETS', 'ALL');


    -- Allow an address to link to a parcel only if the parcel is within 500 feet of the address point.
	insert into address_review (address_id, review_type_id, message)
    select  distinct
			a.address_id, ADDRESS_PARCEL_DISTANCE_GT_500, 'apn:' || p.blk_lot
    from
        address_base ab,
        addresses a,
        address_x_parcels axp,
        parcels p
    where ab.address_base_id = a.address_base_id
    and a.address_id = axp.address_id
    and axp.parcel_id = p.parcel_id
    and a.address_base_flg = true
    and ab.retire_tms is null
    and a.retire_tms is null
    and axp.retire_tms is null
    and p.date_map_drop is null
    and st_distance(p.geometry, ab.geometry) > 500
    and a.address_id not in (
        select address_id
        from address_review
        where resolution_tms is null
        and review_type_id = ADDRESS_PARCEL_DISTANCE_GT_500
    )
    and VALIDATION_SCOPE in ('PARCELS', 'ALL');


	return 'success!';


END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION public._eas_validate_addresses_after_etl(
    VALIDATION_SCOPE character varying(10)
)
OWNER TO postgres;
