-- Function: _eas_retire_base_address()

DROP FUNCTION IF EXISTS _eas_conflate_base_address(
    _address_number     int,
    _street_name        character varying(60),
    _street_suffix      character varying(6),
    _geometry           geometry,
    _current_tms        timestamp without time zone
);

CREATE OR REPLACE FUNCTION _eas_conflate_base_address(
    _address_number     int,
    _street_name        character varying(60),
    _street_suffix      character varying(6),
    _geometry           geometry,
    _current_tms        timestamp without time zone
)
RETURNS int AS
$BODY$
DECLARE

    _message                    character varying(256);
    _count                      int = 0;
    _existing_address_base_id   int;
    _new_ids                    int[];
    _address_base_ids           int[];
    _new_address_base_id        int;
    _new_address_id             int;
    _zone_id                    int;
    _street_segment_id          int;
    _base_address_number_suffix character varying(10);

BEGIN

    _message = _address_number::text || ' ' || _street_name || ' ' || _street_suffix || ' ' || asewkt(_geometry);
    --raise notice '_eas_conflate_base_address: %', _message;

    declare cursorBaseAddressesToConflate cursor is
        select 
            ab.address_base_id,
            ab.zone_id,
            ab.street_segment_id,
            ab.base_address_suffix
        from
            address_base ab,
            street_segments ss,
            streetnames sn,
            cleanup_conflate_base_addresses ccba
        where ab.street_segment_id = ss.street_segment_id
        and ccba.address_base_id = ab.address_base_id
        and ccba.processed_flg = false
        and ab.geometry = _geometry
        and ss.street_segment_id = sn.street_segment_id
        and sn.category = 'MAP'
        and ab.base_address_num = _address_number
        and sn.base_street_name = _street_name
        and sn.street_type = _street_suffix
        order by ab.base_address_suffix;
    begin
        open cursorBaseAddressesToConflate;
        loop
            fetch cursorBaseAddressesToConflate into _existing_address_base_id, _zone_id, _street_segment_id, _base_address_number_suffix;
            if not found then
                exit;
            end if;
            
            begin

                _count = _count + 1;
                
                --raise notice '_base_address_number_suffix % ', _base_address_number_suffix;
                
                -- base address
                if _count = 1 then
                    -- find existing or insert new

                    -- look for existing
                    select into _address_base_ids ARRAY(
                        select distinct ab.address_base_id
                        from address_base ab
                        inner join streetnames sn on ab.street_segment_id = sn.street_segment_id
                        inner join zones z on ab.zone_id = z.zone_id
                        where ab.base_address_num = _address_number
                        and sn.base_street_name = _street_name
                        and ab.retire_tms is null
                        and z.zone_id = _zone_id
                        and coalesce(sn.street_type, '') = coalesce(_street_suffix, '')
                        and ab.base_address_suffix is null
                    );

                    if array_upper(_address_base_ids, 1) is null then
                        -- nothing found - insert new
                        select into _new_ids public._eas_insert_base_address( null::text, _address_number::int, null::text, _zone_id::int, _geometry, _street_segment_id::int, _current_tms::timestamp without time zone, null::int);
                        _new_address_base_id = _new_ids[1];
                    else
                        -- found match
                        if array_upper(_address_base_ids, 1) > 1 then
                            raise exception 'multiple equivalent base address records already exist';
                        end if;
                        _new_address_base_id = _address_base_ids[1];
                    end if;


                end if;

                -- unit address
                -- use existing base address as a template
                _new_address_id = null;
                select address_id into _new_address_id from addresses where address_base_id = _new_address_base_id and unit_num = _base_address_number_suffix limit 1;

                if _new_address_id is null then
                    -- no match found - insert new
                    select into _new_address_id nextval('addresses_address_id_seq');

                    -- domain values; unit type 0: other; floor 105: 'unknown'; disposition code 1: official
                    insert into addresses ( address_id, address_base_id, unit_num, unit_type_id, floor_id, disposition_code, address_base_flg, unq_adds_id, activate_change_request_id, create_tms, last_change_tms)
                    values (_new_address_id, _new_address_base_id, _base_address_number_suffix, 0, 105, 1, false, null, 0, _current_tms, _current_tms);
                end if;

                -- axp
                -- insert trigger may raise exception - we may want to catch and ignore?
                insert into address_x_parcels (parcel_id, address_id, activate_change_request_id, create_tms, last_change_tms)
                select parcel_id, _new_address_id, 0, _current_tms, _current_tms
                from address_x_parcels
                where address_id in (
                    select address_id
                    from addresses 
                    where address_base_id = _existing_address_base_id
                );
                

            end;

        end loop;
        close cursorBaseAddressesToConflate;
    end;


    return _count;


END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION _eas_conflate_base_address(
    _address_number     int,
    _street_name        character varying(60),
    _street_suffix      character varying(6),
    _geometry           geometry,
    _current_tms        timestamp without time zone
)
OWNER TO postgres;
