-- Table: address_base_history

-- DROP TABLE address_base_history cascade;

CREATE TABLE address_base_history
(
  id serial NOT NULL,
  address_base_id integer NOT NULL,
  base_address_prefix character varying(10),
  base_address_num integer NOT NULL,
  base_address_suffix character varying(10),
  create_tms timestamp without time zone NOT NULL,
  retire_tms timestamp without time zone,
  zone_id integer NOT NULL,
  street_segment_id integer,
  distance_to_segment double precision,
  geometry geometry,
  unq_adds_id integer,
  last_change_tms timestamp without time zone,
  history_action character varying(10) NOT NULL,
  CONSTRAINT "address_base_history_PK" PRIMARY KEY (id),
  CONSTRAINT "address_base_history_UNQ"
    UNIQUE (address_base_id, last_change_tms, history_action),
  CONSTRAINT "address_base_history_FK_streets" FOREIGN KEY (street_segment_id)
      REFERENCES street_segments (street_segment_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "address_base_history_FK_zones" FOREIGN KEY (zone_id)
      REFERENCES zones (zone_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (OIDS=FALSE);
ALTER TABLE address_base OWNER TO postgres;

CREATE INDEX "address_base_history_IDX"
  ON address_base_history
  USING btree (last_change_tms);

delete from geometry_columns
where f_table_schema = 'public'
and f_table_name = 'address_base_history';

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
VALUES ('', 'public', 'address_base_history', 'geometry', 2, 2227, 'POINT');
