
-- DROP FUNCTION _eas_address_base_before() cascade;

CREATE FUNCTION _eas_address_base_before() RETURNS trigger AS $_eas_address_base_before$

    DECLARE

        _message character varying(256);

    BEGIN

        -- Some updates do not change data.
        -- http://sfgovdt.jira.com/browse/MAD-66
        
        IF TG_OP = 'DELETE' THEN
            RAISE EXCEPTION 'deleting address_base rows is not allowed';
        ELSIF TG_OP = 'UPDATE' THEN
            IF NEW.retire_tms IS NOT NULL THEN
                -- Do not allow a retired to be retired again. This is attempted by some backend processes and should be considered a defect.
                IF (OLD.retire_tms is not null) THEN
                    RAISE EXCEPTION 'update rejected - address_base - you may not retire a row that is already retired - address_base_id: %', OLD.address_base_id::text;
                END IF;
            END IF;
        ELSE

            -- INSERT

            -- check for duplicates
            -- If a streetname is 'UNKNOWN':
            --  > we do not look for duplicates
            --  > http://code.google.com/p/eas/issues/detail?id=349
            --  > remove this logic after we go live
            PERFORM 1
            FROM
                address_base ab,
                streetnames sn,
                zones z
            WHERE ab.street_segment_id = sn.street_segment_id
            and ab.zone_id = z.zone_id
            and ab.retire_tms is null
            and sn.category = 'MAP'
            and ab.base_address_prefix is not distinct from NEW.base_address_prefix
            and ab.base_address_num =                       NEW.base_address_num
            and ab.base_address_suffix is not distinct from NEW.base_address_suffix
            and ab.retire_tms is null
            and sn.category = 'MAP'
            and sn.base_street_name =                       (select sn.base_street_name from address_base ab, streetnames sn where ab.street_segment_id = sn.street_segment_id and ab.street_segment_id = NEW.street_segment_id and sn.category = 'MAP' limit 1)
            and sn.street_type is not distinct from         (select sn.street_type from address_base ab, streetnames sn where ab.street_segment_id = sn.street_segment_id and ab.street_segment_id = NEW.street_segment_id and sn.category = 'MAP' limit 1)
            and 'UNKNOWN' <>                                (select sn.base_street_name from address_base ab, streetnames sn where ab.street_segment_id = sn.street_segment_id and ab.street_segment_id = NEW.street_segment_id and sn.category = 'MAP' limit 1)
            and z.jurisdiction =                            (select jurisdiction from zones where zone_id = NEW.zone_id);

            IF FOUND THEN
                RAISE EXCEPTION 'insert rejected - address_base - duplicate un-retired row was found';
            END IF;
            
            ----- street number suffix
            _message = null;
            select 'unsupported street number suffix value: ' || NEW.base_address_num || ' ' || NEW.base_address_suffix || ' '  || sn.full_street_name into _message
            from streetnames sn
            left outer join d_address_base_number_suffix nsd on (nsd.suffix_value = NEW.base_address_suffix) 
            where nsd.suffix_value is null
            and NEW.street_segment_id = sn.street_segment_id
            and sn.category = 'MAP';


            if _message is not null then
                RAISE EXCEPTION 'insert rejected - address_base: %', _message;
            end if;
            -----

        END IF;

        RETURN NEW;

    END;
$_eas_address_base_before$ LANGUAGE plpgsql;

CREATE TRIGGER _eas_address_base_before BEFORE INSERT OR UPDATE OR DELETE ON address_base
    FOR EACH ROW EXECUTE PROCEDURE _eas_address_base_before();
