-- Function: _eas_find_equiv_base_address()

DROP FUNCTION IF EXISTS _eas_find_equiv_base_address(
    _number int, 
    _number_suffix character varying(20), 
    _street_name character varying(60), 
    _street_suffix character varying(14),
    _zip_code character varying(10),
    out _address_base_ids integer[]
);

CREATE OR REPLACE FUNCTION _eas_find_equiv_base_address(
    _number int, 
    _number_suffix character varying(20), 
    _street_name character varying(60), 
    _street_suffix character varying(14),
    _zip_code character varying(10),
    out _address_base_ids integer[]
)
RETURNS integer[] AS
$BODY$
DECLARE 


BEGIN

-- TEST
-- select _eas_find_equiv_base_address(1501, null, 'BIRCHWOOD', 'COURT', '94112')

-- This was witten for the avs reload.
-- As such...
--   we wse MAP and ALIAS streets
--   we make zip optional becuase we may not have it
--   odds are that a macth with no zip constraint is an equivalence
--   it's not quite the same as the duplicate check you see in the base_address trigger

select into _address_base_ids ARRAY(
    select distinct ab.address_base_id
    from address_base ab
    inner join streetnames sn on ab.street_segment_id = sn.street_segment_id
    left outer join zones z on ab.zone_id = z.zone_id 
    left outer join d_street_type st on sn.street_type = st.abbreviated
    where ab.base_address_num = _number
    and sn.base_street_name = _street_name
    and ab.retire_tms is null
    and (
        _zip_code is null
        or
        _zip_code is not null and z.zipcode = _zip_code
    )
    and (
        _street_suffix is null
        or
        _street_suffix is not null and coalesce(st.unabbreviated, '') = _street_suffix
    )
    and (
        _number_suffix is null
        or
        _number_suffix is not null and coalesce(ab.base_address_suffix, '') = _number_suffix
    )
);

END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION _eas_find_equiv_base_address(
    _number int, 
    _number_suffix character varying(20), 
    _street_name character varying(60), 
    _street_suffix character varying(14),
    _zip_code character varying(10),
    out _address_base_ids integer[]
)
OWNER TO postgres;
