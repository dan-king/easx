
----- set up primary key field
CREATE SEQUENCE address_x_parcels_id_seq;

ALTER TABLE address_x_parcels ADD COLUMN id INTEGER;

UPDATE address_x_parcels SET id = nextval('address_x_parcels_id_seq');

ALTER TABLE address_x_parcels ALTER COLUMN id SET DEFAULT nextval('address_x_parcels_id_seq');

ALTER TABLE address_x_parcels ALTER COLUMN id SET NOT NULL;

ALTER TABLE address_x_parcels
  DROP CONSTRAINT address_x_parcel_pk;

ALTER TABLE address_x_parcels
  ADD CONSTRAINT address_x_parcel_pk PRIMARY KEY (id);
-----


ALTER TABLE address_x_parcels
   ADD COLUMN activate_change_request_id integer;

ALTER TABLE address_x_parcels
   ADD COLUMN retire_change_request_id integer;

ALTER TABLE address_x_parcels
   ADD COLUMN create_tms timestamp without time zone DEFAULT now();

ALTER TABLE address_x_parcels
   ADD COLUMN last_change_tms timestamp without time zone;

ALTER TABLE address_x_parcels
   ADD COLUMN retire_tms timestamp without time zone;

INSERT INTO cr_change_requests(change_request_id, review_status) VALUES (0, 2);

INSERT INTO change_requests(change_request_id, cr_change_request_id) VALUES (0, 0);

--
update address_x_parcels set activate_change_request_id = 0;
ALTER TABLE address_x_parcels ALTER COLUMN activate_change_request_id SET NOT NULL;
--

--
update address_x_parcels set create_tms = '2009-09-01 16:39:13.599';
ALTER TABLE address_x_parcels ALTER COLUMN create_tms SET NOT NULL;
--

--
update address_x_parcels set last_change_tms = '2009-09-01 16:39:13.599';
ALTER TABLE address_x_parcels ALTER COLUMN last_change_tms SET NOT NULL;
--

-----
CREATE INDEX "address_x_parcels_IDX_parcel_id"
   ON address_x_parcels USING btree (parcel_id);

CREATE INDEX "address_x_parcels_IDX_address_id"
   ON address_x_parcels USING btree (address_id);
-----
