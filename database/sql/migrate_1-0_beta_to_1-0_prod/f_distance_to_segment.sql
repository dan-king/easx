--This function calculates the distance between the address_base.geometry to related street_segment.geometry

-- Function: public._sfmad_distance_to_segment(integer)

-- DROP FUNCTION public._sfmad_distance_to_segment(integer);

CREATE OR REPLACE FUNCTION public._sfmad_distance_to_segment(addr_base_id integer)
  RETURNS double precision AS
$BODY$
DECLARE
dist double precision;
BEGIN
    dist := (select st_distance(st.geometry, ab.geometry)
		FROM address_base ab, street_segments st
		WHERE 1=1
		  AND ab.address_base_id = addr_base_id
		  AND ab.street_segment_id = st.street_segment_id);
    return (dist);
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION public._sfmad_distance_to_segment(integer) OWNER TO postgres;
