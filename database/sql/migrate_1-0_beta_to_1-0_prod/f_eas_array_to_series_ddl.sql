
create or replace function _eas_array_to_series(anyarray) 
returns setof anyelement as $$
  select $1[i] from generate_series(array_lower($1,1), array_upper($1,1)) i;
$$ language'sql' immutable;

alter function _eas_array_to_series(anyarray) owner to postgres;
