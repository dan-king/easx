-- Table: address_x_parcels_history

-- DROP TABLE address_x_parcels_history cascade;

CREATE TABLE address_x_parcels_history
(
  id serial NOT NULL,
  address_x_parcel_id integer NOT NULL,
  parcel_id integer NOT NULL,
  address_id integer NOT NULL,
  confidence_score integer,
  activate_change_request_id integer,
  retire_change_request_id integer,
  create_tms timestamp without time zone DEFAULT now(),
  last_change_tms timestamp without time zone,
  retire_tms timestamp without time zone,
  history_action character varying(10) NOT NULL,
  CONSTRAINT "address_x_parcels_history_PK" PRIMARY KEY (id),
  CONSTRAINT "address_x_parcels_history_FK_address_x_parcels_id" FOREIGN KEY (address_x_parcel_id)
      REFERENCES address_x_parcels (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (OIDS=FALSE);
ALTER TABLE address_x_parcels_history OWNER TO postgres;

CREATE INDEX "address_x_parcels_history_IDX_last_change_tms"
  ON address_x_parcels_history
  USING btree (last_change_tms);
