-- View: vw_base_addresses

-- DROP VIEW vw_base_addresses;

CREATE OR REPLACE VIEW vw_base_addresses AS
    SELECT
        a.address_base_id,
        a.base_address_prefix,
        a.base_address_num,
        a.base_address_suffix,
        st_transform(a.geometry, 900913) AS geometry,
        c.full_street_name,
        d.zipcode,
        d.jurisdiction
    FROM
        address_base a,
        streetnames c,
        zones d
  WHERE a.retire_tms IS NULL
  AND a.zone_id = d.zone_id
  AND a.street_segment_id = c.street_segment_id
  AND c.category::text = 'MAP'::text
  ORDER BY c.base_street_name, c.street_type, a.base_address_num;

ALTER TABLE vw_blocklot_x_address OWNER TO postgres;

/*
ALTER TABLE vw_base_addresses OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_base_addresses TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_base_addresses TO django;
*/