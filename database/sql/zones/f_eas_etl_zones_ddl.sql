
-- If this fails part way through, it is critical that you enable the triggers before you make the application available again.
-- Does the transaction protect us from this problem?

-- DROP FUNCTION _eas_etl_zones();

CREATE OR REPLACE FUNCTION _eas_etl_zones() RETURNS text AS
$BODY$
DECLARE

BEGIN

    LOCK TABLE cr_address_base, address_base, address_base_history IN ACCESS EXCLUSIVE MODE NOWAIT;

    ----- temporarily remove specific constraints on specific tables
    alter table cr_address_base alter column zone_id drop NOT NULL;
    alter table cr_address_base drop constraint "zones_X_cr_address_base";

    alter table address_base alter column zone_id drop NOT NULL;
    alter table address_base drop constraint "zone_X_address_base";
    alter table address_base disable trigger _eas_address_base_before;
    alter table address_base disable trigger _eas_address_base_after;

    alter table address_base_history alter column zone_id drop NOT NULL;
    alter table address_base_history drop constraint "address_base_history_FK_zones";
    -----

    truncate zones;

    insert into zones (zipcode, jurisdiction, active_date, retire_date, geometry)
    select zipcode, jurisdiction, active_date, retire_date, geometry
    from zones_staging;


    ----- update zone_id in cr_address_base
    update cr_address_base set zone_id = null;

    -- addresses that are contained by zones
    update cr_address_base
    set zone_id = z.zone_id
    from zones z
    where st_contains(z.geometry, cr_address_base.geometry);
    
    -- addresses are not contained by zones - for example, the addresses out in the bay - use the nearest zone
    update cr_address_base
    set zone_id = (
        select zone_id
        from zones z
        where st_dwithin(z.geometry, cr_address_base.geometry, 5280)
        order by st_distance(z.geometry, cr_address_base.geometry)
        limit 1
    )
    where cr_address_base.zone_id is null;
    -----


    ----- Same as above but for address_base.
    update address_base set zone_id = null;

    update address_base
    set zone_id = z.zone_id
    from zones z
    where st_contains(z.geometry, address_base.geometry);
    
    update address_base
    set zone_id = (
        select zone_id
        from zones z
        where st_dwithin(z.geometry, address_base.geometry, 5280)
        order by st_distance(z.geometry, address_base.geometry)
        limit 1
    )
    where address_base.zone_id is null;
    -----


    ----- Same as above but for address_base_history.
    update address_base_history set zone_id = null;

    update address_base_history
    set zone_id = z.zone_id
    from zones z
    where st_contains(z.geometry, address_base_history.geometry);
    
    update address_base_history
    set zone_id = (
        select zone_id
        from zones z
        where st_dwithin(z.geometry, address_base_history.geometry, 5280)
        order by st_distance(z.geometry, address_base_history.geometry)
        limit 1
    )
    where address_base_history.zone_id is null;
    -----
    

    ----- add specific constraints back to specific tables
    alter table cr_address_base alter column zone_id set not null;
    alter table cr_address_base
        add CONSTRAINT "zones_X_cr_address_base" FOREIGN KEY (zone_id)
            REFERENCES zones (zone_id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION;

    alter table address_base alter column zone_id set not null;
    alter table address_base
        add CONSTRAINT "zone_X_address_base" FOREIGN KEY (zone_id)
            REFERENCES zones (zone_id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION;
    alter table address_base enable trigger _eas_address_base_before;
    alter table address_base enable trigger _eas_address_base_after;

    alter table address_base_history alter column zone_id set not null;
    alter table address_base_history
        add CONSTRAINT "address_base_history_FK_zones" FOREIGN KEY (zone_id)
            REFERENCES zones (zone_id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION;
    -----


    return 'success!';


END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _eas_etl_zones() OWNER TO eas_dbo;
