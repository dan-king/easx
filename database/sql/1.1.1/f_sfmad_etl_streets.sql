﻿-- Function: public._sfmad_etl_streets()
--
-- Use non-temp tables to allow 2 phase commit.
--

DROP FUNCTION IF EXISTS public._sfmad_etl_streets(
  job_id_in char(36),
  out success boolean,
  out messageText text,
  out newStreetsCount int,
  out newStreetnamesCount int,
  out streetsAlterCount int,
  out streetRetireCount int
);

DROP FUNCTION IF EXISTS public._sfmad_etl_streets();

CREATE OR REPLACE FUNCTION _sfmad_etl_streets(
  job_id_in char(36),
  out success boolean,
  out messageText text,
  out newStreetsCount int,
  out newStreetnamesCount int,
  out streetsAlterCount int,
  out streetRetireCount int
)
RETURNS record AS
$BODY$
DECLARE
  tms_value time;

BEGIN

  success = true;

  -- there are two records in the street segments data that are need to be deleted.  i dont know why (ABL)
  delete from street_segments_staging where seg_cnn = 0;

  -- If the staging table has more records than the target, we have an issue.
  -- Exclude the 'UNKNOWN' segment because it will never be in the DPW data.
  if (select count(*) from street_segments_staging) < (select count(*) from street_segments where st_name != 'UNKNOWN') then
    success = false;
    messageText = 'ETL failed. There are more rows in street_segments than in street_segments_staging.';
    return;
  end if;

  --check that each each street_segments_staging record has a unique seg_cnn value
  if (select count(*) from street_segments_staging) <> (select count(*) from (select distinct (seg_cnn) from street_segments_staging) a) then
    success = false;
    messageText = 'ETL failed. seg_cnn values in street_segments_staging are not unique.';
    return;
  end if;


  truncate street_segments_work;


  --insert new street segment records into main table...generate street_segment_id value
  --1. store the new street segment records
  insert into street_segments_work (
      seg_cnn,
      str_seg_cnn,
      st_name,
      st_type,
      l_f_add,
      l_t_add,
      r_f_add,
      r_t_add,
      f_st,
      t_st,
      f_node_cnn,
      t_node_cnn,
      date_added,
      gds_chg_id_add,
      date_dropped,
      gds_chg_id_dropped,
      date_altered,
      gds_chg_id_altered,
      zip_code,
      district,
      accepted,
      jurisdiction,
      n_hood,
      layer,
      active,
      future,
      toggle_date,
      geometry,
      status
  )
  select
    sss.seg_cnn,
    sss.str_seg_cnn,
    sss.st_name,
    sss.st_type,
    sss.l_f_add,
    sss.l_t_add,
    sss.r_f_add,
    sss.r_t_add,
    sss.f_st,
    sss.t_st,
    sss.f_node_cnn,
    sss.t_node_cnn,
    sss.date_added,
    sss.gds_chg_id_add,
    sss.date_dropped,
    sss.gds_chg_id_dropped,
    sss.date_altered,
    sss.gds_chg_id_altered,
    sss.zip_code,
    sss.district,
    sss.accepted,
    sss.jurisdiction,
    sss.n_hood,
    sss.layer,
    sss.active,
    sss.future,
    sss.toggle_date,
    st_transform(sss.geometry, 2227),
    'NEW'
  FROM street_segments_staging sss
  left join street_segments b on sss.seg_cnn = b.seg_cnn
  where sss.seg_cnn <> 0
  and b.seg_cnn is null;

  --capture count of new street segments to be added
  newStreetsCount = (select count(*) from street_segments_work where status = 'NEW');

  --2.insert new street segments records in street_segments, generate a new street_segment_id
  insert into street_segments (
    seg_cnn,
    str_seg_cnn,
    st_name,
    st_type,
    l_f_add,
    l_t_add,
    r_f_add,
    r_t_add,
    f_st,
    t_st,
    f_node_cnn,
    t_node_cnn,
    date_added,
    gds_chg_id_add,
    date_dropped,
    gds_chg_id_dropped,
    date_altered,
    gds_chg_id_altered,
    zip_code,
    district,
    accepted,
    jurisdiction,
    n_hood,
    layer,
    active,
    future,
    toggle_date,
    create_tms,
    geometry
  )
  Select
    a.seg_cnn,
    a.str_seg_cnn,
    a.st_name,
    a.st_type,
    a.l_f_add,
    a.l_t_add,
    a.r_f_add,
    a.r_t_add,
    a.f_st,
    a.t_st,
    a.f_node_cnn,
    a.t_node_cnn,
    a.date_added,
    a.gds_chg_id_add,
    a.date_dropped,
    a.gds_chg_id_dropped,
    a.date_altered,
    a.gds_chg_id_altered,
    a.zip_code,
    a.district,
    a.accepted,
    a.jurisdiction,
    a.n_hood,
    a.layer,
    a.active,
    a.future,
    a.toggle_date,
    now(),
    a.geometry
  from street_segments_work a
  where a.status = 'NEW';


  --3.add new address range records to the address range table
  insert into street_address_ranges (left_from_address, left_to_address, right_from_address, right_to_address, create_tms, street_segment_id)
  select
    a.l_f_add,
    a.l_t_add,
    a.r_f_add,
    a.r_t_add,
    now(),
    b.street_segment_id
  from  street_segments_work a,
        street_segments b
  where a.seg_cnn = b.seg_cnn
  and a.status = 'NEW';


  --update business values of all parcels based on the latest data without changing street_segment_id

  --store records that have a newly created drop/retire date
  insert into street_segments_work (
      seg_cnn,
      str_seg_cnn,
      st_name,
      st_type,
      l_f_add,
      l_t_add,
      r_f_add,
      r_t_add,
      f_st,
      t_st,
      f_node_cnn,
      t_node_cnn,
      date_added,
      gds_chg_id_add,
      date_dropped,
      gds_chg_id_dropped,
      date_altered,
      gds_chg_id_altered,
      zip_code,
      district,
      accepted,
      jurisdiction,
      n_hood,
      layer,
      active,
      future,
      toggle_date,
      geometry,
      status
  ) select
      sss.seg_cnn,
      sss.str_seg_cnn,
      sss.st_name,
      sss.st_type,
      sss.l_f_add,
      sss.l_t_add,
      sss.r_f_add,
      sss.r_t_add,
      sss.f_st,
      sss.t_st,
      sss.f_node_cnn,
      sss.t_node_cnn,
      sss.date_added,
      sss.gds_chg_id_add,
      sss.date_dropped,
      sss.gds_chg_id_dropped,
      sss.date_altered,
      sss.gds_chg_id_altered,
      sss.zip_code,
      sss.district,
      sss.accepted,
      sss.jurisdiction,
      sss.n_hood,
      sss.layer,
      sss.active,
      sss.future,
      sss.toggle_date,
      st_transform(sss.geometry, 2227),
      'RETIRED'
  from
    street_segments ss,
    street_segments_staging sss
  where ss.seg_cnn = sss.seg_cnn
  AND COALESCE(ss.date_dropped, '1900-01-01') <> COALESCE(sss.date_dropped, '1900-01-01');

  streetRetireCount = (select count(*) from street_segments_work where status = 'RETIRED');

  update street_segments a
  set date_dropped = (
    select b.date_dropped
    from street_segments_work b
    where a.seg_cnn = b.seg_cnn
  )
  from street_segments_work c
  where a.seg_cnn = c.seg_cnn
  and c.status = 'RETIRED';


  -- store records that have a newly created alter dates
  insert into street_segments_work (
      seg_cnn,
      str_seg_cnn,
      st_name,
      st_type,
      l_f_add,
      l_t_add,
      r_f_add,
      r_t_add,
      f_st,
      t_st,
      f_node_cnn,
      t_node_cnn,
      date_added,
      gds_chg_id_add,
      date_dropped,
      gds_chg_id_dropped,
      date_altered,
      gds_chg_id_altered,
      zip_code,
      district,
      accepted,
      jurisdiction,
      n_hood,
      layer,
      active,
      future,
      toggle_date,
      geometry,
      status
  ) select
      sss.seg_cnn,
      sss.str_seg_cnn,
      sss.st_name,
      sss.st_type,
      sss.l_f_add,
      sss.l_t_add,
      sss.r_f_add,
      sss.r_t_add,
      sss.f_st,
      sss.t_st,
      sss.f_node_cnn,
      sss.t_node_cnn,
      sss.date_added,
      sss.gds_chg_id_add,
      sss.date_dropped,
      sss.gds_chg_id_dropped,
      sss.date_altered,
      sss.gds_chg_id_altered,
      sss.zip_code,
      sss.district,
      sss.accepted,
      sss.jurisdiction,
      sss.n_hood,
      sss.layer,
      sss.active,
      sss.future,
      sss.toggle_date,
      st_transform(sss.geometry, 2227),
      'ALTERED'
  from
    street_segments ss,
    street_segments_staging sss
  where ss.seg_cnn = sss.seg_cnn
  and (
    coalesce(ss.date_altered, '1900-01-01') <> coalesce(sss.date_altered, '1900-01-01')
    or coalesce(ss.l_f_add, 0) <> coalesce(sss.l_f_add, 0) 
    or coalesce(ss.l_t_add, 0) <> coalesce(sss.l_t_add, 0) 
    or coalesce(ss.r_f_add, 0) <> coalesce(sss.r_f_add, 0) 
    or coalesce(ss.r_t_add, 0) <> coalesce(sss.r_t_add, 0) 
  );


  streetsAlterCount = (select count(*) from street_segments_work where status = 'ALTERED');

  -- todo
  -- Decide where you want to keep the left-from, left-to, etc and keep it in one single place.
  -- In the meantime, we maintain wherever we see it.
  update street_segments ss
  set 
    date_altered = ssw.date_altered,
    geometry = coalesce(st_transform(ssw.geometry, 2227), ss.geometry),
    l_f_add = ssw.l_f_add,
    l_t_add = ssw.l_t_add,
    r_f_add = ssw.r_f_add,
    r_t_add = ssw.r_t_add
  from street_segments_work ssw
  where ss.seg_cnn = ssw.seg_cnn
  and ssw.status = 'ALTERED'; 


  ----- BEGIN street address ranges
  insert into street_address_ranges (left_from_address, left_to_address, right_from_address, right_to_address, create_tms, street_segment_id)
  select
    a.l_f_add,
    a.l_t_add,
    a.r_f_add,
    a.r_t_add,
    now(),
    b.street_segment_id
  from  street_segments_work a,
        street_segments b
  where a.seg_cnn = b.seg_cnn
  and a.status = 'NEW';

  update street_address_ranges sar
  set 
    left_from_address   = ssw.l_f_add,
    left_to_address     = ssw.l_t_add,
    right_from_address  = ssw.r_f_add,
    right_to_address    = ssw.r_t_add,
    update_tms          = now()
  from  street_segments ss,
        street_segments_work ssw
  where ss.seg_cnn = ssw.seg_cnn
  and ssw.status = 'ALTERED'
  and ss.street_segment_id = sar.street_segment_id;
  ----- END street address ranges


  -- apply an update_tms value to every record that got retired or altered
  update street_segments a
  set update_tms =  now()
  from (
    select ssw.seg_cnn as seg_cnn 
    from street_segments_work ssw
    where ssw.status in ('ALTERED', 'RETIRED')
    group by ssw.seg_cnn
  ) b
  where b.seg_cnn = a.seg_cnn;


  ----- record bounding boxes for map cache refresh
  -- In cases where the street is a vertical or horizontal line - st_envelope returns a linestring instead of a polygon.
  -- Since the geometry column on etl_bboxes requires a polygon, we use st_buffer - which will always return a polygon.
  insert into etl_bboxes (tms, geometry)
  select distinct LOCALTIMESTAMP, st_envelope(st_buffer(geometry, 10))
  from street_segments_work;



  ----- BEGIN streetnames processing
  -- Record the streetnames that need to be processed; these will be used for inserts or updates.
  insert into streetnames_work (
      seg_cnn,
      bsm_streetnameid,
      prefix_direction,
      prefix_type,
      base_street_name,
      street_type,
      suffix_direction,
      suffix_type,
      full_street_name,
      category,
      street_segment_id,
      create_tms
  )
  select
      sns.seg_cnn,
      sns.bsm_streetnameid,
      sns.prefix_direction,
      sns.prefix_type,
      sns.base_street_name,
      sns.street_type,
      sns.suffix_direction,
      sns.suffix_type,
      sns.full_street_name,
      sns.category,
      ss.street_segment_id,
      now()
  from streetnames_staging sns
  join street_segments ss on (sns.seg_cnn = ss.seg_cnn)
  left join streetnames sn on (sns.seg_cnn = sn.seg_cnn and sns.base_street_name = sn.base_street_name and coalesce(sns.street_type, '') = coalesce(sn.street_type, '') and sns.category = sn.category)
  where 1=1
  and sn.streetname_id is null
  and sns.category in ('MAP', 'ALIAS');

  -- todo - this variable should be renamed to "working count"
  newStreetnamesCount = (select count(*) from streetnames_work);

  -- This is a bit tricky/subtle because we need to insert new rows and update existing ones.
  -- But our data model is kind of weak here so we have to do some extra work.

  -- First we update matching MAP rows.
  update streetnames sn
  set base_street_name = snw.base_street_name, 
      street_type = snw.street_type, 
      full_street_name = snw.full_street_name, 
      bsm_streetnameid = snw.bsm_streetnameid
  from streetnames_work snw
  where sn.category = 'MAP' 
  and snw.category = 'MAP'
  and sn.seg_cnn = snw.seg_cnn
  and (sn.base_street_name <> snw.base_street_name or sn.street_type <> snw.street_type or sn.full_street_name <> snw.full_street_name);

  -- Next we insert MAP rows that do not exist.
  insert into streetnames (
      seg_cnn,
      bsm_streetnameid,
      prefix_direction,
      prefix_type,
      base_street_name,
      street_type,
      suffix_direction,
      suffix_type,
      full_street_name,
      category,
      street_segment_id,
      create_tms
  )
  select
      seg_cnn,
      bsm_streetnameid,
      prefix_direction,
      prefix_type,
      base_street_name,
      street_type,
      suffix_direction,
      suffix_type,
      full_street_name,
      category,
      street_segment_id,
      create_tms
  from streetnames_work snw
  where category = 'MAP'
  and not exists (
    select 1
    from streetnames sn
    where sn.full_street_name = snw.full_street_name
    and sn.category = snw.category
    and sn.seg_cnn = snw.seg_cnn
  );

  -- Next we delete ALIAS rows that "match"  MAP rows. This prevents having an ALIAS row that matches a MAP row.
  delete 
  from streetnames
  where streetname_id in (
    -- Find the ALIAS rows in streetnames that match the MAP rows.
    -- DEBUG select sn_map.seg_cnn, sn_map.base_street_name, sn_map.street_type, sn_map.category, '<<< >>>', sn_alias.category, sn_alias.seg_cnn, sn_alias.base_street_name, sn_alias.street_type
    select sn_alias.streetname_id
    from streetnames sn_map,
         streetnames sn_alias
    where sn_map.category = 'MAP' 
    and sn_alias.category = 'ALIAS'
    and sn_map.seg_cnn = sn_alias.seg_cnn
    and sn_map.base_street_name = sn_alias.base_street_name 
    and sn_map.street_type = sn_alias.street_type
    order by sn_map.seg_cnn, sn_map.base_street_name, sn_map.street_type
  );


  -- Next, we insert ALIAS rows.
  -- todo - adopt constraints instead of using 'exists'
  insert into streetnames (
      seg_cnn,
      bsm_streetnameid,
      prefix_direction,
      prefix_type,
      base_street_name,
      street_type,
      suffix_direction,
      suffix_type,
      full_street_name,
      category,
      street_segment_id,
      create_tms
  )
  select
      seg_cnn,
      bsm_streetnameid,
      prefix_direction,
      prefix_type,
      base_street_name,
      street_type,
      suffix_direction,
      suffix_type,
      full_street_name,
      category,
      street_segment_id,
      create_tms
  from streetnames_work snw
  where category = 'ALIAS'
  and not exists (
    select 1
    from streetnames sn
    where sn.full_street_name = snw.full_street_name
    and sn.category = snw.category
    and sn.seg_cnn = snw.seg_cnn
  );
  ----- END streetnames processing


  perform _eas_validate_addresses_after_etl('STREETS');


  messageText = 'Streets ETL Successful. ' || newStreetsCount ||' new street segments added; '|| newStreetnamesCount ||' streetname records added; ' || streetsAlterCount ||' existing street segments altered; ' || streetRetireCount || ' street segments retired.';
  return;

  
END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION _sfmad_etl_streets(
  job_id_in char(36),
  out success boolean,
  out messageText text,
  out newStreetsCount int,
  out newStreetnamesCount int,
  out streetsAlterCount int,
  out streetRetireCount int
) OWNER TO postgres;
