
DROP FUNCTION IF EXISTS avs.process_address_parcel_link(
    _avs_id int,
    _create_tms timestamp without time zone
);

CREATE OR REPLACE FUNCTION avs.process_address_parcel_link(
    _avs_id int,
    _create_tms timestamp without time zone
)
  RETURNS void AS
$BODY$
DECLARE

    _address_id             int;
    _address_base_id        int;
    _address_x_parcel_id    int;
    _parcel_id              int;
    _parcel_retire_tms      timestamp without time zone;
    _std_end_date           timestamp without time zone;
    _row_count              int;
    _create_tms             timestamp without time zone = _create_tms;
    _most_recent_retire_tms timestamp without time zone;

BEGIN


    select 
        avsa.address_id,
        avsa.address_base_id,
        avsa.std_end_date::timestamp without time zone,
        p.parcel_id,
        p.date_map_drop::timestamp without time zone
    into
        _address_id,
        _address_base_id,
        _std_end_date,
        _parcel_id,
        _parcel_retire_tms
    from avs.avs_addresses avsa
    inner join public.parcels p on avsa.blocklot = p.blk_lot
    where id = _avs_id;


    GET DIAGNOSTICS _row_count = ROW_COUNT;
    if _row_count != 1 then
        raise exception 'expected one row but got %', _row_count::text;
    end if;


    if _address_id is null then
        -- This means a unit address was not created.
        -- Therefore we create the address parcel link against the base address.
        select address_id into _address_id
        from addresses
        where address_base_id = _address_base_id
        and address_base_flg = true;
    end if;


    -- If there is an unretired row, use it.
    select id into _address_x_parcel_id 
    from address_x_parcels 
    where address_id = _address_id 
    and parcel_id = _parcel_id 
    and retire_tms is null;
    if _address_x_parcel_id is not null then
        update avs.avs_addresses set address_x_parcel_id = _address_x_parcel_id where id = _avs_id;
        insert into public.address_sources (eas_id, eas_table, source_id, source_system) values(_address_x_parcel_id, 'address_x_parcels', _avs_id, 'AVS');
        return;
    end if;


    -- Get the greatest retire tms if it exists at all.
    select retire_tms into _most_recent_retire_tms
    from address_x_parcels 
    where address_id = _address_id and parcel_id = _parcel_id 
    and retire_tms is not null
    and retire_tms = (
        select max(retire_tms)
        from address_x_parcels 
        where address_id = _address_id and parcel_id = _parcel_id 
        and retire_tms is not null
    );


    ---- special processing for retired rows
    -- assumptions (to simplify things)
    --   Retired rows are all processed before unretired rows
    --   order by end_date asc
    -- background
    --   The address_x_parcel insert and update trigger does not allow over-lapping unretired rows.
    --   AVS does not have create tms data.
    if _std_end_date is not null then
        if _most_recent_retire_tms is not null then
            -- remove the overlap
            _create_tms = _most_recent_retire_tms + interval '1 minute';
            if _std_end_date::date = _create_tms::date then
                _std_end_date = _create_tms + interval '1 minute';
            end if;
        end if;
    end if;
    ----


    ---- special processing for unretired rows
    if _std_end_date is null then
        if _most_recent_retire_tms is not null then
            -- remove the overlap
            _create_tms = _most_recent_retire_tms + interval '1 minute';
        end if;
    end if;
    -----


    --- we are now ready to make some changes
    select into _address_x_parcel_id nextval('address_x_parcels_id_seq');

    insert into address_x_parcels ( id, address_id, parcel_id, activate_change_request_id, create_tms, last_change_tms)
    values ( _address_x_parcel_id, _address_id, _parcel_id, 1, _create_tms, _create_tms);

    update avs.avs_addresses set address_x_parcel_id = _address_x_parcel_id where id = _avs_id;

    begin
        -- We must insert then update because of rather stringent business logic in the trigger.
        if _std_end_date is not null then
            update address_x_parcels
            set
                retire_tms = _std_end_date,
                retire_change_request_id = 1
            where id = _address_x_parcel_id;
        end if;
    exception
        when others then
            update avs.avs_addresses set exception_text = substring(SQLERRM from 1 for 256) where id = _avs_id;
    end;

    insert into public.address_sources (eas_id, eas_table, source_id, source_system) values(_address_x_parcel_id, 'address_x_parcels', _avs_id, 'AVS');


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs.process_address_parcel_link(
    _avs_id int,
    _create_tms timestamp without time zone
) OWNER TO postgres;
