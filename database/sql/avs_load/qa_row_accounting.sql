
-- Here is a reasonably complete accounting of the rows that are imported from AVS to EAS.

/*
AVS has this many rows.
n=336320
*/
select count(*)
from avs.avs_addresses;

/*
AVS has rows that EAS considers duplicates.
What makes these rows distinct in AVS are building count and street status - fields that do not exist in EAS.
Neither of these fields have much integrity in AVS and are not used consistently by DBI.
count of duplciate address_x_parcel values=34685
count of all duplicated rows=90199
count of redundant rows=55514
*/
select (
    select sum(c)
    from (
        select address_x_parcel_id, count(*) as c
        from avs.avs_addresses
        where address_x_parcel_id is not null
        group by address_x_parcel_id
        having count(*) > 1
        order by count(*)
    ) as vtab
)
 -
(
    select count(*)
    from (
        select address_x_parcel_id, count(*) as c
        from avs.avs_addresses
        where address_x_parcel_id is not null
        group by address_x_parcel_id
        having count(*) > 1
        order by count(*)
    ) as vtab
);

/*
Rows that did not make it in.
n=16701
*/
select count(*)
from avs.avs_addresses
where exception_text is not null;


/*
Why did rows not make it in?
exception_text	                                                                                count	percentage
referenced parcel has no geometry	                                                            5398	1.605
insert rejected - address_x_parcels - address geometry is not contained by the specified parcel	4779	1.421
no matching block - lot	                                                                        3906	1.1614
no matching nearby street segment	                                                            883	    0.2625
street name does not exist	                                                                    728	    0.2165
matching nearby street segment may have the wrong suffix - see suggested sql	                416	    0.1237
street - street suffix combination does not exist (non-null suffix)	                            319	    0.0949
street - street suffix combination does not exist (null suffix)	                                272	    0.0809
block lot values are inconsistent	                                                            4	    0.0012
street suffix does not exist in street dataset	                                                3	    0.0009
*/
select *
from avs.vw_load_summary;



/*
OK so now we get to the rows that made it in.
Using thwe numbers above...
n=264105
*/
select 336320 - 55514 - 16701;

/*
Let us confirm the row count for "rows that made it in".
A row in address_x_parcels is equivilent to a row in AVS.
We are testing so a close number is fine.
n=264106 
*/
select count(*)
from address_x_parcels;


/*
Finally the query for Marivic.
The row count here can be somehwat higher becuase we may have been able to create a base address in cases where a unit insert failed or when a address_x_parcel insert failed.
n=268888
*/
SELECT count(*)
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id) 
WHERE 1 = 1
and sn.category = 'MAP';
