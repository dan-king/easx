
DROP FUNCTION IF EXISTS avs.cascade_retire(
    _current_tms timestamp without time zone
);

CREATE OR REPLACE FUNCTION avs.cascade_retire(
    _current_tms timestamp without time zone
)
  RETURNS text AS
$BODY$
DECLARE

    _address_base_id int;
    _count int;

BEGIN

    
    declare cursorAddress cursor for
        select distinct(ab.address_base_id)
        from public.addresses ab;
    begin
        open cursorAddress;
        loop
            fetch cursorAddress into _address_base_id;

            if not found then
                exit;
            end if;

            -- Does this base address as a whole have a any unretired AXPs?
            select count(*) into _count
            from address_base ab
            inner join addresses a on ab.address_base_id = a.address_base_id
            inner join address_x_parcels axp on  a.address_id = axp.address_id
            where 1=1
            and axp.retire_tms is null
            and ab.address_base_id = _address_base_id;

            
            if _count = 0 then
                -- If there are no unretired AXPs, retire the whole base address.
                perform public._eas_retire_base_address(_address_base_id, _current_tms);
            else
                -- If there are any unretired AXPs, do nothing.
                continue;
            end if;


        end loop;
        close cursorAddress;
    end;


    return 'success!';


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs.cascade_retire(
    _current_tms timestamp without time zone
) OWNER TO postgres;
