
DROP FUNCTION IF EXISTS avs.concatenate_unit(
    _avs_unit character varying(10),
    _avs_unit_sfx character varying(10)
);


CREATE OR REPLACE FUNCTION avs.concatenate_unit(
    _avs_unit character varying(10),
    _avs_unit_sfx character varying(10)
)
  RETURNS text AS
$BODY$
DECLARE

    _eas_unit_num character varying(21);

BEGIN

    _avs_unit = coalesce(_avs_unit, '');
    _avs_unit = trim(both ' ' from _avs_unit);

    _avs_unit_sfx = coalesce(_avs_unit_sfx, '');
    _avs_unit_sfx = trim(both ' ' from _avs_unit_sfx);

    _eas_unit_num = _avs_unit || ' ' || _avs_unit_sfx;
    _eas_unit_num = trim(both ' ' from _eas_unit_num);
    _eas_unit_num = nullif(_eas_unit_num, '');

    return _eas_unit_num;

END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs.concatenate_unit(
    _avs_unit character varying(10),
    _avs_unit_sfx character varying(10)
) OWNER TO postgres;
