
-- DROP FUNCTION IF EXISTS avs.init_streets_nearest();

CREATE OR REPLACE FUNCTION avs.init_streets_nearest()
  RETURNS text AS
$BODY$
DECLARE

BEGIN


    delete from avs.streets_nearest;

    insert into avs.streets_nearest (parcel_id, street_segment_id, distance)
    select ppts.parcel_id, ss.street_segment_id, st_distance(ppts.geometry, ss.geometry)
    from avs.parcel_points ppts
    inner join public.street_segments ss on st_dwithin(ppts.geometry, ss.geometry, 500);

    return 'success!';


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE STRICT
COST 100;
ALTER FUNCTION avs.init_streets_nearest() OWNER TO postgres;
