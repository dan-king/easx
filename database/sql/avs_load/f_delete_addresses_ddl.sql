-- DROP FUNCTION IF EXISTS avs.delete_addresses();

CREATE OR REPLACE FUNCTION avs.delete_addresses()
  RETURNS text AS
$BODY$
DECLARE

BEGIN


    -- disable triggers
    alter table address_base disable trigger _eas_address_base_before;
    alter table address_base disable trigger _eas_address_base_after;
    alter table addresses disable trigger _eas_addresses_before;
    alter table addresses disable trigger _eas_addresses_after;
    alter table address_x_parcels disable trigger _eas_address_x_parcels_before;
    alter table address_x_parcels disable trigger _eas_address_x_parcels_after;

    truncate table 
        cr_address_x_parcels, 
        cr_addresses,
        cr_address_base,
        address_review,
        address_x_parcels_history,
        address_x_parcels,
        addresses_history,
        addresses,
        address_base_history,
        address_base,
        change_requests,
        cr_change_requests,
        address_keys,
        xmit_queue,
        address_sources;

    -- enable triggers
    alter table address_base enable trigger _eas_address_base_before;
    alter table address_base enable trigger _eas_address_base_after;
    alter table addresses enable trigger _eas_addresses_before;
    alter table addresses enable trigger _eas_addresses_after;
    alter table address_x_parcels enable trigger _eas_address_x_parcels_before;
    alter table address_x_parcels enable trigger _eas_address_x_parcels_after;

    -- initialize the change request rows so we can do inserts (need these for FK constraints)
    INSERT INTO cr_change_requests(
                change_request_id, concurrency_id, requestor_user_id, requestor_comment, 
                reviewer_user_id, reviewer_comment, review_status, resolve_tms, 
                reviewer_last_update, "name", requestor_last_update)
        VALUES (setval('cr_addresses_cr_address_id_seq', 1), 1, 1, 'zeroth cr change request', 
                1, 'zeroth cr change request', 1, now(), 
                now(), 'zeroth cr change request', now());

    INSERT INTO change_requests(
                change_request_id, cr_change_request_id, requestor_user_id, requestor_comment, 
                reviewer_user_id, reviewer_comment, create_tms)
        VALUES (setval('change_requests_change_request_id_seq', 1), currval('cr_addresses_cr_address_id_seq'), 1, 'zeroth change request', 
                1, 'zeroth change request', now());


    -- select * from cr_change_requests;
    -- select * from change_requests;

    return 'success!';


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE STRICT
COST 100;
ALTER FUNCTION avs.delete_addresses() OWNER TO postgres;
