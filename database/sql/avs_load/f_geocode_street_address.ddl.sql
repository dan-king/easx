-- Given a street number, name and type, this function returns the centerline point
-- on the appropriate street segment.  If _ok_to_find_nearest is true, it will use
-- the closest available street segment if the street number does not fall in an
-- actual segment.

DROP FUNCTION IF EXISTS avs.geocode_street_address(
    _street_number          int,
    _street_name            character varying(64),
    _street_type            character varying(32),
    _end_date               date,
    _ok_to_find_nearest     boolean
);

DROP FUNCTION IF EXISTS avs.geocode_street_address(
    _street_number          int,
    _street_name            character varying(64),
    _street_type            character varying(32),
    _end_date               date,
    _ok_to_find_nearest     boolean,
    address_point           out geometry,
    segment_id              out int
);

CREATE OR REPLACE FUNCTION avs.geocode_street_address(
    _street_number          int,
    _street_name            character varying(64),
    _street_type            character varying(32),
    _end_date               date,
    _ok_to_find_nearest     boolean,
    address_point           out geometry,
    segment_id              out int
) AS
$geocode_street_address$
DECLARE
    _from_number            int;
    _to_number              int;
    _segment_linestring     geometry;
    _on_left                boolean;
    _adjusted_street_number int;
    _displacement           float;
BEGIN

    address_point := null;
    
    -- Find the street segment number for this number / name / type combination,
    -- and whether the number is on the left or right of the street.

    select result_id, on_left
        into strict segment_id, _on_left
        from avs.find_street_segment_by_number(
                _street_number,
                _street_name,
                _street_type,
                _end_date,
                _ok_to_find_nearest);
    
    if segment_id is null then
        return;
    end if;
    
    -- Retrieve the address range from the right or left of the street segment, as
    -- appropriate.
     
    if _on_left then
        select
            l_f_add,
            l_t_add,
            geometry
        into strict _from_number, _to_number, _segment_linestring
        from street_segments
        where street_segments.street_segment_id = segment_id;
    else
        select
            r_f_add,
            r_t_add,
            geometry
        into strict _from_number, _to_number, _segment_linestring
        from street_segments
        where street_segments.street_segment_id = segment_id;
    end if;
    
    -- Clamp the street number to the range of the segment that we've selected.
    
    _adjusted_street_number := greatest(least( _street_number, _to_number), _from_number);
    
    if _from_number = _to_number then
        _displacement := 0.0;
    else
        _displacement := (_adjusted_street_number - _from_number) / ( (_to_number - _from_number)::float );
    end if;
    
    -- Liberated from p220 of "PostGIS in Action," this interpolates the point based
    -- on the street number.
    
    address_point := ST_Line_Interpolate_Point(ST_LineMerge(_segment_linestring), _displacement);
    
    return;
     
END;
$geocode_street_address$
LANGUAGE 'plpgsql' VOLATILE;
