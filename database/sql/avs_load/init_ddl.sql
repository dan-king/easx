

CREATE SCHEMA avs;


----- Table: avs.parcel_points
-- DROP TABLE avs.parcel_points;
CREATE TABLE avs.parcel_points
(
  parcel_id int NOT NULL,
  geometry geometry,
  CONSTRAINT enforce_dims_geometry CHECK (ndims(geometry) = 2),
  CONSTRAINT enforce_geotype_geometry CHECK (geometrytype(geometry) = 'POINT'::text),
  CONSTRAINT enforce_srid_geometry CHECK (srid(geometry) = 2227)
)
WITH (OIDS=FALSE);
ALTER TABLE avs.parcel_points OWNER TO postgres;

CREATE INDEX parcel_points_idx
  ON avs.parcel_points
  USING btree (parcel_id);

CREATE INDEX parcel_points_sidx
  ON avs.parcel_points
  USING gist (geometry);
-----


----- Table: avs.streets_nearest
-- DROP TABLE avs.streets_nearest;
CREATE TABLE avs.streets_nearest
(
  parcel_id int not null,
  street_segment_id int not null,
  distance double precision
)
WITH (OIDS=FALSE);
ALTER TABLE avs.streets_nearest OWNER TO postgres;

CREATE INDEX streets_nearest_idx1
  ON avs.streets_nearest
  USING btree (parcel_id);

CREATE INDEX streets_nearest_idx2
  ON avs.streets_nearest
  USING btree (street_segment_id);
-----


-----
-- DROP TABLE avs.parcels_without_geometry;
CREATE TABLE avs.parcels_without_geometry
(
  blk_lot character(9),
  geometry geometry,
  CONSTRAINT enforce_dims_geometry CHECK (ndims(geometry) = 2),
  CONSTRAINT enforce_srid_geometry CHECK (srid(geometry) = 2227)
)
WITH (OIDS=FALSE);
ALTER TABLE avs.parcels_without_geometry OWNER TO postgres;
-- DROP INDEX avs.parcels_without_geometry_idx_blk_lot
CREATE INDEX parcels_without_geometry_idx_blk_lot
  ON avs.parcels_without_geometry
  USING btree
  (blk_lot);
-----



-----
-- DROP TABLE avs.avs_addresses;

CREATE TABLE avs.avs_addresses
(
  id double precision,
  blocklot character varying(10),
  block character varying(6),
  lot character varying(5),
  structure_number character varying(3),
  address_kind character varying(11),
  address_type character varying(11),
  street_number double precision,
  street_number_sfx character(1),
  avs_street_name character varying(26),
  avs_street_sfx character varying(7),
  avs_street_type character varying(17),
  avs_street_status character varying(10),
  unit character varying(5),
  unit_sfx character varying(11),
  end_date character varying(10),
  -- columns above this line are native to AVS
  -- columns below this line are not from AVS and are used for EAS processing
  std_street_type character varying(32),
  std_end_date character varying(10),
  std_unit character varying(10),
  std_street_number_sfx character(1),
  street_segment_id int,
  zone_id int, 
  address_base_id int,
  address_id int,
  address_x_parcel_id int,
  exception_text character varying(256),
  sql character varying(1024)
)
WITH (OIDS=TRUE);
ALTER TABLE avs.avs_addresses OWNER TO postgres;

CREATE UNIQUE INDEX avs_unq_idx_id
  ON avs.avs_addresses
  USING btree
  (id);

CREATE INDEX avs_idx_avs_street_name
  ON avs.avs_addresses
  USING btree
  (avs_street_name);

CREATE INDEX avs_idx_std_street_type
  ON avs.avs_addresses
  USING btree
  (std_street_type);

CREATE INDEX avs_idx_avs_street_type
  ON avs.avs_addresses
  USING btree
  (avs_street_type);

CREATE INDEX avs_idx_blocklot
  ON avs.avs_addresses
  USING btree
  (blocklot);

CREATE INDEX avs_idx_std_end_date
  ON avs.avs_addresses
  USING btree
  (std_end_date);

CREATE INDEX avs_idx_end_date
  ON avs.avs_addresses
  USING btree
  (end_date);

CREATE INDEX avs_idx_street_number
  ON avs.avs_addresses
  USING btree
  (street_number);

CREATE INDEX avs_idx_address_base_id
  ON avs.avs_addresses
  USING btree
  (address_base_id);

CREATE INDEX avs_idx_address_id
  ON avs.avs_addresses
  USING btree
  (address_id);

CREATE INDEX avs_idx_address_x_parcel_id
  ON avs.avs_addresses
  USING btree
  (address_x_parcel_id);

CREATE INDEX avs_idx_exception_text
  ON avs.avs_addresses
  USING btree
  (exception_text);

CREATE INDEX avs_idx_address_type
  ON avs.avs_addresses
  USING btree
  (address_type);
-----




-----
DROP VIEW IF EXISTS avs.vw_streets_nearest;
CREATE OR REPLACE VIEW avs.vw_streets_nearest AS
    select
        snear.street_segment_id,
        snear.distance,
        snames.base_street_name as base_street_name,
        st.unabbreviated as street_type,
        snames.street_type as street_type_abbreviated,
        snames.category,
        snear.parcel_id,
        ss.date_added,
        ss.date_dropped
    from avs.streets_nearest snear
    inner join public.street_segments ss on ss.street_segment_id = snear.street_segment_id
    inner join public.streetnames snames on snames.street_segment_id = snear.street_segment_id
    left outer join d_street_type st on snames.street_type = st.abbreviated
    where 1=1
    -- Exclude certain streets that will not have addresses (such as stockton tunnel)
    -- If this gets more complex we'll create an exclude table.
    and snear.street_segment_id not in (13412);
ALTER TABLE avs.vw_streets_nearest OWNER TO postgres;
-----


-----
-- Exceptions may come from triggers. The exception data is NOT structured into HINT, DETAIL, etc.
-- If it were, and if we were using 9.2, we could employ "get stacked diagnostics".
-- Until then we use the sort of hack shown in the case statement below.
DROP VIEW IF EXISTS avs.vw_load_summary;
CREATE OR REPLACE VIEW avs.vw_load_summary AS
    select 
        exception_text_general, 
        count, 
        round(count/count_total::numeric * 100, 4) as percentage
    from (
        select 
          (case
            when position('|' in avsa1.exception_text) > 0 then substring(avsa1.exception_text from 1 for position('|' in avsa1.exception_text) - 1)
            when position(':' in avsa1.exception_text) > 0 then substring(avsa1.exception_text from 1 for position(':' in avsa1.exception_text) - 1)
            when position('.' in avsa1.exception_text) > 0 then substring(avsa1.exception_text from 1 for position('.' in avsa1.exception_text) - 1)
            else substring(avsa1.exception_text from 1 for 95)
          end) as exception_text_general,
            count(*) as count,
            (select count(*) from avs.avs_addresses) as count_total
        from avs.avs_addresses avsa1
        group by exception_text_general
        order by count(*) desc
    ) v;
ALTER TABLE avs.vw_load_summary OWNER TO postgres;
-----


-----
DROP VIEW IF EXISTS avs.vw_load_results;
CREATE OR REPLACE VIEW avs.vw_load_results AS
    select 
        avsa.id, avsa.blocklot, avsa.block, avsa.lot, avsa.structure_number, avsa.address_kind, avsa.address_type, 
        avsa.street_number, avsa.street_number_sfx, avsa.avs_street_name, avsa.avs_street_sfx, avsa.avs_street_type, 
        avsa.avs_street_status, avsa.unit, avsa.unit_sfx, avsa.end_date,
        substring(avsa.exception_text from 1 for 95) as exception_text,
        avsa.sql, avsa.std_street_type,
        avsa.std_end_date, avsa.street_segment_id, avsa.zone_id, avsa.address_base_id, avsa.address_id, avsa.address_x_parcel_id
    from avs.avs_addresses avsa
    order by
        avsa.street_number,
        avsa.avs_street_name,
        avsa.std_street_type,
        avsa.street_number_sfx,
        avsa.unit,
        avsa.unit_sfx,
        avsa.blocklot;
ALTER TABLE avs.vw_load_results OWNER TO postgres;
-----


-- DROP TABLE avs.qa;
CREATE TABLE avs.qa
(
  description varchar(255),
  pass boolean
)
WITH (OIDS=FALSE);
ALTER TABLE avs.qa OWNER TO postgres;
-----


----- If you break up or reorder this block mind the currval.

-- http://code.google.com/p/eas/issues/detail?id=349

-- insert row in street_segment
insert into street_segments (
  st_name,
  st_type,
  date_added,
  create_tms,
  geometry
)
select 
    'UNKNOWN',
    null,
    now(),
    now(),
    st_geomFromEWKT(
        'SRID=2227;' || 
        'MULTILINESTRING((' || 
        min(st_x(st_pointn(geometry, 1)))::varchar(32) || ' ' || min(st_y(st_pointn(geometry, 1)))::varchar(32) ||  
        ',' || 
        min(st_x(st_pointn(geometry, 1)) + 100)::varchar(32) || ' ' || min(st_y(st_pointn(geometry, 1)) + 100)::varchar(32) ||  
        '))'
    )
from street_segments;

-- insert MAP and ALIAS rows in streetnames
insert into streetnames (
  seg_cnn,
  base_street_name,
  street_type,
  full_street_name,
  category,
  street_segment_id,
  create_tms
)
values (
    0,
    'UNKNOWN',
    null,
    'UNKNOWN',
    'MAP',
    currval('street_segments_street_segment_id_seq'),
    now()
);


INSERT INTO street_address_ranges(
    left_from_address,
    left_to_address,
    right_from_address,
    right_to_address,
    update_tms,
    create_tms,
    street_segment_id)
VALUES (
    0,
    0,
    0,
    0,
    now(),
    now(),
    currval('street_segments_street_segment_id_seq')
);

-----
