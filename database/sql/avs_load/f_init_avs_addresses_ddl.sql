
-- DROP FUNCTION IF EXISTS avs.init_avs_addresses();

CREATE OR REPLACE FUNCTION avs.init_avs_addresses()
  RETURNS text AS
$BODY$
DECLARE

BEGIN


    ----- set default values and standardize values BEGIN
    -- These all result in table scans - can _you_ make it faster?
    update avs.avs_addresses avsa
    set 
        street_segment_id = null, 
        address_base_id = null, 
        address_id = null, 
        address_x_parcel_id = null, 
        std_street_type = null, 
        std_end_date = null,
        exception_text = null,
        blocklot = trim(both ' ' from blocklot),
        block = trim(both ' ' from block),
        lot = trim(both ' ' from lot);

    update avs.avs_addresses avsa
    set std_street_type = nullif(trim(both ' ' from avs_street_type), 'NO STREET SUFFIX')
    where avs_street_type is not null;

    update avs.avs_addresses avsa
    set std_end_date = nullif(trim(both ' ' from end_date), '')
    where end_date is not null;


    -- http://code.google.com/p/eas/issues/detail?id=462
    -- update avs.avs_addresses
    update avs.avs_addresses
    set std_street_number_sfx = trim(both ' ' from street_number_sfx)
    where exception_text is null
    and street_number_sfx <> 'V';
    ----- set default values and standardize values END




    ----- blanket validations - BEGIN

    ----- street number suffix BEGIN
    -- is street number suffix valid?
    update avs.avs_addresses avsa
    set exception_text = 'invalid street number suffix'
    where avsa.std_street_number_sfx is not null
    and not exists (
        select 1
        from public.d_address_base_number_suffix abns 
        where abns.suffix_value = avsa.std_street_number_sfx
    )
    and avsa.exception_text is null;
    ----- street number suffix END

    -- does street name exist?
    update avs.avs_addresses avsa
    set exception_text = 'street name does not exist'
    where avsa.avs_street_name not in (
        select snd.base_street_name
        from vw_streetnames_distinct snd
    )
    and avsa.exception_text is null;


    -- does street suffix exist?
    update avs.avs_addresses avsa
    set exception_text = 'street suffix does not exist in street dataset'
    where avsa.std_street_type is not null
    and avsa.std_street_type not in (
        select snd.unabbreviated_suffix
        from vw_streetnames_distinct snd
        where snd.unabbreviated_suffix is not null
    )
    and avsa.exception_text is null;

    -- is there geometry?
    -- EAS parcels are required to have geometry so here we use a special copy of citylots that does contain rows with null geometry.
    update avs.avs_addresses avsa
    set exception_text = 'referenced parcel has no geometry'
    where exists (
        select 1
        from avs.parcels_without_geometry p
        where avsa.blocklot = p.blk_lot
    )
    and avsa.exception_text is null;


    -- is block and lot valid?
    update avs.avs_addresses avsa
    set exception_text = 'no matching block - lot'
    where avsa.blocklot not in (
        select p.blk_lot
        from parcels p
    )
    and avsa.exception_text is null;


    -- are block lot columns consistent?
    update avs.avs_addresses avsa
    set exception_text = 'block lot values are inconsistent'
    where avsa.blocklot <> (block || lot)
    and avsa.exception_text is null;


    ----- unit BEGIN
    -- is unit specification valid?
    update avs.avs_addresses avsa
    set exception_text = 'length of concatenated unit num exceeds 10'
    where length(avsa.unit::text || ' ' || avsa.unit_sfx) > 10
    and avsa.exception_text is null;

    update avs.avs_addresses
    set std_unit = avs.concatenate_unit(unit, unit_sfx)
    where exception_text is null;

    -- http://code.google.com/p/eas/issues/detail?id=458
    update avs.avs_addresses
    set std_unit = null
    where std_unit = '0';
    ----- unit END


    -- is std_end_date valid?
    update avs.avs_addresses avsa
    set exception_text = 'invalid end date value'
    where avsa.std_end_date is not null 
    and not public._eas_is_date(avsa.std_end_date);

    ----- blanket validations - END


    return 'success!';


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE STRICT
COST 100;
ALTER FUNCTION avs.init_avs_addresses() OWNER TO postgres;
