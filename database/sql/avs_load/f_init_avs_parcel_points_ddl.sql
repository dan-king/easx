
-- DROP FUNCTION IF EXISTS avs.init_parcel_points();

CREATE OR REPLACE FUNCTION avs.init_parcel_points()
  RETURNS text AS
$BODY$
DECLARE

BEGIN

    ----- parcels_points
    -- use the centroid of the polygon
    -- There currently is no case where the centroid is not contained by the polygon.
    -- In the future, it is unlikely but we may loose a record or two here.
    -- This should be acceptable because we do lots of validation downstream of this step.
    -- Once we move to postgis 1.5 we can union with a query that uses st_closest_point
    delete from avs.parcel_points;

    insert into avs.parcel_points (parcel_id, geometry)
    select p1.parcel_id, 
	    CASE ST_CONTAINS(p1.geometry,ST_CENTROID(p1.geometry))
          WHEN true  THEN ST_CENTROID(p1.geometry)
          WHEN false THEN ST_PointOnSurface(p1.geometry)
        END
        as inside_pt
    from public.parcels p1;
    -----

    return 'success!';


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE STRICT
COST 100;
ALTER FUNCTION avs.init_parcel_points() OWNER TO postgres;
