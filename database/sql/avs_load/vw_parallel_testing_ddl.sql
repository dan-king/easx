
DROP VIEW IF EXISTS avs.vw_qa_parallel;
CREATE OR REPLACE VIEW avs.vw_qa_parallel AS

    SELECT
        --ab.address_base_id,
        --a.address_id,
        ab.base_address_prefix,
        ab.base_address_num,
        ab.base_address_suffix,
        sn.base_street_name,
        sn.street_type,
        a.unit_num,
        p.blk_lot,
        axp.create_tms,
        axp.retire_tms
    FROM address_base ab
    inner join addresses a on (ab.address_base_id = a.address_base_id)
    inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
    inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
    left outer join address_x_parcels axp on (axp.address_id = a.address_id)
    left outer join parcels p on (axp.parcel_id = p.parcel_id)
    WHERE (
        (
            sn.category = 'MAP'
            and (axp.create_tms > '12/4/2011'::date or axp.retire_tms > '12/4/2011'::date)
            and (axp.retire_tms is null or axp.retire_tms < '12/31/2012'::date)
        )
        or (
            ab.base_address_num = 288
            and sn.base_street_name = 'MERSEY'
        )
    )
    and not ( ab.base_address_num = 53	and sn.base_street_name = 'WILDER'	and a.unit_num = '408')
    order by
        ss.st_name,
        ss.st_type,
        a.address_base_flg desc,
        ab.base_address_num,
        ab.base_address_suffix,
        a.unit_num;

ALTER TABLE avs.vw_load_results OWNER TO eas_dbo;

-- select * from avs.vw_qa_parallel;