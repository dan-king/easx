
-- select avs.propose_corrections();
-- select * from avs.avs_addresses where exception_text like 'street - street suffix combination does not exist%';
-- select * from public.vw_streetnames_distinct where base_street_name = 'AVENUE B - TI';
-- select * from public.vw_streetnames_distinct where base_street_name like 'AVENUE B - TI%';

DROP FUNCTION IF EXISTS avs.propose_corrections();

CREATE OR REPLACE FUNCTION avs.propose_corrections()
  RETURNS text AS
$BODY$
DECLARE

    _avs_id         int;
    _exception_text character varying(256);
    _street_name    character varying(26);
    _street_type    character varying(32);
    _street_type_unabbreviated character varying(8);
    _sql            character varying(1024);
    _count          int;

BEGIN

    declare cursorAvsAddress cursor for
        select
            avsa.id,
            avsa.exception_text,
            avsa.avs_street_name,
            avsa.std_street_type
        from avs.avs_addresses avsa
        where avsa.exception_text is not null;
    begin
        open cursorAvsAddress;
        loop
            fetch cursorAvsAddress into _avs_id, _exception_text, _street_name, _street_type;

            if not found then
                exit;
            end if;

            if _exception_text like 'street - street suffix combination does not exist%' then
                -- if there is a street name match, and the suffix is wrong and there is a single option for the suffix, lets propose that as a change.
                select count(*) into _count from public.vw_streetnames_distinct where base_street_name = _street_name;
                if _count = 1 then
                    select unabbreviated_suffix into _street_type_unabbreviated from public.vw_streetnames_distinct where base_street_name = _street_name;
                    if _street_type_unabbreviated is null then
                        _sql = $$update addresses_ set avs_street_sfx = null where id = $$ || _avs_id::text;
                    else
                        _sql = $$update addresses_ set avs_street_sfx = '$$ || _street_type_unabbreviated || $$' where id = $$ || _avs_id::text;
                    end if;
                    update avs.avs_addresses set sql = _sql where id = _avs_id;
                end if;
            end if;


            if _exception_text = 'invalid street number suffix' then
                _sql = 'update addresses_ set street_number_sfx = null where id = ' || _avs_id::text;
                update avs.avs_addresses set sql = _sql where id = _avs_id;
            end if;



        end loop;
        close cursorAvsAddress;
    end;
    ----


    return 'success!';


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs.propose_corrections() OWNER TO postgres;
