UPDATE parcels_provisioning pp 
SET 
	suspended_comment = 'CCNR''s were never recorded',
	suspended_tms = now()
FROM parcels p 
WHERE p.blk_lot IN ('0063037', '0063038') 
AND pp.parcel_id = p.parcel_id;

UPDATE parcels 
SET date_map_drop = now() 
WHERE blk_lot IN ('0063037', '0063038');

