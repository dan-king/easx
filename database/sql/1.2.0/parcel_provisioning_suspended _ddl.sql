ALTER TABLE parcels_provisioning 
	ADD COLUMN suspended_tms timestamp without time zone, 
	ADD COLUMN suspended_comment character varying(500);
