
-- DROP TABLE address_change_report;

CREATE TABLE address_change_report
(
  id serial,
  base_address_num integer,
  base_address_suffix varchar(10),
  full_street_name character varying(255),
  address_base_flg boolean,
  unit_num character varying(10),
  blk_lot character varying(9),
  activate_change_request_id int,
  activate_tms timestamp without time zone,
  activate_reviewer varchar(128),
  retire_change_request_id int,
  retire_tms timestamp without time zone,
  retire_reviewer varchar(128),
  exception varchar(256)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE address_change_report OWNER TO postgres;
