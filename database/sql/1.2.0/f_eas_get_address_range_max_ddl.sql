
-- DROP FUNCTION _eas_get_min_address_range();

-- select 2 = _eas_get_address_range_max(0, 0, 1, 2);
-- select 2 = _eas_get_address_range_max(1, 2, 0, 0);
-- select 99 = _eas_get_address_range_max(1, 10, 23, 99);
-- select 0 = _eas_get_address_range_max(0, 0, 0, 0);

CREATE OR REPLACE FUNCTION _eas_get_address_range_max(_lf int, _lt int, _rf int, _rt int) RETURNS int AS
$BODY$
DECLARE

BEGIN

    if _lf = 0 and _lt = 0 then
        return greatest(_rf, _rt);
    end if;

    if _rf = 0 and _rt = 0 then
        return greatest(_lf, _lt);
    end if;

    return greatest(_lf, _lt, _rf, _rt);


END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _eas_get_address_range_max(lf int, lt int, rf int, rt int) OWNER TO eas_dbo;
