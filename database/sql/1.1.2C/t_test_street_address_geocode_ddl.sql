
-- drop table test_street_address_geocode;

create table test_street_address_geocode (
    id serial,
    address_string character varying(60),
    zip_code_string character varying(10),
    found_match boolean,
    found_eas_match boolean,
    json_response text,
    geometry geometry
);
ALTER TABLE test_street_address_geocode OWNER TO eas_dbo;
GRANT ALL ON TABLE test_street_address_geocode_id_seq TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE test_street_address_geocode TO django;
