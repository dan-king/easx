
-- django 1.3 does not like to run inspectdb without these in place.

GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE address_base_street_provisioning TO django;

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'address_base_street_provisioning', 'provisioned_street_geometry', 2, 2227, 'MULTILINESTRING'
where not exists (
	select 1 from geometry_columns where f_table_schema = 'public'	and f_table_name = 'address_base_street_provisioning'
);

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'street_segments_staging', 'geometry', 2, 2227, 'MULTILINESTRING'
where not exists (
	select 1 from geometry_columns where f_table_schema = 'public'	and f_table_name = 'street_segments_staging'
);

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'vw_address_search', 'geometry', 2, 2227, 'POINT'
where not exists (
	select 1 from geometry_columns where f_table_schema = 'public'	and f_table_name = 'vw_address_search'
);

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'vw_base_address_x_parcels', 'geometry', 2, 2227, 'MULTIPOLYGON'
where not exists (
	select 1 from geometry_columns where f_table_schema = 'public'	and f_table_name = 'vw_base_address_x_parcels'
);

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'vw_base_addresses', 'geometry', 2, 2227, 'POINT'
where not exists (
	select 1 from geometry_columns where f_table_schema = 'public'	and f_table_name = 'vw_base_addresses'
);

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'vw_street_segments_map', 'geometry', 2, 2227, 'MULTILINESTRING'
where not exists (
	select 1 from geometry_columns where f_table_schema = 'public'	and f_table_name = 'vw_street_segments_map'
);

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'vw_streets_for_address', 'geometry', 2, 2227, 'MULTILINESTRING'
where not exists (
	select 1 from geometry_columns where f_table_schema = 'public'	and f_table_name = 'vw_streets_for_address'
);

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'vw_streets_for_new_address', 'geometry', 2, 2227, 'MULTILINESTRING'
where not exists (
	select 1 from geometry_columns where f_table_schema = 'public'	and f_table_name = 'vw_streets_for_new_address'
);

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'parcels_work', 'geometry', 2, 2227, 'MULTIPOLYGON'
where not exists (
	select 1 from geometry_columns where f_table_schema = 'public'	and f_table_name = 'parcels_work'
);

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'v_base_address_search', 'geometry', 2, 2227, 'POINT'
where not exists (
	select 1 from geometry_columns where f_table_schema = 'public'	and f_table_name = 'v_base_address_search'
);

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'vw_invalid_addresses', 'geometry', 2, 2227, 'POINT'
where not exists (
	select 1 from geometry_columns where f_table_schema = 'public'	and f_table_name = 'vw_invalid_addresses'
);

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'test_street_address_geocode', 'geometry', 2, 2227, 'POINT'
where not exists (
	select 1 from geometry_columns where f_table_schema = 'public'	and f_table_name = 'test_street_address_geocode'
);

