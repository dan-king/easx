
select ab.base_address_num, sn.full_street_name, count(*)
from
	address_base ab,
	street_segments ss,
	streetnames sn
where ss.street_segment_id = ab.street_segment_id
and ab.street_segment_id = sn.street_segment_id
and exists ( 
	select 1
	from
		address_base ab1,
		street_segments ss1
	where ss1.street_Segment_id = ab1.street_segment_id
	and ab.base_address_num = ab1.base_address_num
	and ab.base_address_suffix != ab1.base_address_suffix
)
group by ab.base_address_num, sn.full_street_name
order by count(*), sn.full_street_name, ab.base_address_num


select ab.base_address_prefix, ab.base_address_num, ab.base_address_suffix, sn.full_street_name
from
	address_base ab,
	street_segments ss,
	streetnames sn
where ss.street_segment_id = ab.street_segment_id
and ab.street_segment_id = sn.street_segment_id
and exists ( 
	select 1
	from
		address_base ab1,
		street_segments ss1
	where ss1.street_Segment_id = ab1.street_segment_id
	and ab.base_address_num = ab1.base_address_num
	and ab.base_address_suffix != ab1.base_address_suffix
)
order by sn.full_street_name, ab.base_address_num, ab.base_address_suffix;



-- address_base
select base_address_prefix, count(*)
from address_base
group by base_address_prefix
order by count(*) desc;

select base_address_suffix, count(*)
from address_base
group by base_address_suffix
order by count(*) desc;


-- addresses
select unit_num_prefix, count(*)
from addresses
group by unit_num_prefix
order by count(*) desc;

select unit_num_suffix, count(*)
from addresses
group by unit_num_suffix
order by count(*) desc;


-- streetnames
select prefix_direction, count(*)
from streetnames
group by prefix_direction
order by count(*) desc;

select suffix_direction, count(*)
from streetnames
group by suffix_direction
order by count(*) desc;


  
  